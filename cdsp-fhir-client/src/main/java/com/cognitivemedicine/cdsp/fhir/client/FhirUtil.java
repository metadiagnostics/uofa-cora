/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.client;

import ca.uhn.fhir.rest.api.MethodOutcome;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 *
 * @author esteban
 */
public class FhirUtil {
    
    public static class OperationOutcomeException extends RuntimeException {
        
        private static final long serialVersionUID = 1L;
        
        private final OperationOutcome operationOutcome;
        
        private final OperationOutcome.IssueSeverity severity;
        
        private OperationOutcomeException(String message, OperationOutcome.IssueSeverity severity, OperationOutcome operationOutcome) {
            super(message);
            this.severity = severity;
            this.operationOutcome = operationOutcome;
        }
        
        public OperationOutcome getOperationOutcome() {
            return operationOutcome;
        }
        
        public OperationOutcome.IssueSeverity getSeverity() {
            return severity;
        }
    }
    
    /**
     * Processes a MethodOutcome from a create or update request. If the request returns an updated
     * version of the resource, that resource is returned. If the request returns a logical id, that
     * id is set in the original resource. If the request resulted in an error, a runtime exception
     * is thrown.
     * 
     * @param <T>
     * @param outcome The method outcome.
     * @param resource The resource upon which the method was performed.
     * @return If the method returned a new resource, that resource is returned. Otherwise, the
     *         original resource is returned, possibly with an updated logical id.
     */
    @SuppressWarnings("unchecked")
    public static <T extends IBaseResource> T processMethodOutcome(MethodOutcome outcome, T resource) {
        checkOutcome(outcome.getOperationOutcome());
        IIdType id = outcome.getId();
        IBaseResource newResource = outcome.getResource();
        
        if (id != null) {
            resource.setId(id);
        } else if (newResource != null && newResource.getClass() == resource.getClass()) {
            resource = (T) newResource;
        }
        
        return resource;
    }
    
    /**
     * Checks the response from a server request to determine if it is an OperationOutcome with a
     * severity of ERROR or FATAL. If so, it will throw a runtime exception with the diagnostics of
     * the issue(s).
     * 
     * @param resource The resource returned by a server request.
     * @return Returns true if the resource was an OperationOutcome with no critical issues.
     * @throws OperationOutcomeException Exception if severity was ERROR or FATAL.
     */
    public static boolean checkOutcome(IBaseResource resource) {
        if (resource instanceof OperationOutcome) {
            OperationOutcome outcome = (OperationOutcome) resource;
            OperationOutcome.IssueSeverity severity = OperationOutcome.IssueSeverity.INFORMATION;
            
            if (outcome.getIssue() != null && !outcome.getIssue().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                
                for (OperationOutcome.OperationOutcomeIssueComponent issue : outcome.getIssue()) {
                    OperationOutcome.IssueSeverity theSeverity = issue.getSeverityElement().getValue();
                    
                    if (theSeverity == OperationOutcome.IssueSeverity.ERROR || theSeverity == OperationOutcome.IssueSeverity.FATAL) {
                        sb.append(issue.getDiagnostics()).append(" (").append(theSeverity.getDefinition()).append(")\n");
                        severity = theSeverity.ordinal() < severity.ordinal() ? theSeverity : severity;
                    }
                }
                
                if (sb.length() != 0) {
                    throw new OperationOutcomeException(sb.toString(), severity, outcome);
                }
                
            }
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Strips the version qualifier from an id, if present.
     * 
     * @param id The id.
     * @return The id without a version qualifier.
     */
    public static String stripVersion(String id) {
        int i = id.lastIndexOf("/_history");
        return i == -1 ? id : id.substring(0, i);
    }
    
    /**
     * Strips the version qualifier from a resource, if present.
     * 
     * @param resource The resource.
     * @return The input resource, possibly modified.
     */
    public static <T extends IBaseResource> T stripVersion(T resource) {
        IIdType id = resource.getIdElement();
        
        if (id.hasVersionIdPart()) {
            id.setValue(stripVersion(id.getValue()));
            resource.setId(id);
        }
        
        return resource;
    }
    
    /**
     * Returns the resource ID relative path.
     * 
     * @param resource The resource.
     * @return The resource's relative path.
     */
    public static String getResourceIdPath(IBaseResource resource) {
        return getResourceIdPath(resource, true);
    }
    
    /**
     * Returns the resource ID relative path.
     * 
     * @param resource The resource.
     * @param stripVersion If true and the id has a version qualifier, remove it.
     * @return The resource's relative path.
     */
    public static String getResourceIdPath(IBaseResource resource, boolean stripVersion) {
        String id = resource.getIdElement().getResourceType() + "/" + resource.getIdElement().getIdPart();
        return stripVersion ? stripVersion(id) : id;
    }
    
    public static Identifier createIdentifier(String system, String value) {
        Identifier identifier = new Identifier();
        identifier.setSystem(system);
        identifier.setValue(value);
        return identifier;
    }
    
    /**
     * Extracts resources of the specified class from a bundle.
     * 
     * @param <T>
     * @param bundle The bundle.
     * @param clazz Class of resource to extract.
     * @return The list of extracted resources.
     */
    @SuppressWarnings("unchecked")
    public static <T extends IBaseResource> List<T> getEntries(Bundle bundle, Class<T> clazz) {
        return (List<T>) getEntries(bundle, Collections.singletonList(clazz), null);
    }
    
    /**
     * Extracts resources from a bundle according to the inclusion and exclusion criteria.
     * 
     * @param <T>
     * @param bundle The bundle.
     * @param inclusions List of resource classes to extract. May be null to indicate all classes
     *            should be extracted.
     * @param exclusions List of resource classes to be excluded. May be null to indicate no classes
     *            should be excluded. Exclusions take precedence over inclusions.
     * @return The list of extracted resources.
     */
    public static <T extends IBaseResource> List<IBaseResource> getEntries(Bundle bundle, List<Class<T>> inclusions,
                                                                           List<Class<T>> exclusions) {
        List<IBaseResource> entries = new ArrayList<>();
        
        if (bundle != null) {
            for (Bundle.BundleEntryComponent entry : bundle.getEntry()) {
                IBaseResource resource = entry.getResource();
                boolean exclude = exclusions != null && classMatches(exclusions, resource);
                boolean include = !exclude && (inclusions == null || classMatches(inclusions, resource));
                
                if (include) {
                    entries.add(resource);
                }
            }
        }
        
        return entries;
    }
    
    /**
     * Returns true if the resource is assignment-compatible with one of the classes list.
     * 
     * @param classes List of classes to check.
     * @param resource The resource to test.
     * @return True if the resource is assignment-compatible with one of the classes in the list.
     */
    private static <T extends IBaseResource> boolean classMatches(List<Class<T>> classes, IBaseResource resource) {
        for (Class<T> clazz : classes) {
            if (clazz.isInstance(resource)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Concatenates a path fragment to a root path. Ensures that a single "/" character separates
     * the two parts.
     * 
     * @param root The root path.
     * @param fragment The path fragment.
     * @return The concatenated result.
     */
    public static String concatPath(String root, String fragment) {
        while (root.endsWith("/")) {
            root = root.substring(0, root.length() - 1);
        }
        
        while (fragment.startsWith("/")) {
            fragment = fragment.substring(1);
        }
        
        return root + "/" + fragment;
    }
    
}
