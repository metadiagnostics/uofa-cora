/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.client.config;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.server.EncodingEnum;
import com.cognitivemedicine.cdsp.fhir.client.IFhirClientConfigurator;
import com.cognitivemedicine.cdsp.fhir.client.IFhirContextConfigurator;

/**
 *
 * @author esteban
 */
public class FhirConfigurator implements IFhirContextConfigurator, IFhirClientConfigurator {
    
    private FhirVersionEnum version;
    
    private String proxy;
    
    private String rootUrl;
    
    private String authenticationType;
    
    private boolean validateConformance;
    
    private EncodingEnum encoding;
    
    private boolean prettyPrint;

    @Override
    public FhirVersionEnum getVersion() {
        return version;
    }

    public void setVersion(FhirVersionEnum version) {
        this.version = version;
    }

    @Override
    public String getProxy() {
        return proxy;
    }

    public void setProxy(String proxy) {
        this.proxy = proxy;
    }

    @Override
    public String getRootUrl() {
        return rootUrl;
    }

    public void setRootUrl(String rootUrl) {
        this.rootUrl = rootUrl;
    }

    @Override
    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    @Override
    public boolean isValidateConformance() {
        return validateConformance;
    }

    public void setValidateConformance(boolean validateConformance) {
        this.validateConformance = validateConformance;
    }

    @Override
    public EncodingEnum getEncoding() {
        return encoding;
    }

    public void setEncoding(EncodingEnum encoding) {
        this.encoding = encoding;
    }

    @Override
    public boolean isPrettyPrint() {
        return prettyPrint;
    }

    public void setPrettyPrint(boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }
    
    public FhirConfigurator clone(){
       FhirConfigurator clone = new FhirConfigurator();
       clone.setAuthenticationType(this.getAuthenticationType());
       clone.setEncoding(this.getEncoding());
       clone.setPrettyPrint(this.isPrettyPrint());
       clone.setProxy(this.getProxy());
       clone.setRootUrl(this.getRootUrl());
       clone.setValidateConformance(this.isValidateConformance());
       clone.setVersion(this.getVersion());
       
       return clone;
    }
    
}
