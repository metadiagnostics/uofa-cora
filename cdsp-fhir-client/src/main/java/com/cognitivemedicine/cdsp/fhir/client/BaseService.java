/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.client;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;

import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.GenericClient;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.DateClientParam;
import ca.uhn.fhir.rest.gclient.ICriterion;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.ReferenceClientParam;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import ca.uhn.fhir.rest.gclient.TokenClientParam;

/**
 * Base service for accessing FHIR-based services.
 */
public class BaseService {
    
    public static final String SP_IDENTIFIER = "identifier";
    public static final String SP_QUESTIONNAIRE = "questionnaire";
    public static final String SP_PARENT = "parent";
    public static final String SP_CODE = "code";

    public static final String SP_ID = "_id";
    public static final String SP_PATIENT = "patient";
    public static final String SP_LAST_UPDATE = "_lastUpdated";
    public static final String SP_TAG = "_tag";
    public static final String NOT = ":not";
   
    public static final TokenClientParam PARAM_IDENTIFIER = new TokenClientParam(SP_IDENTIFIER);
    public static final TokenClientParam PARAM_CODE = new TokenClientParam(SP_CODE);

    public static final ReferenceClientParam PARAM_PATIENT = new ReferenceClientParam(SP_PATIENT);
    public static final ReferenceClientParam PARAM_QUESTIONNAIRE = new ReferenceClientParam(SP_QUESTIONNAIRE);
    public static final ReferenceClientParam PARAM_PARENT = new ReferenceClientParam(SP_PARENT);
    public static final DateClientParam PARAM_LAST_UPDATE = new DateClientParam(SP_LAST_UPDATE);
    public static final StringClientParam PARAM_ID = new StringClientParam(SP_ID);
    public static final TokenClientParam PARAM_NOT_TAG = new TokenClientParam(SP_TAG + NOT);

    private final IGenericClient client;
    
    public static class TagFilter {
        private String tagSystem;
        private String tagCode;

        public TagFilter(String tagSystem, String tagCode) {
            this.tagSystem = tagSystem;
            this.tagCode = tagCode;
        }

        public String getTagSystem() {
            return tagSystem;
        }

        public String getTagCode() {
            return tagCode;
        }
        
    }

    /**
     * Inject FHIR client.
     *
     * @param client The FHIR client.
     */
    public BaseService(IGenericClient client) {
        this.client = client;
    }

    public BaseService(FhirConfigurator configurator) {
        FhirContext fhirContext = new FhirContext(configurator);
        this.client = fhirContext.newRestfulGenericClient(configurator);
    }

    /**
     * Returns the FHIR client.
     *
     * @return The FHIR client.
     */
    public IGenericClient getClient() {
        return client;
    }

    /**
     * FHIR request to update the given resource.
     *
     * @param resource Resource to update.
     * @return The updated resource.
     * @exception Exception Exception if operation failed.
     */
    public <T extends IBaseResource> T updateResource(T resource) {
        MethodOutcome outcome = getClient().update().resource(FhirUtil.stripVersion(resource)).execute();
        return FhirUtil.processMethodOutcome(outcome, resource);
    }

    /**
     * FHIR request to create the given resource.
     *
     * @param resource Resource to create.
     * @return The created resource.
     * @exception Exception Exception if operation failed.
     */
    public <T extends IBaseResource> T createResource(T resource) {
        MethodOutcome outcome = getClient().create().resource(resource).execute();
        return FhirUtil.processMethodOutcome(outcome, resource);
    }

    /**
     * FHIR request to create or update the given resource. If the resource has
     * no logical identifier, a create operation is requested. Otherwise, an
     * update operation is requested.
     *
     * @param resource Resource to create or update.
     * @return The resource resulting from the operation.
     * @exception Exception Exception if operation failed.
     */
    public <T extends IBaseResource> T createOrUpdateResource(T resource) {
        return resource.getIdElement().isEmpty() ? createResource(resource) : updateResource(resource);
    }

    /**
     * Method creates a resource only if the resource with that identifier does
     * not already exist. At this time, the call appears to create the resource
     * even when it already exists.
     *
     * @param resource A FHIR resource.
     * @param identifier The resource identifier.
     * @return The outcome of the operation.
     */
    public MethodOutcome createResourceIfNotExist(IBaseResource resource, Identifier identifier) {
        return getClient().create().resource(resource).conditional()
            .where(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
            .execute();
    }

    /**
     * Search for patient-based resources of the given class.
     *
     * @param patient Patient to be searched.
     * @param clazz Class of the resources to be returned.
     * @return List of matching resources.
     */
    public <T extends IBaseResource> List<T> searchResourcesForPatient(Patient patient, Class<T> clazz) {
        return searchResourcesForPatient(FhirUtil.getResourceIdPath(patient), clazz);
    }

    /**
     * Search for patient-based resources of the given class.
     *
     * @param patientId The id of the Patient to be searched.
     * @param clazz Class of the resources to be returned.
     * @return List of matching resources.
     */
    public <T extends IBaseResource> List<T> searchResourcesForPatient(String patientId, Class<T> clazz) {
        Bundle bundle = getClient().search().forResource(clazz)
            .where(PARAM_PATIENT.hasId(patientId)).returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Search for patient-based resources of the given class and tag.
     *
     * @param <T>
     * @param patientId The id of the Patient to be searched.
     * @param clazz Class of the resources to be returned.
     * @param tags
     * @return List of matching resources.
     */
    public <T extends IBaseResource> List<T> searchResourcesForPatientAndTag(String patientId, Class<T> clazz, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search().forResource(clazz);
        
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }
        
        Bundle bundle =    query.where(PARAM_PATIENT.hasId(patientId)).returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }
    
    /**
     * Search for all patient-based resources of a given type having a specific
     * code system and tag.
     * 
     * @param <T>
     * @param patientId
     * @param clazz
     * @param codeSystem
     * @param tags
     * @return 
     */
    public <T extends IBaseResource> List<T> searchResourcesWithCodeSystemForPatientAndTag(String patientId, Class<T> clazz, String codeSystem, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search().forResource(clazz);
        
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }
        
        Bundle bundle =   query.where(PARAM_PATIENT.hasId(patientId))
            .and(PARAM_CODE.hasSystemWithAnyCode(codeSystem))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }
    
    /**
     * Search for all patient-based resources of a given type having a specific
     * identifier's system and tag.
     * 
     * @param <T>
     * @param patientId
     * @param clazz
     * @param identifierSystem
     * @param tags
     * @return 
     */
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierSystemForPatientAndTag(String patientId, Class<T> clazz, String identifierSystem, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query =  getClient().search().forResource(clazz);
            
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }

        Bundle bundle = query.where(PARAM_PATIENT.hasId(patientId))
            .and(PARAM_IDENTIFIER.hasSystemWithAnyCode(identifierSystem))
            .returnBundle(Bundle.class).execute();
        return FhirUtil.getEntries(bundle, clazz);
    }
    
    /**
     * Search for all patient-based resources of a given type having a specific
     * identifier (system + value) and tag.
     * 
     * @param <T>
     * @param patientId
     * @param clazz
     * @param identifier 
     * @param tags
     * @return
     */
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierSystemAndValueForPatientAndTag(String patientId, Class<T> clazz, Identifier identifier, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query =  getClient().search().forResource(clazz);
            
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }

        Bundle bundle = query.where(PARAM_PATIENT.hasId(patientId))
            .and(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
            .returnBundle(Bundle.class).execute();
        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Search resources which has reference to a questionnaire id.
     * @param questionnaireId the questionnaire id which must be referenced from the response. It cannot be empty.
     * @param clazz the resource class. It cannot be null.
     * @param identifier the identifier that the resource must have. It cannot be null
     * @param tags the tag list. It cannot be null.
     * @return
     */
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierSystemAndValueForPatientAndTagAndQuestionnaire(String questionnaireId,
            Class<T> clazz, Identifier identifier, TagFilter... tags) {
        Validate.notEmpty(questionnaireId);
        Validate.notNull(clazz);
        Validate.notNull(identifier);
        Validate.notNull(tags);
        IQuery<ca.uhn.fhir.model.api.Bundle> query =  getClient().search().forResource(clazz);
            
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }

        Bundle bundle = query.where(PARAM_QUESTIONNAIRE.hasId(questionnaireId))
                .and(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
            .returnBundle(Bundle.class).execute();
        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Search resources which has reference to a questionnaire id.
     * @param questionnaireId the questionnaire id which must be referenced from the response. It cannot be empty.
     * @param clazz the resource class. It cannot be null.
     * @param parent the parent resource. It cannot be null.
     * @param identifier the identifier that the resource must have. It cannot be null
     * @param tags the tag list. It cannot be null.
     * @return
     */
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierSystemAndValueForPatientAndTagAndQuestionnaireAndParent(String questionnaireId,
            Class<T> clazz,  Resource parent, Identifier identifier, TagFilter... tags) {
        Validate.notEmpty(questionnaireId);
        Validate.notNull(clazz);
        Validate.notNull(parent);
        Validate.notNull(identifier);
        Validate.notNull(tags);
        IQuery<ca.uhn.fhir.model.api.Bundle> query =  getClient().search().forResource(clazz);

        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }

        Bundle bundle = query.where(PARAM_QUESTIONNAIRE.hasId(questionnaireId))
                .and(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
                .and(PARAM_PARENT.hasId(FhirUtil.getResourceIdPath(parent)))
            .returnBundle(Bundle.class).execute();
        return FhirUtil.getEntries(bundle, clazz);
    }
    
    /**
     * Search for all patient-based resources of a given type having a specific
     * identifier's system and code system and tag.
     * 
     * @param <T>
     * @param patientId
     * @param clazz
     * @param identifierSystem
     * @param codeSystem
     * @param tags
     * @return 
     */
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierAndCodeSystemForPatientAndTag(String patientId, Class<T> clazz, String identifierSystem, String codeSystem, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search().forResource(clazz);
        
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }    
            
        Bundle bundle = query.where(PARAM_PATIENT.hasId(patientId))
            .and(PARAM_IDENTIFIER.hasSystemWithAnyCode(identifierSystem))
            .and(PARAM_CODE.hasSystemWithAnyCode(codeSystem))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Returns resources of a given class filtering by a list of identifiers and tag filters.
     * @param clazz the resource class. It cannot be null.
     * @param identifiers the identifiers to filter. They cannot be null.
     * @param tags the tags of the resources to filter. They cannot be null.
     * @return the list of resources of a given type.
     */
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierAndTag(final Class<T> clazz, Identifier[] identifiers, TagFilter... tags) {
        Validate.notNull(identifiers);
        Validate.notNull(tags);
        Validate.notNull(clazz);
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search().forResource(clazz);
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }
        boolean first = true;
        for (Identifier identifier : identifiers) {
            if (first) {
                query.where(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()));
                first = false;
            } else {
                query.and(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()));
            }
        }
            
        Bundle bundle = query.returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }
    
    public <T extends IBaseResource> List<T> searchResourcesWithIdentifierAndCodeSystemForPatientAndTag(String patientId, Class<T> clazz, Identifier identifier, String codeSystem, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search().forResource(clazz);
        
        for (TagFilter tag : tags) {
            query.withTag(tag.tagSystem, tag.tagCode);
        }    
            
        Bundle bundle = query.where(PARAM_PATIENT.hasId(patientId))
             .and(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
            .and(PARAM_CODE.hasSystemWithAnyCode(codeSystem))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }
    
    /**
     * Search for patient-based resources of the given class without the given tag.
     *
     * @param patientId The id of the Patient to be searched.
     * @param clazz Class of the resources to be returned.
     * @param tags
     * @return List of matching resources.
     */
    public <T extends IBaseResource> List<T> searchResourcesForPatientWithoutTag(String patientId, Class<T> clazz, TagFilter... tags) {
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search().forResource(clazz)
            .where(PARAM_PATIENT.hasId(patientId));

        for (TagFilter tag : tags) {
            query.where(PARAM_NOT_TAG.exactly().systemAndCode(tag.tagSystem, tag.tagCode));
        }
        
        Bundle bundle = query.returnBundle(Bundle.class).execute();
        
        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Search for patient-based resources of the given class.
     *
     * @param <T>taken
     * @param patientId The id of the Patient to be searched.
     * @param modifiedAfter
     * @param clazz Class of the resources to be returned.
     * @return List of matching resources.
     */
    public <T extends IBaseResource> List<T> searchResourcesForPatientModifiedAfter(String patientId, Date modifiedAfter, Class<T> clazz) {
        Bundle bundle = getClient().search().forResource(clazz)
            .where(PARAM_PATIENT.hasId(patientId))
            .where(PARAM_LAST_UPDATE.after().second(modifiedAfter))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Search for patient-based resources of the given class without the given tag
     *
     * @param <T>
     * @param patientId The id of the Patient to be searched.
     * @param modifiedAfter
     * @param clazz Class of the resources to be returned.
     * @return List of matching resources.
     */
    public <T extends IBaseResource> List<T> searchResourcesForPatientModifiedAfterWithoutTag(String patientId, Date modifiedAfter, Class<T> clazz, String tagSystem, String tagCode) {
        Bundle bundle = getClient().search().forResource(clazz)
            .where(PARAM_PATIENT.hasId(patientId))
            .where(PARAM_LAST_UPDATE.after().second(modifiedAfter))
            .where(PARAM_NOT_TAG.exactly().systemAndCode(tagSystem, tagCode))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Returns all resources of the given class that contain the identifier.
     *
     * @param system The identifier's system.
     * @param value The identifier's value.
     * @param clazz Class of the resources to be searched.
     * @return List of resources containing the identifier.
     */
    public <T extends IBaseResource> List<T> searchResourcesByIdentifier(String system, String value, Class<T> clazz) {
        return searchResourcesByIdentifier(FhirUtil.createIdentifier(system, value), clazz);
    }

    /**
     * Returns all resources of the given class that contain the identifier.
     *
     * @param identifier Resources with this identifier will be returned.
     * @param clazz Class of the resources to be searched.
     * @return List of resources containing the identifier.
     */
    public <T extends IBaseResource> List<T> searchResourcesByIdentifier(Identifier identifier, Class<T> clazz) {
        Bundle bundle = getClient().search().forResource(clazz)
            .where(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }
    
    /**
     * Returns all resources of the given class that contain the coding.
     *
     * @param system The coding's system.
     * @param code The coding's value.
     * @param clazz Class of the resources to be searched.
     * @return List of resources containing the coding.
     */
    public <T extends IBaseResource> List<T> searchResourcesByCoding(String system, String code, Class<T> clazz) {
        return searchResourcesByCoding(new Coding(system, code, null), clazz);
    }
    
    /**
     * Returns all resources of the given class that contain the coding.
     *
     * @param coding Resources with this coding will be returned.
     * @param clazz Class of the resources to be searched.
     * @return List of resources containing the coding.
     */
    public <T extends IBaseResource> List<T> searchResourcesByCoding(Coding coding, Class<T> clazz) {
        Bundle bundle = getClient().search().forResource(clazz)
            .where(PARAM_CODE.exactly().systemAndCode(coding.getSystem(), coding.getCode()))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }

    public <T extends IBaseResource> List<T> searchResourcesByType(Class<T> clazz) {
        Bundle bundle = getClient().search().forResource(clazz).include(new Include("*"))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }
    
    public <T extends IBaseResource> List<T> searchResourcesByTypeAndModifiedAfter(Class<T> clazz, Date modifiedAfter) {
        Bundle bundle = getClient().search().forResource(clazz)
            .where(PARAM_LAST_UPDATE.after().second(modifiedAfter))
            .returnBundle(Bundle.class).execute();

        return FhirUtil.getEntries(bundle, clazz);
    }

    public ca.uhn.fhir.model.api.Bundle searchPatientsByIdentifierAndRelatedResources(Identifier patientIdentifier) {
        return getClient().search()
            .forResource(Patient.class)
            .where(PARAM_IDENTIFIER.exactly().systemAndIdentifier(patientIdentifier.getSystem(), patientIdentifier.getValue()))
            .revInclude(new Include("*"))
            .execute();
    }

    public Bundle searchPatientsByIdAndRelatedResources(String patientId) {
        return getClient().search()
                .forResource(Patient.class)
                .where(PARAM_ID.matches().value(patientId))
                .revInclude(new Include("*")).returnBundle(Bundle.class)
                .execute();
    }
    
    
    public <T extends IBaseResource> List<T> searchResourcesByIdentifierAndCoding(String identifierSystem, String identifierValue, String codeSystem,
            String code, Class<T> clazz) {
        Identifier identifier = FhirUtil.createIdentifier(identifierSystem, identifierValue);
        Coding coding = new Coding(codeSystem, code, null);
        Bundle bundle = getClient().search().forResource(clazz)
                .where(PARAM_CODE.exactly().systemAndCode(coding.getSystem(), coding.getCode()))
                .and(PARAM_IDENTIFIER.exactly().systemAndIdentifier(identifier.getSystem(), identifier.getValue()))
                .returnBundle(Bundle.class).execute();
        
        return FhirUtil.getEntries(bundle, clazz);
    }

    /**
     * Search a single resource by its id. It will return only one element or null.
     */
    public <T extends IBaseResource> T searchResourceById(String resourceId, Class<T> clazz) {
        Bundle bundle = getClient().search()
            .forResource(clazz)
            .where(PARAM_ID.matches().value(resourceId))
            .returnBundle(Bundle.class).execute();
        List<T> entries = FhirUtil.getEntries(bundle, clazz);
        if (entries != null && !entries.isEmpty()) {
            return entries.get(0);
        }
        return null;
    }
    
    public Patient searchPatientById(String id, String idSystem) {
        List<Patient> patients = this.searchResourcesByIdentifier(idSystem, id, Patient.class);

        if (patients == null || patients.isEmpty()) {
            return null;
        } else if (patients.size() > 1) {
            throw new IllegalStateException("More than one Patient with Identifier " + idSystem + "/" + id + " was found!");
        }
        return patients.get(0);
    }

    /**
     * Deletes all resources of the given class that contain the identifier.
     *
     * @param <T>
     * @param identifier Resources with this identifier will be deleted.
     * @param clazz Class of the resources to be searched.
     * @return Count of deleted resources.
     */
    public <T extends IBaseResource> int deleteResourcesByIdentifier(Identifier identifier, Class<T> clazz) {
        List<T> resources = searchResourcesByIdentifier(identifier, clazz);
        deleteResources(resources);
        return resources.size();
    }

    /**
     * Deletes all resources in the provided list.
     *
     * @param resources Resources to delete.
     */
    public void deleteResources(List<? extends IBaseResource> resources) {
        for (IBaseResource resource : resources) {
            deleteResource(resource);
        }
    }

    /**
     * Deletes a resource.
     *
     * @param resource Resource to delete.
     */
    public void deleteResource(IBaseResource resource) {
        getClient().delete().resourceById(resource.getIdElement()).execute();
    }

    public long deleteResourcesAssociatedWithPatient(Patient patient) {

        Bundle result = this.searchPatientsByIdAndRelatedResources(patient.getId());
        long sum = 0;
        removeParentFromQuestionaireResponses();
        for (Bundle.BundleEntryComponent entry : result.getEntry()) {
            if (entry.getResource() instanceof Patient) {
                continue;
            }
            this.deleteResource(entry.getResource());
            sum++;
        }

        this.deleteResource(patient);
        sum++;

        return sum;
    }

    /**
     * Find all the {@link QuestionnaireResponse}s that have a parent and update them removing the parent reference.
     */
    private void removeParentFromQuestionaireResponses(){
        Bundle bundle = getClient().search()
                .forResource(QuestionnaireResponse.class)
                .where(PARAM_PARENT.isMissing(false))
                .revInclude(new Include("*")).returnBundle(Bundle.class)
                .execute();


        List<QuestionnaireResponse> entries = FhirUtil.getEntries(bundle, QuestionnaireResponse.class);
        if (entries != null && !entries.isEmpty()) {
            for(QuestionnaireResponse qr: entries){
                qr.setParent(null);
                updateResource(qr);
            }
        }

    }

    /**
     * Returns the default FHIR service root url.
     *
     * @return Default FHIR service root url.
     */
    public String getServiceRoot() {
        return ((GenericClient) getClient()).getUrlBase();
    }

    /**
     * For urls without a service root, prepends the default service root.
     *
     * @param url URL to expand.
     * @return URL with a service root prepended.
     */
    public String expandURL(String url) {
        return url.contains(":/") ? url : FhirUtil.concatPath(getServiceRoot(), url);
    }

}
