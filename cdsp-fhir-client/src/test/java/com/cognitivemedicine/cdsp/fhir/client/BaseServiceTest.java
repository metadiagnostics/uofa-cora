/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.client;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import com.cognitivemedicine.cdsp.testcategories.ServiceIntegrationTest;

import ca.uhn.fhir.context.FhirVersionEnum;
import org.hl7.fhir.dstu3.model.Bundle;
import ca.uhn.fhir.model.api.BundleEntry;
import org.hl7.fhir.dstu3.model.AllergyIntolerance;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Reference;

/**
 *
 * @author esteban
 */
@Category(ServiceIntegrationTest.class)
public class BaseServiceTest {

    private static BaseService fhirService;
    private final static Logger LOG = LoggerFactory.getLogger(BaseServiceTest.class);
    
    @BeforeClass
    public static void doBeforeClass(){
        FhirConfigurator fhirConfigurator = new FhirConfigurator();
		fhirConfigurator.setRootUrl("http://localhost:8081/hapi/baseDstu3");
		fhirConfigurator.setVersion(FhirVersionEnum.DSTU3);
		FhirContext fhirContext = new FhirContext(fhirConfigurator);
		fhirService = new BaseService(fhirContext.newRestfulGenericClient(fhirConfigurator));
    }
 
    @Test
    @Ignore("Used for testing while developing")
    public void doRevIncludeTest(){
        

//        Bundle result = fhirService.searchPatientsByIdentifierAndRelatedResources(new IdentifierDt("com.cognitivemedicine", "12"));
        Bundle result = fhirService.searchPatientsByIdAndRelatedResources("4952");

        LOG.info("result = " + result);
        
        if (result != null){
            List<Bundle.BundleEntryComponent> entries = result.getEntry();
            for (Bundle.BundleEntryComponent entry : entries) {
                LOG.info("entry = " + entry.getResource().getResourceType());
            }
        }
    }

    @Test
    public void searchByPatientWithoutTag(){
        Patient patient = new Patient();
        patient = fhirService.createResource(patient);
        AllergyIntolerance allergy = new AllergyIntolerance();
        CodeableConcept substance = new CodeableConcept().addCoding(new Coding().setSystem("test").setCode("test"));
        allergy.addReaction(new AllergyIntolerance.AllergyIntoleranceReactionComponent().setSubstance(substance));
        allergy.setPatient(new Reference(patient.getId()));
        allergy = fhirService.createResource(allergy);
        
        AllergyIntolerance mockAllergy = new AllergyIntolerance();
        CodeableConcept mockAllergySubstance = new CodeableConcept().addCoding(new Coding().setSystem("test").setCode("test2"));
        mockAllergy.addReaction(new AllergyIntolerance.AllergyIntoleranceReactionComponent().setSubstance(mockAllergySubstance));
        mockAllergy.setPatient(new Reference(patient.getId()));
        mockAllergy.getMeta().addTag("testTagSystem", "testTag", "testTag");
        mockAllergy = fhirService.createResource(mockAllergy);
        
        List<AllergyIntolerance> allergies = fhirService.searchResourcesForPatient(patient, AllergyIntolerance.class);
        Assert.assertEquals(2, allergies.size());
        
        allergies = fhirService.searchResourcesForPatientAndTag(patient.getId(), AllergyIntolerance.class, new BaseService.TagFilter("testTagSystem", "testTag"));
        Assert.assertEquals(1, allergies.size());
        Assert.assertEquals(mockAllergy.getId(), allergies.get(0).getId());
        
        allergies = fhirService.searchResourcesForPatientWithoutTag(patient.getId(), AllergyIntolerance.class, new BaseService.TagFilter("testTagSystem", "testTag"));
        Assert.assertEquals(1, allergies.size());
        Assert.assertEquals(allergy.getId(), allergies.get(0).getId());
        
        fhirService.deleteResource(mockAllergy);
        fhirService.deleteResource(allergy);
        fhirService.deleteResource(patient);
        
    }
}
