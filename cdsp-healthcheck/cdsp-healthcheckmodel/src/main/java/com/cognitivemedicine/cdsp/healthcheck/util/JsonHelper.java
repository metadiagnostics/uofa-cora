/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.util;

import java.io.IOException;
import java.net.URISyntaxException;

import com.cognitivemedicine.cdsp.healthcheck.model.HealthCheckStatus;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonHelper {

	private static ObjectMapper mapper = new ObjectMapper();

	public static HealthCheckStatus getHealthCheckStatus(String json) throws IOException, URISyntaxException {
		return (mapper.readValue(json, new TypeReference<HealthCheckStatus>() {
		}));
	}

}
