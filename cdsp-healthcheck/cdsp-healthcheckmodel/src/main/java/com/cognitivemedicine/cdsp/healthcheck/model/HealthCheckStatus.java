/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.model;

import java.util.List;

/**
 * Represents Service health check status
 *
 * @author nbashir
 *
 */
public class HealthCheckStatus {

    private boolean healthy;
    private int statusCode;
    private String statusDescription;
    private List<HealthCheckStatusDetails> statusDetails;
    private VersionInformation versionInformation;

    /**
     * Default constructor for mapping framework.
     */
    public HealthCheckStatus() {
    }

    public HealthCheckStatus(int status) {
        this.statusCode = status;
    }

    public HealthCheckStatus(boolean status) {
        this.healthy = status;
    }

    public HealthCheckStatus(boolean healthy, int statusCode, List<HealthCheckStatusDetails> statusDetails) {
        this(healthy, statusCode, null, statusDetails);
    }

    public HealthCheckStatus(boolean healthy, int statusCode, String statusDescription, List<HealthCheckStatusDetails> statusDetails) {
        super();
        this.healthy = healthy;
        this.statusCode = statusCode;
        this.statusDetails = statusDetails;
        this.statusDescription = statusDescription;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<HealthCheckStatusDetails> getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(List<HealthCheckStatusDetails> statusDetails) {
        this.statusDetails = statusDetails;
    }

    public boolean isHealthy() {
        return healthy;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public VersionInformation getVersionInformation() {
        return versionInformation;
    }

    public void setVersionInformation(VersionInformation versionInformation) {
        this.versionInformation = versionInformation;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Healthy: ").append(healthy).append("\n");
        sb.append("Status Code: ").append(statusCode).append("\n");
        sb.append("Status Description: ").append(statusDescription).append("\n");
        sb.append("Version Info: \n");
        if (versionInformation != null) {
            sb.append("\t Version: ").append(versionInformation.getVersion());
            sb.append("\t Build Branch: ").append(versionInformation.getBuildBranch());
            sb.append("\t Build Number: ").append(versionInformation.getBuildNumber());
            sb.append("\t Build Timestamp: ").append(versionInformation.getBuildTimestamp());
        } else {
            sb.append("\t null");
        }
        sb.append("Status Details: \n");
        if (statusDetails != null) {
            for (HealthCheckStatusDetails hcsd : statusDetails) {
                sb.append("\t Code: ").append(hcsd.getCode());
                sb.append("\t Description : ").append(hcsd.getDescription());
            }
        } else {
            sb.append("\tb null");
        }
        return sb.toString();
    }

}
