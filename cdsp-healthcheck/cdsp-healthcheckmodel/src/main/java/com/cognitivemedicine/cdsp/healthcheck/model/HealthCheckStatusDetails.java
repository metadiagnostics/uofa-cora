/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.model;

/**
 * Represents details of health check status
 * 
 * @author nbashir
 *
 */
public class HealthCheckStatusDetails {

	private String name;
	private int code;
	private String description;
    
    /**
     * Used by HealthCheckService when it converts from {@link HealthCheckStatus} 
     * to {@link HealthCheckStatusDetails}. Individual services shouldn't use 
     * this attribute, they should use {@link HealthCheckStatus#versionInformation}
     * instead.
     */
    private VersionInformation versionInformation;
	
	/**
	 * Default constructor for mapping framework. 
	 */
	public HealthCheckStatusDetails() {
	}

	public HealthCheckStatusDetails(String name, int code, String description) {
		super();
		
		this.name = name;
		this.code = code;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
    public VersionInformation getVersionInformation() {
        return versionInformation;
    }

    public void setVersionInformation(VersionInformation versionInformation) {
        this.versionInformation = versionInformation;
    }
	
}
