/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.util;

public enum HealthCheckDetailStatusType {
	
	INFO(1, "Info"), 
	WARN(2, "Warning"), 
	SEVERE(3, "Severe");

	private final int code;
	private final String defaultDescription;

	private HealthCheckDetailStatusType(int code, String defaultDescription) {
		this.code = code;
		this.defaultDescription = defaultDescription;
	}
	
    public static HealthCheckDetailStatusType findByCode(int code){
        for(HealthCheckDetailStatusType hsdt : values()){
            if( hsdt.code == code){
                return hsdt;
            }
        }
        return null;
    }

	public int getCode() {
		return code;
	}

	public String getDefaultDescription() {
		return defaultDescription;
	}

}