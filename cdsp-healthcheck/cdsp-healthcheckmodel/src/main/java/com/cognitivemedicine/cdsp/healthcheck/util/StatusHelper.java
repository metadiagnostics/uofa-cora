/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.cognitivemedicine.cdsp.healthcheck.model.HealthCheckStatus;
import com.cognitivemedicine.cdsp.healthcheck.model.HealthCheckStatusDetails;

/**
 * Helper class to manipulate {@link HealthCheckStatus} instances.
 * 
 * @author esteban
 */
public class StatusHelper {

    /**
     * Creates a new instance of {@link HealthCheckStatus}.
     * 
     * @param status
     * @param detailStatuses
     * @return 
     */
    public static HealthCheckStatus getHealthCheckStatus(HealthCheckStatusType status, HealthCheckDetailStatusType... detailStatuses) {

        Map<Integer, String> statuses = new LinkedHashMap<>();
        if (detailStatuses != null){
            for (HealthCheckDetailStatusType detailStatus : detailStatuses) {
                statuses.put(detailStatus.getCode(), detailStatus.getDefaultDescription());
            }
        }
        
        return getHealthCheckStatus(status, statuses);
    }
    
    /**
     * Creates a new instance of {@link HealthCheckStatus}.
     * 
     * @param status
     * @param detailCode
     * @param detailDescription
     * @return 
     */
    public static HealthCheckStatus getHealthCheckStatus(HealthCheckStatusType status, int detailCode, String detailDescription) {

        Map<Integer, String> statuses = new LinkedHashMap<>();
        statuses.put(detailCode, detailDescription);
        
        return getHealthCheckStatus(status, statuses);
    }
    
    /**
     * Creates a new instance of {@link HealthCheckStatus}.
     * 
     * @param status
     * @param detailStatuses
     * @return 
     */
    public static HealthCheckStatus getHealthCheckStatus(HealthCheckStatusType status, Map<Integer, String> detailStatuses) {

        HealthCheckStatus healthCheckStatus = new HealthCheckStatus();
        healthCheckStatus.setHealthy(status.getHealthStatus());
        healthCheckStatus.setStatusCode(status.getCode());
        healthCheckStatus.setStatusDescription(status.getDescription());
        List<HealthCheckStatusDetails> statusDetailsList = new ArrayList<>();
        if (detailStatuses != null){
            for (Map.Entry<Integer, String> detailStatus : detailStatuses.entrySet()) {
                statusDetailsList.add(new HealthCheckStatusDetails("", detailStatus.getKey(), detailStatus.getValue()));
            }
        }
        healthCheckStatus.setStatusDetails(statusDetailsList);

        return healthCheckStatus;
    }
    
}
