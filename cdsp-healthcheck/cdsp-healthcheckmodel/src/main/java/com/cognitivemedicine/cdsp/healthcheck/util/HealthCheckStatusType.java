/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.util;

public enum HealthCheckStatusType {
	
    OK(true, 1, "Success"),
    INITIALIZATION_ERROR(false, -1, "Initialization Error"),
    SERVICE_COMMUNICATION_ERROR(false, -2, "Error Communicating with Service"),
    UNEXPECTED_SERVICE_RESPONSE_ERROR(false, -3, "Unexpected Response from Service"),
    NOT_INITIALIZED(false, -4, "Service Not Initialized Yet"),
    GENERIC_ERROR(false, -99, "Error"),
    UNKNOWN_ERROR(false, -100, "Unknown Error");
    
    private final boolean healthStatus;
    private final int code;
    private final String description;
    
    private HealthCheckStatusType(boolean healthStatus, int code, String description) {
        this.healthStatus = healthStatus;
        this.code = code;
        this.description = description;
    }
    
    public static HealthCheckStatusType findByCode(int code){
        for(HealthCheckStatusType hst : values()){
            if( hst.code == code){
                return hst;
            }
        }
        return null;
    }
    
    public boolean getHealthStatus() {
    	return healthStatus;
    }
    
    public int getCode() {
    	return code;
    }
    
    public String getDescription() {
    	return description;
    }
}
