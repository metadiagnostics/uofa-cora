/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.spi;

import com.cognitivemedicine.cdsp.healthcheck.model.HealthCheckStatus;

/**
 * Implementations of this class are specialized in checking the status of
 * a single service or service type.
 * 
 * @author esteban
 */
public interface ServiceHealthChecker {
    
    /**
     * This method is invoked right after a new instance of this class is 
     * created.
     * @param details 
     */
    public void init(ServiceDetails details);
    
    /**
     * This method is periodically executed to check the status of the service
     * being monitored by this implementation.
     * @return 
     */
    public HealthCheckStatus check();
    
    /**
     * This method is invoked when the health-monitor-service is about to be
     * disposed.
     */
    public void dispose();
    
}
