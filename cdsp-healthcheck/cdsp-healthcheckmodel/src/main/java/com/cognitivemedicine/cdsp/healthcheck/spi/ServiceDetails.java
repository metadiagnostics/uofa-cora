/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.spi;

import java.util.Map;

/**
 * Represent details about a service to check its health.
 * 
 * @author nbashir
 *
 */
public class ServiceDetails {

	public ServiceDetails() {
	}

	// Service type
	private String type;
	// Service name
	private String name;
	// How often to check this service, in milliseconds
	private int checkInterval;
    
	// Web Service URL
    private Map<String, String> parameters;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getCheckInterval() {
		return checkInterval;
	}
	public void setCheckInterval(int checkInterval) {
		this.checkInterval = checkInterval;
	}

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
    
    @Override
    public String toString() {
        return "ServiceDetails{" + "type=" + type + ", name=" + name + ", checkInterval=" + checkInterval + '}';
    }
    
}
