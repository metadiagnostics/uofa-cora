/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.healthcheck.spi;

/**
 * Main interface that has to be implemented by providers of different services.
 * 
 * The type returned by the implementations of this interface must be unique
 * among all the ServiceHealthCheckerProviders in the system.
 * 
 * @author esteban
 */
public interface ServiceHealthCheckerProvider {
    
    public String getType();
    
    public ServiceHealthChecker newChecker();
    
    
}
