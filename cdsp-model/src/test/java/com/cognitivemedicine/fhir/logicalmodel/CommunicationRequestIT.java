/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestStatus;
import org.junit.Test;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;



/**
 * @author Jerry Goodnough
 *
 */
public class CommunicationRequestIT {

	@Test
	public void test() {
		//ILM
		CommunicationRequest req = new CommunicationRequest();
		//Simple String based payload
		CommunicationRequest.Payload payload = new CommunicationRequest.Payload();
	
		payload.setContent("This is a test payload");
		req.addPayload(payload);
		//Body Elements
		req.setId("IDxxx0101020");
		IdentifierDt subjectId = new IdentifierDt();
		subjectId.setValue("123456789-1");
		Context ctx = new Context();
		ctx.setSubjectId(subjectId);
		req.setContext(ctx);
		IdentifierDt topicId = new IdentifierDt();
		topicId.setValue("CoombsId_123456789-1_10");
		req.setTopicIdentifier(topicId);
		//Category
		req.getCategory().setText("Notification");
		CodingDt categoryCode = new CodingDt();
		categoryCode.setCode("123");
		categoryCode.setDisplay("Notification");
		categoryCode.setSystem("com.cognitivemedicine.codesystem.communicationsCategory");
		req.getCategory().addCoding(categoryCode);
		
		//Recipient
		Recipient recipient = new Recipient();
		
		CodeableConceptDt recipientType = new CodeableConceptDt();
		
		CodingDt recipientTypeCode = new CodingDt();
		recipientTypeCode.setCode("ROLE");
		recipientTypeCode.setDisplay("Role");
		recipientTypeCode.setSystem("com.cognitivemedicine.codesystem.RecipientType");
		recipientType.addCoding(recipientTypeCode);
		recipient.setType(recipientType);
		
		IdentifierDt roleId = new IdentifierDt();
		roleId.setSystem("com.cognitivemedicine.codesystem.Role");
		roleId.setValue("ANYNURSE");
		recipient.setIdentifier(roleId);
		req.addRecipient(recipient);
		
		
		//Priority
		CodeableConceptDt priority = new CodeableConceptDt();
		CodingDt priorityTypeCode = new CodingDt();
		priorityTypeCode.setCode("HIGH");
		priorityTypeCode.setDisplay("High");
		priorityTypeCode.setSystem("com.cognitivemedicine.codesystem.Priority");
		priority.addCoding(priorityTypeCode);
		req.setPriority(priority);

		//Status 
		req.setStatus(CommunicationRequestStatus.DRAFT.name());
		
		//Attribution
		Attribution attribution = new Attribution();
		attribution.setDescription("This requested was generated soley as test data");
		req.addAttribution(attribution);
	}

}
