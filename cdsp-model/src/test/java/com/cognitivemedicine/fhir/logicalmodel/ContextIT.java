/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jerry Goodnough
 *
 */
public class ContextIT {
	private static final Logger LOG = LoggerFactory.getLogger(ContextIT.class);
	
	@Test
	public void testJonsify() {
		String json = "";
		Context ctx = new Context();

		IdentifierDt subjectId = new IdentifierDt();
		subjectId.setValue("123456789-1");

		IdentifierDt caseId = new IdentifierDt();
		caseId.setValue("Case-1");
		
		ctx.setSubjectId(subjectId);
		ctx.setCaseId(caseId);
	
		
		
		

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		//objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
		//objectMapper.setSerializationInclusion(Include.NON_ABSENT);
		//objectMapper.setSerializationInclusion(Include.NON_DEFAULT);

		try {
			json = objectMapper.writeValueAsString(ctx);
		} 
		catch (JsonProcessingException e) {
			e.printStackTrace();
			fail("JSON did not serialize: "+e.getMessage());
		}
		assertNotNull("The serialized request should not be null",json);
		LOG.debug("JSON serialized Context ="+json.toString());
		assertTrue("JSON Text should not be empty", json.length()>0);
		
		try {
			Context ctx2 = objectMapper.readValue(json, Context.class);
			assertNotNull("Context should not be null",ctx2);
		} catch (IOException e) {
			e.printStackTrace();
			fail("JSON did not deserialize: "+e.getMessage());
		}
	}
	
}
