/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestStatus;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;




/**
 * Unit test for simple App.
 */
public class CommunicationRequestSerializationIT {
	private static final Logger LOG = LoggerFactory.getLogger(CommunicationRequestSerializationIT.class);
	@Test
	public void testJonsify() {
		String json = "";
		CommunicationRequest req = getCommunicationRequest();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		//objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
		//objectMapper.setSerializationInclusion(Include.NON_ABSENT);
		//objectMapper.setSerializationInclusion(Include.NON_DEFAULT);

		try {
			json = objectMapper.writeValueAsString(req);
		} 
		catch (JsonProcessingException e) {
			e.printStackTrace();
			fail("JSON did not serialize: "+e.getMessage());
		}
		assertNotNull("The serialized request should not be null",json);
		LOG.debug("JSON Serialized CommunicationsRequest = ",json.toString());
		assertTrue("JSON Text should not be empty", json.length()>0);
		
		try {
			CommunicationRequest req2 = objectMapper.readValue(json, CommunicationRequest.class);
			assertNotNull(req2);
		} catch (IOException e) {
			e.printStackTrace();
			fail("JSON did not deserialize: "+e.getMessage());
		}
	}
	
	



	public CommunicationRequest  getCommunicationRequest() {
		CommunicationRequest req = new CommunicationRequest();
		// Simple String based payload
		CommunicationRequest.Payload payload = new CommunicationRequest.Payload();
		payload.setContent("This is a test payload");
		req.addPayload(payload);
		IdentifierDt subjectId = new IdentifierDt();
		subjectId.setValue("123456789-1");
		Context ctx = new Context();
		ctx.setSubjectId(subjectId);
		req.setContext(ctx);

		// Body Elements
		req.setId("IDxxx0101020");
		req.setTopicIdentifier("CoombsId_123456789-1_10");
		// Category
		req.getCategory().setText("Notification");
		CodingDt categoryCode = new CodingDt();
		categoryCode.setCode("123");
		categoryCode.setDisplay("Notification");
		categoryCode.setSystem("com.cognitivemedicine.codesystem.communicationsCategory");
		req.getCategory().addCoding(categoryCode);

		// Recipient
		Recipient recipient = new Recipient();

		CodeableConceptDt recipientType = new CodeableConceptDt();

		CodingDt recipientTypeCode = new CodingDt();
		recipientTypeCode.setCode("ROLE");
		recipientTypeCode.setDisplay("Role");
		recipientTypeCode.setSystem("com.cognitivemedicine.codesystem.RecipientType");
		recipientType.addCoding(recipientTypeCode);
		
		recipient.setType(recipientType);

		IdentifierDt roleId = new IdentifierDt();
		roleId.setSystem("com.cognitivemedicine.codesystem.Role");
		roleId.setValue("ANYNURSE");
		recipient.setIdentifier(roleId);
		req.addRecipient(recipient);

		// Priority
		CodeableConceptDt priority = new CodeableConceptDt();
		CodingDt priorityTypeCode = new CodingDt();
		priorityTypeCode.setCode("HIGH");
		priorityTypeCode.setDisplay("High");
		priorityTypeCode.setSystem("com.cognitivemedicine.codesystem.Priority");
		priority.addCoding(priorityTypeCode);
		req.setPriority(priority);

		// Status
		req.setStatus(CommunicationRequestStatus.DRAFT.name());

		// Attribution
		Attribution attribution = new Attribution();
		attribution.setDescription("This requested was generated soley as test data");
		req.addAttribution(attribution);
		return req;
	}

}
