/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.uhn.fhir.model.api.TemporalPrecisionEnum;

/**
 * @author Jerry Goodnough
 *
 */
public class PeriodDtIT {
	private static final Logger LOG = LoggerFactory.getLogger(PeriodDtIT.class);
	@Test
	public void testJonsify() {
		String json = "";

		PeriodDt periodDt = new PeriodDt();
		periodDt.setStart(new DateTimeDt(new Date()));
		periodDt.setEnd(new DateTimeDt(new Date()));
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);

		try {
			json = objectMapper.writeValueAsString(periodDt);
		} 
		catch (JsonProcessingException e) {
			e.printStackTrace();
			fail("JSON did not serialize: "+e.getMessage());
		}
		assertNotNull("The serialized request should not be null",json);
		LOG.debug("JSON Serialized PeriodDt = "+json.toString());
		assertTrue("JSON Text should not be empty", json.length()>0);
		
		try {
			PeriodDt periodDt2 = objectMapper.readValue(json, PeriodDt.class);
			assertNotNull("Context should not be null", periodDt2);
		} catch (IOException e) {
			e.printStackTrace();
			fail("JSON did not deserialize: " + e.getMessage());
		}
	}
}
