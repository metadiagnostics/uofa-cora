/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jerry Goodnough
 *
 */
public class IdentifierDtIT {
	private static final Logger LOG = LoggerFactory.getLogger(IdentifierDtIT.class);
	@Test
	public void testJonsify() {
		String json = "";
		IdentifierDt subjectId = new IdentifierDt();
		subjectId.setValue("123456789-1");
		
		PeriodDt period = new PeriodDt();
		period.setStart(new DateTimeDt(new Date()));
		period.setEnd(new DateTimeDt(new Date()));
		
		subjectId.setPeriod(period);

		subjectId.setUse("Official");
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		//objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
		//objectMapper.setSerializationInclusion(Include.NON_ABSENT);
		//objectMapper.setSerializationInclusion(Include.NON_DEFAULT);
		
		

		try {
			json = objectMapper.writeValueAsString(subjectId);
		} 
		catch (JsonProcessingException e) {
			e.printStackTrace();
			fail("JSON did not serialize: "+e.getMessage());
		}
		assertNotNull("The serialized request should not be null",json);
		LOG.debug("JSON serialized IdentifierDt = "+json.toString());
		assertTrue("JSON Text should not be empty", json.length()>0);
		
		try {
			IdentifierDt identifer2 = objectMapper.readValue(json, IdentifierDt.class);
			assertNotNull("Context should not be null",identifer2);
		} catch (IOException e) {
			e.printStackTrace();
			fail("JSON did not deserialize: "+e.getMessage());
		}
	}
}
