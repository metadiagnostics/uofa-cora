/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jerry Goodnough
 *
 */
public class QuantityDtIT {
	private static final Logger LOG = LoggerFactory.getLogger(QuantityDtIT.class);
	
	@Test
	public void testJonsify() {
		String json = "";
		QuantityDt qdt = new QuantityDt();

		qdt.setCode("codeXYZ");
		CodeDt codeDt = new CodeDt();
		codeDt.setValue("ACode");;
		qdt.setCodeElement(codeDt );
		qdt.setSystem("systemXYZ");
		qdt.setUnit("unitXYZ");
		DecimalDt decimalDt = new DecimalDt();
		decimalDt.setValueAsInteger(1234567890);
		qdt.setValue(decimalDt);
		BigDecimal bigDecimal = new BigDecimal(987654);
		qdt.setValue(bigDecimal);
		double doubleValue = 123456.78;
		qdt.setValue(doubleValue);
		long longValue = 123456789012345678L;
		qdt.setValue(longValue);
				

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		//objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
		//objectMapper.setSerializationInclusion(Include.NON_ABSENT);
		//objectMapper.setSerializationInclusion(Include.NON_DEFAULT);

		try {
			json = objectMapper.writeValueAsString(qdt);
		} 
		catch (JsonProcessingException e) {
			e.printStackTrace();
			fail("JSON did not serialize: "+e.getMessage());
		}
		assertNotNull("The serialized request should not be null",json);
		LOG.debug("JSON Serialized Quantity = "+json.toString());
		assertTrue("JSON Text should not be empty", json.length()>0);
		
		try {
			QuantityDt qdt2 = objectMapper.readValue(json, QuantityDt.class);
			assertNotNull("QuantityDtshould not be null",qdt2);
		} catch (IOException e) {
			e.printStackTrace();
			fail("JSON did not deserialize: "+e.getMessage());
		}
	}
	
}
