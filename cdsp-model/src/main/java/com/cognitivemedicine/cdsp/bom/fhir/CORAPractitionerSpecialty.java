/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.StringType;



public class CORAPractitionerSpecialty {

    private CodeableConcept adaptedClass = null;

    public CORAPractitionerSpecialty() {
        this.adaptedClass = new CodeableConcept();
    }

    public CORAPractitionerSpecialty(CodeableConcept adaptee) {
        this.adaptedClass = adaptee;
    }

    public Coding addCoding() {
        return adaptedClass.addCoding();

    }

    public CORAPractitionerSpecialty addCoding(Coding param) {
        adaptedClass.addCoding(param);
        return this;
    }

    public CodeableConcept getAdaptee() {
        return adaptedClass;
    }

    public List<Coding> getCoding() {
        return adaptedClass.getCoding();
    }

    public Coding getCodingFirstRep() {
        return adaptedClass.getCodingFirstRep();
    }

    public BooleanType getPrimaryInd() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/practitioner-primaryInd");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (BooleanType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for primaryInd");
        }
    }

    public String getText() {
        return adaptedClass.getText();
    }

    public StringType getTextElement() {
        return adaptedClass.getTextElement();
    }


    public void setAdaptee(CodeableConcept param) {
        this.adaptedClass = param;
    }

    public CORAPractitionerSpecialty setCoding(List<Coding> param) {
        adaptedClass.setCoding(param);
        return this;
    }

    public CORAPractitionerSpecialty setPrimaryInd(BooleanType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/practitioner-primaryInd")
                .setValue(param);
        return this;
    }

    public CORAPractitionerSpecialty setText(String param) {
        adaptedClass.setText(param);
        return this;
    }

    public CORAPractitionerSpecialty setText(StringType param) {
        adaptedClass.setTextElement(param);
        return this;
    }
}
