/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.dstu3.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.PositiveIntType;
import org.hl7.fhir.dstu3.model.StringType;

public class CORAContactPoint {

    private ContactPoint adaptedClass = null;

    public CORAContactPoint() {
        this.adaptedClass = new ContactPoint();
    }

    public CORAContactPoint(ContactPoint adaptee) {
        this.adaptedClass = adaptee;
    }

    public ContactPoint getAdaptee() {
        return adaptedClass;
    }

    public Period getPeriod() {
        return adaptedClass.getPeriod();
    }

    public BooleanType getPreferred() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/iso21090-preferred");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (BooleanType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for preferred");
        }
    }

    public Integer getRank() {
        return adaptedClass.getRank();
    }


    public PositiveIntType getRankElement() {
        return adaptedClass.getRankElement();
    }

    public ContactPointSystem getSystem() {
        return adaptedClass.getSystem();
    }

    public Enumeration<ContactPointSystem> getSystemElement() {
        return adaptedClass.getSystemElement();
    }

    public ContactPointUse getUse() {
        return adaptedClass.getUse();
    }

    public Enumeration<ContactPointUse> getUseElement() {
        return adaptedClass.getUseElement();
    }

    public String getValue() {
        return adaptedClass.getValue();
    }

    public StringType getValueElement() {
        return adaptedClass.getValueElement();
    }

    public void setAdaptee(ContactPoint param) {
        this.adaptedClass = param;
    }

    public CORAContactPoint setPeriod(Period param) {
        adaptedClass.setPeriod(param);
        return this;
    }

    public CORAContactPoint setPreferred(BooleanType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/iso21090-preferred")
                .setValue(param);
        return this;
    }

    public CORAContactPoint setRank(Integer param) {
        adaptedClass.setRank(param);
        return this;
    }

    public CORAContactPoint setRank(PositiveIntType param) {
        adaptedClass.setRankElement(param);
        return this;
    }

    public CORAContactPoint setSystem(ContactPointSystem param) {
        adaptedClass.setSystem(param);
        return this;
    }

    public CORAContactPoint setSystem(String param) {
        adaptedClass.setSystem(ContactPointSystem.valueOf(param));
        return this;
    }

    public CORAContactPoint setSystemElement(Enumeration<ContactPointSystem> param) {
        adaptedClass.setSystemElement(param);
        return this;
    }

    public CORAContactPoint setUse(ContactPointUse param) {
        adaptedClass.setUse(param);
        return this;
    }

    public CORAContactPoint setUse(String param) {
        adaptedClass.setUse(ContactPointUse.valueOf(param));
        return this;
    }

    public CORAContactPoint setUseElement(Enumeration<ContactPointUse> param) {
        adaptedClass.setUseElement(param);
        return this;
    }

    public CORAContactPoint setValue(String param) {
        adaptedClass.setValue(param);
        return this;
    }

    public CORAContactPoint setValueElement(StringType param) {
        adaptedClass.setValueElement(param);
        return this;
    }
}
