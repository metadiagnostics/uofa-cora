/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;


import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.InstantType;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.DiagnosticReport.DiagnosticReportPerformerComponent;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;

/**
 * DSTU3 DiagnosticReport
 * @author Jerry Goodnough
 *
 
  
  Not Yet supported
      Reference BasedOn
      Reference Subject
      Reference Specimen
      Reference ImagingStudy
*/

@LogicalModelAddition("Extends our base")
public interface ICORADiagnosticReport
        extends ICORACognitiveBaseIdentifiable<ICORADiagnosticReport> {

    public CodeableConcept addCodedDiagnosis();

    public ICORADiagnosticReport addCodedDiagnosis(CodeableConcept param);

    public DiagnosticReport.DiagnosticReportImageComponent addImage();

    public ICORADiagnosticReport addImage(DiagnosticReport.DiagnosticReportImageComponent param);

    public DiagnosticReportPerformerComponent addPerformer();

    public ICORADiagnosticReport addPerformer(DiagnosticReportPerformerComponent param);

    public Attachment addPresentedForm();

    public ICORADiagnosticReport addPresentedForm(Attachment param);

    public Reference addResult();

    public ICORADiagnosticReport addResult(Reference param);

    public CodeableConcept getCategory();

    public CodeableConcept getCode();

    public List<CodeableConcept> getCodedDiagnosis();

    public CodeableConcept getCodedDiagnosisFirstRep();

    public String getConclusion();

    public StringType getConclusionElement();

    public Reference getContext();

    public Date getEffectiveDateTime();

    public DateTimeType getEffectiveDateTimeElement();

    public Period getEffectivePeriod();

    public CORAEncounterAdapter getEncounterResource();

    public List<DiagnosticReport.DiagnosticReportImageComponent> getImage();

    public DiagnosticReport.DiagnosticReportImageComponent getImageFirstRep();

    public Date getIssued();

    public InstantType getIssuedElement();

    public Reference getLocationPerformed();

    public List<DiagnosticReportPerformerComponent> getPerformer();

    public DiagnosticReportPerformerComponent getPerformerFirstReq();

    public List<Attachment> getPresentedForm();

    public Attachment getPresentedFormFirstRep();

    public List<Reference> getResult();

    public Reference getResultFirstRep();

    public DiagnosticReport.DiagnosticReportStatus getStatus();

    public Enumeration<DiagnosticReport.DiagnosticReportStatus> getStatusElement();

    public ICORADiagnosticReport setCategory(CodeableConcept param);


    public ICORADiagnosticReport setCode(CodeableConcept param);

    public ICORADiagnosticReport setCodededDiagnosis(List<CodeableConcept> param);

    public ICORADiagnosticReport setConclusion(String param);
  

    public ICORADiagnosticReport setConclusionElement(StringType param);

    public ICORADiagnosticReport setContext(Reference param);

    public ICORADiagnosticReport setEffectiveDateTime(Date param);

    public ICORADiagnosticReport setEffectiveDateTimeElement(DateTimeType param);

    public ICORADiagnosticReport setEffectivePeriod(Period param);
    
    
    public ICORADiagnosticReport setEncounterResource(CORAEncounterAdapter param);
   
    public ICORADiagnosticReport setImage(
            List<DiagnosticReport.DiagnosticReportImageComponent> param);
    
    public ICORADiagnosticReport setIssued(Date param);
    
    public ICORADiagnosticReport setIssuedElement(InstantType param);
    
    public ICORADiagnosticReport setLocationPerformed(Reference param);
    
    public ICORADiagnosticReport setPerformer(List<DiagnosticReportPerformerComponent> param);
    
    public ICORADiagnosticReport setPresentedForm(List<Attachment> param);
    
    public ICORADiagnosticReport setResult(List<Reference> param);
    
    public ICORADiagnosticReport setStatus(DiagnosticReport.DiagnosticReportStatus param);
    
    public ICORADiagnosticReport setStatusElement(
            Enumeration<DiagnosticReport.DiagnosticReportStatus> param);
  
}
