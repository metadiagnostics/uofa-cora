/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DecimalType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.ProcedureRequest;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestIntent;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestPriority;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestRequesterComponent;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestStatus;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Timing;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.FHIRExtension;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;



public class CORAProcedureRequestAdapter
        extends CORACognitiveBaseIdentifiable<ProcedureRequest, ICORAProcedureRequest>
        implements ICORAProcedureRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CORAProcedureRequestAdapter() {
        this.adaptedClass = new ProcedureRequest();
    }

    public CORAProcedureRequestAdapter(ProcedureRequest adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {
        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORAProcedureRequest addBasedOn(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }


    @Override
    public CodeableConcept addCategory() {
        return adaptedClass.addCategory();
    }

    @Override
    public ICORAProcedureRequest addCategory(CodeableConcept param) {
        adaptedClass.addCategory(param);
        return this;
    }

    @Override
    public Reference addDefinition() {
        return adaptedClass.addDefinition();
    }

    @Override
    public ICORAProcedureRequest addDefinition(Reference param) {
        adaptedClass.addDefinition(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAProcedureRequest addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Annotation addNote() {
        Annotation item = new Annotation();
        adaptedClass.addNote(item);
        return item;
    }

    @Override
    public ICORAProcedureRequest addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest addPrecondition(CodeableConcept param) {
        if (param != null) {
            Extension extension = new Extension();
            extension.setUrl("http://hl7.org/fhir/StructureDefinition/procedurequest-precondition");
            extension.setValue(param);
            adaptedClass.addExtension(extension);
        }
        return this;
    }

    @Override
    public CodeableConcept addReasonCode() {

        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAProcedureRequest addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {
        return adaptedClass.addReasonReference();
    }

    @Override
    public ICORAProcedureRequest addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public Reference addRelevantHistory() {

        return adaptedClass.addRelevantHistory();
    }

    @Override
    public ICORAProcedureRequest addRelevantHistory(Reference param) {
        adaptedClass.addRelevantHistory(param);
        return this;
    }

    @Override
    public Reference addReplaces() {

        return adaptedClass.addReplaces();
    }

    @Override
    public ICORAProcedureRequest addReplaces(Reference param) {
        adaptedClass.addReplaces(param);
        return this;
    }

    @Override
    public Reference addSpecimen() {

        return adaptedClass.addSpecimen();
    }


    @Override
    public ICORAProcedureRequest addSpecimen(Reference param) {
        adaptedClass.addSpecimen(param);
        return this;
    }

    @Override
    public Reference addSupportingInfo() {
        return adaptedClass.addSupportingInfo();
    }

    @Override
    public ICORAProcedureRequest addSupportingInfo(Reference param) {
        adaptedClass.addSupportingInfo(param);
        return this;
    }

    @Override
    public List<Reference> getApproachBodySite() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/procedurerequest-approachBodySite");
        List<Reference> returnList = new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            returnList.add((Reference) extension.getValue());
        }
        return returnList;
    }

    @Override
    @FHIRExtension(name = "QICore-procedurerequest-appropriatenessScore",
            url = "http://hl7.org/fhir/StructureDefinition/QICore-procedurerequest-appropriatenessScore")
    public DecimalType getAppropriatenessScore() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/QICore-procedurerequest-appropriatenessScore");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (DecimalType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for appropriatenessScore");
        }
    }

    @Override
    public Boolean getAsNeededBoolean() {
        if ((adaptedClass.getAsNeeded() != null)
                && (adaptedClass.getAsNeeded() instanceof BooleanType)) {
            return ((BooleanType) adaptedClass.getAsNeeded()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public BooleanType getAsNeededBooleanElement() {
        if ((adaptedClass.getAsNeeded() != null)
                && (adaptedClass.getAsNeeded() instanceof BooleanType)) {
            return (BooleanType) adaptedClass.getAsNeeded();
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getAsNeededCodeableConcept() {
        if ((adaptedClass.getAsNeeded() != null)
                && (adaptedClass.getAsNeeded() instanceof CodeableConcept)) {
            return (CodeableConcept) adaptedClass.getAsNeeded();
        } else {
            return null;
        }
    }

    @Override
    public Date getAuthoredOn() {
        return adaptedClass.getAuthoredOn();
    }

    @Override
    public DateTimeType getAuthoredOnElement() {
        return adaptedClass.getAuthoredOnElement();
    }

    @Override
    public List<Reference> getBasedOn() {

        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {

        return adaptedClass.getBasedOnFirstRep();
    }

    @Override
    public List<CodeableConcept> getCategory() {

        return adaptedClass.getCategory();
    }

    @Override
    public CodeableConcept getCategoryFirstRep() {

        return adaptedClass.getCategoryFirstRep();
    }


    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }

    @Override
    public Reference getContext() {

        return adaptedClass.getContext();
    }

    @Override
    public List<Reference> getDefinition() {

        return adaptedClass.getDefinition();
    }

    @Override
    public Reference getDefinitionFirstRep() {

        return adaptedClass.getDefinitionFirstRep();
    }

    @Override
    public boolean getDoNotPerform() {

        return adaptedClass.getDoNotPerform();
    }

    @Override
    public BooleanType getDotNotPerformElement() {

        return adaptedClass.getDoNotPerformElement();
    }

    @Override
    @LogicalModelAlias("getContext")
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getContext().getResource() instanceof Encounter) {
            CORAEncounterAdapter profiledType = new CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getContext().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public ProcedureRequestIntent getIntent() {
        return adaptedClass.getIntent();
    }



    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public Type getOccurrence() {
        return adaptedClass.getOccurrence();
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public Date getOccurrenceDateTime() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof DateTimeType)) {
            DateTimeType out = (DateTimeType) adaptedClass.getOccurrence();
            return out.getValue();
        } else {
            return null;
        }
    }

    @Override
    @LogicalModelAlias("getOccurrence")
    public DateTimeType getOccurrenceDateTimeElement() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof DateTimeType)) {
            return (DateTimeType) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public Period getOccurrencePeriod() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof Period)) {
            return (Period) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public Timing getOccurrenceTiming() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof Timing)) {
            return (Timing) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }



    @Override
    public Reference getPerformer() {
        return adaptedClass.getPerformer();
    }



    @Override
    public CodeableConcept getPerformerType() {

        return adaptedClass.getPerformerType();
    }

    @Override
    public List<CodeableConcept> getPrecondition() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/procedurequest-precondition");
        List<CodeableConcept> returnList = new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            CodeableConcept item = (CodeableConcept) extension.getValue();
            returnList.add(item);
        }
        return returnList;
    }

    @Override
    public ProcedureRequestPriority getPriority() {
        return adaptedClass.getPriority();
    }

    @Override
    public Enumeration<ProcedureRequestPriority> getPriorityElement() {
        return adaptedClass.getPriorityElement();
    }

    @Override
    public List<CodeableConcept> getReasonCode() {

        return adaptedClass.getReasonCode();
    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {

        return adaptedClass.getReasonCodeFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {

        return adaptedClass.getReasonReference();
    }


    @Override
    public Reference getReasonReferenceFirstRep() {

        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    @FHIRExtension(name = "procedurerequest-reasonRefused",
            url = "http://hl7.org/fhir/StructureDefinition/procedurerequest-reasonRefused")
    public CodeableConcept getReasonRefused() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/procedurerequest-reasonRefused");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonRefused");
        }
    }



    @Override
    @FHIRExtension(name = "procedurerequest-procedurequest-reasonRejected",
            url = "http://hl7.org/fhir/StructureDefinition/procedurequest-reasonRejected")
    public CodeableConcept getReasonRejected() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/procedurequest-reasonRejected");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonRejected");
        }
    }

    @Override
    public List<Reference> getRelevantHistory() {

        return adaptedClass.getRelevantHistory();
    }

    @Override
    public Reference getRelevantHistoryFirstRep() {

        return adaptedClass.getRelevantHistoryFirstRep();
    }

    @Override
    public List<Reference> getReplaces() {

        return adaptedClass.getReplaces();
    }

    @Override
    public Reference getReplacesFirstRep() {

        return adaptedClass.getReplacesFirstRep();
    }

    @Override
    public ProcedureRequestRequesterComponent getRequester() {

        return adaptedClass.getRequester();
    }

    @Override
    public Identifier getRequisition() {
        return adaptedClass.getRequisition();
    }

    @Override
    @LogicalModelAlias("getOccurrence")
    public Date getScheduledDateTime() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof DateTimeType)) {
            return ((DateTimeType) adaptedClass.getOccurrence()).getValue();
        } else {
            return null;
        }
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public DateTimeType getScheduledDateTimeElement() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof DateTimeType)) {
            return (DateTimeType) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @Override
    @LogicalModelAlias("getOccurrence")
    public Period getScheduledPeriod() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof Period)) {
            return (Period) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @Override
    @LogicalModelAlias("getOccurrence")
    public Timing getScheduledTiming() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof Timing)) {
            return (Timing) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @Override
    public List<Reference> getSpecimen() {

        return adaptedClass.getSpecimen();
    }

    @Override
    public Reference getSpecimenFirstRep() {
        return adaptedClass.getSpecimenFirstRep();
    }

    @Override
    public ProcedureRequestStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<ProcedureRequestStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Reference getSubject() {

        return adaptedClass.getSubject();
    }

    @Override
    public List<Reference> getSupportingInfo() {

        return adaptedClass.getSupportingInfo();
    }

    @Override
    public Reference getSupportingInfoFirstRep() {

        return adaptedClass.getSupportingInfoFirstRep();
    }

    @Override
    public ICORAProcedureRequest setApproachBodySite(List<Reference> param) {
        if ((param != null) && (param.size() > 0)) {
            for (int index = 0; index < param.size(); index++) {
                adaptedClass.addExtension()
                        .setUrl("http://hl7.org/fhir/StructureDefinition/procedurerequest-approachBodySite")
                        .setValue(param.get(index));
            }
        }
        return this;
    }

    @Override
    @FHIRExtension(name = "QICore-procedurerequest-appropriatenessScore",
            url = "http://hl7.org/fhir/StructureDefinition/QICore-procedurerequest-appropriatenessScore")
    public ICORAProcedureRequest setAppropriatenessScore(DecimalType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/QICore-procedurerequest-appropriatenessScore")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setAsNeededBoolean(Boolean param) {
        adaptedClass.setAsNeeded(new BooleanType(param));
        return this;
    }

    @Override
    public ICORAProcedureRequest setAsNeededBoolean(BooleanType param) {
        adaptedClass.setAsNeeded(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setAsNeededCodeableConcept(CodeableConcept param) {
        adaptedClass.setAsNeeded(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setAuthoredOn(Date param) {
        adaptedClass.setAuthoredOn(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setAuthoredOnElement(DateTimeType param) {
        adaptedClass.setAuthoredOnElement(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setCategory(List<CodeableConcept> param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setDefinition(List<Reference> param) {
        adaptedClass.setDefinition(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setDoNotPerform(boolean param) {
        adaptedClass.setDoNotPerform(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setDoNotPerformElement(BooleanType param) {
        adaptedClass.setDoNotPerformElement(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getContext().setResource(param.getAdaptee());
        return this;
    }

    public ICORAProcedureRequest setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setIntent(ProcedureRequestIntent param) {
        adaptedClass.setIntent(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setOccurrence(Type param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @LogicalModelAlias("setOccurrence")
    @Override
    public ICORAProcedureRequest setOccurrenceDate(Date param) {
        adaptedClass.setOccurrence(new DateTimeType(param));
        return this;
    }

    @LogicalModelAlias("setOccurrence")
    @Override
    public ICORAProcedureRequest setOccurrenceDateTimeElement(DateTimeType param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @LogicalModelAlias("setOccurrence")
    @Override
    public ICORAProcedureRequest setOccurrencePeriod(Period param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @LogicalModelAlias("setOccurrence")
    @Override
    public ICORAProcedureRequest setOccurrenceTiming(Timing param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setPerformer(Reference param) {
        adaptedClass.setPerformer(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setPerformerType(CodeableConcept param) {
        adaptedClass.setPerformerType(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setPrecondition(List<CodeableConcept> param) {
        if ((param != null) && (param.size() > 0)) {
            for (int index = 0; index < param.size(); index++) {
                Extension extension = new Extension();
                extension.setUrl(
                        "http://hl7.org/fhir/StructureDefinition/procedurequest-precondition");
                extension.setValue(param.get(index));
                adaptedClass.addExtension(extension);
            }
        }
        return this;
    }

    @Override
    public ICORAProcedureRequest setPriority(ProcedureRequestPriority param) {
        adaptedClass.setPriority(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setPriority(String param) {
        adaptedClass.setPriority(ProcedureRequestPriority.valueOf(param));
        return this;
    }

    @Override
    public ICORAProcedureRequest setPriorityElement(Enumeration<ProcedureRequestPriority> param) {
        adaptedClass.setPriorityElement(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    @FHIRExtension(name = "procedurerequest-reasonRefused",
            url = "http://hl7.org/fhir/StructureDefinition/procedurerequest-reasonRefused")
    public ICORAProcedureRequest setReasonRefused(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/procedurerequest-reasonRefused")
                .setValue(param);
        return this;
    }

    @Override
    @FHIRExtension(name = "procedurerequest-procedurequest-reasonRejected",
            url = "http://hl7.org/fhir/StructureDefinition/procedurequest-reasonRejected")
    public ICORAProcedureRequest setReasonRejected(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/procedurequest-reasonRejected")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setRelevantHistory(List<Reference> param) {
        adaptedClass.setRelevantHistory(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setReplaces(List<Reference> param) {
        adaptedClass.setReplaces(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setRequester(ProcedureRequestRequesterComponent param) {
        adaptedClass.setRequester(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setRequisition(Identifier param) {
        adaptedClass.setRequisition(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledDateTime(Date param) {
        adaptedClass.setOccurrence(new DateTimeType(param));
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledDateTimeElement(DateTimeType param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledPeriod(Period param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledTiming(Timing param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setSpecimen(List<Reference> param) {
        adaptedClass.setSpecimen(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setStatus(ProcedureRequestStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setStatus(String param) {
        adaptedClass.setStatus(ProcedureRequestStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAProcedureRequest setStatusElement(Enumeration<ProcedureRequestStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setSupportingInfo(List<Reference> param) {
        adaptedClass.setSupportingInfo(param);
        return this;
    }

    @Override
    public ICORAProcedureRequest setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAProcedureRequestAdapter [getApproachBodySite()=" + getApproachBodySite()
                + ", getAppropriatenessScore()=" + getAppropriatenessScore()
                + ", getAsNeededBoolean()=" + getAsNeededBoolean()
                + ", getAsNeededCodeableConcept()=" + getAsNeededCodeableConcept()
                + ", getAuthoredOn()=" + getAuthoredOn() + ", getBasedOn()=" + getBasedOn()
                + ", getCategory()=" + getCategory() + ", getCode()=" + getCode()
                + ", getContext()=" + getContext() + ", getDefinition()=" + getDefinition()
                + ", getDoNotPerform()=" + getDoNotPerform() + ", getIdentifier()="
                + getIdentifier() + ", getIntent()=" + getIntent() + ", getNote()=" + getNote()
                + ", getOccurrence()=" + getOccurrence() + ", getPerformer()=" + getPerformer()
                + ", getPrecondition()=" + getPrecondition() + ", getPriority()=" + getPriority()
                + ", getReasonCode()=" + getReasonCode() + ", getReasonReference()="
                + getReasonReference() + ", getReasonRefused()=" + getReasonRefused()
                + ", getReasonRejected()=" + getReasonRejected() + ", getRelevantHistory()="
                + getRelevantHistory() + ", getReplaces()=" + getReplaces() + ", getRequester()="
                + getRequester() + ", getRequisition()=" + getRequisition() + ", getSpecimen()="
                + getSpecimen() + ", getStatus()=" + getStatus() + ", getSubject()=" + getSubject()
                + ", getSupportingInfo()=" + getSupportingInfo() + ", getPrimaryIdentifer()="
                + getPrimaryIdentifer() + ", getContained()=" + getContained() + "]";
    }

    
}
