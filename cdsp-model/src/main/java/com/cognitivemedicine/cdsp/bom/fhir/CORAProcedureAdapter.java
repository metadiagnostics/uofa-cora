/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Location;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Procedure;
import org.hl7.fhir.dstu3.model.Procedure.ProcedureStatus;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;


public class CORAProcedureAdapter extends CORACognitiveBaseIdentifiable<Procedure, ICORAProcedure>
        implements ICORAProcedure {

    private BooleanType elective = new BooleanType((Boolean)(null));
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CORAProcedureAdapter() {
        this.adaptedClass = new Procedure();
    }

    public CORAProcedureAdapter(Procedure adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {
        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORAProcedure addBasedOn(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }

    @Override
    public CodeableConcept addComplication() {
        return adaptedClass.addComplication();
    }

    @Override
    public ICORAProcedure addComplication(CodeableConcept param) {
        adaptedClass.addComplication(param);
        return this;
    }

    @Override
    public Reference addComplicationDetail() {
        return adaptedClass.addComplicationDetail();
    }

    @Override
    public ICORAProcedure addComplicationDetail(Reference param) {
        adaptedClass.addComplicationDetail(param);
        return this;
    }

    @Override
    public Reference addDefinition() {
        return adaptedClass.addDefinition();
    }

    @Override
    public ICORAProcedure addDefinition(Reference param) {
        adaptedClass.addDefinition(param);
        return this;
    }

    @Override
    public Procedure.ProcedureFocalDeviceComponent addFocalDevice() {
        return adaptedClass.addFocalDevice();

    }

    @Override
    public ICORAProcedure addFocalDevice(Procedure.ProcedureFocalDeviceComponent param) {
        adaptedClass.addFocalDevice(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }


    @Override
    public ICORAProcedure addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Annotation addNote() {
        Annotation item = new Annotation();
        adaptedClass.addNote(item);
        return item;
    }

    @Override
    public ICORAProcedure addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }


    @Override
    public Reference addPartOf() {
        return adaptedClass.addPartOf();
    }

    @Override
    public ICORAProcedure addPartOf(Reference param) {
        adaptedClass.addPartOf(param);
        return this;
    }

    @Override
    public Procedure.ProcedurePerformerComponent addPerformer() {
        return adaptedClass.addPerformer();

    }


    @Override
    public ICORAProcedure addPerformer(Procedure.ProcedurePerformerComponent param) {
        adaptedClass.addPerformer(param);
        return this;
    }

    @Override
    public CodeableConcept addReasonCode() {
        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAProcedure addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode();
        return this;
    }

    @Override
    public Reference addReasonReference() {

        return adaptedClass.addReasonReference();
    }

    @Override
    public ICORAProcedure addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public CodeableConcept addUsedCode() {
        return adaptedClass.addUsedCode();
    }

    @Override
    public ICORAProcedure addUsedCode(CodeableConcept param) {
        adaptedClass.addUsedCode(param);
        return this;
    }

    @Override
    public Reference addUsedReference() {
        return adaptedClass.addUsedReference();
    }

    @Override
    public ICORAProcedure addUsedReference(Reference param) {
        adaptedClass.addUsedReference(param);
        return this;
    }

    @Override
    public List<Reference> getApproachBodySite() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/procedure-approachBodySite");
        List<Reference> returnList = new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            returnList.add((Reference) extension.getValue());
        }
        return returnList;
    }

    @Override
    public List<Reference> getBasedOn() {
        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {
        return adaptedClass.getBasedOnFirstRep();
    }

    @Override
    public CodeableConcept getCategory() {
        return adaptedClass.getCategory();
    }


    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }

    @Override
    public List<CodeableConcept> getComplication() {
        return adaptedClass.getComplication();
    }

    @Override
    public List<Reference> getComplicationDetail() {
        return adaptedClass.getComplicationDetail();
    }


    @Override
    public Reference getComplicationDetailFirstRep() {
        return adaptedClass.getComplicationDetailFirstRep();
    }

    @Override
    public CodeableConcept getComplicationFirstRep() {
        return adaptedClass.getComplicationFirstRep();
    }

    @Override
    public Reference getContext() {
        return adaptedClass.getContext();
    }

    @Override
    public List<Reference> getDefinition() {
        return adaptedClass.getDefinition();
    }

    @Override
    public Reference getDefinitionFirstRep() {
        return adaptedClass.getDefinitionFirstRep();
    }

    @LogicalModelAlias("getContext")
    @Override
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getContext().getResource() instanceof Encounter) {
            CORAEncounterAdapter profiledType = new CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getContext().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<Procedure.ProcedureFocalDeviceComponent> getFocalDevice() {
        return adaptedClass.getFocalDevice();
    }

    @Override
    public Procedure.ProcedureFocalDeviceComponent getFocalDeviceFirstRep() {
        return adaptedClass.getFocalDeviceFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public DateTimeType getIncisionDateTime() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/procedure-incisionDateTime");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (DateTimeType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for incisionDateTime");
        }
    }

    @Override
    public Reference getLocation() {
        return adaptedClass.getLocation();
    }

    @Override
    public CORALocationAdapter getLocationResource() {
        if (adaptedClass.getLocation().getResource() instanceof Location) {
            CORALocationAdapter profiledType = new CORALocationAdapter();
            profiledType.setAdaptee((Location) adaptedClass.getLocation().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public boolean getNotDone() {
        return adaptedClass.getNotDone();
    }

    @Override
    public BooleanType getNotDoneElement() {
        return adaptedClass.getNotDoneElement();
    }

    @Override
    public CodeableConcept getNotDoneReason() {
        return adaptedClass.getNotDoneReason();
    }

    @Override
    public List<Annotation> getNote() {
        return null;
    }

    @Override
    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public CodeableConcept getOutcome() {
        return adaptedClass.getOutcome();
    }

    @Override
    public List<Reference> getPartOf() {
        return adaptedClass.getPartOf();
    }

    @Override
    public Reference getPartOfFirstRep() {
        return adaptedClass.getPartOfFirstRep();
    }

    @Override
    public Date getPerformedDateTime() {
        if ((adaptedClass.getPerformed() != null)
                && (adaptedClass.getPerformed() instanceof DateTimeType)) {
            return ((DateTimeType) adaptedClass.getPerformed()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateTimeType getPerformedDateTimeElement() {
        if ((adaptedClass.getPerformed() != null)
                && (adaptedClass.getPerformed() instanceof DateTimeType)) {
            return (DateTimeType) adaptedClass.getPerformed();
        } else {
            return null;
        }
    }

    @Override
    public Period getPerformedPeriod() {
        if ((adaptedClass.getPerformed() != null)
                && (adaptedClass.getPerformed() instanceof Period)) {
            return (Period) adaptedClass.getPerformed();
        } else {
            return null;
        }
    }

    @Override
    public List<Procedure.ProcedurePerformerComponent> getPerformer() {
        return adaptedClass.getPerformer();
    }

    @Override
    public Procedure.ProcedurePerformerComponent getPerformerFirstRep() {
        return adaptedClass.getPerformerFirstRep();
    }

    @Override
    public List<CodeableConcept> getReasonCode() {
        return adaptedClass.getReasonCode();
    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {
        return adaptedClass.getReasonCodeFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {
        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstRep() {

        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public ProcedureStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<ProcedureStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public List<CodeableConcept> getUsedCode() {
        return adaptedClass.getUsedCode();
    }

    @Override
    public CodeableConcept getUsedCodeFirstRep() {
        return adaptedClass.getUsedCodeFirstRep();
    }

    @Override
    public List<Reference> getUsedReference() {
        return adaptedClass.getUsedReference();
    }

    @Override
    public Reference getUsedReferenceFirstRep() {
        return adaptedClass.getUsedReferenceFirstRep();
    }

    @Override
    public ICORAProcedure setApproachBodySite(List<Reference> param) {
        if ((param != null) && (param.size() > 0)) {
            for (int index = 0; index < param.size(); index++) {
                adaptedClass.addExtension()
                        .setUrl("http://hl7.org/fhir/StructureDefinition/procedure-approachBodySite")
                        .setValue(param.get(index));
            }
        }
        return this;
    }

    @Override
    public ICORAProcedure setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORAProcedure setCategory(CodeableConcept param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORAProcedure setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    @Override
    public ICORAProcedure setComplication(List<CodeableConcept> param) {
        adaptedClass.setComplication(param);
        return this;
    }

    @Override
    public ICORAProcedure setComplicationDetail(List<Reference> param) {
        adaptedClass.setComplicationDetail(param);
        return this;
    }

    @Override
    public ICORAProcedure setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORAProcedure setDefinition(List<Reference> param) {
        adaptedClass.setDefinition(param);
        return this;
    }

    @Override
    public ICORAProcedure setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getContext().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAProcedure setFocalDevice(List<Procedure.ProcedureFocalDeviceComponent> param) {
        adaptedClass.setFocalDevice(param);
        return this;
    }

    @Override
    public ICORAProcedure setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAProcedure setIncisionDateTime(DateTimeType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/procedure-incisionDateTime")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAProcedure setLocation(Reference param) {
        adaptedClass.setLocation(param);
        return this;
    }

    @Override
    public ICORAProcedure setLocationResource(CORALocationAdapter param) {
        adaptedClass.getLocation().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAProcedure setNotDone(boolean param) {
        adaptedClass.setNotDone(param);
        return this;
    }

    @Override
    public ICORAProcedure setNotDoneElement(BooleanType param) {
        adaptedClass.setNotDoneElement(param);
        return this;
    }

    @Override
    public ICORAProcedure setNotDoneReason(CodeableConcept param) {
        adaptedClass.setNotDoneReason(param);
        return this;
    }

    @Override
    public ICORAProcedure setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORAProcedure setOutcome(CodeableConcept param) {
        adaptedClass.setOutcome(param);
        return this;
    }

    @Override
    public ICORAProcedure setPartOf(List<Reference> param) {
        adaptedClass.setPartOf(param);
        return this;
    }

    @Override
    public ICORAProcedure setPerformedDateTime(Date param) {
        adaptedClass.setPerformed(new DateTimeType(param));
        return this;
    }

    @Override
    public ICORAProcedure setPerformedDateTimeElement(DateTimeType param) {
        adaptedClass.setPerformed(param);
        return this;
    }

    @Override
    public ICORAProcedure setPerformedPeriod(Period param) {
        adaptedClass.setPerformed(param);
        return this;
    }

    @Override
    public ICORAProcedure setPerformer(List<Procedure.ProcedurePerformerComponent> param) {
        adaptedClass.setPerformer(param);
        return this;
    }

    @Override
    public ICORAProcedure setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }

    @Override
    public ICORAProcedure setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORAProcedure setStatus(ProcedureStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAProcedure setStatus(String param) {
        adaptedClass.setStatus(ProcedureStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAProcedure setStatusElement(Enumeration<ProcedureStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAProcedure setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAProcedure setUsedCode(List<CodeableConcept> param) {
        adaptedClass.setUsedCode(param);
        return this;
    }

    @Override
    public ICORAProcedure setUsedReference(List<Reference> param) {
        adaptedClass.setUsedReference(param);
        return this;
    }
    
    @Override
    public ICORAProcedure setElective(BooleanType isElective) {
        this.elective = isElective;
        return this;
    }
    
    @Override
    public ICORAProcedure setElective(boolean isElective) {
        this.elective.setValue(isElective);
        return this;
    }
    
    @Override
    public BooleanType getElective() {
        return this.elective;
    }

    @Override
    public String toString() {
        return "CORAProcedureAdapter [getApproachBodySite()=" + getApproachBodySite()
                + ", getBasedOn()=" + getBasedOn() + ", getCategory()=" + getCategory()
                + ", getCode()=" + getCode() + ", getComplication()=" + getComplication()
                + ", getComplicationDetail()=" + getComplicationDetail() + ", getContext()="
                + getContext() + ", getDefinition()=" + getDefinition() + ", getFocalDevice()="
                + getFocalDevice() + ", getIdentifier()=" + getIdentifier() + ", getLocation()="
                + getLocation() + ", getNotDone()=" + getNotDone() + ", getNote()=" + getNote()
                + ", getOutcome()=" + getOutcome() + ", getPartOf()=" + getPartOf()
                + ", getPerformedDateTime()=" + getPerformedDateTime() + ", getPerformer()="
                + getPerformer() + ", getReasonCode()=" + getReasonCode() + ", getStatus()="
                + getStatus() + ", getSubject()=" + getSubject() + ", getUsedCode()="
                + getUsedCode() + ", getUsedReference()=" + getUsedReference()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getContained()="
                + getContained() + "]";
    }


}
