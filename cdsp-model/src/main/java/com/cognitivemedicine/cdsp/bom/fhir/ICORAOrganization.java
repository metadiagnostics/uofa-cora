/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.Organization.OrganizationContactComponent;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;


public interface ICORAOrganization extends ICORACognitiveBaseIdentifiable<ICORAOrganization> {

    public Address addAddress();

    public ICORAOrganization addAddress(Address param);

    public OrganizationContactComponent addContact();

    public ICORAOrganization addContact(OrganizationContactComponent param);

    public ContactPoint addTelecom();

    public ICORAOrganization addTelecom(ContactPoint param);

    public Boolean getActive();

    public BooleanType getActiveElement();

    public List<Address> getAddress();

    public Address getAddressFirstRep();

    public List<OrganizationContactComponent> getContact();

    public OrganizationContactComponent getContactFirstRep();

    public String getName();

    public StringType getNameElement();

    public CORAOrganizationAdapter getPartOfResource();

    public List<ContactPoint> getTelecom();

    public ContactPoint getTelecomFirstRep();

    public List<CodeableConcept> getType();

    public ICORAOrganization setActive(Boolean param);

    public ICORAOrganization setActiveElement(BooleanType param);

    public ICORAOrganization setAddress(List<Address> param);

    public ICORAOrganization setContact(List<OrganizationContactComponent> param);

    public ICORAOrganization setName(String param);

    public ICORAOrganization setNameElement(StringType param);

    public ICORAOrganization setPartOfResource(CORAOrganizationAdapter param);

    public ICORAOrganization setTelecom(List<ContactPoint> param);

    public ICORAOrganization setType(List<CodeableConcept> param);
    
    public List<String> getAlias();
    
    public List<StringType> getAliasElement();
    
    public ICORAOrganization addAlias(String param);
    
    public ICORAOrganization addAliasElement(StringType param);  
    
    public String getAliasFirstRep();
    
    public ICORAOrganization setAlias(List<String> param);
    
    public ICORAOrganization setAliasElement(List<StringType> param);
    
    public Reference addEndpoint();
    
    public Reference getEndpointFirstRep();
    
    public List<Reference> getEndpoint();
    
    public ICORAOrganization addEndpoint(Reference param);
    
    public ICORAOrganization setEndpoint(List<Reference> param);
}
