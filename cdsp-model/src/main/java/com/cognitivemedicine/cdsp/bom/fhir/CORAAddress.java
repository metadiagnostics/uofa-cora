/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Address.AddressType;
import org.hl7.fhir.dstu3.model.Address.AddressUse;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.StringType;

import com.cognitivemedicine.cdsp.model.annotation.FHIRExtension;



public class CORAAddress {

	private Address adaptedClass = null;

	public CORAAddress() {
		this.adaptedClass = new Address();
	}

	public CORAAddress(Address adaptee) {
		this.adaptedClass = adaptee;
	}

	public Address getAdaptee() {
		return adaptedClass;
	}

	public void setAdaptee(Address param) {
		this.adaptedClass = param;
	}

	@FHIRExtension(name="iso21090-preferred",url="http://hl7.org/fhir/StructureDefinition/iso21090-preferred")
	public BooleanType getPreferred() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/iso21090-preferred");
		if (extensions == null || extensions.size() <= 0) {
			return null;
		} else if (extensions.size() == 1) {
			return (BooleanType) extensions.get(0).getValue();
		} else {
			throw new RuntimeException("More than one extension exists for preferred");
		}
	}

    @FHIRExtension(name="iso21090-preferred",url="http://hl7.org/fhir/StructureDefinition/iso21090-preferred")
	public CORAAddress setPreferred(BooleanType param) {
		adaptedClass.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/iso21090-preferred").setValue(param);
		return this;
	}

	public List<StringType> getLine() {
		return adaptedClass.getLine();
	}

	public CORAAddress setLine(List<StringType> param) {
		adaptedClass.setLine(param);
		return this;
	}

	public CORAAddress addLineElement(StringType param) {
		adaptedClass.addLine(param.asStringValue());
		return this;
	}

	public CORAAddress addLine(String param) {
		adaptedClass.addLine(param);
		return this;
	}

	public StringType addLineElement() {
	    return adaptedClass.addLineElement();
		
	}

	public StringType getDistrictElement() {
		return adaptedClass.getDistrictElement();
	}

	public String getDistrict() {
		return adaptedClass.getDistrict();
	}

	public CORAAddress setDistrict(String param) {
		adaptedClass.setDistrict(param);
		return this;
	}

	public CORAAddress setDistrictElement(StringType param) {
		adaptedClass.setDistrictElement(param);
		return this;
	}

	public StringType getStateElement() {
		return adaptedClass.getStateElement();
	}

	public String getState() {
		return adaptedClass.getState();
	}

	public CORAAddress setState(String param) {
		adaptedClass.setState(param);
		return this;
	}

	public CORAAddress setStateElement(StringType param) {
		adaptedClass.setStateElement(param);
		return this;
	}

	public StringType getCityElement() {
		return adaptedClass.getCityElement();
	}

	public String getCity() {
		return adaptedClass.getCity();
	}

	public CORAAddress setCity(String param) {
		adaptedClass.setCity(param);
		return this;
	}

	public CORAAddress setCityElement(StringType param) {
		adaptedClass.setCityElement(param);
		return this;
	}

	public Period getPeriod() {
		return adaptedClass.getPeriod();
	}

	public CORAAddress setPeriod(Period param) {
		adaptedClass.setPeriod(param);
		return this;
	}

	public StringType getCountryElement() {
		return adaptedClass.getCountryElement();
	}

	public String getCountry() {
		return adaptedClass.getCountry();
	}

	public CORAAddress setCountry(String param) {
		adaptedClass.setCountry(param);
		return this;
	}

	public CORAAddress setCountry(StringType param) {
		adaptedClass.setCountryElement(param);
		return this;
	}

	public AddressType getType() {
		return adaptedClass.getType();
	}

	public CORAAddress setType(String param) {
		adaptedClass.setType(AddressType.valueOf(param));
		return this;
	}

	public CORAAddress setType(AddressType param) {
		adaptedClass.setType(param);
		return this;
	}

	public Enumeration<AddressType> getTypeElement() {
		return adaptedClass.getTypeElement();
	}

	public CORAAddress setType(Enumeration<AddressType> param) {
		adaptedClass.setTypeElement(param);
		return this;
	}

	public AddressUse getUse() {
		return adaptedClass.getUse();
	}

	public CORAAddress setUse(String param) {
		adaptedClass.setUse(AddressUse.valueOf(param));
		return this;
	}

	public CORAAddress setUse(AddressUse param) {
		adaptedClass.setUse(param);
		return this;
	}

	public Enumeration<AddressUse> getUseElement() {
		return adaptedClass.getUseElement();
	}

	public CORAAddress setUseElement(Enumeration<AddressUse> param) {
		adaptedClass.setUseElement(param);
		return this;
	}

	public StringType getPostalCodeElement() {
		return adaptedClass.getPostalCodeElement();
	}

	public String getPostalCode() {
		return adaptedClass.getPostalCode();
	}

	public CORAAddress setPostalCode(String param) {
		adaptedClass.setPostalCode(param);
		return this;
	}

	public CORAAddress setPostalCode(StringType param) {
		adaptedClass.setPostalCodeElement(param);
		return this;
	}

	public StringType getTextElement() {
		return adaptedClass.getTextElement();
	}

	public String getText() {
		return adaptedClass.getText();
	}

	public CORAAddress setText(String param) {
		adaptedClass.setText(param);
		return this;
	}

	public CORAAddress setTextElement(StringType param) {
		adaptedClass.setTextElement(param);
		return this;
	}
}