/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MedicationStatement;
import org.hl7.fhir.dstu3.model.MedicationStatement.MedicationStatementStatus;
import org.hl7.fhir.dstu3.model.MedicationStatement.MedicationStatementTaken;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;


public class CORAMedicationStatementAdapter
        extends CORACognitiveBaseIdentifiable<MedicationStatement, ICORAMedicationStatement>
        implements ICORAMedicationStatement {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAMedicationStatementAdapter() {
        this.adaptedClass = new MedicationStatement();
    }

    public CORAMedicationStatementAdapter(MedicationStatement adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Dosage addDosage() {
        Dosage item = new Dosage();
        adaptedClass.addDosage(item);
        return item;
    }

    @Override
    public ICORAMedicationStatement addDosage(Dosage param) {
        adaptedClass.addDosage(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAMedicationStatement addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }



    public Annotation addNote() {
        return adaptedClass.addNote();
    }

    public ICORAMedicationStatement addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    @Override
    public CodeableConcept addReasonCode() {
        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAMedicationStatement addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode(param);
        return this;
    }

    @Override
    public CodeableConcept addReasonNotTaken() {

        return adaptedClass.addReasonNotTaken();
    }

    @Override
    public ICORAMedicationStatement addReasonNotTaken(CodeableConcept param) {
        adaptedClass.addReasonNotTaken(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {

        return adaptedClass.addReasonReference();
    }

    @Override
    public ICORAMedicationStatement addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public CodeableConcept getCategory() {

        return adaptedClass.getCategory();
    }


    public Reference getContext() {
        return adaptedClass.getContext();

    }

    @Override
    public Date getDateAsserted() {
        return adaptedClass.getDateAsserted();
    }

    @Override
    public DateTimeType getDateAssertedElement() {
        return adaptedClass.getDateAssertedElement();
    }

    @Override
    public List<Dosage> getDosage() {
        return adaptedClass.getDosage();
    }


    @Override
    public Dosage getDosageFirstRep() {
        return adaptedClass.getDosageFirstRep();
    }

    @Override
    public Date getEffectiveDateTime() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getEffective()).getValue();
        } else {
            return null;
        }
    }


    @Override
    public DateTimeType getEffectiveDateTimeElement() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public Period getEffectivePeriod() {
        if (adaptedClass.getEffective() != null && adaptedClass.getEffective() instanceof Period) {
            return (Period) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public CodeableConcept getMedicationCodeableConcept() {
        if (adaptedClass.getMedication() != null
                && adaptedClass.getMedication() instanceof CodeableConcept) {
            return (CodeableConcept) adaptedClass.getMedication();
        } else {
            return null;
        }
    }

    @Override
    public Reference getMedicationReference() {
        if (adaptedClass.getMedication() != null
                && adaptedClass.getMedication() instanceof Reference) {
            return (Reference) adaptedClass.getMedication();
        } else {
            return null;
        }
    }

    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getSubject().getResource() instanceof Patient) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<CodeableConcept> getReasonCode() {

        return adaptedClass.getReasonCode();
    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {

        return adaptedClass.getReasonCodeFirstRep();
    }


    @Override
    public List<CodeableConcept> getReasonNotTaken() {
        return adaptedClass.getReasonNotTaken();
    }

    @Override
    public CodeableConcept getReasonNotTakenFirstRep() {
        return adaptedClass.getReasonNotTakenFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {

        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstRep() {

        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public MedicationStatementStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<MedicationStatementStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public MedicationStatementTaken getTaken() {
        return adaptedClass.getTaken();
    }

    @Override
    public ICORAMedicationStatement setCategory(CodeableConcept param) {
        adaptedClass.setCategory(param);
        return this;
    }

    public ICORAMedicationStatement setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setDateAsserted(Date param) {
        adaptedClass.setDateAsserted(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setDateAssertedElement(DateTimeType param) {
        adaptedClass.setDateAssertedElement(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setDosage(List<Dosage> param) {
        adaptedClass.setDosage(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setEffectiveDateTime(Date param) {
        adaptedClass.setEffective(new DateTimeType(param));
        return this;
    }

    @Override
    public ICORAMedicationStatement setEffectiveDateTime(DateTimeType param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setEffectivePeriod(Period param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setMedicationCodeableConcept(CodeableConcept param) {
        adaptedClass.setMedication(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setMedicationReference(Reference param) {
        adaptedClass.setMedication(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getSubject().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedicationStatement setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setReasonNotTaken(List<CodeableConcept> param) {
        adaptedClass.setReasonNotTaken(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setStatus(MedicationStatementStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setStatus(String param) {
        adaptedClass.setStatus(MedicationStatementStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAMedicationStatement setStatusElement(Enumeration<MedicationStatementStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    public ICORAMedicationStatement setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setTaken(MedicationStatementTaken param) {
        adaptedClass.setTaken(param);
        return this;
    }

    @Override
    public ICORAMedicationStatement setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }


}
