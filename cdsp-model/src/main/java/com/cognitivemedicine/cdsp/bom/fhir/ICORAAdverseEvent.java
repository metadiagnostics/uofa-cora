/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Basic;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.StringType;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;

@LogicalModelAddition("Extends our base")
public interface ICORAAdverseEvent extends ICORACognitiveBaseIdentifiable<ICORAAdverseEvent> {

	public List<CodeType> getCategory();

	public List<CORAAdverseEventCause> getCause();

	public List<StringType> getClinicalStudy();

	public CodeableConcept getCode();

	public Date getCreated();

	public DateType getCreatedElement();

	public List<BooleanType> getDidNotOccur();

	public List<DateTimeType> getDiscoveryDateTime();

	public List<Reference> getLocation();

	public List<Period> getPeriod();

	public List<Reference> getReaction();

	public List<CodeType> getSeverity();

	public CORAPatientAdapter getSubjectResource();

	public List<CodeableConcept> getType();

	public ICORAAdverseEvent setCategory(List<CodeType> param);

	public ICORAAdverseEvent setCause(List<CORAAdverseEventCause> param);

	public ICORAAdverseEvent setClinicalStudy(List<StringType> param);

	public ICORAAdverseEvent setCode(CodeableConcept param);

	public ICORAAdverseEvent setCreated(Date param);

	public ICORAAdverseEvent setCreatedElement(DateType param);

	public ICORAAdverseEvent setDidNotOccur(List<BooleanType> param);

	public ICORAAdverseEvent setDiscoveryDateTime(List<DateTimeType> param);

	public ICORAAdverseEvent setLocation(List<Reference> param);

	public ICORAAdverseEvent setPeriod(List<Period> param);

	public ICORAAdverseEvent setReaction(List<Reference> param);

	public ICORAAdverseEvent setSeverity(List<CodeType> param);

	public ICORAAdverseEvent setSubjectResource(CORAPatientAdapter param);

	public ICORAAdverseEvent setType(List<CodeableConcept> param);
}