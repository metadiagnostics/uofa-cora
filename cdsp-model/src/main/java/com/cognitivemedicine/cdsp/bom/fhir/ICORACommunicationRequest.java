/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.CommunicationRequest;
import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationPriority;
import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestRequesterComponent;
import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestStatus;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.exceptions.FHIRException;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * CommunicationRequest interface
 * 
 * @author Jerry Goodnough
 * @version  DSTU3 Verified
 */
public interface ICORACommunicationRequest
        extends ICORACognitiveBaseIdentifiable<ICORACommunicationRequest> {

    public Reference addBasedOn();

    public ICORACommunicationRequest addBasedOn(Reference param);

    public CodeableConcept addCategory();

    public ICORACommunicationRequest addCategory(CodeableConcept param);

    public Annotation addNote();

    public ICORACommunicationRequest addNote(Annotation param);

    public CommunicationRequest.CommunicationRequestPayloadComponent addPayload();

    public ICORACommunicationRequest addPayload(
            CommunicationRequest.CommunicationRequestPayloadComponent param);

    public Reference addReasonReference();

    public ICORACommunicationRequest addReasonReference(Reference param);

    public Reference addReplaces();

    public ICORACommunicationRequest addReplaces(Reference param);

    public Reference addTopic();

    public ICORACommunicationRequest addTopic(Reference param);

    public Date getAuthoredOn();


    public DateTimeType getAuthoredOnElement();

    public List<Reference> getBasedOn();

    public Reference getBasedOnFirstRep();

    public List<CodeableConcept> getCategory();

    public CodeableConcept getCategoryFirstRep();

    public Reference getContext();

    public Identifier getGroupIdentifier();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();


    public Type getOccurrence();

    public DateTimeType getOccurrenceDateTime() throws FHIRException;


    public Period getOccurrencePeriod() throws FHIRException;

    public List<CommunicationRequest.CommunicationRequestPayloadComponent> getPayload();

    public CommunicationRequest.CommunicationRequestPayloadComponent getPayloadFirstRep();

    public CommunicationPriority getPriority();

    public Enumeration<CommunicationPriority> getPriorityElement();

    public List<Reference> getReasonReference();


    public Reference getReasonReferenceFirstReq();

    public CodeableConcept getReasonRejected();

    public List<Reference> getReplaces();

    public Reference getReplacesFirstReq();

    public Date getRequestedOn();

    public DateTimeType getRequestedOnElement();

    public CommunicationRequestRequesterComponent getRequester();

    @LogicalModelAlias("getOccurrenceDataType")
    public Date getScheduledDateTime();

    @LogicalModelAlias("getOccurrenceDataType")
    public DateTimeType getScheduledDateTimeElement();

    @LogicalModelAlias("getOccurrencePeriod")
    public Period getScheduledPeriod();

    public CommunicationRequestStatus getStatus();

    public Enumeration<CommunicationRequestStatus> getStatusElement();

    public Resource getSubjectResource();

    public List<Reference> getTopic();

    public Reference getTopicFirstReq();

    public ICORACommunicationRequest setAuthoredOn(Date param);

    public ICORACommunicationRequest setAuthoredOnElement(DateTimeType param);

    public ICORACommunicationRequest setBasedOn(List<Reference> param);

    public ICORACommunicationRequest setCategory(List<CodeableConcept> param);

    public ICORACommunicationRequest setContext(Reference param);

    public ICORACommunicationRequest setGroupIdentifier(Identifier param);

    public ICORACommunicationRequest setNote(List<Annotation> param);

    public ICORACommunicationRequest setOccurrence(Type param);

    public ICORACommunicationRequest setPayload(
            List<CommunicationRequest.CommunicationRequestPayloadComponent> param);

    public ICORACommunicationRequest setPriority(CommunicationPriority param);

    public ICORACommunicationRequest setPriorityElement(Enumeration<CommunicationPriority> param);

    public ICORACommunicationRequest setReasonReference(List<Reference> param);

    public ICORACommunicationRequest setReasonRejected(CodeableConcept param);

    public ICORACommunicationRequest setReplaces(List<Reference> param);

    @LogicalModelAlias("authoredOn")
    public ICORACommunicationRequest setRequestedOn(Date param);

    @LogicalModelAlias("authoredOnElement")
    public ICORACommunicationRequest setRequestedOnElement(DateTimeType param);

    public ICORACommunicationRequest setRequester(CommunicationRequestRequesterComponent param);

    @LogicalModelAlias("setOccurrence")
    public ICORACommunicationRequest setScheduledDateTime(Date param);

    @LogicalModelAlias("setOccurrence")
    public ICORACommunicationRequest setScheduledDateTime(DateTimeType param);

    @LogicalModelAlias("setOccurrence")
    public ICORACommunicationRequest setScheduledPeriod(Period param);

    public ICORACommunicationRequest setStatus(CommunicationRequestStatus param);

    public ICORACommunicationRequest setStatusElement(
            Enumeration<CommunicationRequestStatus> param);

    public ICORACommunicationRequest setSubjectResource(Resource param);


    public ICORACommunicationRequest setTopic(List<Reference> param);

}
