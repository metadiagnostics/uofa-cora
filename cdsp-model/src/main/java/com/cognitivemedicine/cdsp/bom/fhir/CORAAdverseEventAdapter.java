/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Basic;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;



public class CORAAdverseEventAdapter extends CORACognitiveBaseIdentifiable<Basic, ICORAAdverseEvent>
		implements ICORAAdverseEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CORAAdverseEventAdapter() {
		this.adaptedClass = new Basic();
	}

	public CORAAdverseEventAdapter(Basic adaptee) {
		this.adaptedClass = adaptee;
	}

	@Override
    public Identifier addIdentifier() {
		Identifier item = new Identifier();
		adaptedClass.addIdentifier(item);
		return item;
	}

	@Override
    public ICORAAdverseEvent addIdentifier(Identifier param) {
		adaptedClass.addIdentifier(param);
		return this;
	}

	@Override
    public List<CodeType> getCategory() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-category");
		List<CodeType> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((CodeType) extension.getValue());
		}
		return returnList;
	}

	@Override
    public List<CORAAdverseEventCause> getCause() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-cause");
		List<CORAAdverseEventCause> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			CORAAdverseEventCause udt = new CORAAdverseEventCause();
			udt.setRootObjectExtension(extension);
			returnList.add(udt);
		}
		return returnList;
	}

	@Override
    public List<StringType> getClinicalStudy() {
		List<Extension> extensions = adaptedClass.getExtensionsByUrl(
				"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-clinicalStudy");
		List<StringType> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((StringType) extension.getValue());
		}
		return returnList;
	}

	@Override
    public CodeableConcept getCode() {
		return adaptedClass.getCode();
	}



	@Override
    public Date getCreated() {
		return adaptedClass.getCreated();
	}

	@Override
    public DateType getCreatedElement() {
		return adaptedClass.getCreatedElement();
	}

	@Override
    public List<BooleanType> getDidNotOccur() {
		List<Extension> extensions = adaptedClass.getExtensionsByUrl(
				"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-didNotOccur");
		List<BooleanType> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((BooleanType) extension.getValue());
		}
		return returnList;
	}

	@Override
    public List<DateTimeType> getDiscoveryDateTime() {
		List<Extension> extensions = adaptedClass.getExtensionsByUrl(
				"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-discoveryDateTime");
		List<DateTimeType> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((DateTimeType) extension.getValue());
		}
		return returnList;
	}

	@Override
    public List<Identifier> getIdentifier() {
		return adaptedClass.getIdentifier();
	}

	@Override
    public Identifier getIdentifierFirstRep() {
		return adaptedClass.getIdentifierFirstRep();
	}


	@Override
    public List<Reference> getLocation() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-location");
		List<Reference> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((Reference) extension.getValue());
		}
		return returnList;
	}

	@Override
    public List<Period> getPeriod() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-period");
		List<Period> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((Period) extension.getValue());
		}
		return returnList;
	}

	@Override
    public List<Reference> getReaction() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-reaction");
		List<Reference> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((Reference) extension.getValue());
		}
		return returnList;
	}

	@Override
    public List<CodeType> getSeverity() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-severity");
		List<CodeType> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((CodeType) extension.getValue());
		}
		return returnList;
	}

	@Override
    public CORAPatientAdapter getSubjectResource() {
		if (adaptedClass.getSubject().getResource() instanceof Patient) {
			CORAPatientAdapter profiledType = new CORAPatientAdapter();
			profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
			return profiledType;
		} else {
			return null;
		}
	}

	@Override
    public Narrative getText() {
		return adaptedClass.getText();
	}

	@Override
    public List<CodeableConcept> getType() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-type");
		List<CodeableConcept> returnList = new java.util.ArrayList<>();
		for (Extension extension : extensions) {
			returnList.add((CodeableConcept) extension.getValue());
		}
		return returnList;
	}

	@Override
    public ICORAAdverseEvent setCategory(List<CodeType> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param.size(); index++) {
				adaptedClass.addExtension().setUrl(					"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-category").setValue(
						param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setCause(List<CORAAdverseEventCause> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param
					.size(); index++) {
				adaptedClass.addExtension(param.get(index).getRootObjectExtension());
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setClinicalStudy(List<StringType> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param.size(); index++) {
				adaptedClass.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-clinicalStudy").setValue(param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setCode(CodeableConcept param) {
		adaptedClass.setCode(param);
		return this;
	}

	@Override
    public ICORAAdverseEvent setCreated(Date param) {
		adaptedClass.setCreated(param);
		return this;
	}

	@Override
    public ICORAAdverseEvent setCreatedElement(DateType param) {
		adaptedClass.setCreatedElement(param);
		return this;
	}

	@Override
    public ICORAAdverseEvent setDidNotOccur(List<BooleanType> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-didNotOccur").setValue(
						param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setDiscoveryDateTime(List<DateTimeType> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-discoveryDateTime").setValue(
						param.get(index));
			}
		}
		return this;
	}

	public ICORAAdverseEvent setId(IdType param) {
		adaptedClass.setId(param);
		return this;
	}

	@Override
    public ICORAAdverseEvent setIdentifier(List<Identifier> param) {
		adaptedClass.setIdentifier(param);
		return this;
	}

	@Override
    public ICORAAdverseEvent setLocation(List<Reference> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param
					.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-location").setValue(
						param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setPeriod(List<Period> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-period").setValue(
						param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setReaction(List<Reference> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param
					.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-reaction").setValue(
						param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setSeverity(List<CodeType> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-severity").setValue(
						param.get(index));
			}
		}
		return this;
	}

	@Override
    public ICORAAdverseEvent setSubjectResource(CORAPatientAdapter param) {
		adaptedClass.getSubject().setResource(param.getAdaptee());
		return this;
	}

	@Override
    public ICORAAdverseEvent setText(Narrative param) {
		adaptedClass.setText(param);
		return this;
	}

	@Override
    public ICORAAdverseEvent setType(List<CodeableConcept> param) {
		if (param != null && param.size() > 0) {
			for (int index = 0; index < param
					.size(); index++) {
				adaptedClass.addExtension().setUrl(
						"http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-type").setValue(
						param.get(index));
			}
		}
		return this;
	}

    @Override
    public String toString() {
        return "CORAAdverseEventAdapter [getCategory()=" + getCategory() + ", getCause()="
                + getCause() + ", getClinicalStudy()=" + getClinicalStudy() + ", getCode()="
                + getCode() + ", getCreated()=" + getCreated() + ", getDidNotOccur()="
                + getDidNotOccur() + ", getDiscoveryDateTime()=" + getDiscoveryDateTime()
                + ", getIdentifier()=" + getIdentifier() + ", getLocation()=" + getLocation()
                + ", getPeriod()=" + getPeriod() + ", getReaction()=" + getReaction()
                + ", getSeverity()=" + getSeverity() + ", getSubjectResource()="
                + getSubjectResource() + ", getText()=" + getText() + ", getType()=" + getType()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + ", getId()=" + getId() + "]";
    }
	
	
}