/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Location;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;



public interface ICORALocation extends ICORACognitiveBaseIdentifiable<ICORALocation> {

    public ICORALocation addAlias(String param);

    public StringType addAliasElement();

    public ContactPoint addTelecom();

    public ICORALocation addTelecom(ContactPoint param);

    public Address getAddress();

    public List<StringType> getAlias();

    public StringType getDescriptionElement();

    public List<Reference> getEndpoint();

    public Reference getEndpointFirstrep();

    public CORAOrganizationAdapter getManagingOrganizationResource();

    public Location.LocationMode getMode();

    public Enumeration<Location.LocationMode> getModeElement();

    public String getName();

    public StringType getNameElement();

    public Coding getOperationalStatus();

    public CORALocationAdapter getPartOfResource();

    public CodeableConcept getPhysicalType();

    public Location.LocationPositionComponent getPosition();

    public Location.LocationStatus getStatus();

    public Enumeration<Location.LocationStatus> getStatusElement();

    public List<ContactPoint> getTelecom();

    public ContactPoint getTelecomFirstRep();

    public CodeableConcept getType();

    public ICORALocation setAddress(Address param);

    public ICORALocation setAlias(List<StringType> param);

    public ICORALocation setDescription(String param);

    public ICORALocation setDescriptionElement(StringType param);

    public ICORALocation setEndpoint(List<Reference> param);

    public ICORALocation setManagingOrganizationResource(CORAOrganizationAdapter param);

    public ICORALocation setMode(Location.LocationMode param);

    public ICORALocation setMode(String param);

    public ICORALocation setModeElement(Enumeration<Location.LocationMode> param);

    public ICORALocation setName(String param);

    public ICORALocation setNameElement(StringType param);


    public ICORALocation setOperationalStatus(Coding param);

    public ICORALocation setPartOfResource(CORALocationAdapter param);

    public ICORALocation setPhysicalType(CodeableConcept param);

    public ICORALocation setPosition(Location.LocationPositionComponent param);

    public ICORALocation setStatus(Location.LocationStatus param);

    public ICORALocation setStatus(String param);

    public ICORALocation setStatusElement(Enumeration<Location.LocationStatus> param);

    public ICORALocation setTelecom(List<ContactPoint> param);

    public ICORALocation setType(CodeableConcept param);
}
