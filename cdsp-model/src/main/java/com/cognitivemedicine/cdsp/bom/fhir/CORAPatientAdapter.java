/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.IntegerType;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Patient.PatientCommunicationComponent;

import com.cognitivemedicine.cdsp.model.annotation.FHIRExtension;



public class CORAPatientAdapter extends CORACognitiveBaseIdentifiable<Patient, ICORAPatient>
        implements ICORAPatient {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAPatientAdapter() {
        this.adaptedClass = new Patient();
    }

    public CORAPatientAdapter(Patient adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Address addAddress() {
        Address item = new Address();
        adaptedClass.addAddress(item);
        return item;
    }

    @Override
    public ICORAPatient addAddress(Address param) {
        adaptedClass.addAddress(param);
        return this;
    }

    @Override
    public Patient.PatientCommunicationComponent addCommunication() {
        Patient.PatientCommunicationComponent item = new Patient.PatientCommunicationComponent();
        adaptedClass.addCommunication(item);
        return item;
    }

    @Override
    public ICORAPatient addCommunication(PatientCommunicationComponent param) {
        adaptedClass.addCommunication(param);
        return this;
    }

    @Override
    public Patient.ContactComponent addContact() {
        Patient.ContactComponent item = new Patient.ContactComponent();
        adaptedClass.addContact(item);
        return item;
    }

    @Override
    public ICORAPatient addContact(Patient.ContactComponent param) {
        adaptedClass.addContact(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAPatient addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Patient.PatientLinkComponent addLink() {
        Patient.PatientLinkComponent item = new Patient.PatientLinkComponent();
        adaptedClass.addLink(item);
        return item;
    }

    @Override
    public ICORAPatient addLink(Patient.PatientLinkComponent param) {
        adaptedClass.addLink(param);
        return this;
    }

    @Override
    public HumanName addName() {
        HumanName item = new HumanName();
        adaptedClass.addName(item);
        return item;
    }

    @Override
    public ICORAPatient addName(HumanName param) {
        adaptedClass.addName(param);
        return this;
    }

    @Override
    public Attachment addPhoto() {
        Attachment item = new Attachment();
        adaptedClass.addPhoto(item);
        return item;
    }

    @Override
    public ICORAPatient addPhoto(Attachment param) {
        adaptedClass.addPhoto(param);
        return this;
    }

    @Override
    public ContactPoint addTelecom() {
        ContactPoint item = new ContactPoint();
        adaptedClass.addTelecom(item);
        return item;
    }

    @Override
    public ICORAPatient addTelecom(ContactPoint param) {
        adaptedClass.addTelecom(param);
        return this;
    }

    @Override
    public CORAAddress addWrappedAddress() {
        Address item = new Address();
        adaptedClass.addAddress(item);
        return new com.cognitivemedicine.cdsp.bom.fhir.CORAAddress(item);
    }

    @Override
    public ICORAPatient addWrappedAddress(CORAAddress param) {
        if (param != null) {
            adaptedClass.addAddress(param.getAdaptee());
        }
        return this;
    }

    @Override
    public CORAContactPoint addWrappedTelecom() {
        ContactPoint item = new ContactPoint();
        adaptedClass.addTelecom(item);
        return new com.cognitivemedicine.cdsp.bom.fhir.CORAContactPoint(item);
    }

    @Override
    public ICORAPatient addWrappedTelecom(CORAContactPoint param) {
        if (param != null) {
            adaptedClass.addTelecom(param.getAdaptee());
        }
        return this;
    }

    @Override
    public boolean getActive() {
        return adaptedClass.getActive();
    }

    @Override
    public BooleanType getActiveElement() {
        return adaptedClass.getActiveElement();
    }

    @Override
    public List<Address> getAddress() {
        return adaptedClass.getAddress();
    }

    @Override
    public Address getAddressFirstRep() {
        return adaptedClass.getAddressFirstRep();
    }

    @Override
    public Patient.AnimalComponent getAnimal() {
        return adaptedClass.getAnimal();
    }

    @Override
    public Date getBirthDate() {
        return adaptedClass.getBirthDate();
    }

    @Override
    public DateType getBirthDateElement() {
        return adaptedClass.getBirthDateElement();
    }
    @FHIRExtension(name="birthPlace",url="http://hl7.org/fhir/StructureDefinition/birthPlace")
    @Override
    public Address getBirthPlace() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/birthPlace");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Address) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for birthPlace");
        }
    }
    @FHIRExtension(name="patient-birthTime",url="http://hl7.org/fhir/StructureDefinition/patient-birthTime")
    @Override
    public DateTimeType getBirthTime() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-birthTime");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (DateTimeType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for birthTime");
        }
    }

    @Override
    public BooleanType getCadavericDonor() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/patient-cadavericDonor");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (BooleanType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for cadavericDonor");
        }
    }

    @Override
    public List<CORAPatientClinicalTrial> getClinicalTrial() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial");
        List<com.cognitivemedicine.cdsp.bom.fhir.CORAPatientClinicalTrial> returnList =
                new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAPatientClinicalTrial udt =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAPatientClinicalTrial();
            udt.setRootObjectExtension(extension);
            returnList.add(udt);
        }
        return returnList;
    }

    @Override
    public List<Patient.PatientCommunicationComponent> getCommunication() {
        return adaptedClass.getCommunication();
    }

    @Override
    public Patient.PatientCommunicationComponent getCommunicationFirstRep() {
        return adaptedClass.getCommunicationFirstRep();
    }

    @Override
    public List<Patient.ContactComponent> getContact() {
        return adaptedClass.getContact();
    }

    @Override
    public Patient.ContactComponent getContactFirstRep() {
        return adaptedClass.getContactFirstRep();
    }


    @Override
    public Boolean getDeceasedBoolean() {
        if (adaptedClass.getDeceased() != null
                && adaptedClass.getDeceased() instanceof BooleanType) {
            return ((BooleanType) adaptedClass.getDeceased()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public BooleanType getDeceasedBooleanElement() {
        if (adaptedClass.getDeceased() != null
                && adaptedClass.getDeceased() instanceof BooleanType) {
            return (BooleanType) adaptedClass.getDeceased();
        } else {
            return null;
        }
    }

    @Override
    public Date getDeceasedDateTime() {
        if (adaptedClass.getDeceased() != null
                && adaptedClass.getDeceased() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getDeceased()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateTimeType getDeceasedDateTimeElement() {
        if (adaptedClass.getDeceased() != null
                && adaptedClass.getDeceased() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getDeceased();
        } else {
            return null;
        }
    }

    @Override
    public List<CodeableConcept> getDisability() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-disability");
        List<CodeableConcept> returnList = new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            returnList.add((CodeableConcept) extension.getValue());
        }
        return returnList;
    }

    @Override
    public CodeableConcept getEthnicity() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/us-core-ethnicity");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for ethnicity");
        }
    }

    @Override
    public String getGender() {
        if (adaptedClass.getGender() == null){
            return null;
        }
        return adaptedClass.getGender().toString();
    }

    @Override
    public AdministrativeGender getGenderElement() {
        return adaptedClass.getGender();
    }



    @Override
    public List<Identifier> getIdentifier() {

        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {

        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<Patient.PatientLinkComponent> getLink() {
        return adaptedClass.getLink();
    }

    @Override
    public Patient.PatientLinkComponent getLinkFirstRep() {
        return adaptedClass.getLinkFirstRep();
    }

    @Override
    public CORAOrganizationAdapter getManagingOrganizationResource() {
        if (adaptedClass.getManagingOrganization().getResource() instanceof Organization) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAOrganizationAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAOrganizationAdapter();
            profiledType.setAdaptee(
                    (Organization) adaptedClass.getManagingOrganization().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getMaritalStatus() {
        return adaptedClass.getMaritalStatus();
    }

    @Override
    public CodeableConcept getMilitaryService() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/QICore-patient-militaryService");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for militaryService");
        }
    }

    @Override
    public Boolean getMultipleBirthBoolean() {
        if (adaptedClass.getMultipleBirth() != null
                && adaptedClass.getMultipleBirth() instanceof BooleanType) {
            return ((BooleanType) adaptedClass.getMultipleBirth()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public BooleanType getMultipleBirthBooleanElement() {
        if (adaptedClass.getMultipleBirth() != null
                && adaptedClass.getMultipleBirth() instanceof BooleanType) {
            return (BooleanType) adaptedClass.getMultipleBirth();
        } else {
            return null;
        }
    }

    @Override
    public Integer getMultipleBirthInteger() {
        if (adaptedClass.getMultipleBirth() != null
                && adaptedClass.getMultipleBirth() instanceof IntegerType) {
            return ((IntegerType) adaptedClass.getMultipleBirth()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public IntegerType getMultipleBirthIntegerElement() {
        if (adaptedClass.getMultipleBirth() != null
                && adaptedClass.getMultipleBirth() instanceof IntegerType) {
            return (IntegerType) adaptedClass.getMultipleBirth();
        } else {
            return null;
        }
    }

    @Override
    public List<HumanName> getName() {
        return adaptedClass.getName();
    }

    @Override
    public HumanName getNameFirstRep() {
        return adaptedClass.getNameFirstRep();
    }

    @Override
    public List<CORAPatientNationality> getNationality() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-nationality");
        List<com.cognitivemedicine.cdsp.bom.fhir.CORAPatientNationality> returnList =
                new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAPatientNationality udt =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAPatientNationality();
            udt.setRootObjectExtension(extension);
            returnList.add(udt);
        }
        return returnList;
    }

    @Override
    public List<Attachment> getPhoto() {
        return adaptedClass.getPhoto();
    }

    @Override
    public Attachment getPhotoFirstRep() {
        return adaptedClass.getPhotoFirstRep();
    }

    @Override
    public CodeableConcept getRace() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/us-core-race");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for race");
        }
    }

    @Override
    public CodeableConcept getReligion() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/us-core-religion");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for religion");
        }
    }

    @Override
    public List<ContactPoint> getTelecom() {
        return adaptedClass.getTelecom();
    }

    @Override
    public ContactPoint getTelecomFirstRep() {
        return adaptedClass.getTelecomFirstRep();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public List<CORAAddress> getWrappedAddress() {
        List<CORAAddress> items = new java.util.ArrayList<>();
        for (Address type : adaptedClass.getAddress()) {
            items.add(new CORAAddress(type));
        }
        return items;
    }

    @Override
    public CORAAddress getWrappedAddressFirstRep() {
        CORAAddress wrapperItem = new CORAAddress();
        Address item = adaptedClass.getAddressFirstRep();
        if (item != null) {
            wrapperItem = new CORAAddress(item);
        }
        return wrapperItem;
    }

    @Override
    public List<CORAContactPoint> getWrappedTelecom() {
        List<CORAContactPoint> items = new java.util.ArrayList<>();
        for (ContactPoint type : adaptedClass.getTelecom()) {
            items.add(new CORAContactPoint(type));
        }
        return items;
    }

    @Override
    public CORAContactPoint getWrappedTelecomFirstRep() {
        CORAContactPoint wrapperItem = new CORAContactPoint();
        ContactPoint item = adaptedClass.getTelecomFirstRep();
        if (item != null) {
            wrapperItem = new CORAContactPoint(item);
        }
        return wrapperItem;
    }

    @Override
    public ICORAPatient setActive(boolean param) {
        adaptedClass.setActive(param);
        return this;
    }

    @Override
    public ICORAPatient setActive(BooleanType param) {
        adaptedClass.setActiveElement(param);
        return this;
    }

    @Override
    public ICORAPatient setAddress(List<Address> param) {
        adaptedClass.setAddress(param);
        return this;
    }

    @Override
    public ICORAPatient setAnimal(Patient.AnimalComponent param) {
        adaptedClass.setAnimal(param);
        return this;
    }

    @Override
    public ICORAPatient setBirthDate(Date param) {
        adaptedClass.setBirthDate(param);
        return this;
    }

    @Override
    public ICORAPatient setBirthDateElement(DateType param) {
        adaptedClass.setBirthDateElement(param);
        return this;
    }
    @FHIRExtension(name="birthPlace",url="http://hl7.org/fhir/StructureDefinition/birthPlace")
    @Override
    public ICORAPatient setBirthPlace(Address param) {
        adaptedClass.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/birthPlace")
                .setValue(param);
        return this;
    }
    @FHIRExtension(name="patient-birthTime",url="http://hl7.org/fhir/StructureDefinition/patient-birthTime")
    @Override
    public ICORAPatient setBirthTime(DateTimeType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/patient-birthTime")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAPatient setCadavericDonor(BooleanType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/patient-cadavericDonor")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAPatient setClinicalTrial(List<CORAPatientClinicalTrial> param) {
        if (param != null && param.size() > 0) {
            for (int index = 0; index < param.size(); index++) {
                adaptedClass.addExtension(param.get(index).getRootObjectExtension());
            }
        }
        return this;
    }

    @Override
    public ICORAPatient setCommunication(List<Patient.PatientCommunicationComponent> param) {
        adaptedClass.setCommunication(param);
        return this;
    }

    @Override
    public ICORAPatient setContact(List<Patient.ContactComponent> param) {
        adaptedClass.setContact(param);
        return this;
    }

    @Override
    public ICORAPatient setDeceasedBoolean(Boolean param) {
        adaptedClass.setDeceased(new BooleanType(param));
        return this;
    }

    @Override
    public ICORAPatient setDeceasedBoolean(BooleanType param) {
        adaptedClass.setDeceased(param);
        return this;
    }

    @Override
    public ICORAPatient setDeceasedDateTime(Date param) {
        adaptedClass.setDeceased(new DateTimeType(param));
        return this;
    }

    @Override
    public ICORAPatient setDeceasedDateTime(DateTimeType param) {
        adaptedClass.setDeceased(param);
        return this;
    }

    @Override
    public ICORAPatient setDisability(List<CodeableConcept> param) {
        if (param != null && param.size() > 0) {
            for (int index = 0; index < param.size(); index++) {
                adaptedClass.addExtension()
                        .setUrl("http://hl7.org/fhir/StructureDefinition/patient-disability")
                        .setValue(param.get(index));
            }
        }
        return this;
    }

    @Override
    public ICORAPatient setEthnicity(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/us-core-ethnicity")
                .setValue(param);
        return this;
    }

    public ICORAPatient setGender(String param) {
        adaptedClass.setGender(AdministrativeGender.valueOf(param));
        return this;
    }

    public ICORAPatient setGenderElement(AdministrativeGender param) {
        adaptedClass.setGender(param);
        return this;
    }


    public ICORAPatient setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public ICORAPatient setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAPatient setLink(List<Patient.PatientLinkComponent> param) {
        adaptedClass.setLink(param);
        return this;
    }

    @Override
    public ICORAPatient setManagingOrganizationResource(CORAOrganizationAdapter param) {
        adaptedClass.getManagingOrganization().setResource(param.getAdaptee());
        return this;
    }

    public ICORAPatient setMaritalStatus(CodeableConcept param) {
        adaptedClass.setMaritalStatus(param);
        return this;
    }

    @Override
    public ICORAPatient setMilitaryService(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/QICore-patient-militaryService")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAPatient setMultipleBirthBoolean(Boolean param) {
        adaptedClass.setMultipleBirth(new BooleanType(param));
        return this;
    }

    @Override
    public ICORAPatient setMultipleBirthBoolean(BooleanType param) {
        adaptedClass.setMultipleBirth(param);
        return this;
    }

    @Override
    public ICORAPatient setMultipleBirthInteger(Integer param) {
        adaptedClass.setMultipleBirth(new IntegerType(param));
        return this;
    }

    @Override
    public ICORAPatient setMultipleBirthInteger(IntegerType param) {
        adaptedClass.setMultipleBirth(param);
        return this;
    }

    @Override
    public ICORAPatient setName(List<HumanName> param) {
        adaptedClass.setName(param);
        return this;
    }

    @Override
    public ICORAPatient setNationality(List<CORAPatientNationality> param) {
        if (param != null && param.size() > 0) {
            for (int index = 0; index < param.size(); index++) {
                adaptedClass.addExtension(param.get(index).getRootObjectExtension());
            }
        }
        return this;
    }

    @Override
    public ICORAPatient setPhoto(List<Attachment> param) {
        adaptedClass.setPhoto(param);
        return this;
    }

    @Override
    public ICORAPatient setRace(CodeableConcept param) {
        adaptedClass.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/us-core-race")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAPatient setReligion(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/us-core-religion").setValue(param);
        return this;
    }

    @Override
    public ICORAPatient setTelecom(List<ContactPoint> param) {
        adaptedClass.setTelecom(param);
        return this;
    }

    @Override
    public ICORAPatient setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }


    @Override
    public ICORAPatient setWrappedAddress(List<CORAAddress> param) {
        List<Address> items = new java.util.ArrayList<>();
        for (CORAAddress item : param) {
            items.add(item.getAdaptee());
        }
        adaptedClass.setAddress(items);
        return this;
    }

    @Override
    public ICORAPatient setWrappedTelecom(List<CORAContactPoint> param) {
        List<ContactPoint> items = new java.util.ArrayList<>();
        for (CORAContactPoint item : param) {
            items.add(item.getAdaptee());
        }
        adaptedClass.setTelecom(items);
        return this;
    }



}
