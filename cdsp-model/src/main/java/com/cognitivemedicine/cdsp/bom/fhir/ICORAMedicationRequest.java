/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.MedicationRequest.MedicationRequestIntent;
import org.hl7.fhir.dstu3.model.MedicationRequest.MedicationRequestRequesterComponent;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * DSTU Medication request - Replace the concept of an order
 * 
 * @author Jerry Goodnough
 *
 */

public interface ICORAMedicationRequest
        extends ICORACognitiveBaseIdentifiable<ICORAMedicationRequest> {

    public Reference addBasedOn();

    public ICORAMedicationRequest addBasedOn(Reference param);

    public Reference addDefinition();

    public ICORAMedicationRequest addDefinition(Reference param);

    public Reference addDetectedIssue();

    public ICORAMedicationRequest addDetectedIssue(Reference param);

    public Dosage addDosageInstruction();

    public ICORAMedicationRequest addDosageInstruction(Dosage param);

    public Reference addEventHistory();

    public ICORAMedicationRequest addEventHistory(Reference param);

    public Annotation addNote();

    public ICORAMedicationRequest addNote(Annotation param);

    public CodeableConcept addReasonCode();

    public ICORAMedicationRequest addReasonCode(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAMedicationRequest addReasonReference(Reference param);

    public Reference addSupportingInformation();

    public ICORAMedicationRequest addSupportingInformation(Reference param);

    public CORADosage addWrappedDosageInstruction();

    public ICORAMedicationRequest addWrappedDosageInstruction(CORADosage param);


    public Date getAuthoredOn();

    public DateTimeType getAuthoredOnElement();

    public List<Reference> getBasedOn();

    public Reference getBasedOnFirstRep();

    public Reference getContext();

    public List<Reference> getDefinition();

    public Reference getDefinitionFirstRep();

    public List<Reference> getDetectedIssue();

    public Reference getDetectedIssueFirstRep();


    public MedicationRequest.MedicationRequestDispenseRequestComponent getDispenseRequest();

    public List<Dosage> getDosageInstruction();

    public Dosage getDosageInstructionFirstRep();

    @LogicalModelAlias("getContext")
    public CORAEncounterAdapter getEncounterResource();

    public List<Reference> getEventHistory();

    public Reference getEventHistoryFirstRep();

    public Identifier getGroupIdentifier();

    public MedicationRequestIntent getIntent();

    public CodeableConcept getMedicationCodeableConcept();

    public Reference getMedicationReference();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    @LogicalModelAlias("getSubject")
    public CORAPatientAdapter getPatientResource();

    public MedicationRequest getPriorPrescriptionResource();

    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<Reference> getReasonReference();

    public Reference getReasonReferenceFirstRep();

    public MedicationRequestRequesterComponent getRequester();

    public MedicationRequest.MedicationRequestStatus getStatus();

    public Enumeration<MedicationRequest.MedicationRequestStatus> getStatusElement();



    public Reference getSubject();

    public MedicationRequest.MedicationRequestSubstitutionComponent getSubstitution();

    public List<Reference> getSupportingInformation();

    public Reference getSupportingInformationFirstRep();

    public List<CORADosage> getWrappedDosageInstruction();

    public CORADosage getWrappedDosageInstructionFirstRep();

    public ICORAMedicationRequest setAuthoredOn(Date param);

    public ICORAMedicationRequest setAuthoredOnElement(DateTimeType param);

    public ICORAMedicationRequest setBasedOn(List<Reference> param);

    public ICORAMedicationRequest setContext(Reference param);

    public ICORAMedicationRequest setDefinition(List<Reference> param);

    public ICORAMedicationRequest setDetectedIssue(List<Reference> param);

    public ICORAMedicationRequest setDispenseRequest(
            MedicationRequest.MedicationRequestDispenseRequestComponent param);

    public ICORAMedicationRequest setDosageInstruction(List<Dosage> param);

    @LogicalModelAlias("setContext")
    public ICORAMedicationRequest setEncounterResource(CORAEncounterAdapter param);

    public ICORAMedicationRequest setEventHistory(List<Reference> param);

    public ICORAMedicationRequest setGroupIdentifier(Identifier param);

    public ICORAMedicationRequest setIntent(MedicationRequestIntent param);

    public ICORAMedicationRequest setMedicationCodeableConcept(CodeableConcept param);

    public ICORAMedicationRequest setMedicationReference(Reference param);

    public ICORAMedicationRequest setNote(List<Annotation> param);

    @LogicalModelAlias("setSubject")
    public ICORAMedicationRequest setPatientResource(CORAPatientAdapter param);

    public ICORAMedicationRequest setPriorPrescriptionResource(MedicationRequest param);

    public ICORAMedicationRequest setReasonCode(List<CodeableConcept> param);

    public ICORAMedicationRequest setReasonReference(List<Reference> param);

    public ICORAMedicationRequest setRequester(MedicationRequestRequesterComponent param);

    public ICORAMedicationRequest setStatus(MedicationRequest.MedicationRequestStatus param);

    public ICORAMedicationRequest setStatus(String param);

    public ICORAMedicationRequest setStatusElement(
            Enumeration<MedicationRequest.MedicationRequestStatus> param);

    public ICORAMedicationRequest setSubject(Reference param);

    public ICORAMedicationRequest setSubstitution(
            MedicationRequest.MedicationRequestSubstitutionComponent param);

    public ICORAMedicationRequest setSupportingInformation(List<Reference> param);

    public ICORAMedicationRequest setWrappedDosageInstruction(List<CORADosage> param);

}
