/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.MedicationAdministration.MedicationAdministrationPerformerComponent;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;



public interface ICORAMedicationAdministration
        extends ICORACognitiveBaseIdentifiable<ICORAMedicationAdministration> {

    public Reference addDefinition();

    public ICORAMedicationAdministration addDefinition(Reference param);

    public Reference addEventHistory();

    public ICORAMedicationAdministration addEventHistory(Reference param);

    public Annotation addNote();

    public ICORAMedicationAdministration addNote(Annotation param);

    public Reference addPartOf();

    public ICORAMedicationAdministration addPartOf(Reference param);


    public MedicationAdministrationPerformerComponent addPerformer();

    public ICORAMedicationAdministration addPerformer(
            MedicationAdministrationPerformerComponent param);

    public CodeableConcept addReasonCode();

    public ICORAMedicationAdministration addReasonCode(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAMedicationAdministration addReasonReference(Reference param);

    public Reference addSupportingInformation();

    public ICORAMedicationAdministration addSupportingInformation(Reference param);

    public CodeableConcept getCategory();

    public Reference getContext();

    public List<Reference> getDefinition();

    public Reference getDefinitionFirstRep();

    public MedicationAdministration.MedicationAdministrationDosageComponent getDosage();

    public Type getEffective();


    @LogicalModelAlias("getEffective")
    public Date getEffectiveTimeDateTime();

    @LogicalModelAlias("getEffective")
    public DateTimeType getEffectiveTimeDateTimeElement();

    @LogicalModelAlias("getEffective")
    public Period getEffectiveTimePeriod();

    public CORAEncounterAdapter getEncounterResource();

    public List<Reference> getEventHistory();

    public Reference getEventHistoryFirstRep();

    public CodeableConcept getMedicationCodeableConcept();

    public Reference getMedicationReference();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public List<Reference> getPartOf();

    public Reference getPartOfFirstRep();

    public CORAPatientAdapter getPatientResource();

    public List<MedicationAdministrationPerformerComponent> getPerformer();

    public MedicationAdministrationPerformerComponent getPerformerFirstRep();

    public CORAMedicationRequestAdapter getPrescriptionResource();


    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<Reference> getReasonReference();

    public Reference getReasonReferenceFirstReq();

    public MedicationAdministration.MedicationAdministrationStatus getStatus();

    public Enumeration<MedicationAdministration.MedicationAdministrationStatus> getStatusElement();

    public Reference getSubject();

    public List<Reference> getSupportingInformation();

    public Reference getSupportingInformationFirstRep();

    public Boolean getNotGiven();

    public BooleanType getNotGivenElement();

    public ICORAMedicationAdministration setCategory(CodeableConcept param);

    public ICORAMedicationAdministration setContext(Reference param);

    public ICORAMedicationAdministration setDefinition(List<Reference> param);


    public ICORAMedicationAdministration setDosage(
            MedicationAdministration.MedicationAdministrationDosageComponent param);

    public ICORAMedicationAdministration setEffective(Type param);

    @LogicalModelAlias("setEffective")
    public ICORAMedicationAdministration setEffectiveTimeDateTime(Date param);

    @LogicalModelAlias("setEffective")
    public ICORAMedicationAdministration setEffectiveTimeDateTime(DateTimeType param);

    @LogicalModelAlias("setEffective")
    public ICORAMedicationAdministration setEffectiveTimePeriod(Period param);


    public ICORAMedicationAdministration setEncounterResource(CORAEncounterAdapter param);

    public ICORAMedicationAdministration setEventHistory(List<Reference> param);

    public ICORAMedicationAdministration setMedicationCodeableConcept(CodeableConcept param);

    public ICORAMedicationAdministration setMedicationReference(Reference param);

    public ICORAMedicationAdministration setNote(List<Annotation> param);

    public ICORAMedicationAdministration setPartOf(List<Reference> param);

    public ICORAMedicationAdministration setPatientResource(CORAPatientAdapter param);

    public ICORAMedicationAdministration setPerformer(
            List<MedicationAdministrationPerformerComponent> param);

    public ICORAMedicationAdministration setPrescriptionResource(
            CORAMedicationRequestAdapter param);

    public ICORAMedicationAdministration setReasonCode(List<CodeableConcept> param);

    public ICORAMedicationAdministration setReasonReference(List<Reference> param);


    public ICORAMedicationAdministration setStatus(
            MedicationAdministration.MedicationAdministrationStatus param);

    public ICORAMedicationAdministration setStatus(String param);

    public ICORAMedicationAdministration setStatusElement(
            Enumeration<MedicationAdministration.MedicationAdministrationStatus> param);

    public ICORAMedicationAdministration setSubject(Reference param);

    public ICORAMedicationAdministration setSupportingInformation(List<Reference> param);

    public ICORAMedicationAdministration setNotGiven(boolean param);

    public ICORAMedicationAdministration setNotGivenElement(BooleanType param);
}
