/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Procedure;
import org.hl7.fhir.dstu3.model.Procedure.ProcedureStatus;
import org.hl7.fhir.dstu3.model.Reference;

/**
 * DSTU3/HAPI2.4/BOM Procedure
 *
 * @author Jerry Goodnough
 *
 */

public interface ICORAProcedure extends ICORACognitiveBaseIdentifiable<ICORAProcedure> {

    public Reference addBasedOn();

    public ICORAProcedure addBasedOn(Reference param);

    public CodeableConcept addComplication();

    public ICORAProcedure addComplication(CodeableConcept param);

    public Reference addComplicationDetail();

    public ICORAProcedure addComplicationDetail(Reference param);

    public Reference addDefinition();

    public ICORAProcedure addDefinition(Reference param);

    public Procedure.ProcedureFocalDeviceComponent addFocalDevice();

    public ICORAProcedure addFocalDevice(Procedure.ProcedureFocalDeviceComponent param);

    public Annotation addNote();

    public ICORAProcedure addNote(Annotation param);

    public Reference addPartOf();

    public ICORAProcedure addPartOf(Reference param);

    public Procedure.ProcedurePerformerComponent addPerformer();

    public ICORAProcedure addPerformer(Procedure.ProcedurePerformerComponent param);

    public CodeableConcept addReasonCode();

    public ICORAProcedure addReasonCode(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAProcedure addReasonReference(Reference param);

    public CodeableConcept addUsedCode();

    public ICORAProcedure addUsedCode(CodeableConcept param);

    public Reference addUsedReference();

    public ICORAProcedure addUsedReference(Reference param);

    public List<Reference> getApproachBodySite();

    public List<Reference> getBasedOn();

    public Reference getBasedOnFirstRep();

    public CodeableConcept getCategory();

    public CodeableConcept getCode();

    public List<CodeableConcept> getComplication();

    public List<Reference> getComplicationDetail();

    public Reference getComplicationDetailFirstRep();

    public CodeableConcept getComplicationFirstRep();

    public Reference getContext();

    public List<Reference> getDefinition();

    public Reference getDefinitionFirstRep();

    public CORAEncounterAdapter getEncounterResource();

    public List<Procedure.ProcedureFocalDeviceComponent> getFocalDevice();

    public Procedure.ProcedureFocalDeviceComponent getFocalDeviceFirstRep();

    public DateTimeType getIncisionDateTime();

    public Reference getLocation();

    public CORALocationAdapter getLocationResource();

    public boolean getNotDone();

    public BooleanType getNotDoneElement();

    public CodeableConcept getNotDoneReason();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public CodeableConcept getOutcome();

    public List<Reference> getPartOf();

    public Reference getPartOfFirstRep();

    public Date getPerformedDateTime();

    public DateTimeType getPerformedDateTimeElement();

    public Period getPerformedPeriod();

    public List<Procedure.ProcedurePerformerComponent> getPerformer();

    public Procedure.ProcedurePerformerComponent getPerformerFirstRep();

    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<Reference> getReasonReference();


    public Reference getReasonReferenceFirstRep();

    public ProcedureStatus getStatus();

    public Enumeration<ProcedureStatus> getStatusElement();

    public Reference getSubject();
    
    public BooleanType getElective();

    public List<CodeableConcept> getUsedCode();

    public CodeableConcept getUsedCodeFirstRep();

    public List<Reference> getUsedReference();

    public Reference getUsedReferenceFirstRep();

    public ICORAProcedure setApproachBodySite(List<Reference> param);

    public ICORAProcedure setBasedOn(List<Reference> param);

    public ICORAProcedure setCategory(CodeableConcept param);

    public ICORAProcedure setCode(CodeableConcept param);

    public ICORAProcedure setComplication(List<CodeableConcept> param);

    public ICORAProcedure setComplicationDetail(List<Reference> param);

    public ICORAProcedure setContext(Reference param);

    public ICORAProcedure setDefinition(List<Reference> param);

    public ICORAProcedure setEncounterResource(CORAEncounterAdapter param);

    public ICORAProcedure setFocalDevice(List<Procedure.ProcedureFocalDeviceComponent> param);

    public ICORAProcedure setIncisionDateTime(DateTimeType param);

    public ICORAProcedure setLocation(Reference param);

    public ICORAProcedure setLocationResource(CORALocationAdapter param);

    public ICORAProcedure setNotDone(boolean param);

    public ICORAProcedure setNotDoneElement(BooleanType param);

    public ICORAProcedure setNotDoneReason(CodeableConcept param);

    public ICORAProcedure setNote(List<Annotation> param);

    public ICORAProcedure setOutcome(CodeableConcept param);

    public ICORAProcedure setPartOf(List<Reference> param);

    public ICORAProcedure setPerformedDateTime(Date param);

    public ICORAProcedure setPerformedDateTimeElement(DateTimeType param);

    public ICORAProcedure setPerformedPeriod(Period param);

    public ICORAProcedure setPerformer(List<Procedure.ProcedurePerformerComponent> param);

    public ICORAProcedure setReasonCode(List<CodeableConcept> param);

    public ICORAProcedure setReasonReference(List<Reference> param);

    public ICORAProcedure setStatus(ProcedureStatus param);

    public ICORAProcedure setStatus(String param);

    public ICORAProcedure setStatusElement(Enumeration<ProcedureStatus> param);

    public ICORAProcedure setSubject(Reference param);

    public ICORAProcedure setUsedCode(List<CodeableConcept> param);

    public ICORAProcedure setUsedReference(List<Reference> param);

    public ICORAProcedure setElective(BooleanType isElective);
    
    public ICORAProcedure setElective(boolean isElective);
}
