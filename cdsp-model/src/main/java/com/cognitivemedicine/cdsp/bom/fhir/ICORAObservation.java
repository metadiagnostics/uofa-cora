/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.InstantType;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Observation.ObservationStatus;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Ratio;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.SampledData;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.TimeType;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;

/**
 * DSTU3 FHIR and BOM
 * @author Jerry Goodnough
 * 
 * Missing elements:
 * 
 *      Device
 *
 */

public interface ICORAObservation extends ICORACognitiveBaseIdentifiable<ICORAObservation> {

    
    public Reference addBasedOn(); 
    
    public ICORAObservation addBasedOn(Reference param);  
    
    public CodeableConcept addCategory();
    
    
	public ICORAObservation addCategory(CodeableConcept param);

	public Observation.ObservationComponentComponent addComponent();

	public ICORAObservation addComponent(Observation.ObservationComponentComponent param);

	public Observation.ObservationReferenceRangeComponent addReferenceRange();

	public ICORAObservation addReferenceRange(Observation.ObservationReferenceRangeComponent param);

	public Observation.ObservationRelatedComponent addRelated();

	public ICORAObservation addRelated(Observation.ObservationRelatedComponent param);

	public List<Reference> getBasedOn();

	public Reference getBasedOnFirstRep();

	public CodeableConcept getBodyPosition();

	public CodeableConcept getBodySite();

	public List<CodeableConcept> getCategory();

	public CodeableConcept getCategoryFirstRep();

	public CodeableConcept getCode();

	public String getComment();

	public StringType getCommentElement();

	public List<Observation.ObservationComponentComponent> getComponent();

	public Observation.ObservationComponentComponent getComponentFirstRep();

	public Reference getContext();

	public CodeableConcept getDataAbsentReason();

	public Date getEffectiveDateTime();

	public DateTimeType getEffectiveDateTimeElement();

	public Period getEffectivePeriod();

	@LogicalModelAlias("getContext")
	public CORAEncounterAdapter getEncounterResource();

	@LogicalModelAddition("To be added to IQCore")
	public Identifier getFirstIdentiferBySystem(String System);

	public CodeableConcept getInterpretation();

	public Date getIssued();

	public InstantType getIssuedElement();

	public CodeableConcept getMethod();

	@LogicalModelAddition("To be added to IQCore")
	public Identifier getOrderIdentifier();

	@LogicalModelAddition("To be added to IQCore")
	public Coding getPrimaryCoding();

	public List<Observation.ObservationReferenceRangeComponent> getReferenceRange();

	public Observation.ObservationReferenceRangeComponent getReferenceRangeFirstRep();

	public List<Observation.ObservationRelatedComponent> getRelated();

	public Observation.ObservationRelatedComponent getRelatedFirstRep();

	public CORASpecimenAdapter getSpecimenResource();

	public ObservationStatus getStatus();

	public Enumeration<Observation.ObservationStatus> getStatusElement();

	public Reference getSubject();

	public Type getValue();

	public Attachment getValueAttachment();
	public boolean getValueBoolean();
	
	public BooleanType getValueBooleanElement();
	
	public CodeableConcept getValueCodeableConcept();
	
	public Date getValueDateTime();
	
	public DateTimeType getValueDateTimeElement();
	
	public Period getValuePeriod();

	public Quantity getValueQuantity();

	public Range getValueRange();

	public Ratio getValueRatio();

	public SampledData getValueSampledData();

	public String getValueString();

	public StringType getValueStringElement();

	public String getValueTime();

	public TimeType getValueTimeElement();

	public ICORAObservation setBasedOn(List<Reference> param);

	public ICORAObservation setBodyPosition(CodeableConcept param);

	public ICORAObservation setBodySite(CodeableConcept param);

	public ICORAObservation setCategory(List<CodeableConcept> param);

	public ICORAObservation setCode(CodeableConcept param);

	public ICORAObservation setComment(String param);

	public ICORAObservation setCommentElement(StringType param);

	public ICORAObservation setComponent(List<Observation.ObservationComponentComponent> param);

	public ICORAObservation setContext(Reference param);

	public ICORAObservation setDataAbsentReason(CodeableConcept param);

	public ICORAObservation setEffectiveDateTime(Date param);

	public ICORAObservation setEffectiveDateTimeElement(DateTimeType param);

	public ICORAObservation setEffectivePeriod(Period param);

	@LogicalModelAlias("setContext")
	public ICORAObservation setEncounterResource(CORAEncounterAdapter param);

	public ICORAObservation setInterpretation(CodeableConcept param);

	public ICORAObservation setIssued(Date param);

	public ICORAObservation setIssued(InstantType param);

	public ICORAObservation setMethod(CodeableConcept param);

	@LogicalModelAddition("To be added to IQCore")
	public ICORAObservation setOrderIdentifer(Identifier orderId);

	@LogicalModelAddition("To be added to IQCore")
	public ICORAObservation setPrimaryCoding(Coding param);

	public ICORAObservation setReferenceRange(List<Observation.ObservationReferenceRangeComponent> param);

	public ICORAObservation setRelated(List<Observation.ObservationRelatedComponent> param);

	public ICORAObservation setSpecimenResource(CORASpecimenAdapter param);

	public ICORAObservation setStatus(Observation.ObservationStatus param);

	public ICORAObservation setStatus(String param);

	@LogicalModelChange("Change set status to setStatusElement")
	public ICORAObservation setStatusElement(Enumeration<Observation.ObservationStatus> param);

	public ICORAObservation setSubject(Reference param);

	public ICORAObservation setValue(Type param);

	public ICORAObservation setValueAttachment(Attachment param);

	public ICORAObservation setValueBoolean(boolean param);

	public ICORAObservation setValueBooleanElement(BooleanType param);

	public ICORAObservation setValueCodeableConcept(CodeableConcept param);

	public ICORAObservation setValueDateTime(Date param);

	public ICORAObservation setValueDateTime(DateTimeType param);
	
    public ICORAObservation setValuePeriod(Period param);
    
    public ICORAObservation setValueQuantity(Quantity param);

    public ICORAObservation setValueRange(Range param);
    
    public ICORAObservation setValueRatio(Ratio param);
    
    public ICORAObservation setValueSampledData(SampledData param);
    
    public ICORAObservation setValueString(String param);
    
    public ICORAObservation setValueString(StringType param);
    
    public ICORAObservation setValueTime(String param);
    
    public ICORAObservation setValueTime(TimeType param);

}