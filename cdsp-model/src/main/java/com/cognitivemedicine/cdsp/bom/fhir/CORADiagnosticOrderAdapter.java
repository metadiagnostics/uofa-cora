/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import org.hl7.fhir.dstu3.model.ProcedureRequest;



public class CORADiagnosticOrderAdapter extends CORAProcedureRequestAdapter {


	/**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORADiagnosticOrderAdapter() {
		super();
		adaptedClass.setIntent(ProcedureRequest.ProcedureRequestIntent.ORDER);
	}

	public CORADiagnosticOrderAdapter(ProcedureRequest adaptee) {
		this.adaptedClass = adaptee;
        adaptedClass.setIntent(ProcedureRequest.ProcedureRequestIntent.ORDER);
	}



}