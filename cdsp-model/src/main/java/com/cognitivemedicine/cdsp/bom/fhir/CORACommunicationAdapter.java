/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Communication;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Reference;

/**
 * Adapted for HAPI Communication
 * 
 * @author Jerry Goodnough
 * @version DSTU3 Verified
 */

public class CORACommunicationAdapter
        extends CORACognitiveBaseIdentifiable<Communication, ICORACommunication>
        implements ICORACommunication {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORACommunicationAdapter() {
        this.adaptedClass = new Communication();
    }

    public CORACommunicationAdapter(Communication adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {
        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORACommunication addBasedOn(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }

    @Override
    public CodeableConcept addCategory() {
        return adaptedClass.addCategory();
    }

    @Override
    public ICORACommunication addCategory(CodeableConcept param) {
        adaptedClass.addCategory(param);
        return this;
    }

    @Override
    public Reference addDefinition() {
        return adaptedClass.addDefinition();
    }

    @Override
    public ICORACommunication addDefinition(Reference param) {
        adaptedClass.addDefinition(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORACommunication addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public CodeableConcept addMedium() {
        return adaptedClass.addMedium();
    }

    @Override
    public ICORACommunication addMedium(CodeableConcept param) {
        adaptedClass.addMedium(param);
        return this;
    }

    @Override
    public Annotation addNote() {
        return adaptedClass.addNote();
    }

    @Override
    public ICORACommunication addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    @Override
    public Reference addPartOf() {
        return adaptedClass.addPartOf();
    }

    @Override
    public ICORACommunication addPartOf(Reference param) {
        adaptedClass.addPartOf(param);
        return this;
    }

    @Override
    public Communication.CommunicationPayloadComponent addPayload() {
        Communication.CommunicationPayloadComponent item =
                new Communication.CommunicationPayloadComponent();
        adaptedClass.addPayload(item);
        return item;
    }

    @Override
    public ICORACommunication addPayload(Communication.CommunicationPayloadComponent param) {
        adaptedClass.addPayload(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {
        return adaptedClass.addReasonReference();
    }

    @Override
    public ICORACommunication addReasonReference(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }

    @Override
    public Reference addTopic() {
        return adaptedClass.addTopic();
    }

    @Override
    public ICORACommunication addTopic(Reference param) {
        adaptedClass.addTopic(param);
        return this;
    }

    @Override
    public List<Reference> getBasedOn() {
        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {
        return adaptedClass.getBasedOnFirstRep();
    }

    @Override
    public List<CodeableConcept> getCategory() {
        return adaptedClass.getCategory();
    }

    @Override
    public CodeableConcept getCategoryFirstRep() {
        return adaptedClass.getCategoryFirstRep();
    }

    @Override
    public Reference getContext() {
        return adaptedClass.getContext();
    }

    @Override
    public List<Reference> getDefinition() {
        return adaptedClass.getDefinition();
    }


    @Override
    public Reference getDefinitionFirstReq() {
        return adaptedClass.getDefinitionFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }


    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<CodeableConcept> getMedium() {
        return adaptedClass.getMedium();
    }

    @Override
    public CodeableConcept getMediumFirstReq() {
        return adaptedClass.getMediumFirstRep();
    }

    @Override
    public boolean getNotDone() {
        return adaptedClass.getNotDone();
    }

    @Override
    public BooleanType getNotDoneElement() {
        return adaptedClass.getNotDoneElement();
    }

    @Override
    public CodeableConcept getNotDoneReason() {
        return adaptedClass.getNotDoneReason();
    }

    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public List<Reference> getPartOf() {
        return adaptedClass.getPartOf();
    }


    @Override
    public Reference getPartOfFirstReq() {
        return adaptedClass.getPartOfFirstRep();
    }

    @Override
    public List<Communication.CommunicationPayloadComponent> getPayload() {
        return adaptedClass.getPayload();
    }

    @Override
    public Communication.CommunicationPayloadComponent getPayloadFirstRep() {
        return adaptedClass.getPayloadFirstRep();
    }

    @Override
    public CodeableConcept getReasonNotPerformed() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/communication-reasonNotPerformed");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonNotPerformed");
        }
    }

    @Override
    public List<Reference> getReasonReference() {
        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstReq() {
        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public Date getReceived() {
        return adaptedClass.getReceived();
    }

    @Override
    public DateTimeType getReceivedElement() {
        return adaptedClass.getReceivedElement();
    }

    @Override
    public Date getSent() {
        return adaptedClass.getSent();
    }

    @Override
    public DateTimeType getSentElement() {
        return adaptedClass.getSentElement();
    }

    @Override
    public Communication.CommunicationStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<Communication.CommunicationStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }



    @Override
    public CORAPatientAdapter getSubjectResource() {
        if (adaptedClass.getSubject().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }


    @Override
    public List<Reference> getTopic() {
        return adaptedClass.getTopic();

    }

    @Override
    public Reference getTopicFirstReq() {
        return adaptedClass.getTopicFirstRep();
    }

    @Override
    public ICORACommunication setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORACommunication setCategory(List<CodeableConcept> param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORACommunication setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORACommunication setDefinition(List<Reference> param) {
        adaptedClass.setDefinition(param);
        return this;
    }

    public ICORACommunication setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public ICORACommunication setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORACommunication setMedium(List<CodeableConcept> param) {
        adaptedClass.setMedium(param);
        return this;
    }

    @Override
    public ICORACommunication setNotDone(boolean param) {
        adaptedClass.setNotDone(param);
        return this;
    }



    @Override
    public ICORACommunication setNotDoneElement(BooleanType param) {
        adaptedClass.setNotDoneElement(param);
        return this;
    }

    @Override
    public ICORACommunication setNotDoneReason(CodeableConcept param) {
        adaptedClass.setNotDoneReason(param);
        return this;
    }

    @Override
    public ICORACommunication setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORACommunication setPartOf(List<Reference> param) {
        adaptedClass.setPartOf(param);
        return this;
    }

    @Override
    public ICORACommunication setPayload(List<Communication.CommunicationPayloadComponent> param) {
        adaptedClass.setPayload(param);
        return this;
    }


    @Override
    public ICORACommunication setReasonNotPerformed(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/communication-reasonNotPerformed")
                .setValue(param);
        return this;
    }

    @Override
    public ICORACommunication setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORACommunication setReceived(Date param) {
        adaptedClass.setReceived(param);
        return this;
    }

    @Override
    public ICORACommunication setReceivedElement(DateTimeType param) {
        adaptedClass.setReceivedElement(param);
        return this;
    }


    @Override
    public ICORACommunication setSent(Date param) {
        adaptedClass.setSent(param);
        return this;
    }

    @Override
    public ICORACommunication setSentElement(DateTimeType param) {
        adaptedClass.setSentElement(param);
        return this;
    }


    @Override
    public ICORACommunication setStatus(Communication.CommunicationStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORACommunication setStatusElement(
            Enumeration<Communication.CommunicationStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORACommunication setSubjectResource(CORAPatientAdapter param) {
        adaptedClass.getSubject().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORACommunication setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public ICORACommunication setTopic(List<Reference> param) {
        adaptedClass.setTopic(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORACommunicationAdapter [getBasedOn()=" + getBasedOn() + ", getBasedOnFirstRep()="
                + getBasedOnFirstRep() + ", getCategory()=" + getCategory() + ", getContext()="
                + getContext() + ", getDefinition()=" + getDefinition()
                + ", getDefinitionFirstReq()=" + getDefinitionFirstReq()
                + ", getIdentifierFirstRep()=" + getIdentifierFirstRep() + ", getMedium()="
                + getMedium() + ", getNotDone()=" + getNotDone() + ", getNotDoneElement()="
                + getNotDoneElement() + ", getNotDoneReason()=" + getNotDoneReason()
                + ", getNote()=" + getNote() + ", getPartOf()=" + getPartOf() + ", getPayload()="
                + getPayload() + ", getReasonNotPerformed()=" + getReasonNotPerformed()
                + ", getReasonReference()=" + getReasonReference() + ", getReceived()="
                + getReceived() + ", getSent()=" + getSent() + ", getStatus()=" + getStatus()
                + ", getText()=" + getText() + ", getTopic()=" + getTopic()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + ", getId()=" + getId() + "]";
    }
    
}

