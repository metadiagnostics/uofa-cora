/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Immunization;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Immunization.ImmunizationPractitionerComponent;
import org.hl7.fhir.dstu3.model.Immunization.ImmunizationStatus;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

@LogicalModelAddition("Extends our base")
public interface ICORAImmunization extends ICORACognitiveBaseIdentifiable<ICORAImmunization> {

    public ImmunizationPractitionerComponent addPractitioner();

    public ICORAImmunization addPractitioner(ImmunizationPractitionerComponent param);

    public Immunization.ImmunizationReactionComponent addReaction();

    public ICORAImmunization addReaction(Immunization.ImmunizationReactionComponent param);

    public Immunization.ImmunizationVaccinationProtocolComponent addVaccinationProtocol();

    public ICORAImmunization addVaccinationProtocol(
            Immunization.ImmunizationVaccinationProtocolComponent param);

    public Date getDate();

    public DateTimeType getDateElement();

    public SimpleQuantity getDoseQuantity();

    public CORAEncounterAdapter getEncounterResource();

    public Date getExpirationDate();

    public DateType getExpirationDateElement();

    public Immunization.ImmunizationExplanationComponent getExplanation();

    public CORALocationAdapter getLocationResource();

    public String getLotNumber();


    public StringType getLotNumberElement();

    public CORAOrganizationAdapter getManufacturerResource();

    public boolean getNotGiven();


    public BooleanType getNotGivenElement();

    public CORAPatientAdapter getPatientResource();

    public List<ImmunizationPractitionerComponent> getPractitioner();

    public ImmunizationPractitionerComponent getPractitionerFirstRep();

    public boolean getPrimarySource();

    public BooleanType getPrimarySourceElement();

    public List<Immunization.ImmunizationReactionComponent> getReaction();

    public Immunization.ImmunizationReactionComponent getReactionFirstRep();

    public CodeableConcept getReportOrigin();

    public CodeableConcept getRoute();

    public CodeableConcept getSite();

    public ImmunizationStatus getStatus();

    public Enumeration<ImmunizationStatus> getStatusElement();

    public List<Immunization.ImmunizationVaccinationProtocolComponent> getVaccinationProtocol();

    public Immunization.ImmunizationVaccinationProtocolComponent getVaccinationProtocolFirstRep();

    public CodeableConcept getVaccineCode();

    public ICORAImmunization setDate(Date param);

    public ICORAImmunization setDateElement(DateTimeType param);

    public ICORAImmunization setDoseQuantity(SimpleQuantity param);

    public ICORAImmunization setEncounterResource(CORAEncounterAdapter param);


    public ICORAImmunization setExpirationDate(Date param);

    public ICORAImmunization setExpirationDateElement(DateType param);


    public ICORAImmunization setExplanation(Immunization.ImmunizationExplanationComponent param);

    public ICORAImmunization setLocationResource(CORALocationAdapter param);

    public ICORAImmunization setLotNumber(String param);

    public ICORAImmunization setLotNumberElement(StringType param);

    public ICORAImmunization setManufacturerResource(CORAOrganizationAdapter param);

    public ICORAImmunization setNotGiven(boolean param);

    public ICORAImmunization setNotGivenElement(BooleanType param);

    public ICORAImmunization setPatientResource(CORAPatientAdapter param);

    public ICORAImmunization setPractitioner(List<ImmunizationPractitionerComponent> param);

    public ICORAImmunization setPrimarySource(boolean param);

    public ICORAImmunization setPrimarySourceElement(BooleanType param);

    public ICORAImmunization setReaction(List<Immunization.ImmunizationReactionComponent> param);

    public ICORAImmunization setReportOrigin(CodeableConcept param);

    public ICORAImmunization setRoute(CodeableConcept param);

    public ICORAImmunization setSite(CodeableConcept param);

    public ICORAImmunization setStatus(ImmunizationStatus param);

    public ICORAImmunization setStatusElement(Enumeration<ImmunizationStatus> param);

    public ICORAImmunization setVaccinationProtocol(
            List<Immunization.ImmunizationVaccinationProtocolComponent> param);

    public ICORAImmunization setVaccineCode(CodeableConcept param);


}
