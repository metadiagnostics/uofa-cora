/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse;
import org.hl7.fhir.dstu3.model.QuestionnaireResponse.QuestionnaireResponseItemComponent;

public class CORAQuestionnaireResponseAdapter extends CORACognitiveBaseIdentifiable<QuestionnaireResponse, ICORAQuestionnaireResponse>
        implements ICORAQuestionnaireResponse, Serializable {

    private static final long serialVersionUID = 1L;

    public CORAQuestionnaireResponseAdapter() {
        this.adaptedClass = new org.hl7.fhir.dstu3.model.QuestionnaireResponse();
    }

    public CORAQuestionnaireResponseAdapter(QuestionnaireResponse adaptee) {
        this.adaptedClass = adaptee;
    }

    public List<QuestionnaireResponseItemComponent> getItem() {
        return this.adaptedClass.getItem();
    }

    @Override
    public Identifier addIdentifier() {
        Identifier identifier = new Identifier();
        adaptedClass.setIdentifier(identifier);
        return identifier;

    }

    @Override
    public ICORAQuestionnaireResponse addIdentifier(Identifier param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public List<Identifier> getIdentifier() {
        List<Identifier> identifiers = new ArrayList<>();
        if (adaptedClass.getIdentifier() != null) {
            identifiers.add(adaptedClass.getIdentifier());
        }
        return identifiers;
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public ICORAQuestionnaireResponse setIdentifier(List<Identifier> param) {
        throw new IllegalStateException("This class does support only one identifier");
    }


}
