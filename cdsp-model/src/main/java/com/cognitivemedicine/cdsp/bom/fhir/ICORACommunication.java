/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Communication;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;
/**
 * CommunicationInterface
 * 
 * @author Jerry Goodnough
 * @version  DSTU3 Verified
 */

public interface ICORACommunication extends ICORACognitiveBaseIdentifiable<ICORACommunication> {

    public Reference addBasedOn();

    public ICORACommunication addBasedOn(Reference param);

    public CodeableConcept addCategory();

    public ICORACommunication addCategory(CodeableConcept param);

    public Reference addDefinition();

    public ICORACommunication addDefinition(Reference param);

    public CodeableConcept addMedium();

    public ICORACommunication addMedium(CodeableConcept param);

    public Annotation addNote();

    public ICORACommunication addNote(Annotation param);

    public Reference addPartOf();

    public ICORACommunication addPartOf(Reference param);

    public Communication.CommunicationPayloadComponent addPayload();

    public ICORACommunication addPayload(Communication.CommunicationPayloadComponent param);

    public Reference addReasonReference();

    public ICORACommunication addReasonReference(Reference param);

    public Reference addTopic();

    public ICORACommunication addTopic(Reference param);

    public List<Reference> getBasedOn();

    public Reference getBasedOnFirstRep();

    public List<CodeableConcept> getCategory();

    public CodeableConcept getCategoryFirstRep();


    public Reference getContext();

    public List<Reference> getDefinition();

    public Reference getDefinitionFirstReq();

    public List<CodeableConcept> getMedium();

    public CodeableConcept getMediumFirstReq();

    public boolean getNotDone();

    public BooleanType getNotDoneElement();

    public CodeableConcept getNotDoneReason();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public List<Reference> getPartOf();

    public Reference getPartOfFirstReq();

    public List<Communication.CommunicationPayloadComponent> getPayload();

    public Communication.CommunicationPayloadComponent getPayloadFirstRep();

    public CodeableConcept getReasonNotPerformed();

    public List<Reference> getReasonReference();

    public Reference getReasonReferenceFirstReq();

    public Date getReceived();

    public DateTimeType getReceivedElement();

    public Date getSent();

    public DateTimeType getSentElement();

    public Communication.CommunicationStatus getStatus();

    public Enumeration<Communication.CommunicationStatus> getStatusElement();

    public CORAPatientAdapter getSubjectResource();

    public List<Reference> getTopic();

    public Reference getTopicFirstReq();

    public ICORACommunication setBasedOn(List<Reference> param);

    public ICORACommunication setCategory(List<CodeableConcept> param);

    public ICORACommunication setContext(Reference param);

    public ICORACommunication setDefinition(List<Reference> param);

    public ICORACommunication setMedium(List<CodeableConcept> param);

    public ICORACommunication setNotDone(boolean param);

    public ICORACommunication setNotDoneElement(BooleanType param);

    public ICORACommunication setNotDoneReason(CodeableConcept param);

    public ICORACommunication setNote(List<Annotation> param);

    public ICORACommunication setPartOf(List<Reference> param);

    public ICORACommunication setPayload(List<Communication.CommunicationPayloadComponent> param);

    public ICORACommunication setReasonNotPerformed(CodeableConcept param);

    public ICORACommunication setReasonReference(List<Reference> param);

    public ICORACommunication setReceived(Date param);

    public ICORACommunication setReceivedElement(DateTimeType param);

    public ICORACommunication setSent(Date param);

    public ICORACommunication setSentElement(DateTimeType param);

    public ICORACommunication setStatus(Communication.CommunicationStatus param);

    public ICORACommunication setStatusElement(
            Enumeration<Communication.CommunicationStatus> param);

    public ICORACommunication setSubjectResource(CORAPatientAdapter param);

    public ICORACommunication setTopic(List<Reference> param);

}
