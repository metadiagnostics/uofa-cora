/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.IntegerType;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Resource;

import com.cognitivemedicine.cdsp.model.annotation.FHIRExtension;



public interface ICORAPatient extends ICORACognitiveBaseIdentifiable<ICORAPatient> {

    public Address addAddress();

    public ICORAPatient addAddress(Address param);

    public Patient.PatientCommunicationComponent addCommunication();

    public ICORAPatient addCommunication(Patient.PatientCommunicationComponent param);

    public Patient.ContactComponent addContact();

    public ICORAPatient addContact(Patient.ContactComponent param);

    public Patient.PatientLinkComponent addLink();

    public ICORAPatient addLink(Patient.PatientLinkComponent param);

    public HumanName addName();

    public ICORAPatient addName(HumanName param);

    public Attachment addPhoto();

    public ICORAPatient addPhoto(Attachment param);

    public ContactPoint addTelecom();

    public ICORAPatient addTelecom(ContactPoint param);

    public CORAAddress addWrappedAddress();

    public ICORAPatient addWrappedAddress(CORAAddress param);

    public CORAContactPoint addWrappedTelecom();

    public ICORAPatient addWrappedTelecom(CORAContactPoint param);

    public boolean getActive();

    public BooleanType getActiveElement();
    //TODO:  Figure out why folks are using this
    public Patient getAdaptee();

    public List<Address> getAddress();

    public Address getAddressFirstRep();

    public Patient.AnimalComponent getAnimal();

    public Date getBirthDate();

    public DateType getBirthDateElement();

    @FHIRExtension(name = "birthPlace", url = "http://hl7.org/fhir/StructureDefinition/birthPlace")
    public Address getBirthPlace();

    @FHIRExtension(name = "patient-birthTime",
            url = "http://hl7.org/fhir/StructureDefinition/patient-birthTime")
    public DateTimeType getBirthTime();

    public BooleanType getCadavericDonor();

    public List<CORAPatientClinicalTrial> getClinicalTrial();

    public List<Patient.PatientCommunicationComponent> getCommunication();

    public Patient.PatientCommunicationComponent getCommunicationFirstRep();

    public List<Patient.ContactComponent> getContact();

    public Patient.ContactComponent getContactFirstRep();

    public Boolean getDeceasedBoolean();

    public BooleanType getDeceasedBooleanElement();

    public Date getDeceasedDateTime();

    public DateTimeType getDeceasedDateTimeElement();

    public List<CodeableConcept> getDisability();

    public CodeableConcept getEthnicity();

    public String getGender();

    public AdministrativeGender getGenderElement();

    public List<Patient.PatientLinkComponent> getLink();

    public Patient.PatientLinkComponent getLinkFirstRep();

    public CORAOrganizationAdapter getManagingOrganizationResource();

    public CodeableConcept getMaritalStatus();

    public CodeableConcept getMilitaryService();

    public Boolean getMultipleBirthBoolean();

    public BooleanType getMultipleBirthBooleanElement();

    public Integer getMultipleBirthInteger();

    public IntegerType getMultipleBirthIntegerElement();

    public List<HumanName> getName();

    public HumanName getNameFirstRep();

    public List<CORAPatientNationality> getNationality();

    public List<Attachment> getPhoto();

    public Attachment getPhotoFirstRep();

    public CodeableConcept getRace();

    public CodeableConcept getReligion();

    public List<ContactPoint> getTelecom();

    public ContactPoint getTelecomFirstRep();

    public List<CORAAddress> getWrappedAddress();

    public CORAAddress getWrappedAddressFirstRep();

    public List<CORAContactPoint> getWrappedTelecom();

    public CORAContactPoint getWrappedTelecomFirstRep();

    public ICORAPatient setActive(boolean param);

    public ICORAPatient setActive(BooleanType param);

    public ICORAPatient setAddress(List<Address> param);

    public ICORAPatient setAnimal(Patient.AnimalComponent param);

    public ICORAPatient setBirthDate(Date param);

    public ICORAPatient setBirthDateElement(DateType param);

    @FHIRExtension(name = "birthPlace", url = "http://hl7.org/fhir/StructureDefinition/birthPlace")
    public ICORAPatient setBirthPlace(Address param);

    @FHIRExtension(name = "patient-birthTime",
            url = "http://hl7.org/fhir/StructureDefinition/patient-birthTime")
    public ICORAPatient setBirthTime(DateTimeType param);

    public ICORAPatient setCadavericDonor(BooleanType param);

    public ICORAPatient setClinicalTrial(List<CORAPatientClinicalTrial> param);

    public ICORAPatient setCommunication(List<Patient.PatientCommunicationComponent> param);

    public ICORAPatient setContact(List<Patient.ContactComponent> param);

    @Override
    public ICORAPatient setContained(List<Resource> param);

    public ICORAPatient setDeceasedBoolean(Boolean param);

    public ICORAPatient setDeceasedBoolean(BooleanType param);

    public ICORAPatient setDeceasedDateTime(Date param);

    public ICORAPatient setDeceasedDateTime(DateTimeType param);

    public ICORAPatient setDisability(List<CodeableConcept> param);

    public ICORAPatient setEthnicity(CodeableConcept param);

    public ICORAPatient setGender(String param);

    public ICORAPatient setGenderElement(AdministrativeGender param);

    public ICORAPatient setLink(List<Patient.PatientLinkComponent> param);

    public ICORAPatient setManagingOrganizationResource(CORAOrganizationAdapter param);

    public ICORAPatient setMaritalStatus(CodeableConcept param);

    public ICORAPatient setMilitaryService(CodeableConcept param);

    public ICORAPatient setMultipleBirthBoolean(Boolean param);

    public ICORAPatient setMultipleBirthBoolean(BooleanType param);

    public ICORAPatient setMultipleBirthInteger(Integer param);

    public ICORAPatient setMultipleBirthInteger(IntegerType param);

    public ICORAPatient setName(List<HumanName> param);

    public ICORAPatient setNationality(List<CORAPatientNationality> param);

    public ICORAPatient setPhoto(List<Attachment> param);

    public ICORAPatient setRace(CodeableConcept param);

    public ICORAPatient setReligion(CodeableConcept param);

    public ICORAPatient setTelecom(List<ContactPoint> param);

    public ICORAPatient setWrappedAddress(List<CORAAddress> param);

    public ICORAPatient setWrappedTelecom(List<CORAContactPoint> param);
}
