/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Appointment;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Duration;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * DSTU3 / QICore / BOM Encounter
 * 
 * @author Jerry Goodnough
 *
 *
 * Missing Elements:
 * 
 *    ClassHistory
 *    EpisodeOfCare
 *    IncommingReferral
 *    Appointment
 *    PartOf
 */

@LogicalModelAddition("Extends our base")
public interface ICORAEncounter extends ICORACognitiveBaseIdentifiable<ICORAEncounter> {
    
    public Encounter.DiagnosisComponent addDiagnosis();
    public Encounter.DiagnosisComponent getDiagnosisFirstRep();
    public List<Encounter.DiagnosisComponent> getDiagnosis();
    public ICORAEncounter addDiagnosis(Encounter.DiagnosisComponent param);
    public ICORAEncounter setDiagnosis(List<Encounter.DiagnosisComponent> param);
    
    public CodeableConcept addReason();
    public CodeableConcept getReasonFirstRep();
    public List<CodeableConcept> getReason();
    public ICORAEncounter addReason(CodeableConcept param);
    public ICORAEncounter setReason(List<CodeableConcept> param);
        
    public Reference getSubject();
    public ICORAEncounter setSubject(Reference param);

    public CodeableConcept addType();
    public CodeableConcept getTypeFirstRep();
    public List<CodeableConcept> getType();
    public ICORAEncounter addType(CodeableConcept param);
    public ICORAEncounter setType(List<CodeableConcept> param);

    public Encounter.EncounterLocationComponent addLocation();

    public ICORAEncounter addLocation(Encounter.EncounterLocationComponent param);

    public Encounter.EncounterParticipantComponent addParticipant();

    public ICORAEncounter addParticipant(Encounter.EncounterParticipantComponent param);

    public Encounter.StatusHistoryComponent addStatusHistory();

    public ICORAEncounter addStatusHistory(Encounter.StatusHistoryComponent param);

    public Appointment getAppointmentResource();

    public Coding getClass_();

    @LogicalModelAlias("getClass_")
    public Coding getClassElement();


    public Encounter.EncounterHospitalizationComponent getHospitalization();

    public Duration getLength();

    public List<Encounter.EncounterLocationComponent> getLocation();

    public Encounter.EncounterLocationComponent getLocationFirstRep();

    public List<Encounter.EncounterParticipantComponent> getParticipant();

    public Encounter.EncounterParticipantComponent getParticipantFirstRep();

    public CORAEncounterAdapter getPartOfResource();

    public CORAPatientAdapter getPatientResource();

    public Period getPeriod();

    public CodeableConcept getPriority();

    public CodeableConcept getReasonCancelled();

    public List<CORAEncounterRelatedCondition> getRelatedCondition();

    public CORAOrganizationAdapter getServiceProviderResource();

    public Encounter.EncounterStatus getStatus();

    public Enumeration<Encounter.EncounterStatus> getStatusElement();

    public List<Encounter.StatusHistoryComponent> getStatusHistory();

    public Encounter.StatusHistoryComponent getStatusHistoryFirstRep();

    public ICORAEncounter setAppointmentResource(Appointment param);

    public ICORAEncounter setClass_(Coding param);

    @LogicalModelAlias("setClass_")
    public ICORAEncounter setClassElement(Coding param);

    public ICORAEncounter setHospitalization(Encounter.EncounterHospitalizationComponent param);

    public ICORAEncounter setLength(Duration param);

    public ICORAEncounter setLocation(List<Encounter.EncounterLocationComponent> param);

    public ICORAEncounter setParticipant(List<Encounter.EncounterParticipantComponent> param);

    public ICORAEncounter setPartOfResource(CORAEncounterAdapter param);

    public ICORAEncounter setPatientResource(CORAPatientAdapter param);

    public ICORAEncounter setPeriod(Period param);

    public ICORAEncounter setPriority(CodeableConcept param);

    public ICORAEncounter setReasonCancelled(CodeableConcept param);

    public ICORAEncounter setRelatedCondition(List<CORAEncounterRelatedCondition> param);

    public ICORAEncounter setServiceProviderResource(CORAOrganizationAdapter param);

    public ICORAEncounter setStatus(Encounter.EncounterStatus param);

    public ICORAEncounter setStatusElement(Enumeration<Encounter.EncounterStatus> param);

    public ICORAEncounter setStatusHistory(List<Encounter.StatusHistoryComponent> param);
}
