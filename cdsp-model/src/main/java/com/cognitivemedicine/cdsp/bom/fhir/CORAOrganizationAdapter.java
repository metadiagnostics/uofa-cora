/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;

/**
 * DSTU3/HAPI 2.4/BOM Organization
 * 
 * @author Jerry Goodnough
 *
 */

public class CORAOrganizationAdapter
        extends CORACognitiveBaseIdentifiable<Organization, ICORAOrganization>
        implements ICORAOrganization {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAOrganizationAdapter() {
        this.adaptedClass = new Organization();
    }

    public CORAOrganizationAdapter(Organization adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Address addAddress() {
        Address item = new Address();
        adaptedClass.addAddress(item);
        return item;
    }

    @Override
    public ICORAOrganization addAddress(Address param) {
        adaptedClass.addAddress(param);
        return this;
    }

    @Override
    public ICORAOrganization addAlias(String param) {
        adaptedClass.addAlias(param);
        return this;
    }

    @Override
    public ICORAOrganization addAliasElement(StringType param) {
        adaptedClass.addAlias(param.asStringValue());
        return this;
    }

    @Override
    public Organization.OrganizationContactComponent addContact() {

        return adaptedClass.addContact();

    }

    @Override
    public ICORAOrganization addContact(Organization.OrganizationContactComponent param) {
        adaptedClass.addContact(param);
        return this;
    }

    @Override
    public Reference addEndpoint() {

        return adaptedClass.addEndpoint();
    }

    @Override
    public ICORAOrganization addEndpoint(Reference param) {
        adaptedClass.addEndpoint(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAOrganization addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public ContactPoint addTelecom() {
        ContactPoint item = new ContactPoint();
        adaptedClass.addTelecom(item);
        return item;
    }

    @Override
    public ICORAOrganization addTelecom(ContactPoint param) {
        adaptedClass.addTelecom(param);
        return this;
    }

    @Override
    public Boolean getActive() {
        return adaptedClass.getActive();
    }

    @Override
    public BooleanType getActiveElement() {
        return adaptedClass.getActiveElement();
    }


    @Override
    public List<Address> getAddress() {
        return adaptedClass.getAddress();
    }

    @Override
    public Address getAddressFirstRep() {
        return adaptedClass.getAddressFirstRep();
    }

    @Override
    public List<String> getAlias() {
        List<StringType> aliases = adaptedClass.getAlias();
        ArrayList<String> out = new ArrayList<>(aliases.size());
        for (StringType str : aliases) {
            out.add(str.asStringValue());
        }
        return out;
    }

    @Override
    public List<StringType> getAliasElement() {

        return adaptedClass.getAlias();
    }

    @Override
    public String getAliasFirstRep() {

        StringType s = adaptedClass.getAlias().get(0);
        if (s != null) {
            return s.asStringValue();
        } else {
            return null;
        }
    }

    @Override
    public List<Organization.OrganizationContactComponent> getContact() {
        return adaptedClass.getContact();
    }

    @Override
    public Organization.OrganizationContactComponent getContactFirstRep() {
        return adaptedClass.getContactFirstRep();
    }

    @Override
    public List<Reference> getEndpoint() {

        return adaptedClass.getEndpoint();
    }

    @Override
    public Reference getEndpointFirstRep() {

        return adaptedClass.getEndpointFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public String getName() {
        return adaptedClass.getName();
    }

    @Override
    public StringType getNameElement() {
        return adaptedClass.getNameElement();
    }


    @Override
    public CORAOrganizationAdapter getPartOfResource() {
        if (adaptedClass.getPartOf().getResource() instanceof Organization) {
            CORAOrganizationAdapter profiledType = new CORAOrganizationAdapter();
            profiledType.setAdaptee((Organization) adaptedClass.getPartOf().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<ContactPoint> getTelecom() {
        return adaptedClass.getTelecom();
    }


    @Override
    public ContactPoint getTelecomFirstRep() {
        return adaptedClass.getTelecomFirstRep();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public List<CodeableConcept> getType() {
        return adaptedClass.getType();
    }

    @Override
    public ICORAOrganization setActive(Boolean param) {
        adaptedClass.setActive(param);
        return this;
    }

    @Override
    public ICORAOrganization setActiveElement(BooleanType param) {
        adaptedClass.setActiveElement(param);
        return this;
    }

    @Override
    public ICORAOrganization setAddress(List<Address> param) {
        adaptedClass.setAddress(param);
        return this;
    }

    @Override
    public ICORAOrganization setAlias(List<String> param) {
        ArrayList<StringType> sa = new ArrayList<>(param.size());
        for (String s : param) {
            sa.add(new StringType(s));
        }
        return this;
    }

    @Override
    public ICORAOrganization setAliasElement(List<StringType> param) {
        adaptedClass.setAlias(param);
        return this;
    }

    @Override
    public ICORAOrganization setContact(List<Organization.OrganizationContactComponent> param) {
        adaptedClass.setContact(param);
        return this;
    }

    @Override
    public ICORAOrganization setEndpoint(List<Reference> param) {
        adaptedClass.setEndpoint(param);
        return this;
    }

    public ICORAOrganization setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public ICORAOrganization setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAOrganization setName(String param) {
        adaptedClass.setName(param);
        return this;
    }

    @Override
    public ICORAOrganization setNameElement(StringType param) {
        adaptedClass.setNameElement(param);
        return this;
    }

    @Override
    public ICORAOrganization setPartOfResource(CORAOrganizationAdapter param) {
        adaptedClass.getPartOf().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAOrganization setTelecom(List<ContactPoint> param) {
        adaptedClass.setTelecom(param);
        return this;
    }

    @Override
    public ICORAOrganization setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public ICORAOrganization setType(List<CodeableConcept> param) {
        adaptedClass.setType(param);
        return this;
    }

    @Override
    public String toString() {
        final int maxLen = 10;
        return "CORAOrganizationAdapter [getActive()=" + getActive() + ", getAddress()="
                + (getAddress() != null
                        ? getAddress().subList(0, Math.min(getAddress().size(), maxLen)) : null)
                + ", getAlias()="
                + (getAlias() != null ? getAlias().subList(0, Math.min(getAlias().size(), maxLen))
                        : null)
                + ", getContact()="
                + (getContact() != null
                        ? getContact().subList(0, Math.min(getContact().size(), maxLen)) : null)
                + ", getEndpoint()="
                + (getEndpoint() != null
                        ? getEndpoint().subList(0, Math.min(getEndpoint().size(), maxLen)) : null)
                + ", getIdentifier()="
                + (getIdentifier() != null
                        ? getIdentifier().subList(0, Math.min(getIdentifier().size(), maxLen))
                        : null)
                + ", getName()=" + getName() + ", getTelecom()="
                + (getTelecom() != null
                        ? getTelecom().subList(0, Math.min(getTelecom().size(), maxLen)) : null)
                + ", getText()=" + getText() + ", getType()="
                + (getType() != null ? getType().subList(0, Math.min(getType().size(), maxLen))
                        : null)
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }
    
}
