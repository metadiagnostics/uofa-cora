/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Location;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;


public class CORALocationAdapter extends CORACognitiveBaseIdentifiable<Location, ICORALocation>
        implements ICORALocation {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORALocationAdapter() {
        this.adaptedClass = new Location();
    }

    public CORALocationAdapter(Location adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public ICORALocation addAlias(String param) {
        adaptedClass.addAlias(param);
        return this;
    }

    @Override
    public StringType addAliasElement() {
        return adaptedClass.addAliasElement();
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORALocation addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public ContactPoint addTelecom() {
        ContactPoint item = new ContactPoint();
        adaptedClass.addTelecom(item);
        return item;
    }


    @Override
    public ICORALocation addTelecom(ContactPoint param) {
        adaptedClass.addTelecom(param);
        return this;
    }

    @Override
    public Address getAddress() {
        return adaptedClass.getAddress();
    }


    @Override
    public List<StringType> getAlias() {
        return adaptedClass.getAlias();
    }

    public String getDescription() {
        return adaptedClass.getDescription();
    }

    @Override
    public StringType getDescriptionElement() {
        return adaptedClass.getDescriptionElement();
    }

    @Override
    public List<Reference> getEndpoint() {
        return adaptedClass.getEndpoint();
    }

    @Override
    public Reference getEndpointFirstrep() {
        return adaptedClass.getEndpointFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public CORAOrganizationAdapter getManagingOrganizationResource() {
        if (adaptedClass.getManagingOrganization().getResource() instanceof Organization) {
            CORAOrganizationAdapter profiledType = new CORAOrganizationAdapter();
            profiledType.setAdaptee(
                    (Organization) adaptedClass.getManagingOrganization().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Location.LocationMode getMode() {
        return adaptedClass.getMode();
    }

    @Override
    public Enumeration<Location.LocationMode> getModeElement() {
        return adaptedClass.getModeElement();
    }

    @Override
    public String getName() {
        return adaptedClass.getName();
    }

    @Override
    public StringType getNameElement() {
        return adaptedClass.getNameElement();
    }

    @Override
    public Coding getOperationalStatus() {
        return adaptedClass.getOperationalStatus();
    }

    @Override
    public CORALocationAdapter getPartOfResource() {
        if (adaptedClass.getPartOf().getResource() instanceof Location) {
            CORALocationAdapter profiledType = new CORALocationAdapter();
            profiledType.setAdaptee((Location) adaptedClass.getPartOf().getResource());
            return profiledType;
        } else {
            return null;
        }
    }


    @Override
    public CodeableConcept getPhysicalType() {
        return adaptedClass.getPhysicalType();
    }

    @Override
    public Location.LocationPositionComponent getPosition() {
        return adaptedClass.getPosition();
    }

    @Override
    public Location.LocationStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<Location.LocationStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public List<ContactPoint> getTelecom() {
        return adaptedClass.getTelecom();
    }

    @Override
    public ContactPoint getTelecomFirstRep() {
        return adaptedClass.getTelecomFirstRep();
    }

    @Override
    public CodeableConcept getType() {
        return adaptedClass.getType();
    }

    @Override
    public ICORALocation setAddress(Address param) {
        adaptedClass.setAddress(param);
        return this;
    }

    @Override
    public ICORALocation setAlias(List<StringType> param) {
        adaptedClass.setAlias(param);
        return this;
    }

    @Override
    public ICORALocation setDescription(String param) {
        adaptedClass.setDescription(param);
        return this;
    }

    @Override
    public ICORALocation setDescriptionElement(StringType param) {
        adaptedClass.setDescriptionElement(param);
        return this;
    }

    @Override
    public ICORALocation setEndpoint(List<Reference> param) {
        adaptedClass.setEndpoint(param);
        return this;
    }

    public ICORALocation setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public ICORALocation setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORALocation setManagingOrganizationResource(CORAOrganizationAdapter param) {
        adaptedClass.getManagingOrganization().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORALocation setMode(Location.LocationMode param) {
        adaptedClass.setMode(param);
        return this;
    }

    @Override
    public ICORALocation setMode(String param) {
        adaptedClass.setMode(Location.LocationMode.valueOf(param));
        return this;
    }

    @Override
    public ICORALocation setModeElement(Enumeration<Location.LocationMode> param) {
        adaptedClass.setModeElement(param);
        return this;
    }

    @Override
    public ICORALocation setName(String param) {
        adaptedClass.setName(param);
        return this;
    }

    @Override
    public ICORALocation setNameElement(StringType param) {
        adaptedClass.setNameElement(param);
        return this;
    }


    @Override
    public ICORALocation setOperationalStatus(Coding param) {
        adaptedClass.setOperationalStatus(param);
        return this;
    }

    @Override
    public ICORALocation setPartOfResource(CORALocationAdapter param) {
        adaptedClass.getPartOf().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORALocation setPhysicalType(CodeableConcept param) {
        adaptedClass.setPhysicalType(param);
        return this;
    }

    @Override
    public ICORALocation setPosition(Location.LocationPositionComponent param) {
        adaptedClass.setPosition(param);
        return this;
    }

    @Override
    public ICORALocation setStatus(Location.LocationStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORALocation setStatus(String param) {
        adaptedClass.setStatus(Location.LocationStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORALocation setStatusElement(Enumeration<Location.LocationStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORALocation setTelecom(List<ContactPoint> param) {
        adaptedClass.setTelecom(param);
        return this;
    }

    @Override
    public ICORALocation setType(CodeableConcept param) {
        adaptedClass.setType(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORALocationAdapter [getDescription()=" + getDescription() + ", getEndpoint()="
                + getEndpoint() + ", getIdentifier()=" + getIdentifier()
                + ", getManagingOrganizationResource()=" + getManagingOrganizationResource()
                + ", getMode()=" + getMode() + ", getName()=" + getName()
                + ", getOperationalStatus()=" + getOperationalStatus() + ", getPartOfResource()="
                + getPartOfResource() + ", getPhysicalType()=" + getPhysicalType()
                + ", getPosition()=" + getPosition() + ", getStatus()=" + getStatus()
                + ", getTelecom()=" + getTelecom() + ", getType()=" + getType()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }
    
    
}
