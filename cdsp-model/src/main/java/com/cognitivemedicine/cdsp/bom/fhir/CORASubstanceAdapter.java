/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Substance;
import org.hl7.fhir.dstu3.model.Substance.FHIRSubstanceStatus;


public class CORASubstanceAdapter extends CORACognitiveBaseIdentifiable<Substance, ICORASubstance>
        implements ICORASubstance {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CORASubstanceAdapter() {
        this.adaptedClass = new Substance();
    }

    public CORASubstanceAdapter(Substance adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORASubstance addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Substance.SubstanceIngredientComponent addIngredient() {
        return adaptedClass.addIngredient();
    }

    @Override
    public ICORASubstance addIngredient(Substance.SubstanceIngredientComponent param) {
        adaptedClass.addIngredient(param);
        return this;
    }

    @Override
    public Substance.SubstanceInstanceComponent addInstance() {

        return adaptedClass.addInstance();
    }

    @Override
    public ICORASubstance addInstance(Substance.SubstanceInstanceComponent param) {
        adaptedClass.addInstance(param);
        return this;
    }

    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }


    @Override
    public String getDescription() {
        return adaptedClass.getDescription();
    }

    @Override
    public StringType getDescriptionElement() {
        return adaptedClass.getDescriptionElement();
    }



    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<Substance.SubstanceIngredientComponent> getIngredient() {
        return adaptedClass.getIngredient();
    }

    @Override
    public Substance.SubstanceIngredientComponent getIngredientFirstRep() {
        return adaptedClass.getIngredientFirstRep();
    }

    @Override
    public List<Substance.SubstanceInstanceComponent> getInstance() {
        return adaptedClass.getInstance();
    }

    @Override
    public Substance.SubstanceInstanceComponent getInstanceFirstRep() {
        return adaptedClass.getInstanceFirstRep();
    }


    @Override
    public FHIRSubstanceStatus getStatus() {
        return adaptedClass.getStatus();
    }



    @Override
    public ICORASubstance setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    @Override
    public ICORASubstance setDescription(String param) {
        adaptedClass.setDescription(param);
        return this;
    }


    @Override
    public ICORASubstance setDescriptionElement(StringType param) {
        adaptedClass.setDescriptionElement(param);
        return this;
    }

    @Override
    public ICORASubstance setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORASubstance setIngredient(List<Substance.SubstanceIngredientComponent> param) {
        adaptedClass.setIngredient(param);
        return this;
    }

    @Override
    public ICORASubstance setInstance(List<Substance.SubstanceInstanceComponent> param) {
        adaptedClass.setInstance(param);
        return this;
    }

    @Override
    public ICORASubstance setStatus(FHIRSubstanceStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }
}
