/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Goal;
import org.hl7.fhir.dstu3.model.Goal.GoalStatus;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Reference;


public class CORAGoalAdapter extends CORACognitiveBaseIdentifiable<Goal, ICORAGoal>
        implements ICORAGoal {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAGoalAdapter() {
        this.adaptedClass = new Goal();
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAGoal addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }



    @Override
    public CodeableConcept addOutcomeCode() {
        return adaptedClass.addOutcomeCode();
    }



    @Override
    public ICORAGoal addOutcomeCode(CodeableConcept param) {
        adaptedClass.addOutcomeCode(param);
        return this;
    }

    @Override
    public Reference addOutcomeReference() {
        return adaptedClass.addOutcomeReference();
    }

    @Override
    public ICORAGoal addOutcomeReference(Reference param) {
        adaptedClass.addOutcomeReference(param);
        return this;
    }

    @Override
    public CodeableConcept getDescription() {
        return adaptedClass.getDescription();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<CodeableConcept> getOutcomeCode() {
        return adaptedClass.getOutcomeCode();
    }

    @Override
    public CodeableConcept getOutcomeCodeFirstRep() {
        return adaptedClass.getOutcomeCodeFirstRep();
    }

    @Override
    public List<Reference> getOutcomeReference() {
        return adaptedClass.getOutcomeReference();
    }

    @Override
    public Reference getOutcomeReferenceFirstRep() {
        return adaptedClass.getOutcomeReferenceFirstRep();
    }

    @Override
    public CodeableConcept getPriority() {
        return adaptedClass.getPriority();
    }

    @Override
    public CodeableConcept getReasonRejected() {
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/goal-reasonRejected");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonRejected");
        }
    }


    @Override
    public CodeableConcept getStartCodeableConcept() {
        if (adaptedClass.getStart() != null && adaptedClass.getStart() instanceof CodeableConcept) {
            return (CodeableConcept) adaptedClass.getStart();
        } else {
            return null;
        }
    }



    @Override
    public Date getStartDate() {
        if (adaptedClass.getStart() != null && adaptedClass.getStart() instanceof DateType) {
            return ((DateType) adaptedClass.getStart()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateType getStartDateElement() {
        if (adaptedClass.getStart() != null && adaptedClass.getStart() instanceof DateType) {
            return (DateType) adaptedClass.getStart();
        } else {
            return null;
        }
    }

    @Override
    public GoalStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Date getStatusDate() {
        return adaptedClass.getStatusDate();
    }

    @Override
    public DateType getStatusDateElement() {
        return adaptedClass.getStatusDateElement();
    }

    @Override
    public Enumeration<Goal.GoalStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public String getStatusReason() {
        return adaptedClass.getStatusReason();
    }

    @Override
    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public Goal.GoalTargetComponent getTarget() {
        return adaptedClass.getTarget();
    }

    @Override
    public ICORAGoal setDescription(CodeableConcept param) {
        adaptedClass.setDescription(param);

        return this;
    }

    @Override
    public ICORAGoal setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAGoal setOutcomeCode(List<CodeableConcept> param) {
        adaptedClass.setOutcomeCode(param);
        return this;
    }

    @Override
    public ICORAGoal setOutcomeReference(List<Reference> param) {
        adaptedClass.setOutcomeReference(param);
        return this;
    }

    @Override
    public ICORAGoal setPriority(CodeableConcept param) {
        adaptedClass.setPriority(param);
        return this;
    }

    @Override
    public ICORAGoal setReasonRejected(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/goal-reasonRejected")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAGoal setStartCodeableConcept(CodeableConcept param) {
        adaptedClass.setStart(param);
        return this;
    }

    @Override
    public ICORAGoal setStartDate(Date param) {
        adaptedClass.setStart(new DateType(param));
        return this;
    }

    @Override
    public ICORAGoal setStartDate(DateType param) {
        adaptedClass.setStart(param);
        return this;
    }

    @Override
    public ICORAGoal setStatus(Goal.GoalStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAGoal setStatus(String param) {
        adaptedClass.setStatus(Goal.GoalStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAGoal setStatusDate(Date param) {
        adaptedClass.setStatusDate(param);
        return this;
    }

    @Override
    public ICORAGoal setStatusDateElement(DateType param) {
        adaptedClass.setStatusDateElement(param);
        return this;
    }

    @Override
    public ICORAGoal setStatusElement(Enumeration<Goal.GoalStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAGoal setStatusReason(String param) {
        adaptedClass.setStatusReason(param);
        return this;
    }

    @Override
    public ICORAGoal setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAGoal setTarget(Goal.GoalTargetComponent param) {
        adaptedClass.setTarget(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAGoalAdapter [getOutcomeCode()=" + getOutcomeCode() + ", getOutcomeReference()="
                + getOutcomeReference() + ", getPriority()=" + getPriority()
                + ", getReasonRejected()=" + getReasonRejected() + ", getStartCodeableConcept()="
                + getStartCodeableConcept() + ", getStartDate()=" + getStartDate()
                + ", getStatus()=" + getStatus() + ", getStatusDate()=" + getStatusDate()
                + ", getStatusReason()=" + getStatusReason() + ", getSubject()=" + getSubject()
                + ", getTarget()=" + getTarget() + ", getPrimaryIdentifer()="
                + getPrimaryIdentifer() + ", getCORAContext()=" + getCORAContext() + "]";
    }

}
