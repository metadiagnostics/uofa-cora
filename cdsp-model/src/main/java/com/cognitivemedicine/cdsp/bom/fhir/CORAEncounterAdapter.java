/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Appointment;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Duration;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;
/**
 * DSTU3 Encounter BOM adapter
 * @author Jerry Goodnough
 *
 * 
 * Missing Elements:
 * 
 *    ClassHistory
 *    EpisodeOfCare
 *    IncommingReferral
 *    Appointment
 *    PartOf
 */

public class CORAEncounterAdapter extends CORACognitiveBaseIdentifiable<Encounter, ICORAEncounter>
        implements ICORAEncounter {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAEncounterAdapter() {
        this.adaptedClass = new Encounter();
    }

    public CORAEncounterAdapter(Encounter adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Encounter.DiagnosisComponent addDiagnosis() {
        return adaptedClass.addDiagnosis();
    }

    @Override
    public ICORAEncounter addDiagnosis(Encounter.DiagnosisComponent param) {
        adaptedClass.addDiagnosis(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAEncounter addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Encounter.EncounterLocationComponent addLocation() {
        return adaptedClass.addLocation();

    }

    @Override
    public ICORAEncounter addLocation(Encounter.EncounterLocationComponent param) {
        adaptedClass.addLocation(param);
        return this;
    }

    @Override
    public Encounter.EncounterParticipantComponent addParticipant() {
        return adaptedClass.addParticipant();

    }

    @Override
    public ICORAEncounter addParticipant(Encounter.EncounterParticipantComponent param) {
        adaptedClass.addParticipant(param);
        return this;
    }

    @Override
    public CodeableConcept addReason() {
        return adaptedClass.addReason();
    }

    @Override
    public ICORAEncounter addReason(CodeableConcept param) {
        adaptedClass.addReason(param);
        return this;
    }

    @Override
    public Encounter.StatusHistoryComponent addStatusHistory() {

        return adaptedClass.addStatusHistory();
    }


    @Override
    public ICORAEncounter addStatusHistory(Encounter.StatusHistoryComponent param) {
        adaptedClass.addStatusHistory(param);
        return this;
    }


    @Override
    public CodeableConcept addType() {
        return adaptedClass.addType();
    }

    @Override
    public ICORAEncounter addType(CodeableConcept param) {
        adaptedClass.addType(param);
        return this;
    }



    @Override
    public Appointment getAppointmentResource() {
        if (adaptedClass.getAppointment().getResource() instanceof Appointment) {
            return (Appointment) adaptedClass.getAppointment().getResource();
        } else {
            return null;
        }
    }

    @Override
    public Coding getClass_() {
        return adaptedClass.getClass_();
    }

    @Override
    @LogicalModelAlias("getClass_")
    public Coding getClassElement() {
        return adaptedClass.getClass_();
    }

    @Override
    public List<Encounter.DiagnosisComponent> getDiagnosis() {
        return adaptedClass.getDiagnosis();
    }

    @Override
    public Encounter.DiagnosisComponent getDiagnosisFirstRep() {
        return adaptedClass.getDiagnosisFirstRep();
    }

    @Override
    public Encounter.EncounterHospitalizationComponent getHospitalization() {
        return adaptedClass.getHospitalization();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public Duration getLength() {
        return adaptedClass.getLength();
    }

    @Override
    public List<Encounter.EncounterLocationComponent> getLocation() {
        return adaptedClass.getLocation();
    }

    @Override
    public Encounter.EncounterLocationComponent getLocationFirstRep() {
        return adaptedClass.getLocationFirstRep();
    }

    @Override
    public List<Encounter.EncounterParticipantComponent> getParticipant() {
        return adaptedClass.getParticipant();
    }

    @Override
    public Encounter.EncounterParticipantComponent getParticipantFirstRep() {
        return adaptedClass.getParticipantFirstRep();
    }

    @Override
    public CORAEncounterAdapter getPartOfResource() {
        if (adaptedClass.getPartOf().getResource() instanceof Encounter) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getPartOf().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getSubject().getResource() instanceof Patient) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Period getPeriod() {
        return adaptedClass.getPeriod();
    }

    @Override
    public CodeableConcept getPriority() {
        return adaptedClass.getPriority();
    }

    @Override
    public List<CodeableConcept> getReason() {
        return adaptedClass.getReason();
    }

    @Override
    public CodeableConcept getReasonCancelled() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/encounter-reasonCancelled");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonCancelled");
        }
    }

    @Override
    public CodeableConcept getReasonFirstRep() {
        return adaptedClass.getReasonFirstRep();
    }


    @Override
    public List<CORAEncounterRelatedCondition> getRelatedCondition() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/encounter-relatedCondition");
        List<com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterRelatedCondition> returnList =
                new java.util.ArrayList<>();
        for (Extension extension : extensions) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterRelatedCondition udt =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterRelatedCondition();
            udt.setRootObjectExtension(extension);
            returnList.add(udt);
        }
        return returnList;
    }


    @Override
    public CORAOrganizationAdapter getServiceProviderResource() {
        if (adaptedClass.getServiceProvider().getResource() instanceof Organization) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAOrganizationAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAOrganizationAdapter();
            profiledType.setAdaptee((Organization) adaptedClass.getServiceProvider().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Encounter.EncounterStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<Encounter.EncounterStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public List<Encounter.StatusHistoryComponent> getStatusHistory() {
        return adaptedClass.getStatusHistory();
    }

    @Override
    public Encounter.StatusHistoryComponent getStatusHistoryFirstRep() {
        return adaptedClass.getStatusHistoryFirstRep();
    }

    @Override
    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public List<CodeableConcept> getType() {
        return adaptedClass.getType();
    }

    @Override
    public CodeableConcept getTypeFirstRep() {
        return adaptedClass.getTypeFirstRep();
    }

    @Override
    public ICORAEncounter setAppointmentResource(Appointment param) {
        adaptedClass.getAppointment().setResource(param);
        return this;
    }

    @Override
    public ICORAEncounter setClass_(Coding param) {
        adaptedClass.setClass_(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setClass_")
    public ICORAEncounter setClassElement(Coding param) {
        adaptedClass.setClass_(param);
        return this;
    }


    @Override
    public ICORAEncounter setDiagnosis(List<Encounter.DiagnosisComponent> param) {
        adaptedClass.setDiagnosis(param);
        return this;
    }

    @Override
    public ICORAEncounter setHospitalization(Encounter.EncounterHospitalizationComponent param) {
        adaptedClass.setHospitalization(param);
        return this;
    }


    @Override
    public ICORAEncounter setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }


    @Override
    public ICORAEncounter setLength(Duration param) {
        adaptedClass.setLength(param);
        return this;
    }

    @Override
    public ICORAEncounter setLocation(List<Encounter.EncounterLocationComponent> param) {
        adaptedClass.setLocation(param);
        return this;
    }

    @Override
    public ICORAEncounter setParticipant(List<Encounter.EncounterParticipantComponent> param) {
        adaptedClass.setParticipant(param);
        return this;
    }

    @Override
    public ICORAEncounter setPartOfResource(CORAEncounterAdapter param) {
        adaptedClass.getPartOf().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAEncounter setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getSubject().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAEncounter setPeriod(Period param) {
        adaptedClass.setPeriod(param);
        return this;
    }

    @Override
    public ICORAEncounter setPriority(CodeableConcept param) {
        adaptedClass.setPriority(param);
        return this;
    }

    @Override
    public ICORAEncounter setReason(List<CodeableConcept> param) {
        adaptedClass.setReason(param);
        return this;
    }

    @Override
    public ICORAEncounter setReasonCancelled(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/encounter-reasonCancelled")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAEncounter setRelatedCondition(List<CORAEncounterRelatedCondition> param) {
        if (param != null && param.size() > 0) {
            for (int index =
                    0; index < param
                            .size(); index++) {
                adaptedClass.addExtension(param.get(index).getRootObjectExtension());
            }
        }
        return this;
    }

    @Override
    public ICORAEncounter setServiceProviderResource(CORAOrganizationAdapter param) {
        adaptedClass.getServiceProvider().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAEncounter setStatus(Encounter.EncounterStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAEncounter setStatusElement(Enumeration<Encounter.EncounterStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAEncounter setStatusHistory(List<Encounter.StatusHistoryComponent> param) {
        adaptedClass.setStatusHistory(param);
        return this;
    }

    @Override
    public ICORAEncounter setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAEncounter setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public ICORAEncounter setType(List<CodeableConcept> param) {
        adaptedClass.setType(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAEncounterAdapter [getAppointmentResource()=" + getAppointmentResource()
                + ", getClass_()=" + getClass_() + ", getDiagnosis()=" + getDiagnosis()
                + ", getHospitalization()=" + getHospitalization() + ", getIdentifier()="
                + getIdentifier() + ", getLength()=" + getLength() + ", getLocation()="
                + getLocation() + ", getParticipant()=" + getParticipant()
                + ", getPartOfResource()=" + getPartOfResource() + ", getPeriod()=" + getPeriod()
                + ", getPriority()=" + getPriority() + ", getReason()=" + getReason()
                + ", getReasonCancelled()=" + getReasonCancelled() + ", getRelatedCondition()="
                + getRelatedCondition() + ", getServiceProviderResource()="
                + getServiceProviderResource() + ", getStatus()=" + getStatus()
                + ", getStatusHistory()=" + getStatusHistory() + ", getSubject()=" + getSubject()
                + ", getText()=" + getText() + ", getType()=" + getType()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getContained()="
                + getContained() + "]";
    }

}
