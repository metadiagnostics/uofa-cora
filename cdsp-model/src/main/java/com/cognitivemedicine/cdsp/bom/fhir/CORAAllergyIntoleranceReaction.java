/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.AllergyIntolerance;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceCriticality;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceSeverity;
import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.exceptions.FHIRException;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelRemoval;

public class CORAAllergyIntoleranceReaction {

    private AllergyIntolerance.AllergyIntoleranceReactionComponent adaptedClass = null;

    public CORAAllergyIntoleranceReaction() {
        this.adaptedClass = new AllergyIntolerance.AllergyIntoleranceReactionComponent();
    }

    public CORAAllergyIntoleranceReaction(
            AllergyIntolerance.AllergyIntoleranceReactionComponent adaptee) {
        this.adaptedClass = adaptee;
    }


    public CodeableConcept addManifestation()
    {
        return adaptedClass.addManifestation();
    }

    public CORAAllergyIntoleranceReaction addManifestation(CodeableConcept param)
    {
        adaptedClass.addManifestation(param);
        return this;
    }
    public Annotation addNote()
    {
        return adaptedClass.addNote();
    }
    
    public CORAAllergyIntoleranceReaction addNote(Annotation param)
    {
        adaptedClass.addNote(param);
        return this;
        
    }
    public AllergyIntolerance.AllergyIntoleranceReactionComponent getAdaptee() {
        return adaptedClass;
    }
    
    
    public String getCertainty() {
	    List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/allergyintolerance-certainty");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            Extension ext =extensions.get(0);
            CodeType cd = (CodeType) ext.getValue();
            return cd.getValue();
        } else {
            throw new RuntimeException("More than one extension exists for duration");
        }
	}
    
    public String getDescription() {
        return adaptedClass.getDescription();
    }
    public StringType getDescriptionElement() {
        return adaptedClass.getDescriptionElement();
    }

    public Extension getDuration() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/allergyintolerance-duration");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Extension) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for duration");
        }
    }

    public CodeableConcept getExposureRoute() {
        return adaptedClass.getExposureRoute();
    }

    public String getId() {
        return adaptedClass.getId();
    }

    public StringType getIdElement() {
        return adaptedClass.getIdElement();
    }

    public List<CodeableConcept> getManifestation()
    {
        return adaptedClass.getManifestation();
    }

    public CodeableConcept getManifestationFirstRep()
    {
        return adaptedClass.getManifestationFirstRep();
    }


    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }


    public Annotation getNoteFirstRep()
    {
        return adaptedClass.getNoteFirstRep();
    }
    
    public Date getOnset() {
        return adaptedClass.getOnset();
    }

    public DateTimeType getOnsetElement() {
        return adaptedClass.getOnsetElement();
    }

    public AllergyIntoleranceSeverity getSeverity() {
        return adaptedClass.getSeverity();
    }
    
    public Enumeration<AllergyIntoleranceSeverity> getSeverityElement()
    {
        return adaptedClass.getSeverityElement();
    }

    public CodeableConcept getSubstance() {
        return adaptedClass.getSubstance();
    }

    public void setAdaptee(AllergyIntolerance.AllergyIntoleranceReactionComponent param) {
        this.adaptedClass = param;
    }

    @LogicalModelRemoval("BoundCodeType no longer in HAPI")
    /*
     * public BoundCodeType<AllergyIntoleranceCertaintyEnum> getCertaintyElement() { return
     * adaptedClass.getCertaintyElement(); }
     */
    
    public CORAAllergyIntoleranceReaction setCertainty(AllergyIntoleranceCriticality param) {
        CodeType code = new CodeType(param.toString());
                   
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/allergyintolerance-certainty");
        if (extensions == null || extensions.size() <= 0) {
            adaptedClass.addExtension("http://hl7.org/fhir/StructureDefinition/allergyintolerance-certainty",code);
        } else if (extensions.size() == 1) {        
            extensions.get(0).setValue(code);
        } else {
            throw new RuntimeException("More than one extension exists for duration");
        }
        return this;

    }

    public CORAAllergyIntoleranceReaction setCertainty(String param) {
        //Check the input value
        try {
            @SuppressWarnings("unused")
            AllergyIntoleranceCriticality chk = AllergyIntoleranceCriticality.fromCode(param);
        } catch (FHIRException e) {
            throw new RuntimeException("Invalid Certainty Code ("+param+")",e);
        }
        CodeType code = new CodeType(param);
           
        List<Extension> extensions = adaptedClass
                .getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/allergyintolerance-certainty");
        if (extensions == null || extensions.size() <= 0) {
            adaptedClass.addExtension("http://hl7.org/fhir/StructureDefinition/allergyintolerance-certainty",code);
        } else if (extensions.size() == 1) {        
            extensions.get(0).setValue(code);
        } else {
            throw new RuntimeException("More than one extension exists for duration");
        }
        return this;
    }

    public CORAAllergyIntoleranceReaction setDescription(String param) {
         adaptedClass.setDescription(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setDescriptionElement(StringType param) {
        adaptedClass.setDescriptionElement(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setDuration(Extension param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/allergyintolerance-duration")
                .setValue(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setExposureRoute(CodeableConcept param) {
        adaptedClass.setExposureRoute(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setId(String param) {
        adaptedClass.setId(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setIdElement(StringType param) {
        adaptedClass.setIdElement(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setManifestation(List<CodeableConcept> param)
    {
        adaptedClass.setManifestation(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setOnset(Date param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @LogicalModelChange("Change to follow new Element convention")
    public CORAAllergyIntoleranceReaction setOnsetElement(DateTimeType param) {
        adaptedClass.setOnsetElement(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setSeverity(AllergyIntoleranceSeverity param) {
        adaptedClass.setSeverity(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setSeverityElement(Enumeration<AllergyIntoleranceSeverity> param) {
        adaptedClass.setSeverityElement(param);
        return this;
    }

    public CORAAllergyIntoleranceReaction setSubstance(CodeableConcept param) {
        adaptedClass.setSubstance(param);
        return this;
    }
}
