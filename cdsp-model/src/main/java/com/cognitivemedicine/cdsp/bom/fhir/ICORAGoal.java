/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Duration;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Goal;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;

/**
 * DSTU3/QCICORE/BOM Goal
 * 
 * @author Jerry Goodnough
 *
 * 
 *         Missing:
 * 
 *         expressedBy 
 *         addresses
 */
@LogicalModelAddition("Extends our base")
public interface ICORAGoal extends ICORACognitiveBaseIdentifiable<ICORAGoal> {


    public CodeableConcept addOutcomeCode();

    public ICORAGoal addOutcomeCode(CodeableConcept param);

    public Reference addOutcomeReference();

    public ICORAGoal addOutcomeReference(Reference param);

    public CodeableConcept getDescription();

    public List<CodeableConcept> getOutcomeCode();

    public CodeableConcept getOutcomeCodeFirstRep();

    public List<Reference> getOutcomeReference();

    public Reference getOutcomeReferenceFirstRep();

    public CodeableConcept getPriority();

    public CodeableConcept getReasonRejected();


    public CodeableConcept getStartCodeableConcept();

    public Date getStartDate();

    public DateType getStartDateElement();

    public Goal.GoalStatus getStatus();

    public Date getStatusDate();

    public DateType getStatusDateElement();

    public Enumeration<Goal.GoalStatus> getStatusElement();

    public String getStatusReason();

    public Reference getSubject();

    public Goal.GoalTargetComponent getTarget();

    public ICORAGoal setDescription(CodeableConcept param);

    public ICORAGoal setOutcomeCode(List<CodeableConcept> param);

    public ICORAGoal setOutcomeReference(List<Reference> param);

    public ICORAGoal setPriority(CodeableConcept param);

    public ICORAGoal setReasonRejected(CodeableConcept param);

    public ICORAGoal setStartCodeableConcept(CodeableConcept param);

    public ICORAGoal setStartDate(Date param);

    public ICORAGoal setStartDate(DateType param);

    public ICORAGoal setStatus(Goal.GoalStatus param);

    public ICORAGoal setStatus(String param);

    public ICORAGoal setStatusDate(Date param);

    public ICORAGoal setStatusDateElement(DateType param);

    public ICORAGoal setStatusElement(Enumeration<Goal.GoalStatus> param);

    public ICORAGoal setStatusReason(String param);

    public ICORAGoal setSubject(Reference param);

    public ICORAGoal setTarget(Goal.GoalTargetComponent param);
}
