/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.CommunicationRequest;
import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationPriority;
import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestRequesterComponent;
import org.hl7.fhir.dstu3.model.CommunicationRequest.CommunicationRequestStatus;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.exceptions.FHIRException;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * Adapter for HAPI CommunicationRequest
 * 
 * @author Jerry Goodnough
 * @version DSTU3 Verified
 */

public class CORACommunicationRequestAdapter
        extends CORACognitiveBaseIdentifiable<CommunicationRequest, ICORACommunicationRequest>
        implements ICORACommunicationRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORACommunicationRequestAdapter() {
        this.adaptedClass = new CommunicationRequest();
    }

    public CORACommunicationRequestAdapter(CommunicationRequest adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {
        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORACommunicationRequest addBasedOn(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }

    @Override
    public CodeableConcept addCategory() {
        return adaptedClass.addCategory();
    }

    @Override
    public ICORACommunicationRequest addCategory(CodeableConcept param) {
        adaptedClass.addCategory(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }


    @Override
    public ICORACommunicationRequest addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Annotation addNote() {
        return adaptedClass.addNote();
    }

    @Override
    public ICORACommunicationRequest addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    @Override
    public CommunicationRequest.CommunicationRequestPayloadComponent addPayload() {
        CommunicationRequest.CommunicationRequestPayloadComponent item =
                new CommunicationRequest.CommunicationRequestPayloadComponent();
        adaptedClass.addPayload(item);
        return item;
    }

    @Override
    public ICORACommunicationRequest addPayload(
            CommunicationRequest.CommunicationRequestPayloadComponent param) {
        adaptedClass.addPayload(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {
        return adaptedClass.addReasonReference();
    }

    @Override
    public ICORACommunicationRequest addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public Reference addReplaces() {
        return adaptedClass.addReplaces();
    }

    @Override
    public ICORACommunicationRequest addReplaces(Reference param) {
        adaptedClass.addReplaces(param);
        return this;
    }

    @Override
    public Reference addTopic() {
        return adaptedClass.addTopic();
    }

    @Override
    public ICORACommunicationRequest addTopic(Reference param) {
        adaptedClass.addTopic(param);
        return this;
    }

    @Override
    public Date getAuthoredOn() {
        return adaptedClass.getAuthoredOn();
    }

    @Override
    public DateTimeType getAuthoredOnElement() {
        return adaptedClass.getAuthoredOnElement();
    }

    @Override
    public List<Reference> getBasedOn() {
        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {
        return adaptedClass.getBasedOnFirstRep();
    }

    @Override
    public List<CodeableConcept> getCategory() {
        return adaptedClass.getCategory();
    }

    @Override
    public CodeableConcept getCategoryFirstRep() {
        return adaptedClass.getCategoryFirstRep();
    }


    @Override
    public Reference getContext() {
        return adaptedClass.getContext();
    }

    @Override
    public Identifier getGroupIdentifier() {
        return adaptedClass.getGroupIdentifier();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public Type getOccurrence() {
        return adaptedClass.getOccurrence();
    }

    @Override
    public DateTimeType getOccurrenceDateTime() throws FHIRException {
        return adaptedClass.getOccurrenceDateTimeType();
    }

    @Override
    public Period getOccurrencePeriod() throws FHIRException {
        return adaptedClass.getOccurrencePeriod();
    }

    @Override
    public List<CommunicationRequest.CommunicationRequestPayloadComponent> getPayload() {
        return adaptedClass.getPayload();
    }

    @Override
    public CommunicationRequest.CommunicationRequestPayloadComponent getPayloadFirstRep() {
        return adaptedClass.getPayloadFirstRep();
    }

    @Override
    public CommunicationPriority getPriority() {
        return adaptedClass.getPriority();
    }


    @Override
    public Enumeration<CommunicationPriority> getPriorityElement() {
        return adaptedClass.getPriorityElement();
    }

    @Override
    public List<Reference> getReasonReference() {
        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstReq() {
        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public CodeableConcept getReasonRejected() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/communicationrequest-reasonRejected");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonRejected");
        }
    }

    @Override
    public List<Reference> getReplaces() {
        return adaptedClass.getReplaces();
    }

    @Override
    public Reference getReplacesFirstReq() {
        return adaptedClass.getReplacesFirstRep();
    }

    @Override
    public Date getRequestedOn() {
        return adaptedClass.getAuthoredOn();
    }

    @Override
    public DateTimeType getRequestedOnElement() {
        return adaptedClass.getAuthoredOnElement();
    }

    @Override
    public CommunicationRequestRequesterComponent getRequester() {
        return adaptedClass.getRequester();
    }

    @Override
    public Date getScheduledDateTime() {
        if (adaptedClass.getOccurrence() != null
                && adaptedClass.getOccurrence() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getOccurrence()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateTimeType getScheduledDateTimeElement() {
        if (adaptedClass.getOccurrence() != null
                && adaptedClass.getOccurrence() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @Override
    public Period getScheduledPeriod() {
        if (adaptedClass.getOccurrence() != null
                && adaptedClass.getOccurrence() instanceof Period) {
            return (Period) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @Override
    public CommunicationRequestStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<CommunicationRequestStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Resource getSubjectResource() {
        return adaptedClass.getSubjectTarget();
    }


    @Override
    public List<Reference> getTopic() {
        return adaptedClass.getTopic();
    }

    @Override
    public Reference getTopicFirstReq() {
        return adaptedClass.getTopicFirstRep();
    }

    @Override
    public ICORACommunicationRequest setAuthoredOn(Date param) {
        adaptedClass.setAuthoredOn(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setAuthoredOnElement(DateTimeType param) {
        adaptedClass.setAuthoredOnElement(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setCategory(List<CodeableConcept> param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setGroupIdentifier(Identifier param) {
        adaptedClass.setGroupIdentifier(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }


    @Override
    public ICORACommunicationRequest setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setOccurrence(Type param) {
        adaptedClass.setOccurrence(param);
        return this;
    }


    @Override
    public ICORACommunicationRequest setPayload(
            List<CommunicationRequest.CommunicationRequestPayloadComponent> param) {
        adaptedClass.setPayload(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setPriority(CommunicationPriority param) {
        adaptedClass.setPriority(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setPriorityElement(Enumeration<CommunicationPriority> param) {
        adaptedClass.setPriorityElement(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setReasonRejected(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/communicationrequest-reasonRejected")
                .setValue(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setReplaces(List<Reference> param) {
        adaptedClass.setReplaces(param);
        return this;
    }

    @Override
    @LogicalModelAlias("authoredOn")
    public ICORACommunicationRequest setRequestedOn(Date param) {
        adaptedClass.setAuthoredOn(param);
        return this;
    }

    @Override
    @LogicalModelAlias("authoredOnElement")
    public ICORACommunicationRequest setRequestedOnElement(DateTimeType param) {
        adaptedClass.setAuthoredOnElement(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setRequester(CommunicationRequestRequesterComponent param) {
        adaptedClass.setRequester(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORACommunicationRequest setScheduledDateTime(Date param) {
        adaptedClass.setOccurrence(new DateTimeType(param));
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORACommunicationRequest setScheduledDateTime(DateTimeType param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setOccurrence")
    public ICORACommunicationRequest setScheduledPeriod(Period param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setStatus(CommunicationRequestStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setStatusElement(
            Enumeration<CommunicationRequestStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setSubjectResource(Resource param) {
        adaptedClass.setSubjectTarget(param);
        return this;
    }

    @Override
    public ICORACommunicationRequest setTopic(List<Reference> param) {
        adaptedClass.setTopic(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORACommunicationRequestAdapter [getAuthoredOn()=" + getAuthoredOn()
                + ", getBasedOnFirstRep()=" + getBasedOnFirstRep() + ", getCategoryFirstRep()="
                + getCategoryFirstRep() + ", getGroupIdentifier()=" + getGroupIdentifier()
                + ", getIdentifierFirstRep()=" + getIdentifierFirstRep() + ", getNoteFirstRep()="
                + getNoteFirstRep() + ", getOccurrence()=" + getOccurrence()
                + ", getPayloadFirstRep()=" + getPayloadFirstRep() + ", getPriority()="
                + getPriority() + ", getReasonReferenceFirstReq()=" + getReasonReferenceFirstReq()
                + ", getReasonRejected()=" + getReasonRejected() + ", getReplacesFirstReq()="
                + getReplacesFirstReq() + ", getRequestedOn()=" + getRequestedOn()
                + ", getRequester()=" + getRequester() + ", getStatus()=" + getStatus()
                + ", getTopicFirstReq()=" + getTopicFirstReq() + "]";
    }



}
