/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Identifier.IdentifierUse;
import org.hl7.fhir.dstu3.model.InstantType;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Observation.ObservationStatus;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Ratio;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.SampledData;
import org.hl7.fhir.dstu3.model.Specimen;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.TimeType;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.exceptions.FHIRException;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;

public class CORAObservationAdapter
        extends CORACognitiveBaseIdentifiable<Observation, ICORAObservation>
        implements ICORAObservation, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAObservationAdapter() {
        this.adaptedClass = new org.hl7.fhir.dstu3.model.Observation();
    }

    public CORAObservationAdapter(Observation adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {

        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORAObservation addBasedOn(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }

    @Override
    public CodeableConcept addCategory() {
        return adaptedClass.addCategory();
    }

    @Override
    public ICORAObservation addCategory(CodeableConcept param) {
        adaptedClass.addCategory(param);
        return this;
    }

    @Override
    public Observation.ObservationComponentComponent addComponent() {
        Observation.ObservationComponentComponent item =
                new Observation.ObservationComponentComponent();
        adaptedClass.addComponent(item);
        return item;
    }

    @Override
    public ICORAObservation addComponent(Observation.ObservationComponentComponent param) {
        adaptedClass.addComponent(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAObservation addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Observation.ObservationReferenceRangeComponent addReferenceRange() {
        Observation.ObservationReferenceRangeComponent item =
                new Observation.ObservationReferenceRangeComponent();
        adaptedClass.addReferenceRange(item);
        return item;
    }

    @Override
    public ICORAObservation addReferenceRange(
            Observation.ObservationReferenceRangeComponent param) {
        adaptedClass.addReferenceRange(param);
        return this;
    }

    @Override
    public Observation.ObservationRelatedComponent addRelated() {
        Observation.ObservationRelatedComponent item =
                new Observation.ObservationRelatedComponent();
        adaptedClass.addRelated(item);
        return item;
    }

    @Override
    public ICORAObservation addRelated(Observation.ObservationRelatedComponent param) {
        adaptedClass.addRelated(param);
        return this;
    }

    @Override
    public List<Reference> getBasedOn() {
        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {

        return adaptedClass.getBasedOnFirstRep();
    }

    @Override
    public CodeableConcept getBodyPosition() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/observation-bodyPosition");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for bodyPosition");
        }
    }

    @Override
    public CodeableConcept getBodySite() {
        return adaptedClass.getBodySite();
    }

    @Override
    public List<CodeableConcept> getCategory() {
        return adaptedClass.getCategory();
    }

    @Override
    public CodeableConcept getCategoryFirstRep() {
        return adaptedClass.getCategoryFirstRep();
    }

    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }

    @Override
    public String getComment() {
        return adaptedClass.getComment();
    }

    @Override
    public StringType getCommentElement() {
        return adaptedClass.getCommentElement();
    }

    @Override
    public List<Observation.ObservationComponentComponent> getComponent() {
        return adaptedClass.getComponent();
    }

    @Override
    public Observation.ObservationComponentComponent getComponentFirstRep() {
        return adaptedClass.getComponentFirstRep();
    }

    @Override
    public Reference getContext() {
        return adaptedClass.getContext();

    }

    @Override
    public CodeableConcept getDataAbsentReason() {
        return adaptedClass.getDataAbsentReason();
    }

    @Override
    public Date getEffectiveDateTime() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getEffective()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateTimeType getEffectiveDateTimeElement() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public Period getEffectivePeriod() {
        if (adaptedClass.getEffective() != null && adaptedClass.getEffective() instanceof Period) {
            return (Period) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getContext().getResource() instanceof Encounter) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getContext().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public CodeableConcept getInterpretation() {
        return adaptedClass.getInterpretation();
    }

    @Override
    public Date getIssued() {
        return adaptedClass.getIssued();
    }

    @Override
    public InstantType getIssuedElement() {
        return adaptedClass.getIssuedElement();
    }

    @Override
    public CodeableConcept getMethod() {
        return adaptedClass.getMethod();
    }

    @Override
    @LogicalModelAddition("To be added to IQCore")
    public Identifier getOrderIdentifier() {
        return getFirstIdentiferBySystem("OrderId");
    }

    @Override
    @LogicalModelAddition("To be added to IQCore")
    public Coding getPrimaryCoding() {
        return getCode().getCodingFirstRep();
    }

    @Override
    public List<Observation.ObservationReferenceRangeComponent> getReferenceRange() {
        return adaptedClass.getReferenceRange();
    }

    @Override
    public Observation.ObservationReferenceRangeComponent getReferenceRangeFirstRep() {
        return adaptedClass.getReferenceRangeFirstRep();
    }

    @Override
    public List<Observation.ObservationRelatedComponent> getRelated() {
        return adaptedClass.getRelated();
    }

    @Override
    public Observation.ObservationRelatedComponent getRelatedFirstRep() {
        return adaptedClass.getRelatedFirstRep();
    }

    @Override
    public CORASpecimenAdapter getSpecimenResource() {
        if (adaptedClass.getSpecimen().getResource() instanceof Specimen) {
            com.cognitivemedicine.cdsp.bom.fhir.CORASpecimenAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORASpecimenAdapter();
            profiledType.setAdaptee((Specimen) adaptedClass.getSpecimen().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public ObservationStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<ObservationStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public Type getValue() {
        return adaptedClass.getValue();
    }

    @Override
    public Attachment getValueAttachment() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof Attachment) {
            return (Attachment) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public boolean getValueBoolean() {
        boolean value = false;
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof BooleanType) {

            try {
                value = adaptedClass.getValueBooleanType().booleanValue();
            } catch (FHIRException e) {
            }

        }
        return value;
    }


    @Override
    public BooleanType getValueBooleanElement() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof BooleanType) {
            return (BooleanType) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getValueCodeableConcept() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof CodeableConcept) {
            return (CodeableConcept) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public Date getValueDateTime() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getValue()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateTimeType getValueDateTimeElement() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public Period getValuePeriod() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof Period) {
            return (Period) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public Quantity getValueQuantity() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof Quantity) {
            return (Quantity) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public Range getValueRange() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof Range) {
            return (Range) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public Ratio getValueRatio() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof Ratio) {
            return (Ratio) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public SampledData getValueSampledData() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof SampledData) {
            return (SampledData) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public String getValueString() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof StringType) {
            return ((StringType) adaptedClass.getValue()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public StringType getValueStringElement() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof StringType) {
            return (StringType) adaptedClass.getValue();
        } else {
            return null;
        }
    }

    @Override
    public String getValueTime() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof TimeType) {
            return ((TimeType) adaptedClass.getValue()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public TimeType getValueTimeElement() {
        if (adaptedClass.getValue() != null && adaptedClass.getValue() instanceof TimeType) {
            return (TimeType) adaptedClass.getValue();
        } else {
            return null;
        }
    }


    @Override
    public ICORAObservation setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORAObservation setBodyPosition(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/observation-bodyPosition")
                .setValue(param);
        // Note in the old code the first boolean was "isModifier"
        return this;
    }

    @Override
    public ICORAObservation setBodySite(CodeableConcept param) {
        adaptedClass.setBodySite(param);
        return this;
    }

    @Override
    public ICORAObservation setCategory(List<CodeableConcept> param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORAObservation setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    @Override
    public ICORAObservation setComment(String param) {
        adaptedClass.setComment(param);
        return this;
    }

    @Override
    public ICORAObservation setCommentElement(StringType param) {
        adaptedClass.setCommentElement(param);
        return this;
    }

    @Override
    public ICORAObservation setComponent(List<Observation.ObservationComponentComponent> param) {
        adaptedClass.setComponent(param);
        return this;
    }

    @Override
    public ICORAObservation setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORAObservation setDataAbsentReason(CodeableConcept param) {
        adaptedClass.setDataAbsentReason(param);
        return this;
    }

    @Override
    public ICORAObservation setEffectiveDateTime(Date param) {
        adaptedClass.setEffective(new DateTimeType(param));
        return this;
    }

    @Override
    public ICORAObservation setEffectiveDateTimeElement(DateTimeType param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORAObservation setEffectivePeriod(Period param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORAObservation setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getContext().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAObservation setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAObservation setInterpretation(CodeableConcept param) {
        adaptedClass.setInterpretation(param);
        return this;
    }

    @Override
    public ICORAObservation setIssued(Date param) {
        adaptedClass.setIssued(param);
        return this;
    }

    @Override
    public ICORAObservation setIssued(InstantType param) {

        adaptedClass.setIssuedElement(param);
        return this;
    }

    @Override
    public ICORAObservation setMethod(CodeableConcept param) {
        adaptedClass.setMethod(param);
        return this;
    }

    @Override
    @LogicalModelAddition("To be added to IQCore")
    public ICORAObservation setOrderIdentifer(Identifier orderId) {
        String system = "OrderId";
        boolean isPrimary = false;

        List<Identifier> identifiers = getIdentifier();
        Iterator<Identifier> itr = identifiers.iterator();
        // Pull the old order Id if present
        while (itr.hasNext()) {
            Identifier id = itr.next();

            String idsystem = id.getSystem();
            if (idsystem != null) {
                if (id.getSystem().compareTo(system) == 0) {
                    IdentifierUse use = id.getUse();
                    if (use != null) {
                        if (id.getUse() == IdentifierUse.OFFICIAL) {
                            isPrimary = true;
                        }
                    }
                    itr.remove();
                    break;
                }
            }
        }
        orderId.setSystem(system);
        if (isPrimary) {
            orderId.setUse(IdentifierUse.OFFICIAL);
        }
        identifiers.add(orderId);

        return this;
    }

    /**
     * Any prior code in the first position will be removed
     */
    @Override
    @LogicalModelAddition("To be added to IQCore")
    public ICORAObservation setPrimaryCoding(Coding param) {
        CodeableConcept cc = getCode();
        if (cc == null) {
            cc = new CodeableConcept();
            setCode(cc);
        }

        List<Coding> coding = cc.getCoding();

        if (coding == null) {
            cc.addCoding(param);
        } else {
            if (coding.size() > 0) {
                coding.remove(0);
            }
            coding.add(0, param);
        }

        return this;
    }


    @Override
    public ICORAObservation setReferenceRange(
            List<Observation.ObservationReferenceRangeComponent> param) {
        adaptedClass.setReferenceRange(param);
        return this;
    }

    @Override
    public ICORAObservation setRelated(List<Observation.ObservationRelatedComponent> param) {
        adaptedClass.setRelated(param);
        return this;
    }

    @Override
    public ICORAObservation setSpecimenResource(CORASpecimenAdapter param) {
        adaptedClass.getSpecimen().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAObservation setStatus(ObservationStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAObservation setStatus(String param) {
        adaptedClass.setStatus(ObservationStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAObservation setStatusElement(Enumeration<ObservationStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAObservation setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAObservation setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public ICORAObservation setValue(Type param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueAttachment(Attachment param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueBoolean(boolean param) {
        BooleanType val = new BooleanType(param);
        adaptedClass.setValue(val);
        return this;
    }

    @Override
    public ICORAObservation setValueBooleanElement(BooleanType param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueCodeableConcept(CodeableConcept param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueDateTime(Date param) {
        adaptedClass.setValue(new DateTimeType(param));
        return this;
    }

    @Override
    public ICORAObservation setValueDateTime(DateTimeType param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValuePeriod(Period param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueQuantity(Quantity param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueRange(Range param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueRatio(Ratio param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueSampledData(SampledData param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueString(String param) {
        adaptedClass.setValue(new StringType(param));
        return this;
    }

    @Override
    public ICORAObservation setValueString(StringType param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public ICORAObservation setValueTime(String param) {
        adaptedClass.setValue(new TimeType(param));
        return this;
    }

    @Override
    public ICORAObservation setValueTime(TimeType param) {
        adaptedClass.setValue(param);
        return this;
    }

    @Override
    public String toString() {
        final int maxLen = 10;
        return "CORAObservationAdapter [getBasedOn()="
                + (getBasedOn() != null
                        ? getBasedOn().subList(0, Math.min(getBasedOn().size(), maxLen)) : null)
                + ", getBodyPosition()=" + getBodyPosition() + ", getBodySite()=" + getBodySite()
                + ", getCategory()="
                + (getCategory() != null
                        ? getCategory().subList(0, Math.min(getCategory().size(), maxLen)) : null)
                + ", getCode()=" + getCode() + ", getComment()=" + getComment()
                + ", getComponent()="
                + (getComponent() != null
                        ? getComponent().subList(0, Math.min(getComponent().size(), maxLen)) : null)
                + ", getContext()=" + getContext() + ", getDataAbsentReason()="
                + getDataAbsentReason() + ", getEffectiveDateTime()=" + getEffectiveDateTime()
                + ", getEffectivePeriod()=" + getEffectivePeriod() + ", getIdentifier()="
                + (getIdentifier() != null
                        ? getIdentifier().subList(0, Math.min(getIdentifier().size(), maxLen))
                        : null)
                + ", getInterpretation()=" + getInterpretation() + ", getIssued()=" + getIssued()
                + ", getMethod()=" + getMethod() + ", getOrderIdentifier()=" + getOrderIdentifier()
                + ", getPrimaryCoding()=" + getPrimaryCoding() + ", getRelated()="
                + (getRelated() != null
                        ? getRelated().subList(0, Math.min(getRelated().size(), maxLen)) : null)
                + ", getSpecimenResource()=" + getSpecimenResource() + ", getStatus()="
                + getStatus() + ", getSubject()=" + getSubject() + ", getText()=" + getText()
                + ", getValue()=" + getValue() + ", getPrimaryIdentifer()=" + getPrimaryIdentifer()
                + ", getCORAContext()=" + getCORAContext() + "]";
    }


}
