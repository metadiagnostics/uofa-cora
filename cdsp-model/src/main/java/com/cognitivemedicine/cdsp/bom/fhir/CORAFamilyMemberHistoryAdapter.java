/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Age;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.FamilyMemberHistory;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.exceptions.FHIRException;


public class CORAFamilyMemberHistoryAdapter
        extends CORACognitiveBaseIdentifiable<FamilyMemberHistory, ICORAFamilyMemberHistory>
        implements ICORAFamilyMemberHistory, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAFamilyMemberHistoryAdapter() {
        this.adaptedClass = new FamilyMemberHistory();
    }

    public CORAFamilyMemberHistoryAdapter(FamilyMemberHistory adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public FamilyMemberHistory.FamilyMemberHistoryConditionComponent addCodition(
            FamilyMemberHistory.FamilyMemberHistoryConditionComponent param) {
        adaptedClass.addCondition(param);
        return param;
    }

    @Override
    public FamilyMemberHistory.FamilyMemberHistoryConditionComponent addCondition() {
        return adaptedClass.addCondition();
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAFamilyMemberHistory addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public CodeableConcept addReasonCode() {
        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAFamilyMemberHistory addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {
        return adaptedClass.addReasonReference();
    }

    @Override
    public ICORAFamilyMemberHistory addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public Age getAgeAge() {
        try {
            return adaptedClass.getAgeAge();
        } catch (FHIRException e) {
            return null;
        }
    }

    @Override
    public Range getAgeRange() {
        if (adaptedClass.getAge() != null && adaptedClass.getAge() instanceof Range) {
            return (Range) adaptedClass.getAge();
        } else {
            return null;
        }
    }

    @Override
    public String getAgeString() {
        if (adaptedClass.getAge() != null && adaptedClass.getAge() instanceof StringType) {
            return ((StringType) adaptedClass.getAge()).getValueAsString();
        } else {
            return null;
        }
    }

    // Gender

    @Override
    public DateType getBornDate() {
        if (adaptedClass.getBorn() != null && adaptedClass.getBorn() instanceof DateType) {
            return (DateType) adaptedClass.getBorn();
        } else {
            return null;
        }
    }

    @Override
    public Period getBornPeriod() {
        if (adaptedClass.getBorn() != null && adaptedClass.getBorn() instanceof Period) {
            return (Period) adaptedClass.getBorn();
        } else {
            return null;
        }
    }

    @Override
    public String getBornString() {

        if (adaptedClass.getBorn() != null && adaptedClass.getBorn() instanceof StringType) {
            return ((StringType) adaptedClass.getBorn()).getValueAsString();
        } else {
            return null;
        }
    }

    @Override
    public List<FamilyMemberHistory.FamilyMemberHistoryConditionComponent> getCondition() {
        return adaptedClass.getCondition();
    }

    // Status

    @Override
    public FamilyMemberHistory.FamilyMemberHistoryConditionComponent getConditionFirstRep() {
        return adaptedClass.getConditionFirstRep();
    }


    @Override
    public Date getDate() {
        return adaptedClass.getDate();
    }

    @Override
    public DateTimeType getDateElement() {
        return adaptedClass.getDateElement();
    }

    @Override
    public Age getDeceasedAge() {
        try {
            return adaptedClass.getDeceasedAge();
        } catch (FHIRException e) {
            return null;
        }
    }

    @Override
    public Boolean getDeceasedBoolean() {
        if (adaptedClass.getDeceased() != null
                && adaptedClass.getDeceased() instanceof BooleanType) {
            return ((BooleanType) adaptedClass.getDeceased()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public DateType getDeceasedDate() {
        if (adaptedClass.getDeceased() != null && adaptedClass.getDeceased() instanceof DateType) {
            return (DateType) adaptedClass.getDeceased();
        } else {
            return null;
        }
    }


    @Override
    public Range getDeceasedRange() {
        if (adaptedClass.getDeceased() != null && adaptedClass.getDeceased() instanceof Range) {
            return (Range) adaptedClass.getDeceased();
        } else {
            return null;
        }
    }

    @Override
    public String getDeceasedString() {
        if (adaptedClass.getDeceased() != null
                && adaptedClass.getDeceased() instanceof StringType) {
            return ((StringType) adaptedClass.getDeceased()).getValueAsString();
        } else {
            return null;
        }
    }

    @Override
    public boolean getEstimatedAge() {
        return adaptedClass.getEstimatedAge();
    }

    @Override
    public AdministrativeGender getGender() {
        return adaptedClass.getGender();
    }

    @Override
    public Enumeration<AdministrativeGender> getGenderElement() {
        return adaptedClass.getGenderElement();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public String getName() {
        return adaptedClass.getName();
    }

    @Override
    public boolean getNotDone() {
        return adaptedClass.getNotDone();
    }

    @Override
    public CodeableConcept getNotDoneReason() {
        return adaptedClass.getNotDoneReason();
    }

    @Override
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getPatient().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getPatient().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<CodeableConcept> getReasonCode() {
        return adaptedClass.getReasonCode();
    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {
        return adaptedClass.getReasonCodeFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {
        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstRep() {
        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public CodeableConcept getRelationship() {
        return adaptedClass.getRelationship();
    }

    @Override
    public FamilyMemberHistory.FamilyHistoryStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<FamilyMemberHistory.FamilyHistoryStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public ICORAFamilyMemberHistory setAgeQuantity(Age param) {
        adaptedClass.setAge(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setAgeRange(Range param) {
        adaptedClass.setAge(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setAgeString(String param) {
        StringType param1 = new StringType();
        param1.setValueAsString(param);
        adaptedClass.setAge(param1);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setBornDate(DateType param) {
        adaptedClass.setBorn(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setBornPeriod(Period param) {
        adaptedClass.setBorn(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setBornString(String param) {
        StringType param1 = new StringType();
        param1.setValueAsString(param);
        adaptedClass.setBorn(param1);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setCondition(
            List<FamilyMemberHistory.FamilyMemberHistoryConditionComponent> param) {
        adaptedClass.setCondition(param);
        return this;
    }


    @Override
    public ICORAFamilyMemberHistory setDate(Date date) {
        adaptedClass.setDate(date);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setDateElement(DateTimeType param) {
        adaptedClass.setDateElement(param);
        return this;
    }
    // Condition related code

    @Override
    public ICORAFamilyMemberHistory setDeceasedBoolean(Boolean param) {
        BooleanType param1 = new BooleanType();
        param1.setValue(param);
        adaptedClass.setDeceased(param1);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setDeceasedDate(DateType param) {
        adaptedClass.setDeceased(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setDeceasedQuantity(Age param) {
        adaptedClass.setDeceased(param);
        return this;
    }



    @Override
    public ICORAFamilyMemberHistory setDeceasedRange(Range param) {
        adaptedClass.setDeceased(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setDeceasedString(String param) {
        StringType param1 = new StringType();
        param1.setValueAsString(param);
        adaptedClass.setDeceased(param1);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setEstimatedAge(boolean param) {
        adaptedClass.setEstimatedAge(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setGender(AdministrativeGender param) {
        adaptedClass.setGender(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setGender(String param) {
        adaptedClass.setGender(AdministrativeGender.valueOf(param));
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setGenderElement(Enumeration<AdministrativeGender> param) {
        adaptedClass.setGenderElement(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setName(String name) {
        adaptedClass.setName(name);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setNotDone(boolean param) {
        adaptedClass.setNotDone(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setNotDoneReason(CodeableConcept param) {
        adaptedClass.setNotDoneReason(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getPatient().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setRelationship(CodeableConcept param) {
        adaptedClass.setRelationship(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setStatus(FamilyMemberHistory.FamilyHistoryStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setStatusElement(
            Enumeration<FamilyMemberHistory.FamilyHistoryStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAFamilyMemberHistory setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public String toString() {
        try{
            return "CORAFamilyMemberHistoryAdapter [getAgeAge()=" + getAgeAge() + ", getAgeRange()="
                    + getAgeRange() + ", getAgeString()=" + getAgeString() + ", getBornDate()="
                    + getBornDate() + ", getBornPeriod()=" + getBornPeriod() + ", getBornString()="
                    + getBornString() + ", getDate()=" + getDate() + ", getDeceasedAge()="
                    + getDeceasedAge() + ", getDeceasedBoolean()=" + getDeceasedBoolean()
                    + ", getDeceasedDate()=" + getDeceasedDate() + ", getDeceasedRange()="
                    + getDeceasedRange() + ", getDeceasedString()=" + getDeceasedString()
                    + ", getEstimatedAge()=" + getEstimatedAge() + ", getGender()=" + getGender()
                    + ", getIdentifier()=" + getIdentifier() + ", getName()=" + getName()
                    + ", getNotDone()=" + getNotDone() + ", getNotDoneReason()=" + getNotDoneReason()
                    + ", getReasonCode()=" + getReasonCode() + ", getReasonReference()="
                    + getReasonReference() + ", getRelationship()=" + getRelationship()
                    + ", getStatus()=" + getStatus() + ", getText()=" + getText()
                    + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                    + getCORAContext() + "]";
        } catch (Exception e){
            return "CORAFamilyMemberHistoryAdapter[NPE while converting to String! Fix this!]";
        }
    }

}
