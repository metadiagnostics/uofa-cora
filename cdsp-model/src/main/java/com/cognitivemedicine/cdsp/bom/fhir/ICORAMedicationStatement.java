/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.MedicationStatement.MedicationStatementStatus;
import org.hl7.fhir.dstu3.model.MedicationStatement.MedicationStatementTaken;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * DSTU3 BOM Interface
 * @author Jerry Goodnough
 *
 */

public interface ICORAMedicationStatement
        extends ICORACognitiveBaseIdentifiable<ICORAMedicationStatement> {

    public Dosage addDosage();

    public ICORAMedicationStatement addDosage(Dosage param);

    public Annotation addNote();

    public ICORAMedicationStatement addNote(Annotation param);

    public CodeableConcept addReasonCode();

    public ICORAMedicationStatement addReasonCode(CodeableConcept param);

    public CodeableConcept addReasonNotTaken();

    public ICORAMedicationStatement addReasonNotTaken(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAMedicationStatement addReasonReference(Reference param);

    public CodeableConcept getCategory();

    public Reference getContext();

    public Date getDateAsserted();

    public DateTimeType getDateAssertedElement();

    public List<Dosage> getDosage();

    public Dosage getDosageFirstRep();

    public Date getEffectiveDateTime();

    public DateTimeType getEffectiveDateTimeElement();

    public Period getEffectivePeriod();

    public CodeableConcept getMedicationCodeableConcept();

    public Reference getMedicationReference();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    @LogicalModelAlias("getSubject")
    public CORAPatientAdapter getPatientResource();

    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<CodeableConcept> getReasonNotTaken();

    public CodeableConcept getReasonNotTakenFirstRep();

    public List<Reference> getReasonReference();
    
    public Reference getReasonReferenceFirstRep();
    
    public MedicationStatementStatus getStatus();
    
    public Enumeration<MedicationStatementStatus> getStatusElement();
    
    public Reference getSubject();

    public MedicationStatementTaken  getTaken();
    
    public ICORAMedicationStatement setCategory(CodeableConcept param);
    
    public ICORAMedicationStatement setContext(Reference param);
    
    public ICORAMedicationStatement setDateAsserted(Date param);
    
    public ICORAMedicationStatement setDateAssertedElement(DateTimeType param);
    
    public ICORAMedicationStatement setDosage(List<Dosage> param);
    
    public ICORAMedicationStatement setEffectiveDateTime(Date param);
    
    public ICORAMedicationStatement setEffectiveDateTime(DateTimeType param);
    
    public ICORAMedicationStatement setEffectivePeriod(Period param);
    
    public ICORAMedicationStatement setMedicationCodeableConcept(CodeableConcept param);
    
    public ICORAMedicationStatement setMedicationReference(Reference param);
    
    public ICORAMedicationStatement setNote(List<Annotation> param);
    
    @LogicalModelAlias("setSubject")
    public ICORAMedicationStatement setPatientResource(CORAPatientAdapter param);
    
    public ICORAMedicationStatement setReasonCode(List<CodeableConcept> param);
    
    public ICORAMedicationStatement setReasonNotTaken(List<CodeableConcept> param);
    
    public ICORAMedicationStatement setReasonReference(List<Reference> param);
    
    
    public ICORAMedicationStatement setStatus(MedicationStatementStatus param);
    
    public ICORAMedicationStatement setStatus(String param);
    
    public ICORAMedicationStatement setStatusElement(Enumeration<MedicationStatementStatus> param);
    
    public ICORAMedicationStatement setSubject(Reference param);
    
    public ICORAMedicationStatement setTaken(MedicationStatementTaken param);
    
}
