/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Practitioner;

public class CORAPractitionerAdapter
        extends CORACognitiveBaseIdentifiable<Practitioner, ICORAPractitioner>
        implements ICORAPractitioner {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CORAPractitionerAdapter() {
        this.adaptedClass = new Practitioner();
    }

    public CORAPractitionerAdapter(Practitioner adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Address addAddress() {
        Address item = new Address();
        adaptedClass.addAddress(item);
        return item;
    }

    @Override
    public ICORAPractitioner addAddress(Address param) {
        adaptedClass.addAddress(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAPractitioner addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public HumanName addName() {
        return this.addName();
    }

    @Override
    public ICORAPractitioner addName(HumanName param) {
        adaptedClass.addName(param);
        return this;
    }

    @Override
    public Attachment addPhoto() {
        Attachment item = new Attachment();
        adaptedClass.addPhoto(item);
        return item;
    }

    @Override
    public ICORAPractitioner addPhoto(Attachment param) {
        adaptedClass.addPhoto(param);
        return this;
    }

    @Override
    public Practitioner.PractitionerQualificationComponent addQualification() {

        return addQualification();
    }

    @Override
    public ICORAPractitioner addQualification(
            Practitioner.PractitionerQualificationComponent param) {
        adaptedClass.addQualification(param);
        return this;
    }

    @Override
    public ContactPoint addTelecom() {
        ContactPoint item = new ContactPoint();
        adaptedClass.addTelecom(item);
        return item;
    }

    @Override
    public ICORAPractitioner addTelecom(ContactPoint param) {
        adaptedClass.addTelecom(param);
        return this;
    }

    @Override
    public Boolean getActive() {
        return adaptedClass.getActive();
    }

    @Override
    public BooleanType getActiveElement() {
        return adaptedClass.getActiveElement();
    }

    @Override
    public List<Address> getAddress() {
        return adaptedClass.getAddress();
    }

    @Override
    public Address getAddressFirstRep() {
        return adaptedClass.getAddressFirstRep();
    }

    @Override
    public Date getBirthDate() {
        return adaptedClass.getBirthDate();
    }


    @Override
    public DateType getBirthDateElement() {
        return adaptedClass.getBirthDateElement();
    }

    @Override
    public CodeableConcept getClassification() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/practitioner-classification");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for classification");
        }
    }


    @Override
    public AdministrativeGender getGender() {
        return adaptedClass.getGender();
    }

    @Override
    public Enumeration<AdministrativeGender> getGenderElement() {
        return adaptedClass.getGenderElement();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<HumanName> getName() {
        return adaptedClass.getName();
    }



    @Override
    public HumanName getNameFirstRep() {

        return adaptedClass.getNameFirstRep();
    }

    @Override
    public List<Attachment> getPhoto() {
        return adaptedClass.getPhoto();
    }

    @Override
    public Attachment getPhotoFirstRep() {
        return adaptedClass.getPhotoFirstRep();
    }

    @Override
    public List<Practitioner.PractitionerQualificationComponent> getQualification() {
        return adaptedClass.getQualification();
    }

    @Override
    public Practitioner.PractitionerQualificationComponent getQualificationFirstRep() {
        return adaptedClass.getQualificationFirstRep();
    }

    @Override
    public List<ContactPoint> getTelecom() {
        return adaptedClass.getTelecom();
    }

    @Override
    public ContactPoint getTelecomFirstRep() {
        return adaptedClass.getTelecomFirstRep();
    }

    @Override
    public ICORAPractitioner setActive(Boolean param) {
        adaptedClass.setActive(param);
        return this;
    }

    @Override
    public ICORAPractitioner setActiveElement(BooleanType param) {
        adaptedClass.setActiveElement(param);
        return this;
    }

    @Override
    public ICORAPractitioner setAddress(List<Address> param) {
        adaptedClass.setAddress(param);
        return this;
    }



    @Override
    public ICORAPractitioner setBirthDate(Date param) {
        adaptedClass.setBirthDate(param);
        return this;
    }

    @Override
    public ICORAPractitioner setBirthDateElement(DateType param) {
        adaptedClass.setBirthDateElement(param);
        return this;
    }

    @Override
    public ICORAPractitioner setClassification(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/practitioner-classification")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAPractitioner setGender(AdministrativeGender param) {
        adaptedClass.setGender(param);
        return this;
    }

    @Override
    public ICORAPractitioner setGender(String param) {
        adaptedClass.setGender(AdministrativeGender.valueOf(param));
        return this;
    }

    @Override
    public ICORAPractitioner setGenderElement(Enumeration<AdministrativeGender> param) {
        adaptedClass.setGenderElement(param);
        return this;
    }


    @Override
    public ICORAPractitioner setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAPractitioner setName(List<HumanName> param) {
        adaptedClass.setName(param);
        return this;
    }

    @Override
    public ICORAPractitioner setPhoto(List<Attachment> param) {
        adaptedClass.setPhoto(param);
        return this;
    }

    @Override
    public ICORAPractitioner setQualification(
            List<Practitioner.PractitionerQualificationComponent> param) {
        adaptedClass.setQualification(param);
        return this;
    }

    @Override
    public ICORAPractitioner setTelecom(List<ContactPoint> param) {
        adaptedClass.setTelecom(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAPractitionerAdapter [getActive()=" + getActive() + ", getAddress()="
                + getAddress() + ", getBirthDate()=" + getBirthDate() + ", getClassification()="
                + getClassification() + ", getGender()=" + getGender() + ", getName()=" + getName()
                + ", getPhoto()=" + getPhoto() + ", getQualification()=" + getQualification()
                + ", getTelecom()=" + getTelecom() + ", getPrimaryIdentifer()="
                + getPrimaryIdentifer() + "]";
    }



}
