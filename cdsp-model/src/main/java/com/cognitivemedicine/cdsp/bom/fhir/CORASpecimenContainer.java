/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.IntegerType;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.Specimen;
import org.hl7.fhir.dstu3.model.StringType;



public class CORASpecimenContainer {

    private Specimen.SpecimenContainerComponent adaptedClass = null;

    public CORASpecimenContainer() {
        this.adaptedClass = new Specimen.SpecimenContainerComponent();
    }

    public CORASpecimenContainer(Specimen.SpecimenContainerComponent adaptee) {
        this.adaptedClass = adaptee;
    }

    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    public CORASpecimenContainer addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    public Specimen.SpecimenContainerComponent getAdaptee() {
        return adaptedClass;
    }

    public CodeableConcept getAdditiveCodeableConcept() {
        if ((adaptedClass.getAdditive() != null)
                && (adaptedClass.getAdditive() instanceof CodeableConcept)) {
            return (CodeableConcept) adaptedClass.getAdditive();
        } else {
            return null;
        }
    }

    public Reference getAdditiveReference() {
        if ((adaptedClass.getAdditive() != null)
                && (adaptedClass.getAdditive() instanceof Reference)) {
            return (Reference) adaptedClass.getAdditive();
        } else {
            return null;
        }
    }

    public SimpleQuantity getCapacity() {
        return adaptedClass.getCapacity();
    }

    public String getDescription() {
        return adaptedClass.getDescription();
    }

    public StringType getDescriptionElement() {
        return adaptedClass.getDescriptionElement();
    }

    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    public IntegerType getSequenceNumber() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/specimen-sequenceNumber");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (IntegerType) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for sequenceNumber");
        }
    }

    public SimpleQuantity getSpecimenQuantity() {
        return adaptedClass.getSpecimenQuantity();
    }

    public CodeableConcept getType() {
        return adaptedClass.getType();
    }

    public void setAdaptee(Specimen.SpecimenContainerComponent param) {
        this.adaptedClass = param;
    }

    public CORASpecimenContainer setAdditiveCodeableConcept(CodeableConcept param) {
        adaptedClass.setAdditive(param);
        return this;
    }

    public CORASpecimenContainer setAdditiveReference(Reference param) {
        adaptedClass.setAdditive(param);
        return this;
    }

    public CORASpecimenContainer setCapacity(SimpleQuantity param) {
        adaptedClass.setCapacity(param);
        return this;
    }


    public CORASpecimenContainer setDescription(String param) {
        adaptedClass.setDescription(param);
        return this;
    }

    public CORASpecimenContainer setDescriptionElement(StringType param) {
        adaptedClass.setDescriptionElement(param);
        return this;
    }

    public CORASpecimenContainer setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    public CORASpecimenContainer setSequenceNumber(IntegerType param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/specimen-sequenceNumber")
                .setValue(param);
        return this;
    }

    public CORASpecimenContainer setSpecimenQuantity(SimpleQuantity param) {
        adaptedClass.setSpecimenQuantity(param);
        return this;
    }

    public CORASpecimenContainer setType(CodeableConcept param) {
        adaptedClass.setType(param);
        return this;
    }
}
