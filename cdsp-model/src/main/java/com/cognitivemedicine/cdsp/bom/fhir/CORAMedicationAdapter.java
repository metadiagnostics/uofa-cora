/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Medication;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Organization;

public class CORAMedicationAdapter extends CORACognitiveBase<Medication, ICORAMedication>
        implements ICORAMedication {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAMedicationAdapter() {
        this.adaptedClass = new Medication();
    }

    public CORAMedicationAdapter(Medication adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Attachment addImage() {
        return adaptedClass.addImage();
    }

    @Override
    public ICORAMedication addImage(Attachment param) {
        adaptedClass.addImage(param);
        return this;
    }

    @Override
    public Medication.MedicationIngredientComponent addIngredient() {
        return adaptedClass.addIngredient();
    }


    @Override
    public ICORAMedication addIngredient(Medication.MedicationIngredientComponent param) {
        adaptedClass.addIngredient(param);
        return this;
    }

    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }



    @Override
    public CodeableConcept getForm() {
        return adaptedClass.getForm();
    }

    @Override
    public List<Attachment> getImage() {
        return adaptedClass.getImage();
    }



    @Override
    public Attachment getImageFirstRep() {
        return adaptedClass.getImageFirstRep();
    }

    @Override
    public List<Medication.MedicationIngredientComponent> getIngredient() {
        return adaptedClass.getIngredient();
    }

    @Override
    public Medication.MedicationIngredientComponent getIngredientFirstRep() {
        return adaptedClass.getIngredientFirstRep();
    }

    @Override
    public Boolean getIsBrand() {
        return adaptedClass.getIsBrand();
    }

    @Override
    public BooleanType getIsBrandElement() {
        return adaptedClass.getIsBrandElement();
    }

    @Override
    public boolean getIsOverTheCounter() {
        return adaptedClass.getIsOverTheCounter();
    }

    @Override
    public BooleanType getIsOverTheCounterElement() {
        return adaptedClass.getIsOverTheCounterElement();
    }

    @Override
    public CORAOrganizationAdapter getManufacturerResource() {
        if (adaptedClass.getManufacturer().getResource() instanceof Organization) {
            CORAOrganizationAdapter profiledType = new CORAOrganizationAdapter();
            profiledType.setAdaptee((Organization) adaptedClass.getManufacturer().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Medication.MedicationPackageComponent getPackage() {
        return adaptedClass.getPackage();
    }

    @Override
    public Medication.MedicationStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<Medication.MedicationStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public ICORAMedication setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    @Override
    public ICORAMedication setForm(CodeableConcept param) {
        adaptedClass.setForm(param);
        return this;
    }

    @Override
    public ICORAMedication setImage(List<Attachment> param) {
        adaptedClass.setImage(param);
        return this;
    }

    @Override
    public ICORAMedication setIngredient(List<Medication.MedicationIngredientComponent> param) {
        adaptedClass.setIngredient(param);
        return this;
    }

    @Override
    public ICORAMedication setIsBrand(boolean param) {
        adaptedClass.setIsBrand(param);
        return this;
    }

    @Override
    public ICORAMedication setIsBrandElement(BooleanType param) {
        adaptedClass.setIsBrandElement(param);
        return this;
    }

    @Override
    public ICORAMedication setIsOverTheCounter(boolean param) {
        adaptedClass.setIsOverTheCounter(param);
        return this;
    }

    @Override
    public ICORAMedication setIsOverTheCounterElement(BooleanType param) {
        adaptedClass.setIsOverTheCounterElement(param);
        return this;
    }

    @Override
    public ICORAMedication setManufacturerResource(CORAOrganizationAdapter param) {
        adaptedClass.getManufacturer().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedication setPackage(Medication.MedicationPackageComponent param) {
        adaptedClass.setPackage(param);
        return this;
    }

    @Override
    public ICORAMedication setStatus(Medication.MedicationStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAMedication setStatusElement(Enumeration<Medication.MedicationStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAMedicationAdapter [getCode()=" + getCode() + ", getForm()=" + getForm()
                + ", getImage()=" + getImage() + ", getIngredient()=" + getIngredient()
                + ", getIsBrand()=" + getIsBrand() + ", getIsOverTheCounter()="
                + getIsOverTheCounter() + ", getManufacturerResource()=" + getManufacturerResource()
                + ", getPackage()=" + getPackage() + ", getStatus()=" + getStatus() + ", getText()="
                + getText() + ", getCORAContext()=" + getCORAContext() + "]";
    }
    
}
