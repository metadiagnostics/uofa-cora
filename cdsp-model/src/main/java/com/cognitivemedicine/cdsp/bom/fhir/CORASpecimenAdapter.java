/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Specimen;
import org.hl7.fhir.dstu3.model.Specimen.SpecimenProcessingComponent;
import org.hl7.fhir.dstu3.model.Specimen.SpecimenStatus;

public class CORASpecimenAdapter extends CORACognitiveBaseIdentifiable<Specimen, ICORASpecimen>
        implements ICORASpecimen {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CORASpecimenAdapter() {
        this.adaptedClass = new Specimen();
    }

    public CORASpecimenAdapter(Specimen adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Specimen.SpecimenContainerComponent addContainer() {
        return adaptedClass.addContainer();
    }

    @Override
    public ICORASpecimen addContainer(Specimen.SpecimenContainerComponent param) {
        adaptedClass.addContainer(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORASpecimen addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Annotation addNote() {

        return adaptedClass.addNote();
    }

    @Override
    public ICORASpecimen addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }


    @Override
    public Specimen.SpecimenProcessingComponent addProcessing() {

        return adaptedClass.addProcessing();
    }

    @Override
    public ICORASpecimen addProcessing(SpecimenProcessingComponent param) {
        adaptedClass.addProcessing(param);
        return this;
    }

    @Override
    public Reference addRequest() {

        return adaptedClass.addRequest();
    }


    @Override
    public ICORASpecimen addRequest(Reference param) {
        adaptedClass.addRequest(param);
        return this;
    }

    @Override
    public CORASpecimenContainer addWrappedContainer() {
        Specimen.SpecimenContainerComponent item = new Specimen.SpecimenContainerComponent();
        adaptedClass.addContainer(item);
        return new CORASpecimenContainer(item);
    }


    @Override
    public ICORASpecimen addWrappedContainer(CORASpecimenContainer param) {
        if (param != null) {
            adaptedClass.addContainer(param.getAdaptee());
        }
        return this;
    }

    @Override
    public Identifier getAccessionIdentifier() {
        return adaptedClass.getAccessionIdentifier();
    }

    @Override
    public Specimen.SpecimenCollectionComponent getCollection() {
        return adaptedClass.getCollection();
    }

    @Override
    public List<Specimen.SpecimenContainerComponent> getContainer() {
        return adaptedClass.getContainer();
    }

    @Override
    public Specimen.SpecimenContainerComponent getContainerFirstRep() {
        return adaptedClass.getContainerFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<Annotation> getNote() {

        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {

        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public List<Specimen.SpecimenProcessingComponent> getProcessing() {
        return adaptedClass.getProcessing();
    }

    @Override
    public Specimen.SpecimenProcessingComponent getProcessingFirstRep() {
        return adaptedClass.getProcessingFirstRep();
    }

    @Override
    public Date getReceivedTime() {
        return adaptedClass.getReceivedTime();
    }

    @Override
    public DateTimeType getReceivedTimeElement() {
        return adaptedClass.getReceivedTimeElement();
    }

    @Override
    public List<Reference> getRequest() {

        return adaptedClass.getRequest();
    }


    @Override
    public Reference getRequestFirstRep() {

        return adaptedClass.getRequestFirstRep();
    }

    @Override
    public SpecimenStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<SpecimenStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }


    @Override
    public Reference getSubject() {

        return adaptedClass.getSubject();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public CodeableConcept getType() {
        return adaptedClass.getType();
    }

    @Override
    public List<CORASpecimenContainer> getWrappedContainer() {
        List<CORASpecimenContainer> items = new java.util.ArrayList<>();
        for (Specimen.SpecimenContainerComponent type : adaptedClass.getContainer()) {
            items.add(new CORASpecimenContainer(type));
        }
        return items;
    }

    @Override
    public CORASpecimenContainer getWrappedContainerFirstRep() {
        CORASpecimenContainer wrapperItem = new CORASpecimenContainer();
        Specimen.SpecimenContainerComponent item = adaptedClass.getContainerFirstRep();
        if (item != null) {
            wrapperItem = new CORASpecimenContainer(item);
        }
        return wrapperItem;
    }



    @Override
    public ICORASpecimen setAccessionIdentifier(Identifier param) {
        adaptedClass.setAccessionIdentifier(param);
        return this;
    }

    @Override
    public ICORASpecimen setCollection(Specimen.SpecimenCollectionComponent param) {
        adaptedClass.setCollection(param);
        return this;
    }

    @Override
    public ICORASpecimen setContainer(List<Specimen.SpecimenContainerComponent> param) {
        adaptedClass.setContainer(param);
        return this;
    }

    public ICORASpecimen setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public ICORASpecimen setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORASpecimen setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORASpecimen setProcessing(List<Specimen.SpecimenProcessingComponent> param) {
        adaptedClass.setProcessing(param);
        return this;
    }

    @Override
    public ICORASpecimen setReceivedTime(Date param) {
        adaptedClass.setReceivedTime(param);
        return this;
    }

    @Override
    public ICORASpecimen setReceivedTimeElement(DateTimeType param) {
        adaptedClass.setReceivedTimeElement(param);
        return this;
    }

    @Override
    public ICORASpecimen setRequest(List<Reference> param) {
        adaptedClass.setRequest(param);
        return this;
    }

    @Override
    public ICORASpecimen setStatus(SpecimenStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORASpecimen setStatus(String param) {
        adaptedClass.setStatus(SpecimenStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORASpecimen setStatusElement(Enumeration<SpecimenStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORASpecimen setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORASpecimen setType(CodeableConcept param) {
        adaptedClass.setType(param);
        return this;
    }

    @Override
    public ICORASpecimen setWrappedContainer(List<CORASpecimenContainer> param) {
        List<Specimen.SpecimenContainerComponent> items = new java.util.ArrayList<>();
        for (CORASpecimenContainer item : param) {
            items.add(item.getAdaptee());
        }
        adaptedClass.setContainer(items);
        return this;
    }

    @Override
    public String toString() {
        return "CORASpecimenAdapter [getAccessionIdentifier()=" + getAccessionIdentifier()
                + ", getCollection()=" + getCollection() + ", getContainer()=" + getContainer()
                + ", getIdentifier()=" + getIdentifier() + ", getNote()=" + getNote()
                + ", getProcessing()=" + getProcessing() + ", getReceivedTime()="
                + getReceivedTime() + ", getRequest()=" + getRequest() + ", getStatus()="
                + getStatus() + ", getSubject()=" + getSubject() + ", getText()=" + getText()
                + ", getType()=" + getType() + ", getPrimaryIdentifer()=" + getPrimaryIdentifer()
                + ", getCORAContext()=" + getCORAContext() + "]";
    }
    
    
}
