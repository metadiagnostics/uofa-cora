/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Immunization;
import org.hl7.fhir.dstu3.model.Immunization.ImmunizationPractitionerComponent;
import org.hl7.fhir.dstu3.model.Immunization.ImmunizationStatus;
import org.hl7.fhir.dstu3.model.Location;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.StringType;


public class CORAImmunizationAdapter
        extends CORACognitiveBaseIdentifiable<Immunization, ICORAImmunization>
        implements ICORAImmunization {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAImmunizationAdapter() {
        this.adaptedClass = new Immunization();
    }

    public CORAImmunizationAdapter(Immunization adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAImmunization addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public ImmunizationPractitionerComponent addPractitioner() {
        return adaptedClass.addPractitioner();
    }

    @Override
    public ICORAImmunization addPractitioner(ImmunizationPractitionerComponent param) {
        adaptedClass.addPractitioner(param);
        return this;
    }

    @Override
    public Immunization.ImmunizationReactionComponent addReaction() {

        return adaptedClass.addReaction();
    }

    @Override
    public ICORAImmunization addReaction(Immunization.ImmunizationReactionComponent param) {
        adaptedClass.addReaction(param);
        return this;
    }


    @Override
    public Immunization.ImmunizationVaccinationProtocolComponent addVaccinationProtocol() {
        return adaptedClass.addVaccinationProtocol();
    }

    @Override
    public ICORAImmunization addVaccinationProtocol(
            Immunization.ImmunizationVaccinationProtocolComponent param) {
        adaptedClass.addVaccinationProtocol(param);
        return this;
    }

    @Override
    public Date getDate() {
        return adaptedClass.getDate();
    }

    @Override
    public DateTimeType getDateElement() {
        return adaptedClass.getDateElement();
    }

    @Override
    public SimpleQuantity getDoseQuantity() {
        return adaptedClass.getDoseQuantity();
    }

    @Override
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getEncounter().getResource() instanceof Encounter) {
            CORAEncounterAdapter profiledType = new CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getEncounter().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Date getExpirationDate() {
        return adaptedClass.getExpirationDate();
    }


    @Override
    public DateType getExpirationDateElement() {
        return adaptedClass.getExpirationDateElement();
    }

    @Override
    public Immunization.ImmunizationExplanationComponent getExplanation() {
        return adaptedClass.getExplanation();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public CORALocationAdapter getLocationResource() {
        if (adaptedClass.getLocation().getResource() instanceof Location) {
            CORALocationAdapter profiledType = new CORALocationAdapter();
            profiledType.setAdaptee((Location) adaptedClass.getLocation().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public String getLotNumber() {
        return adaptedClass.getLotNumber();
    }


    @Override
    public StringType getLotNumberElement() {
        return adaptedClass.getLotNumberElement();
    }



    @Override
    public CORAOrganizationAdapter getManufacturerResource() {
        if (adaptedClass.getManufacturer().getResource() instanceof Organization) {
            CORAOrganizationAdapter profiledType = new CORAOrganizationAdapter();
            profiledType.setAdaptee((Organization) adaptedClass.getManufacturer().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public boolean getNotGiven() {
        return adaptedClass.getNotGiven();
    }



    @Override
    public BooleanType getNotGivenElement() {
        return adaptedClass.getNotGivenElement();
    }

    @Override
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getPatient().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getPatient().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<ImmunizationPractitionerComponent> getPractitioner() {
        return adaptedClass.getPractitioner();
    }

    @Override
    public ImmunizationPractitionerComponent getPractitionerFirstRep() {
        return adaptedClass.getPractitionerFirstRep();
    }

    @Override
    public boolean getPrimarySource() {
        return adaptedClass.getPrimarySource();
    }

    @Override
    public BooleanType getPrimarySourceElement() {
        return adaptedClass.getPrimarySourceElement();
    }

    @Override
    public List<Immunization.ImmunizationReactionComponent> getReaction() {
        return adaptedClass.getReaction();
    }

    @Override
    public Immunization.ImmunizationReactionComponent getReactionFirstRep() {
        return adaptedClass.getReactionFirstRep();
    }

    @Override
    public CodeableConcept getReportOrigin() {
        return adaptedClass.getReportOrigin();
    }

    @Override
    public CodeableConcept getRoute() {
        return adaptedClass.getRoute();
    }

    @Override
    public CodeableConcept getSite() {
        return adaptedClass.getSite();
    }

    @Override
    public ImmunizationStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<ImmunizationStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public List<Immunization.ImmunizationVaccinationProtocolComponent> getVaccinationProtocol() {
        return adaptedClass.getVaccinationProtocol();
    }

    @Override
    public Immunization.ImmunizationVaccinationProtocolComponent getVaccinationProtocolFirstRep() {
        return adaptedClass.getVaccinationProtocolFirstRep();
    }

    @Override
    public CodeableConcept getVaccineCode() {
        return adaptedClass.getVaccineCode();
    }

    @Override
    public ICORAImmunization setDate(Date param) {
        adaptedClass.setDate(param);
        return this;
    }


    @Override
    public ICORAImmunization setDateElement(DateTimeType param) {
        adaptedClass.setDateElement(param);
        return this;
    }

    @Override
    public ICORAImmunization setDoseQuantity(SimpleQuantity param) {
        adaptedClass.setDoseQuantity(param);
        return this;
    }

    @Override
    public ICORAImmunization setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getEncounter().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAImmunization setExpirationDate(Date param) {
        adaptedClass.setExpirationDate(param);
        return this;
    }

    @Override
    public ICORAImmunization setExpirationDateElement(DateType param) {
        adaptedClass.setExpirationDateElement(param);
        return this;
    }

    @Override
    public ICORAImmunization setExplanation(Immunization.ImmunizationExplanationComponent param) {
        adaptedClass.setExplanation(param);
        return this;
    }

    @Override
    public ICORAImmunization setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAImmunization setLocationResource(CORALocationAdapter param) {
        adaptedClass.getLocation().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAImmunization setLotNumber(String param) {
        adaptedClass.setLotNumber(param);
        return this;
    }

    @Override
    public ICORAImmunization setLotNumberElement(StringType param) {
        adaptedClass.setLotNumberElement(param);
        return this;
    }

    @Override
    public ICORAImmunization setManufacturerResource(CORAOrganizationAdapter param) {
        adaptedClass.getManufacturer().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAImmunization setNotGiven(boolean param) {
        adaptedClass.setNotGiven(param);
        return this;
    }

    @Override
    public ICORAImmunization setNotGivenElement(BooleanType param) {
        adaptedClass.setNotGivenElement(param);
        return this;
    }

    @Override
    public ICORAImmunization setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getPatient().setResource(param.getAdaptee());
        return this;
    }


    @Override
    public ICORAImmunization setPractitioner(List<ImmunizationPractitionerComponent> param) {
        adaptedClass.setPractitioner(param);
        return this;
    }

    @Override
    public ICORAImmunization setPrimarySource(boolean param) {
        adaptedClass.setPrimarySource(param);
        return this;
    }

    @Override
    public ICORAImmunization setPrimarySourceElement(BooleanType param) {
        adaptedClass.setPrimarySourceElement(param);
        return this;
    }

    @Override
    public ICORAImmunization setReaction(List<Immunization.ImmunizationReactionComponent> param) {
        adaptedClass.setReaction(param);
        return this;
    }

    @Override
    public ICORAImmunization setReportOrigin(CodeableConcept param) {
        adaptedClass.setReportOrigin(param);
        return this;
    }

    @Override
    public ICORAImmunization setRoute(CodeableConcept param) {
        adaptedClass.setRoute(param);
        return this;
    }

    @Override
    public ICORAImmunization setSite(CodeableConcept param) {
        adaptedClass.setSite(param);
        return this;
    }

    @Override
    public ICORAImmunization setStatus(ImmunizationStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAImmunization setStatusElement(Enumeration<ImmunizationStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAImmunization setVaccinationProtocol(
            List<Immunization.ImmunizationVaccinationProtocolComponent> param) {
        adaptedClass.setVaccinationProtocol(param);
        return this;
    }

    @Override
    public ICORAImmunization setVaccineCode(CodeableConcept param) {
        adaptedClass.setVaccineCode(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAImmunizationAdapter [getDate()=" + getDate() + ", getDoseQuantity()="
                + getDoseQuantity() + ", getExpirationDate()=" + getExpirationDate()
                + ", getExplanation()=" + getExplanation() + ", getIdentifier()=" + getIdentifier()
                + ", getLocationResource()=" + getLocationResource() + ", getLotNumber()="
                + getLotNumber() + ", getManufacturerResource()=" + getManufacturerResource()
                + ", getNotGiven()=" + getNotGiven() + ", getNotGivenElement()="
                + getNotGivenElement() + ", getPractitioner()=" + getPractitioner()
                + ", getPrimarySource()=" + getPrimarySource() + ", getReaction()=" + getReaction()
                + ", getReportOrigin()=" + getReportOrigin() + ", getRoute()=" + getRoute()
                + ", getSite()=" + getSite() + ", getStatus()=" + getStatus() + ", getText()="
                + getText() + ", getVaccinationProtocol()=" + getVaccinationProtocol()
                + ", getVaccineCode()=" + getVaccineCode() + ", getPrimaryIdentifer()="
                + getPrimaryIdentifer() + ", getCORAContext()=" + getCORAContext() + "]";
    }
    
    
}
