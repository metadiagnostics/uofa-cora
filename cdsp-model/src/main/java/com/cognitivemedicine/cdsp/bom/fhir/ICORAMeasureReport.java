/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

/**
 *  Strawman for The Status of a specific measure on a particular axis.
 * 
 * @author Jerry Goodnough
 *
 */
public interface ICORAMeasureReport extends ICORACognitiveBaseIdentifiable<ICORAMeasureReport> {

    public enum ApplicablityStatus {
        No,
        Unknown,
        Yes
    };

    // status
    // type
    // measure
    // patient
    // date
    // reportingOrganization
    // period
    // group
    //
    // ?evaluatedResources
    // +ApplicablityStatus applicablity
    // +List<CodeableConcept> applicablityDetail
    // +boolean meet
    // +List<CodeableConcept> reasonsMeet;
    // +List<CodeableConcept> reasonsNotMeet;
    // +Identifier reportingAgent


    // Note: overlap between report and outcome here - Secondary analysis

}
