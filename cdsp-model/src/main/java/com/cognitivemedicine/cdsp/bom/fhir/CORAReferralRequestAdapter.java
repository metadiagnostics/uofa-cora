/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.ReferralRequest;
import org.hl7.fhir.dstu3.model.ReferralRequest.ReferralCategory;
import org.hl7.fhir.dstu3.model.ReferralRequest.ReferralPriority;
import org.hl7.fhir.dstu3.model.ReferralRequest.ReferralRequestStatus;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Timing;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

public class CORAReferralRequestAdapter
        extends CORACognitiveBaseIdentifiable<ReferralRequest, ICORAReferralRequest>
        implements ICORAReferralRequest {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CORAReferralRequestAdapter() {
        this.adaptedClass = new ReferralRequest();
    }

    public CORAReferralRequestAdapter(ReferralRequest adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {

        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORAReferralRequest addBasedOn(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }


    @Override
    public Reference addDefinition() {

        return adaptedClass.addDefinition();
    }

    @Override
    public ICORAReferralRequest addDefinition(Reference param) {
        adaptedClass.addBasedOn(param);
        return this;
    }


    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAReferralRequest addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public Annotation addNote() {

        return adaptedClass.addNote();
    }

    @Override
    public ICORAReferralRequest addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    @Override
    public CodeableConcept addReasonCode() {

        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAReferralRequest addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {

        return adaptedClass.addReasonReference();
    }



    @Override
    public ICORAReferralRequest addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public Reference addRelevantHistory() {

        return adaptedClass.addRelevantHistory();
    }

    @Override
    public ICORAReferralRequest addRelevantHistory(Reference param) {
        adaptedClass.addRelevantHistory(param);
        return this;
    }

    @Override
    public Reference addReplaces() {

        return adaptedClass.addReplaces();
    }

    @Override
    public ICORAReferralRequest addReplaces(Reference param) {
        adaptedClass.addReplaces(param);
        return this;
    }

    @Override
    public Reference addSupportingInfo() {

        return adaptedClass.addSupportingInfo();
    }



    @Override
    public ICORAReferralRequest addSupportingInfo(Reference param) {
        adaptedClass.addSupportingInfo(param);
        return this;
    }

    @Override
    public Date getAuthoredOn() {
        return adaptedClass.getAuthoredOn();
    }

    @Override
    public DateTimeType getAuthoredOnElement() {
        return adaptedClass.getAuthoredOnElement();
    }

    @Override
    public List<Reference> getBasedOn() {

        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {

        return adaptedClass.getBasedOnFirstRep();
    }

    @Override
    public Reference getContext() {
        return adaptedClass.getContext();
    }


    @Override
    public List<Reference> getDefinition() {

        return adaptedClass.getDefinition();
    }

    @Override
    public Reference getDefinitionFirstRep() {

        return adaptedClass.getDefinitionFirstRep();
    }


    @Override
    public String getDescription() {
        return adaptedClass.getDescription();
    }

    @Override
    public StringType getDescriptionElement() {
        return adaptedClass.getDescriptionElement();
    }

    @Override
    @LogicalModelAlias("getContext")
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getContext().getResource() instanceof Encounter) {
            CORAEncounterAdapter profiledType = new CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getContext().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public Identifier getGroupIdentifer() {

        return adaptedClass.getGroupIdentifier();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public ReferralCategory getIntent() {
        return adaptedClass.getIntent();
    }

    @Override
    public List<Annotation> getNote() {

        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {

        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public Type getOccurrence() {
        return adaptedClass.getOccurrence();
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public Date getOccurrenceDateTime() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof DateTimeType)) {
            DateTimeType out = (DateTimeType) adaptedClass.getOccurrence();
            return out.getValue();
        } else {
            return null;
        }
    }

    @Override
    @LogicalModelAlias("getOccurrence")
    public DateTimeType getOccurrenceDateTimeElement() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof DateTimeType)) {
            return (DateTimeType) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public Period getOccurrencePeriod() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof Period)) {
            return (Period) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @LogicalModelAlias("getOccurrence")
    @Override
    public Timing getOccurrenceTiming() {
        if ((adaptedClass.getOccurrence() != null)
                && (adaptedClass.getOccurrence() instanceof Timing)) {
            return (Timing) adaptedClass.getOccurrence();
        } else {
            return null;
        }
    }

    @Override
    @LogicalModelAlias("getSubject")
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getSubject().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public ReferralPriority getPriority() {
        return adaptedClass.getPriority();
    }

    @Override
    public List<CodeableConcept> getReasonCode() {

        return adaptedClass.getReasonCode();
    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {

        return adaptedClass.getReasonCodeFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {

        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstRep() {

        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public CodeableConcept getReasonRefused() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/referralrequest-reasonRefused");
        if ((extensions == null) || (extensions.size() <= 0)) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonRefused");
        }
    }

    @Override
    public List<Reference> getRelevantHistory() {

        return adaptedClass.getRelevantHistory();
    }

    @Override
    public Reference getRelevantHistoryFirstRep() {

        return adaptedClass.getRelevantHistoryFirstRep();
    }

    @Override
    public List<Reference> getReplaces() {

        return adaptedClass.getReplaces();
    }

    @Override
    public Reference getReplacesFirstRep() {

        return adaptedClass.getReplacesFirstRep();
    }

    @Override
    public CodeableConcept getSpecialty() {
        return adaptedClass.getSpecialty();
    }

    @Override
    public ReferralRequestStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<ReferralRequestStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Reference getSubject() {

        return adaptedClass.getSubject();
    }

    @Override
    public List<Reference> getSupportingInfo() {

        return adaptedClass.getSupportingInfo();
    }

    @Override
    public Reference getSupportingInfoFirstRep() {

        return adaptedClass.getSupportingInfoFirstRep();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }

    @Override
    public CodeableConcept getType() {
        return adaptedClass.getType();
    }

    @Override
    public ICORAReferralRequest setAuthoredOn(Date param) {
        adaptedClass.setAuthoredOn(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setAuthoredOnElement(DateTimeType param) {
        adaptedClass.setAuthoredOnElement(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setDefinition(List<Reference> param) {
        adaptedClass.setDefinition(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setDescription(String param) {
        adaptedClass.setDescription(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setDescriptionElement(StringType param) {
        adaptedClass.setDescriptionElement(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setContext")
    public ICORAReferralRequest setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getContext().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAReferralRequest setGroupIdentifer(Identifier param) {
        adaptedClass.setGroupIdentifier(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setIntent(ReferralCategory param) {
        adaptedClass.setIntent(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setOccurrence(Type param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setOccurrenceDate(Date param) {
        adaptedClass.setOccurrence(new DateTimeType(param));
        return this;
    }

    @Override
    public ICORAReferralRequest setOccurrenceDateTimeElement(DateTimeType param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setOccurrencePeriod(Period param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setOccurrenceTiming(Timing param) {
        adaptedClass.setOccurrence(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setSubject")
    public ICORAReferralRequest setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getSubject().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAReferralRequest setPriority(ReferralPriority param) {
        adaptedClass.setPriority(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setReasonRefused(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/referralrequest-reasonRefused")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setRelevantHistory(List<Reference> param) {
        adaptedClass.setRelevantHistory(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setReplaces(List<Reference> param) {
        adaptedClass.setReplaces(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setSpecialty(CodeableConcept param) {
        adaptedClass.setSpecialty(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setStatus(ReferralRequestStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setStatus(String param) {
        adaptedClass.setStatus(ReferralRequestStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAReferralRequest setStatusElement(Enumeration<ReferralRequestStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setSupportingInfo(List<Reference> param) {
        adaptedClass.setSupportingInfo(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public ICORAReferralRequest setType(CodeableConcept param) {
        adaptedClass.setType(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAReferralRequestAdapter [getAuthoredOn()=" + getAuthoredOn() + ", getBasedOn()="
                + getBasedOn() + ", getContext()=" + getContext() + ", getDefinition()="
                + getDefinition() + ", getDescription()=" + getDescription()
                + ", getGroupIdentifer()=" + getGroupIdentifer() + ", getIdentifier()="
                + getIdentifier() + ", getNote()=" + getNote() + ", getOccurrence()="
                + getOccurrence() + ", getPriority()=" + getPriority() + ", getReasonCode()="
                + getReasonCode() + ", getReasonReference()=" + getReasonReference()
                + ", getReasonRefused()=" + getReasonRefused() + ", getRelevantHistory()="
                + getRelevantHistory() + ", getReplaces()=" + getReplaces() + ", getSpecialty()="
                + getSpecialty() + ", getStatus()=" + getStatus() + ", getSubject()=" + getSubject()
                + ", getSupportingInfo()=" + getSupportingInfo() + ", getText()=" + getText()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }


}
