/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DecimalType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestIntent;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestPriority;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestRequesterComponent;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestStatus;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Timing;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.FHIRExtension;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * DTSU3/HAPI2.4/BOM ProcedureRequest (Replaces order)
 * @author Jerry Goodnough
 *
 */

public interface ICORAProcedureRequest
        extends ICORACognitiveBaseIdentifiable<ICORAProcedureRequest> {

    public Reference addBasedOn();

    public ICORAProcedureRequest addBasedOn(Reference param);

    public CodeableConcept addCategory();

    public ICORAProcedureRequest addCategory(CodeableConcept param);

    public Reference addDefinition();

    public ICORAProcedureRequest addDefinition(Reference param);

    public Annotation addNote();

    public ICORAProcedureRequest addNote(Annotation param);

    public ICORAProcedureRequest addPrecondition(CodeableConcept param);

    public CodeableConcept addReasonCode();

    public ICORAProcedureRequest addReasonCode(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAProcedureRequest addReasonReference(Reference param);

    public Reference addRelevantHistory();

    public ICORAProcedureRequest addRelevantHistory(Reference param);

    public Reference addReplaces();

    public ICORAProcedureRequest addReplaces(Reference param);

    public Reference addSpecimen();

    public ICORAProcedureRequest addSpecimen(Reference param);

    public Reference addSupportingInfo();

    public ICORAProcedureRequest addSupportingInfo(Reference param);


    public List<Reference> getApproachBodySite();


    @FHIRExtension(name = "QICore-procedurerequest-appropriatenessScore",
            url = "http://hl7.org/fhir/StructureDefinition/QICore-procedurerequest-appropriatenessScore")
    public DecimalType getAppropriatenessScore();

    public Boolean getAsNeededBoolean();

    public BooleanType getAsNeededBooleanElement();

    public CodeableConcept getAsNeededCodeableConcept();

    public Date getAuthoredOn();

    public DateTimeType getAuthoredOnElement();

    public List<Reference> getBasedOn();

    public Reference getBasedOnFirstRep();

    public List<CodeableConcept> getCategory();

    public CodeableConcept getCategoryFirstRep();

    public CodeableConcept getCode();

    public Reference getContext();

    public List<Reference> getDefinition();

    public Reference getDefinitionFirstRep();

    public boolean getDoNotPerform();

    public BooleanType getDotNotPerformElement();

    public CORAEncounterAdapter getEncounterResource();

    public ProcedureRequestIntent getIntent();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public Type getOccurrence();
    @LogicalModelAlias("getOccurrence")
    public Date getOccurrenceDateTime();
    @LogicalModelAlias("getOccurrence")
    public DateTimeType getOccurrenceDateTimeElement();
    @LogicalModelAlias("getOccurrence")
    public Period getOccurrencePeriod();
    @LogicalModelAlias("getOccurrence")
    public Timing getOccurrenceTiming();

    public Reference getPerformer();

    public CodeableConcept getPerformerType();


    public List<CodeableConcept> getPrecondition();

    public ProcedureRequestPriority getPriority();

    public Enumeration<ProcedureRequestPriority> getPriorityElement();

    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<Reference> getReasonReference();

    public Reference getReasonReferenceFirstRep();

    @FHIRExtension(name = "procedurerequest-reasonRefused",
            url = "http://hl7.org/fhir/StructureDefinition/procedurerequest-reasonRefused")
    public CodeableConcept getReasonRefused();

    @FHIRExtension(name = "procedurerequest-procedurequest-reasonRejected",
            url = "http://hl7.org/fhir/StructureDefinition/procedurequest-reasonRejected")
    public CodeableConcept getReasonRejected();

    public List<Reference> getRelevantHistory();


    public Reference getRelevantHistoryFirstRep();

    public List<Reference> getReplaces();

    public Reference getReplacesFirstRep();

    // Reference Type: Performer
    public ProcedureRequestRequesterComponent getRequester();

    public Identifier getRequisition();

    @LogicalModelAlias("getOccurrence")
    public Date getScheduledDateTime();

    @LogicalModelAlias("getOccurrence")
    public DateTimeType getScheduledDateTimeElement();

    @LogicalModelAlias("getOccurrence")
    public Period getScheduledPeriod();

    @LogicalModelAlias("getOccurrence")
    public Timing getScheduledTiming();

    public List<Reference> getSpecimen();

    public Reference getSpecimenFirstRep();

    public ProcedureRequestStatus getStatus();

    public Enumeration<ProcedureRequestStatus> getStatusElement();

    public Reference getSubject();

    public List<Reference> getSupportingInfo();

    public Reference getSupportingInfoFirstRep();

    public ICORAProcedureRequest setApproachBodySite(List<Reference> param);

    @FHIRExtension(name = "QICore-procedurerequest-appropriatenessScore",
            url = "http://hl7.org/fhir/StructureDefinition/QICore-procedurerequest-appropriatenessScore")
    public ICORAProcedureRequest setAppropriatenessScore(DecimalType param);

    public ICORAProcedureRequest setAsNeededBoolean(Boolean param);

    public ICORAProcedureRequest setAsNeededBoolean(BooleanType param);

    public ICORAProcedureRequest setAsNeededCodeableConcept(CodeableConcept param);

    public ICORAProcedureRequest setAuthoredOn(Date param);

    public ICORAProcedureRequest setAuthoredOnElement(DateTimeType param);

    public ICORAProcedureRequest setBasedOn(List<Reference> param);

    public ICORAProcedureRequest setCategory(List<CodeableConcept> param);

    public ICORAProcedureRequest setCode(CodeableConcept param);

    public ICORAProcedureRequest setContext(Reference param);

    public ICORAProcedureRequest setDefinition(List<Reference> param);

    public ICORAProcedureRequest setDoNotPerform(boolean param);

    public ICORAProcedureRequest setDoNotPerformElement(BooleanType param);

    public ICORAProcedureRequest setEncounterResource(CORAEncounterAdapter param);

    public ICORAProcedureRequest setIntent(ProcedureRequestIntent param);

    public ICORAProcedureRequest setNote(List<Annotation> param);

    public ICORAProcedureRequest setOccurrence(Type param);

    public ICORAProcedureRequest setOccurrenceDate(Date param);

    public ICORAProcedureRequest setOccurrenceDateTimeElement(DateTimeType param);

    public ICORAProcedureRequest setOccurrencePeriod(Period param);

    public ICORAProcedureRequest setOccurrenceTiming(Timing param);

    public ICORAProcedureRequest setPerformer(Reference param);

    public ICORAProcedureRequest setPerformerType(CodeableConcept param);

    public ICORAProcedureRequest setPrecondition(List<CodeableConcept> param);

    public ICORAProcedureRequest setPriority(ProcedureRequestPriority param);

    public ICORAProcedureRequest setPriority(String param);

    public ICORAProcedureRequest setPriorityElement(Enumeration<ProcedureRequestPriority> param);

    public ICORAProcedureRequest setReasonCode(List<CodeableConcept> param);

    public ICORAProcedureRequest setReasonReference(List<Reference> param);

    @FHIRExtension(name = "procedurerequest-reasonRefused",
            url = "http://hl7.org/fhir/StructureDefinition/procedurerequest-reasonRefused")
    public ICORAProcedureRequest setReasonRefused(CodeableConcept param);

    @FHIRExtension(name = "procedurerequest-procedurequest-reasonRejected",
            url = "http://hl7.org/fhir/StructureDefinition/procedurequest-reasonRejected")
    public ICORAProcedureRequest setReasonRejected(CodeableConcept param);

    public ICORAProcedureRequest setRelevantHistory(List<Reference> param);

    public ICORAProcedureRequest setReplaces(List<Reference> param);

    public ICORAProcedureRequest setRequester(ProcedureRequestRequesterComponent param);

    public ICORAProcedureRequest setRequisition(Identifier param);

    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledDateTime(Date param);

    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledDateTimeElement(DateTimeType param);

    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledPeriod(Period param);

    @LogicalModelAlias("setOccurrence")
    public ICORAProcedureRequest setScheduledTiming(Timing param);

    public ICORAProcedureRequest setSpecimen(List<Reference> param);

    public ICORAProcedureRequest setStatus(ProcedureRequestStatus param);

    public ICORAProcedureRequest setStatus(String param);

    public ICORAProcedureRequest setStatusElement(Enumeration<ProcedureRequestStatus> param);

    public ICORAProcedureRequest setSubject(Reference param);

    public ICORAProcedureRequest setSupportingInfo(List<Reference> param);

}
