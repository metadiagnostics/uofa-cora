/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.IntegerType;
import org.hl7.fhir.dstu3.model.Location;
import org.hl7.fhir.dstu3.model.MedicationDispense;
import org.hl7.fhir.dstu3.model.MedicationDispense.MedicationDispensePerformerComponent;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;



public class CORAMedicationDispenseAdapter extends CORACognitiveBaseIdentifiable<MedicationDispense, ICORAMedicationDispense>
		implements ICORAMedicationDispense {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CORAMedicationDispenseAdapter() {
		this.adaptedClass = new MedicationDispense();
	}

	public CORAMedicationDispenseAdapter(MedicationDispense adaptee) {
		this.adaptedClass = adaptee;
	}

	@Override
    public Reference addAuthorizingPrescription()
    {
        return adaptedClass.addAuthorizingPrescription();
    }

	@Override
    public ICORAMedicationDispense addAuthorizingPrescription(Reference param)
    {
        adaptedClass.addAuthorizingPrescription(param);
        return this;
    }

	@Override
    public Reference addDetectedIssue()
    {
        return adaptedClass.addDetectedIssue();
    }

	@Override
    public ICORAMedicationDispense addDetectedIssue(Reference param)
    {
        adaptedClass.addDetectedIssue(param);
        return this;
    }


	@Override
    public Dosage addDosageInstruction() {
		return adaptedClass.addDosageInstruction();
	}

	@Override
    public ICORAMedicationDispense addDosageInstruction(Dosage param) {
		adaptedClass.addDosageInstruction(param);
		return this;
	}

	
	@Override
    public Reference addEventHistory()
    {
        return adaptedClass.addEventHistory();
    }

	@Override
    public ICORAMedicationDispense addEventHistory(Reference param)
    {
        adaptedClass.addEventHistory(param);
        return this;
    }



    @Override
    public Identifier addIdentifier() {
        return adaptedClass.addIdentifier();
    }

	

	@Override
    public ICORAMedicationDispense addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

	@Override
    public Annotation addNote()
    {
        return adaptedClass.addNote();
    }

	@Override
    public ICORAMedicationDispense addNote(Annotation param)
    {
        adaptedClass.addNote(param);
        return this;
    }


	@Override
    public Reference addPartOf()
    {
        return adaptedClass.addPartOf();
    }

	@Override
    public ICORAMedicationDispense addPartOf(Reference param)
    {
        adaptedClass.addPartOf(param);
        return this;
    }

	@Override
    public MedicationDispensePerformerComponent addPerformer()
    {
        return adaptedClass.addPerformer();
    }

	@Override
    public ICORAMedicationDispense addPerformer(MedicationDispensePerformerComponent param)
    {
        adaptedClass.addPerformer(param);
        return this;
    }

	@Override
    public Reference addSupportingInformation()
    {
        return adaptedClass.addSupportingInformation();
    }

	@Override
    public ICORAMedicationDispense addSupportingInformation(Reference param)
    {
        adaptedClass.addSupportingInformation(param);
        return this;
    }

	@Override
    public CORADosage addWrappedDosageInstruction() {
	    Dosage item = new Dosage();
		adaptedClass.addDosageInstruction(item);
		return new com.cognitivemedicine.cdsp.bom.fhir.CORADosage(item);
	}

	@Override
    public ICORAMedicationDispense addWrappedDosageInstruction(CORADosage param) {
		if (param != null) {
			adaptedClass.addDosageInstruction(param.getAdaptee());
		}
		return this;
	}

	@Override
    public List<Reference> getAuthorizingPrescription()
    {
        return adaptedClass.getAuthorizingPrescription();
    }

	@Override
    public Reference getAuthorizingPrescriptionFirstRep()
    {
        return adaptedClass.getAuthorizingPrescriptionFirstRep();
    }

	@Override
    public CodeableConcept getCategory()
    {
        return adaptedClass.getCategory();
    }

	@Override
    public Reference getContext()
    {
        return adaptedClass.getContext();
    }

	@Override
    public SimpleQuantity getDaysSupply() {
		return adaptedClass.getDaysSupply();
	}

	@Override
    public CORALocationAdapter getDestinationResource() {
		if (adaptedClass.getDestination().getResource() instanceof Location) {
			CORALocationAdapter profiledType = new CORALocationAdapter();
			profiledType.setAdaptee(
					(Location) adaptedClass.getDestination().getResource());
			return profiledType;
		} else {
			return null;
		}
	}

	@Override
    public List<Reference> getDetectedIssue()
    {
        return adaptedClass.getDetectedIssue();
    }


	@Override
    public Reference getDetectedIssueFirstRep()
    {
        return adaptedClass.getDetectedIssueFirstRep();
    }

	@Override
    public List<Dosage> getDosageInstruction() {
		return adaptedClass.getDosageInstruction();
	}


	@Override
    public Dosage getDosageInstructionFirstRep() {
		return adaptedClass.getDosageInstructionFirstRep();
	}

	@Override
    public List<Reference> getEventHistory()
    {
        return adaptedClass.getEventHistory();
    }

	@Override
    public Reference getEventHistoryFirstRep()
    {
        return adaptedClass.getEventHistoryFirstRep();
    }


	@Override
    public List<Identifier> getIdentifier() {
		return adaptedClass.getIdentifier();
	}

	@Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

	@Override
    public CodeableConcept getMedicationCodeableConcept() {
		if (adaptedClass.getMedication() != null
				&& adaptedClass.getMedication() instanceof CodeableConcept) {
			return (CodeableConcept) adaptedClass.getMedication();
		} else {
			return null;
		}
	}

	@Override
    public Reference getMedicationReference() {
		if (adaptedClass.getMedication() != null
				&& adaptedClass.getMedication() instanceof Reference) {
			return (Reference) adaptedClass.getMedication();
		} else {
			return null;
		}
	}

	@Override
    public boolean getNotDone()
    {
        return adaptedClass.getNotDone();
    }

	@Override
    public BooleanType getNotDoneElement()
    {
        return adaptedClass.getNotDoneElement();
    }

	@Override
    public Type getNotDoneReason()
    {
        return adaptedClass.getNotDoneReason();
    }

	@Override
    public List<Annotation> getNote() {
		return adaptedClass.getNote();
	}

	@Override
    public Annotation getNoteFirstRep()
    {
        return adaptedClass.getNoteFirstRep();
    }

	@Override
    public List<Reference> getPartOf()
    {
        return adaptedClass.getPartOf();
    }


	@Override
    public Reference getPartOfFirstRep()
    {
        return adaptedClass.getPartOfFirstRep();
    }

	@Override
    @LogicalModelAlias("getSubject")
	public CORAPatientAdapter getPatientResource() {
		if (adaptedClass.getSubject().getResource() instanceof Patient) {
			CORAPatientAdapter profiledType = new CORAPatientAdapter();
			profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
			return profiledType;
		} else {
			return null;
		}
	}

	@Override
    public List<MedicationDispensePerformerComponent> getPerformer()
    {
        return adaptedClass.getPerformer();
    }

	@Override
    public MedicationDispensePerformerComponent getPerformerFirstRep()
    {
        return adaptedClass.getPerformerFirstRep();
    }

	@Override
    public SimpleQuantity getQuantity() {
		return adaptedClass.getQuantity();
	}

	@Override
    public IntegerType getRefillsRemaining() {
		List<Extension> extensions = adaptedClass
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-refillsRemaining");
		if (extensions == null || extensions.size() <= 0) {
			return null;
		} else if (extensions.size() == 1) {
			return (IntegerType) extensions.get(0).getValue();
		} else {
			throw new RuntimeException("More than one extension exists for refillsRemaining");
		}
	}

	@Override
    public MedicationDispense.MedicationDispenseStatus getStatus() {
		return adaptedClass.getStatus();
	}


    @Override
    public Enumeration<MedicationDispense.MedicationDispenseStatus> getStatusElement() {
		return adaptedClass.getStatusElement();
	}

    @Override
    public Reference getSubject()
    {
        return adaptedClass.getSubject();
    }

    @Override
    public MedicationDispense.MedicationDispenseSubstitutionComponent getSubstitution() {
		return adaptedClass.getSubstitution();
	}

    @Override
    public List<Reference> getSupportingInformation()
    {
        return adaptedClass.getSupportingInformation();
    }
    @Override
    public Reference getSupportingInformationFirstRep()
    {
        return adaptedClass.getSupportingInformationFirstRep();
    }
    @Override
    public Narrative getText() {
		return adaptedClass.getText();
	}
    
    @Override
    public CodeableConcept getType() {
		return adaptedClass.getType();
	}
    @Override
    public Period getValidityPeriod() {
		List<Extension> extensions = adaptedClass.getExtensionsByUrl(
				"http://hl7.org/fhir/StructureDefinition/medicationdispense-validityPeriod");
		if (extensions == null || extensions.size() <= 0) {
			return null;
		} else if (extensions.size() == 1) {
			return (Period) extensions.get(0).getValue();
		} else {
			throw new RuntimeException("More than one extension exists for validityPeriod");
		}
	}
    
    @Override
    public Date getWhenHandedOver() {
		return adaptedClass.getWhenHandedOver();
	}
    @Override
    public DateTimeType getWhenHandedOverElement() {
		return adaptedClass.getWhenHandedOverElement();
	}
    
    @Override
    public Date getWhenPrepared() {
		return adaptedClass.getWhenPrepared();
	}
    @Override
    public DateTimeType getWhenPreparedElement() {
		return adaptedClass.getWhenPreparedElement();
	}
    @Override
    public List<CORADosage> getWrappedDosageInstruction() {
		List<CORADosage> items = new java.util.ArrayList<>();
		for (Dosage type : adaptedClass
				.getDosageInstruction()) {
			items.add(new CORADosage(type));
		}
		return items;
	}
    @Override
    public CORADosage getWrappedDosageInstructionFirstRep() {
		CORADosage wrapperItem = new com.cognitivemedicine.cdsp.bom.fhir.CORADosage();
		Dosage item = adaptedClass
				.getDosageInstructionFirstRep();
		if (item != null) {
			wrapperItem = new CORADosage(item);
		}
		return wrapperItem;
	}
    @Override
    public ICORAMedicationDispense setAuthorizingPrescription(List<Reference> param)
    {
        adaptedClass.setAuthorizingPrescription(param);
        return this;
    }
    
    @Override
    public ICORAMedicationDispense setCategory(CodeableConcept param)
    {
        adaptedClass.setCategory(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setContext(Reference param)
    {
        adaptedClass.setContext(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setDaysSupply(SimpleQuantity param) {
		adaptedClass.setDaysSupply(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setDestinationResource(CORALocationAdapter param) {
		adaptedClass.getDestination().setResource(param.getAdaptee());
		return this;
	}
    @Override
    public ICORAMedicationDispense setDetectedIssue(List<Reference> param)
    {
        adaptedClass.setDetectedIssue(param);
        return this;
    }
    
    @Override
    public ICORAMedicationDispense setDosageInstruction(List<Dosage> param) {
		adaptedClass.setDosageInstruction(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setEventHistory(List<Reference> param)
    {
        adaptedClass.setEventHistory(param);
        return this;
    }
    public ICORAMedicationDispense setId(IdType param) {
		adaptedClass.setId(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setIdentifier(List<Identifier> param) {
		adaptedClass.setIdentifier(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setMedicationCodeableConcept(CodeableConcept param) {
		adaptedClass.setMedication(param);
		return this;
	}
    
    @Override
    public ICORAMedicationDispense setMedicationReference(Reference param) {
		adaptedClass.setMedication(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setNotDone(boolean param)
    {
        adaptedClass.setNotDone(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setNotDoneElement(BooleanType param)
    {
        adaptedClass.setNotDoneElement(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setNotDoneReason(Type param)
    {
        adaptedClass.setNotDoneReason(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setNote(List<Annotation>  param) {
		adaptedClass.setNote(param);
		return this;
	}
    
    @Override
    public ICORAMedicationDispense setPartOf(List<Reference> param)
    {
        adaptedClass.setPartOf(param);
        return this;
    }
    @Override
    @LogicalModelAlias("setPatient")
	public ICORAMedicationDispense setPatientResource(CORAPatientAdapter param) {
		adaptedClass.getSubject().setResource(param.getAdaptee());
		return this;
	}
    @Override
    public ICORAMedicationDispense setPerformer(List<MedicationDispensePerformerComponent> param)
    {
        adaptedClass.setPerformer(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setQuantity(SimpleQuantity param) {
		adaptedClass.setQuantity(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setRefillsRemaining(IntegerType param) {
		adaptedClass.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-refillsRemaining").setValue(param);
		return this;
	}
    
    @Override
    public ICORAMedicationDispense setStatus(MedicationDispense.MedicationDispenseStatus param) {
		adaptedClass.setStatus(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setStatus(String param) {
		adaptedClass.setStatus(MedicationDispense.MedicationDispenseStatus.valueOf(param));
		return this;
	}
    @Override
    public ICORAMedicationDispense setStatusElement(Enumeration<MedicationDispense.MedicationDispenseStatus> param) {
		adaptedClass.setStatusElement(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setSubject(Reference param)
    {
        adaptedClass.setSubject(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setSubstitution(MedicationDispense.MedicationDispenseSubstitutionComponent param) {
		adaptedClass.setSubstitution(param);
		return this;
	}
    
    
    @Override
    public ICORAMedicationDispense setSupportingInformation(List<Reference> param)
    {
        adaptedClass.setSupportingInformation(param);
        return this;
    }
    @Override
    public ICORAMedicationDispense setType(CodeableConcept param) {
		adaptedClass.setType(param);
		return this;
	}
    
    @Override
    public ICORAMedicationDispense setValidityPeriod(Period param) {
		adaptedClass.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/medicationdispense-validityPeriod").setValue(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setWhenHandedOver(Date param) {
		adaptedClass.setWhenHandedOver(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setWhenHandedOverElement(DateTimeType param) {
		adaptedClass.setWhenHandedOverElement(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setWhenPrepared(Date param) {
		adaptedClass.setWhenPrepared(param);
		return this;
	}
    
    @Override
    public ICORAMedicationDispense setWhenPreparedElement(DateTimeType param) {
		adaptedClass.setWhenPreparedElement(param);
		return this;
	}
    @Override
    public ICORAMedicationDispense setWrappedDosageInstruction(
			List<CORADosage> param) {
		List<Dosage> items = new java.util.ArrayList<>();
		for (CORADosage item : param) {
			items.add(item.getAdaptee());
		}
		adaptedClass.setDosageInstruction(items);
		return this;
	}
    
  
}