/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.ReferralRequest;
import org.hl7.fhir.dstu3.model.ReferralRequest.ReferralCategory;
import org.hl7.fhir.dstu3.model.ReferralRequest.ReferralPriority;
import org.hl7.fhir.dstu3.model.ReferralRequest.ReferralRequestStatus;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Timing;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * DTSU3/HAPI2.4/BOM ReferralRequest/Order
 * 
 * @author Jerry Goodnough
 *
 */
public interface ICORAReferralRequest extends ICORACognitiveBaseIdentifiable<ICORAReferralRequest> {
    public Reference addBasedOn();

    public ICORAReferralRequest addBasedOn(Reference param);

    public Reference addDefinition();

    public ICORAReferralRequest addDefinition(Reference param);

    public Annotation addNote();

    public ICORAReferralRequest addNote(Annotation param);

    public CodeableConcept addReasonCode();


    public ICORAReferralRequest addReasonCode(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAReferralRequest addReasonReference(Reference param);

    public Reference addRelevantHistory();

    public ICORAReferralRequest addRelevantHistory(Reference param);

    public Reference addReplaces();

    public ICORAReferralRequest addReplaces(Reference param);

    public Reference addSupportingInfo();


    public ICORAReferralRequest addSupportingInfo(Reference param);

    public Date getAuthoredOn();

    public DateTimeType getAuthoredOnElement();


    public List<Reference> getBasedOn();

    public Reference getBasedOnFirstRep();


    public Reference getContext();

    public List<Reference> getDefinition();

    public Reference getDefinitionFirstRep();

    public String getDescription();

    public StringType getDescriptionElement();

    public CORAEncounterAdapter getEncounterResource();

    public Identifier getGroupIdentifer();

    public ReferralCategory getIntent();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public Type getOccurrence();

    @LogicalModelAlias("getOccurrence")
    public Date getOccurrenceDateTime();

    @LogicalModelAlias("getOccurrence")
    public DateTimeType getOccurrenceDateTimeElement();

    @LogicalModelAlias("getOccurrence")
    public Period getOccurrencePeriod();

    @LogicalModelAlias("getOccurrence")
    public Timing getOccurrenceTiming();

    public CORAPatientAdapter getPatientResource();

    public ReferralPriority getPriority();

    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<Reference> getReasonReference();

    public Reference getReasonReferenceFirstRep();

    public CodeableConcept getReasonRefused();

    public List<Reference> getRelevantHistory();

    public Reference getRelevantHistoryFirstRep();

    public List<Reference> getReplaces();

    public Reference getReplacesFirstRep();

    public CodeableConcept getSpecialty();

    public ReferralRequestStatus getStatus();

    public Enumeration<ReferralRequestStatus> getStatusElement();

    public Reference getSubject();


    public List<Reference> getSupportingInfo();

    public Reference getSupportingInfoFirstRep();

    public CodeableConcept getType();

    public void setAdaptee(ReferralRequest param);

    public ICORAReferralRequest setAuthoredOn(Date param);

    public ICORAReferralRequest setAuthoredOnElement(DateTimeType param);

    public ICORAReferralRequest setBasedOn(List<Reference> param);

    public ICORAReferralRequest setContext(Reference param);

    public ICORAReferralRequest setDefinition(List<Reference> param);

    public ICORAReferralRequest setDescription(String param);

    public ICORAReferralRequest setDescriptionElement(StringType param);

    public ICORAReferralRequest setEncounterResource(CORAEncounterAdapter param);

    public ICORAReferralRequest setGroupIdentifer(Identifier param);

    public ICORAReferralRequest setIntent(ReferralCategory param);

    public ICORAReferralRequest setNote(List<Annotation> param);

    public ICORAReferralRequest setOccurrence(Type param);

    public ICORAReferralRequest setOccurrenceDate(Date param);

    public ICORAReferralRequest setOccurrenceDateTimeElement(DateTimeType param);

    public ICORAReferralRequest setOccurrencePeriod(Period param);

    public ICORAReferralRequest setOccurrenceTiming(Timing param);

    public ICORAReferralRequest setPatientResource(CORAPatientAdapter param);

    public ICORAReferralRequest setPriority(ReferralPriority param);

    public ICORAReferralRequest setReasonCode(List<CodeableConcept> param);

    public ICORAReferralRequest setReasonReference(List<Reference> param);

    public ICORAReferralRequest setReasonRefused(CodeableConcept param);

    public ICORAReferralRequest setRelevantHistory(List<Reference> param);

    public ICORAReferralRequest setReplaces(List<Reference> param);

    public ICORAReferralRequest setSpecialty(CodeableConcept param);

    public ICORAReferralRequest setStatus(ReferralRequestStatus param);

    public ICORAReferralRequest setStatus(String param);
    
    public ICORAReferralRequest setStatusElement(Enumeration<ReferralRequestStatus> param);
    
    public ICORAReferralRequest setSubject(Reference param);
    
    public ICORAReferralRequest setSupportingInfo(List<Reference> param);
    
    public ICORAReferralRequest setType(CodeableConcept param);
    
}
