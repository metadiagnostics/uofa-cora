/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Ratio;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Timing;
import org.hl7.fhir.exceptions.FHIRException;

public class CORADosage implements ICORADosage {

    private Dosage adaptedClass = null;

    public CORADosage() {
        this.adaptedClass = new Dosage();
    }

    public CORADosage(Dosage adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Dosage getAdaptee() {
        return adaptedClass;
    }

    @Override
    public List<CodeableConcept> getAdditionalInstructions() {
        return adaptedClass.getAdditionalInstruction();
    }

    @Override
    public Boolean getAsNeededBoolean() {
        if (adaptedClass.getAsNeeded() != null
                && adaptedClass.getAsNeeded() instanceof BooleanType) {
            return ((BooleanType) adaptedClass.getAsNeeded()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public BooleanType getAsNeededBooleanElement() {
        if (adaptedClass.getAsNeeded() != null
                && adaptedClass.getAsNeeded() instanceof BooleanType) {
            return (BooleanType) adaptedClass.getAsNeeded();
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getAsNeededCodeableConcept() {
        if (adaptedClass.getAsNeeded() != null
                && adaptedClass.getAsNeeded() instanceof CodeableConcept) {
            return (CodeableConcept) adaptedClass.getAsNeeded();
        } else {
            return null;
        }
    }

    @Override
    public SimpleQuantity getDoseQuantity() {
        if (adaptedClass.getDose() != null && adaptedClass.getDose() instanceof SimpleQuantity) {
            return (SimpleQuantity) adaptedClass.getDose();
        } else {
            return null;
        }
    }

    @Override
    public Range getDoseRange() {
        if (adaptedClass.getDose() != null && adaptedClass.getDose() instanceof Range) {
            return (Range) adaptedClass.getDose();
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getDoseType() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-doseType");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for doseType");
        }
    }

    @Override
    public String getId() {
        return adaptedClass.getId();
    }

    @Override
    public Extension getInfuseOver() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-infuseOver");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Extension) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for infuseOver");
        }
    }

    @Override
    public Ratio getMaxDeliveryRate() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-maxDeliveryRate");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Ratio) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for maxDeliveryRate");
        }
    }

    @Override
    public Quantity getMaxDeliveryVolume() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-maxDeliveryVolume");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Quantity) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for maxDeliveryVolume");
        }
    }

    @Override
    public SimpleQuantity getMaxDosePerAdministration() {
        return adaptedClass.getMaxDosePerAdministration();
    }

    @Override
    public SimpleQuantity getMaxDosePerLifetime() {
        return adaptedClass.getMaxDosePerLifetime();

    }

    @Override
    public Ratio getMaxDosePerPeriod() {
        return adaptedClass.getMaxDosePerPeriod();
    }

    @Override
    public CodeableConcept getMethod() {
        return adaptedClass.getMethod();
    }

    @Override
    public Ratio getMinDosePerPeriod() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-minDosePerPeriod");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Ratio) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for minDosePerPeriod");
        }
    }

    @Override
    public String getPatientInstructions() {
        return adaptedClass.getPatientInstruction();
    }

    @Override
    public Ratio getRateGoal() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-rateGoal");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Ratio) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for rateGoal");
        }
    }

    @Override
    public Ratio getRateIncrement() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-rateIncrement");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Ratio) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for rateIncrement");
        }
    }

    @Override
    public Extension getRateIncrementInterval() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/pharmacy-core-rateIncrementInterval");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Extension) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for rateIncrementInterval");
        }
    }

    @Override
    public SimpleQuantity getRateQuantity() {
        SimpleQuantity out = null;
        try {
            out = adaptedClass.getRateSimpleQuantity();
        } catch (FHIRException e) {
            // Eat the exception
        }
        return out;
    }

    @Override
    public Range getRateRange() {
        if (adaptedClass.getRate() != null && adaptedClass.getRate() instanceof Range) {
            return (Range) adaptedClass.getRate();
        } else {
            return null;
        }
    }

    @Override
    public Ratio getRateRatio() {
        if (adaptedClass.getRate() != null && adaptedClass.getRate() instanceof Ratio) {
            return (Ratio) adaptedClass.getRate();
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getRoute() {
        return adaptedClass.getRoute();
    }

    @Override
    public int getSequence() {
        return adaptedClass.getSequence();
    }

    @Override
    public CodeableConcept getSiteCodeableConcept() {
        if (adaptedClass.getSite() != null && adaptedClass.getSite() instanceof CodeableConcept) {
            return adaptedClass.getSite();
        } else {
            return null;
        }
    }

    @Override
    public CodeableConcept getSiteReference() {
        return adaptedClass.getSite();
    }

    @Override
    public String getText() {
        return adaptedClass.getText();
    }

    @Override
    public StringType getTextElement() {
        return adaptedClass.getTextElement();
    }

    @Override
    public Timing getTiming() {
        return adaptedClass.getTiming();
    }

    @Override
    public void setAdaptee(Dosage param) {
        this.adaptedClass = param;
    }

    @Override
    public CORADosage setAdditionalInstructions(List<CodeableConcept> param) {
        adaptedClass.setAdditionalInstruction(param);
        return this;
    }

    @Override
    public CORADosage setAsNeededBoolean(boolean param) {
        BooleanType t = new BooleanType(param);
        adaptedClass.setAsNeeded(t);
        return this;
    }

    public CORADosage setAsNeededBoolean(Boolean param) {
        adaptedClass.setAsNeeded(new BooleanType(param));
        return this;
    }

    @Override
    public CORADosage setAsNeededBoolean(BooleanType param) {
        adaptedClass.setAsNeeded(param);
        return this;
    }

    @Override
    public CORADosage setAsNeededCodeableConcept(CodeableConcept param) {
        adaptedClass.setAsNeeded(param);
        return this;
    }

    @Override
    public CORADosage setDoseQuantity(SimpleQuantity param) {
        adaptedClass.setDose(param);
        return this;
    }

    @Override
    public CORADosage setDoseRange(Range param) {
        adaptedClass.setDose(param);
        return this;
    }

    @Override
    public CORADosage setDoseType(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-doseType")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setId(String param) {
        adaptedClass.setId(param);
        return this;
    }

    @Override
    public CORADosage setInfuseOver(Extension param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-infuseOver")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setMaxDeliveryRate(Ratio param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-maxDeliveryRate")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setMaxDeliveryVolume(Quantity param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-maxDeliveryVolume")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setMaxDosePerAdministration(SimpleQuantity param) {
        adaptedClass.setMaxDosePerAdministration(param);
        return this;
    }

    @Override
    public CORADosage setMaxDosePerLifetime(SimpleQuantity param) {
        adaptedClass.setMaxDosePerLifetime(param);
        return this;
    }

    @Override
    public CORADosage setMaxDosePerPeriod(Ratio param) {
        adaptedClass.setMaxDosePerPeriod(param);
        return this;
    }

    @Override
    public CORADosage setMethod(CodeableConcept param) {
        adaptedClass.setMethod(param);
        return this;
    }

    @Override
    public CORADosage setMinDosePerPeriod(Ratio param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-minDosePerPeriod")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setPatientInstructions(String param) {
        adaptedClass.setPatientInstruction(param);
        return this;
    }

    @Override
    public CORADosage setRateGoal(Ratio param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-rateGoal")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setRateIncrement(Ratio param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-rateIncrement")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setRateIncrementInterval(Extension param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/pharmacy-core-rateIncrementInterval")
                .setValue(param);
        return this;
    }

    @Override
    public CORADosage setRateQuantity(SimpleQuantity param) {
        adaptedClass.setRate(param);
        return this;
    }

    @Override
    public CORADosage setRateRange(Range param) {
        adaptedClass.setRate(param);
        return this;
    }

    @Override
    public CORADosage setRateRatio(Ratio param) {
        adaptedClass.setRate(param);
        return this;
    }

    @Override
    public CORADosage setRoute(CodeableConcept param) {
        adaptedClass.setRoute(param);
        return this;
    }

    @Override
    public CORADosage setSequence(int param) {
        adaptedClass.setSequence(param);
        return this;
    }

    @Override
    public CORADosage setSite(CodeableConcept param) {
        adaptedClass.setSite(param);
        return this;
    }

    @Override
    public CORADosage setSiteCodeableConcept(CodeableConcept param) {
        adaptedClass.setSite(param);
        return this;
    }

    @Override
    public CORADosage setText(String param) {
        adaptedClass.setText(param);
        return this;
    }

    public CORADosage setText(StringType param) {
        adaptedClass.setTextElement(param);
        return this;
    }

    @Override
    public CORADosage setTextElement(StringType param) {
        adaptedClass.setTextElement(param);
        return this;
    }

    @Override
    public CORADosage setTiming(Timing param) {
        adaptedClass.setTiming(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORADosage [getAdditionalInstructions()=" + getAdditionalInstructions()
                + ", getAsNeededBoolean()=" + getAsNeededBoolean()
                + ", getAsNeededCodeableConcept()=" + getAsNeededCodeableConcept()
                + ", getDoseQuantity()=" + getDoseQuantity() + ", getDoseType()=" + getDoseType()
                + ", getId()=" + getId() + ", getInfuseOver()=" + getInfuseOver()
                + ", getMaxDeliveryRate()=" + getMaxDeliveryRate() + ", getMaxDeliveryVolume()="
                + getMaxDeliveryVolume() + ", getMaxDosePerAdministration()="
                + getMaxDosePerAdministration() + ", getMaxDosePerLifetime()="
                + getMaxDosePerLifetime() + ", getMaxDosePerPeriod()=" + getMaxDosePerPeriod()
                + ", getMethod()=" + getMethod() + ", getMinDosePerPeriod()="
                + getMinDosePerPeriod() + ", getPatientInstructions()=" + getPatientInstructions()
                + ", getRateGoal()=" + getRateGoal() + ", getRateIncrement()=" + getRateIncrement()
                + ", getRateIncrementInterval()=" + getRateIncrementInterval()
                + ", getRateQuantity()=" + getRateQuantity() + ", getRateRange()=" + getRateRange()
                + ", getRateRatio()=" + getRateRatio() + ", getRoute()=" + getRoute()
                + ", getSequence()=" + getSequence() + ", getSiteCodeableConcept()="
                + getSiteCodeableConcept() + ", getSiteReference()=" + getSiteReference()
                + ", getText()=" + getText() + ", getTiming()=" + getTiming() + "]";
    }
    
    
}
