/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Age;
import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.Condition.ConditionClinicalStatus;
import org.hl7.fhir.dstu3.model.Condition.ConditionVerificationStatus;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * ConditionInterface
 * 
 * @author Jerry Goodnough
 * @version  DSTU3 Verified
 */

public interface ICORACondition extends ICORACognitiveBaseIdentifiable<ICORACondition> {


    public CodeableConcept addCategory();

    public ICORACondition addCategory(CodeableConcept param);

    public Condition.ConditionEvidenceComponent addEvidence();

    public ICORACondition addEvidence(Condition.ConditionEvidenceComponent param);

    public Annotation addNote();

    public ICORACondition addNote(Annotation param);

    public Type getAbatement();

    public Age getAbatementAge();

    public Boolean getAbatementBoolean();

    public BooleanType getAbatementBooleanElement();

    public Date getAbatementDateTime();

    public DateTimeType getAbatementDateTimeElement();

    public Period getAbatementPeriod();

    public Quantity getAbatementQuantity();

    public Range getAbatementRange();

    public String getAbatementString();

    public StringType getAbatementStringElement();

    public Date getAssertedDate();

    public DateTimeType getAssertedDateElement();

    public List<CodeableConcept> getCategory();

    public CodeableConcept getCategoryFirstRep();

    public ConditionClinicalStatus getClinicalStatus();

    public Enumeration<ConditionClinicalStatus> getClinicalStatusElement();



    public CodeableConcept getCode();

    public Reference getContext();

    public CodeableConcept getCriticality();

    @LogicalModelAlias("getAssertedDate")
    public Date getDateRecorded();

    @LogicalModelAlias("getAssertedDateElement")
    public DateTimeType getDateRecordedElement();

    public CORAEncounterAdapter getEncounterResource();

    public List<Condition.ConditionEvidenceComponent> getEvidence();

    public Condition.ConditionEvidenceComponent getEvidenceFirstRep();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public Age getOnsetAge();

    public Date getOnsetDateTime();

    public DateTimeType getOnsetDateTimeElement();

    public Period getOnsetPeriod();

    public Quantity getOnsetQuantity();

    public Range getOnsetRange();

    public String getOnsetString();

    public StringType getOnsetStringElement();

    @LogicalModelAlias("getSubectTarget")
    public CORAPatientAdapter getPatientResource();

    public CodeableConcept getSeverity();

    public Condition.ConditionStageComponent getStage();

    public Reference getSubject();

    public Resource getSubjectTarget();

    public ConditionVerificationStatus getVerificationStatus();

    public Enumeration<ConditionVerificationStatus> getVerificationStatusElement();

    public ICORACondition setAbatement(Type param);

    @LogicalModelAlias("setOnset")
    public ICORACondition setAbatementAge(Age param);

    public ICORACondition setAbatementBoolean(Boolean param);

    public ICORACondition setAbatementBoolean(BooleanType param);

    public ICORACondition setAbatementDateTime(Date param);

    public ICORACondition setAbatementDateTime(DateTimeType param);

    public ICORACondition setAbatementPeriod(Period param);

    public ICORACondition setAbatementQuantity(Quantity param);

    public ICORACondition setAbatementRange(Range param);

    public ICORACondition setAbatementString(String param);

    public ICORACondition setAbatementString(StringType param);

    public ICORACondition setAssertedDate(Date param);

    public ICORACondition setAssertedDateElement(DateTimeType param);

    public ICORACondition setCategory(List<CodeableConcept> param);

    public ICORACondition setClinicalStatus(ConditionClinicalStatus param);

    public ICORACondition setClinicalStatusElement(Enumeration<ConditionClinicalStatus> param);

    public ICORACondition setCode(CodeableConcept param);

    public ICORACondition setContext(Reference param);

    public ICORACondition setCriticality(CodeableConcept param);

    @LogicalModelAlias("setAssertedDate")
    public ICORACondition setDateRecorded(Date param);

    @LogicalModelAlias("setAssertedDateElement")
    public ICORACondition setDateRecordedElement(DateTimeType param);

    public ICORACondition setEncounterResource(CORAEncounterAdapter param);

    public ICORACondition setEvidence(List<Condition.ConditionEvidenceComponent> param);

    public ICORACondition setNote(List<Annotation> param);

    public ICORACondition setOnset(Type param);

    @LogicalModelAlias("setOnset")
    public ICORACondition setOnsetAge(Age param);

    public ICORACondition setOnsetDateTime(Date param);

    public ICORACondition setOnsetDateTime(DateTimeType param);

    public ICORACondition setOnsetPeriod(Period param);

    public ICORACondition setOnsetQuantity(Quantity param);

    public ICORACondition setOnsetRange(Range param);

    public ICORACondition setOnsetString(String param);

    public ICORACondition setOnsetStringElement(StringType param);

    @LogicalModelAlias("setSubjectTarget")
    public ICORACondition setPatientResource(CORAPatientAdapter param);

    public ICORACondition setSeverity(CodeableConcept param);

    public ICORACondition setStage(Condition.ConditionStageComponent param);

    public ICORACondition setSubject(Reference param);

    public ICORACondition setSubjectTarget(Resource param);

    public ICORACondition setVerificationStatus(ConditionVerificationStatus param);

    public ICORACondition setVerificationStatusElement(
            Enumeration<ConditionVerificationStatus> param);

}
