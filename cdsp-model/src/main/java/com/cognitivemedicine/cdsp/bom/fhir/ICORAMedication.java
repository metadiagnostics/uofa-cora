/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Medication;


public interface ICORAMedication extends ICORACognitiveBase<ICORAMedication> {

    public Attachment addImage();

    public ICORAMedication addImage(Attachment param);

    public Medication.MedicationIngredientComponent addIngredient();

    public ICORAMedication addIngredient(Medication.MedicationIngredientComponent param);

    public CodeableConcept getCode();

    public CodeableConcept getForm();

    public List<Attachment> getImage();

    public Attachment getImageFirstRep();

    public List<Medication.MedicationIngredientComponent> getIngredient();

    public Medication.MedicationIngredientComponent getIngredientFirstRep();

    public Boolean getIsBrand();

    public BooleanType getIsBrandElement();

    public boolean getIsOverTheCounter();

    public BooleanType getIsOverTheCounterElement();

    public CORAOrganizationAdapter getManufacturerResource();

    public Medication.MedicationPackageComponent getPackage();

    public Medication.MedicationStatus getStatus();

    public Enumeration<Medication.MedicationStatus> getStatusElement();

    public ICORAMedication setCode(CodeableConcept param);

    public ICORAMedication setForm(CodeableConcept param);

    public ICORAMedication setImage(List<Attachment> param);

    public ICORAMedication setIngredient(List<Medication.MedicationIngredientComponent> param);

    public ICORAMedication setIsBrand(boolean param);

    public ICORAMedication setIsBrandElement(BooleanType param);

    public ICORAMedication setIsOverTheCounter(boolean param);

    public ICORAMedication setIsOverTheCounterElement(BooleanType param);

    public ICORAMedication setManufacturerResource(CORAOrganizationAdapter param);

    public ICORAMedication setPackage(Medication.MedicationPackageComponent param);

    public ICORAMedication setStatus(Medication.MedicationStatus param);

    public ICORAMedication setStatusElement(Enumeration<Medication.MedicationStatus> param);


}
