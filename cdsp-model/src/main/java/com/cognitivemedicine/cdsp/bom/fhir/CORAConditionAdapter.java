/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Age;
import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.Condition.ConditionClinicalStatus;
import org.hl7.fhir.dstu3.model.Condition.ConditionVerificationStatus;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.exceptions.FHIRException;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;

/**
 * Adapted for HAPI Condition
 * 
 * @author Jerry Goodnough
 * @version DSTU3 Verified
 */

public class CORAConditionAdapter extends CORACognitiveBaseIdentifiable<Condition, ICORACondition>
        implements ICORACondition {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAConditionAdapter() {
        this.adaptedClass = new Condition();
    }

    public CORAConditionAdapter(Condition adaptee) {
        this.adaptedClass = adaptee;
    }

    public CodeableConcept addCategory() {
        return adaptedClass.addCategory();
    }

    public ICORACondition addCategory(CodeableConcept param) {
        adaptedClass.addCategory(param);
        return this;
    }

    public Condition.ConditionEvidenceComponent addEvidence() {
        Condition.ConditionEvidenceComponent item = new Condition.ConditionEvidenceComponent();
        adaptedClass.addEvidence(item);
        return item;
    }

    public ICORACondition addEvidence(Condition.ConditionEvidenceComponent param) {
        adaptedClass.addEvidence(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        return adaptedClass.addIdentifier();
    }

    @Override
    public ICORACondition addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    public Annotation addNote() {
        return adaptedClass.addNote();
    }

    public ICORACondition addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    public Type getAbatement() {
        return adaptedClass.getAbatement();
    }

    public Age getAbatementAge() {
        Age out = null;
        try {
            out = adaptedClass.getAbatementAge();
        } catch (FHIRException e) {
            // Transparent conversion failure.
        }
        return out;
    }

    public Boolean getAbatementBoolean() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof BooleanType) {
            return ((BooleanType) adaptedClass.getAbatement()).getValue();
        } else {
            return null;
        }
    }

    public BooleanType getAbatementBooleanElement() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof BooleanType) {
            return (BooleanType) adaptedClass.getAbatement();
        } else {
            return null;
        }
    }

    public Date getAbatementDateTime() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getAbatement()).getValue();
        } else {
            return null;
        }
    }

    public DateTimeType getAbatementDateTimeElement() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getAbatement();
        } else {
            return null;
        }
    }

    public Period getAbatementPeriod() {
        if (adaptedClass.getAbatement() != null && adaptedClass.getAbatement() instanceof Period) {
            return (Period) adaptedClass.getAbatement();
        } else {
            return null;
        }
    }

    public Quantity getAbatementQuantity() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof Quantity) {
            return (Quantity) adaptedClass.getAbatement();
        } else {
            return null;
        }
    }

    public Range getAbatementRange() {
        if (adaptedClass.getAbatement() != null && adaptedClass.getAbatement() instanceof Range) {
            return (Range) adaptedClass.getAbatement();
        } else {
            return null;
        }
    }

    public String getAbatementString() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof StringType) {
            return ((StringType) adaptedClass.getAbatement()).getValue();
        } else {
            return null;
        }
    }

    public StringType getAbatementStringElement() {
        if (adaptedClass.getAbatement() != null
                && adaptedClass.getAbatement() instanceof StringType) {
            return (StringType) adaptedClass.getAbatement();
        } else {
            return null;
        }
    }

    public Date getAssertedDate() {
        return adaptedClass.getAssertedDate();
    }

    public DateTimeType getAssertedDateElement() {
        return adaptedClass.getAssertedDateElement();
    }

    public List<CodeableConcept> getCategory() {
        return adaptedClass.getCategory();
    }

    public CodeableConcept getCategoryFirstRep() {
        return adaptedClass.getCategoryFirstRep();
    }

    public ConditionClinicalStatus getClinicalStatus() {
        return adaptedClass.getClinicalStatus();
    }

    public Enumeration<ConditionClinicalStatus> getClinicalStatusElement() {
        return adaptedClass.getClinicalStatusElement();
    }

    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }

    public Reference getContext() {
        return adaptedClass.getContext();
    }

    public CodeableConcept getCriticality() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/condition-criticality");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for criticality");
        }
    }

    @LogicalModelAlias("getAssertedDate")
    public Date getDateRecorded() {
        return adaptedClass.getAssertedDate();
    }

    @LogicalModelAlias("getAssertedDateElement")
    public DateTimeType getDateRecordedElement() {
        return adaptedClass.getAssertedDateElement();
    }

    @Override
    public CORAEncounterAdapter getEncounterResource() {
        CORAEncounterAdapter out = null;

        Resource ctx = adaptedClass.getContextTarget();
        if ((ctx != null) && (ctx instanceof Encounter)) {
            out = new CORAEncounterAdapter((Encounter) ctx);
        }
        return out;
    }

    public List<Condition.ConditionEvidenceComponent> getEvidence() {
        return adaptedClass.getEvidence();
    }

    public Condition.ConditionEvidenceComponent getEvidenceFirstRep() {
        return adaptedClass.getEvidenceFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    public Age getOnsetAge() {
        Age out = null;
        try {
            out = adaptedClass.getOnsetAge();
        } catch (FHIRException e) {
            // Transparent conversion failure.
            e.printStackTrace();
        }
        return out;
    }

    public Date getOnsetDateTime() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getOnset()).getValue();
        } else {
            return null;
        }
    }

    public DateTimeType getOnsetDateTimeElement() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getOnset();
        } else {
            return null;
        }
    }

    public Period getOnsetPeriod() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof Period) {
            return (Period) adaptedClass.getOnset();
        } else {
            return null;
        }
    }

    public Quantity getOnsetQuantity() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof Quantity) {
            return (Quantity) adaptedClass.getOnset();
        } else {
            return null;
        }
    }

    public Range getOnsetRange() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof Range) {
            return (Range) adaptedClass.getOnset();
        } else {
            return null;
        }
    }

    public String getOnsetString() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof StringType) {
            return ((StringType) adaptedClass.getOnset()).getValue();
        } else {
            return null;
        }
    }

    public StringType getOnsetStringElement() {
        if (adaptedClass.getOnset() != null && adaptedClass.getOnset() instanceof StringType) {
            return (StringType) adaptedClass.getOnset();
        } else {
            return null;
        }
    }

    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getSubjectTarget() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubjectTarget());
            return profiledType;
        } else {
            return null;
        }
    }

    public CodeableConcept getSeverity() {
        return adaptedClass.getSeverity();
    }


    public Condition.ConditionStageComponent getStage() {
        return adaptedClass.getStage();
    }

    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    public Resource getSubjectTarget() {
        return adaptedClass.getSubjectTarget();
    }


    public Narrative getText() {
        return adaptedClass.getText();
    }


    public ConditionVerificationStatus getVerificationStatus() {
        return adaptedClass.getVerificationStatus();
    }

    public Enumeration<ConditionVerificationStatus> getVerificationStatusElement() {
        return adaptedClass.getVerificationStatusElement();
    }

    public ICORACondition setAbatement(Type param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    @LogicalModelAlias("setOnset")
    public ICORACondition setAbatementAge(Age param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    public ICORACondition setAbatementBoolean(Boolean param) {
        adaptedClass.setAbatement(new BooleanType(param));
        return this;
    }

    public ICORACondition setAbatementBoolean(BooleanType param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    public ICORACondition setAbatementDateTime(Date param) {
        adaptedClass.setAbatement(new DateTimeType(param));
        return this;
    }

    public ICORACondition setAbatementDateTime(DateTimeType param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    public ICORACondition setAbatementPeriod(Period param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    public ICORACondition setAbatementQuantity(Quantity param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    public ICORACondition setAbatementRange(Range param) {
        adaptedClass.setAbatement(param);
        return this;
    }

    public ICORACondition setAbatementString(String param) {
        adaptedClass.setAbatement(new StringType(param));
        return this;
    }

    public ICORACondition setAbatementString(StringType param) {
        adaptedClass.setAbatement(param);
        return this;
    }


    public ICORACondition setAssertedDate(Date param) {
        adaptedClass.setAssertedDate(param);
        return this;
    }

    public ICORACondition setAssertedDateElement(DateTimeType param) {
        adaptedClass.setAssertedDateElement(param);
        return this;
    }

    public ICORACondition setCategory(List<CodeableConcept> param) {
        adaptedClass.setCategory(param);
        return this;
    }

    public ICORACondition setClinicalStatus(ConditionClinicalStatus param) {
        adaptedClass.setClinicalStatus(param);
        return this;
    }

    public ICORACondition setClinicalStatusElement(Enumeration<ConditionClinicalStatus> param) {
        adaptedClass.setClinicalStatusElement(param);
        return this;
    }

    public ICORACondition setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    public ICORACondition setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    public ICORACondition setCriticality(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/condition-criticality")
                .setValue(param);
        return this;
    }

    @LogicalModelAlias("setAssertedDate")
    public ICORACondition setDateRecorded(Date param) {
        adaptedClass.setAssertedDate(param);
        return this;
    }

    @LogicalModelAlias("setAssertedDateElement")
    public ICORACondition setDateRecordedElement(DateTimeType param) {
        adaptedClass.setAssertedDateElement(param);
        return this;
    }

    @Override
    public ICORACondition setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.setContextTarget(param.adaptedClass);
        return this;
    }

    public ICORACondition setEvidence(List<Condition.ConditionEvidenceComponent> param) {
        adaptedClass.setEvidence(param);
        return this;
    }

    @Override
    public ICORACondition setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    public ICORACondition setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    public ICORACondition setOnset(Type param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @LogicalModelAlias("setOnset")
    public ICORACondition setOnsetAge(Age param) {
        adaptedClass.setOnset(param);
        return this;
    }

    public ICORACondition setOnsetDateTime(Date param) {
        adaptedClass.setOnset(new DateTimeType(param));
        return this;
    }

    public ICORACondition setOnsetDateTime(DateTimeType param) {
        adaptedClass.setOnset(param);
        return this;
    }

    public ICORACondition setOnsetPeriod(Period param) {
        adaptedClass.setOnset(param);
        return this;
    }

    public ICORACondition setOnsetQuantity(Quantity param) {
        adaptedClass.setOnset(param);
        return this;
    }

    public ICORACondition setOnsetRange(Range param) {
        adaptedClass.setOnset(param);
        return this;
    }

    public ICORACondition setOnsetString(String param) {
        adaptedClass.setOnset(new StringType(param));
        return this;
    }

    public ICORACondition setOnsetString(StringType param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @Override
    public ICORACondition setOnsetStringElement(StringType param) {
        adaptedClass.setOnset(param);
        return this;
    }

    public ICORACondition setPatientResource(CORAPatientAdapter param) {
        adaptedClass.setSubjectTarget(param.getAdaptee());
        return this;
    }


    public ICORACondition setSeverity(CodeableConcept param) {
        adaptedClass.setSeverity(param);
        return this;
    }

    public ICORACondition setStage(Condition.ConditionStageComponent param) {
        adaptedClass.setStage(param);
        return this;
    }



    public ICORACondition setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    public ICORACondition setSubjectTarget(Resource param) {
        adaptedClass.setSubjectTarget(param);
        return this;
    }

    public ICORACondition setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    public ICORACondition setVerificationStatus(ConditionVerificationStatus param) {
        adaptedClass.setVerificationStatus(param);
        return this;
    }

    public ICORACondition setVerificationStatusElement(
            Enumeration<ConditionVerificationStatus> param) {
        adaptedClass.setVerificationStatusElement(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORAConditionAdapter [getAbatement()=" + getAbatement() + ", getAssertedDate()="
                + getAssertedDate() + ", getCategory()=" + getCategory() + ", getClinicalStatus()="
                + getClinicalStatus() + ", getCode()=" + getCode() + ", getContext()="
                + getContext() + ", getCriticality()=" + getCriticality() + ", getDateRecorded()="
                + getDateRecorded() + ", getEvidence()=" + getEvidence() + ", getIdentifier()="
                + getIdentifier() + ", getNote()=" + getNote() + ", getOnsetQuantity()="
                + getOnsetQuantity() + ", getSeverity()=" + getSeverity() + ", getStage()="
                + getStage() + ", getSubject()=" + getSubject() + ", getText()=" + getText()
                + ", getVerificationStatus()=" + getVerificationStatus()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }
    
    
}
