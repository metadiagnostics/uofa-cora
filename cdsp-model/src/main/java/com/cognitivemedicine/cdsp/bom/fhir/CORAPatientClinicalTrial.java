/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.StringType;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;

public class CORAPatientClinicalTrial {

	private Extension adaptedClass = null;
	public static final String uri = "http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial";
	private Extension rootObjectExtension = new Extension( uri);

	public CORAPatientClinicalTrial() {
		this.adaptedClass = new Extension();
	}

	public CORAPatientClinicalTrial(Extension adaptee) {
		this.adaptedClass = adaptee;
	}

	public Extension getAdaptee() {
		return adaptedClass;
	}

	public void setAdaptee(Extension param) {
		this.adaptedClass = param;
	}

	public CodeableConcept getReason() {
		CodeableConcept returnValue;
		java.util.List<Extension> extensions = rootObjectExtension
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#reason");
		if (extensions.size() == 1) {
			returnValue = (CodeableConcept) extensions.get(0).getValue();
		} else if (extensions.size() == 0) {
			returnValue = null;
		} else {
			throw new IllegalStateException("More than one extension specified for this object.");
		}
		return returnValue;
	}

	public CORAPatientClinicalTrial setReason(CodeableConcept param) {
		java.util.List<Extension> extensions = rootObjectExtension
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#reason");
		if (extensions.size() == 1) {
			extensions.get(0).setValue(param);
		} else if (extensions.size() == 0) {
			rootObjectExtension.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#reason").setValue(param);
		} else {
			throw new IllegalStateException("More than one extension specified for this object.");
		}
		return this;
	}

	public Period getPeriod() {
		Period returnValue;
		java.util.List<Extension> extensions = rootObjectExtension
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#period");
		if (extensions.size() == 1) {
			returnValue = (Period) extensions.get(0).getValue();
		} else if (extensions.size() == 0) {
			returnValue = null;
		} else {
			throw new IllegalStateException("More than one extension specified for this object.");
		}
		return returnValue;
	}

	public CORAPatientClinicalTrial setPeriod(Period param) {
		java.util.List<Extension> extensions = rootObjectExtension
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#period");
		if (extensions.size() == 1) {
			extensions.get(0).setValue(param);
		} else if (extensions.size() == 0) {
				rootObjectExtension.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#period").setValue(param);
		} else {
			throw new IllegalStateException("More than one extension specified for this object.");
		}
		return this;
	}

	public StringType getNCT() {
		StringType returnValue;
		java.util.List<Extension> extensions = rootObjectExtension
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#NCT");
		if (extensions.size() == 1) {
			returnValue = (StringType) extensions.get(0).getValue();
		} else if (extensions.size() == 0) {
			returnValue = null;
		} else {
			throw new IllegalStateException("More than one extension specified for this object.");
		}
		return returnValue;
	}

	public CORAPatientClinicalTrial setNCT(StringType param) {
		java.util.List<Extension> extensions = rootObjectExtension
				.getExtensionsByUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#NCT");
		if (extensions.size() == 1) {
			extensions.get(0).setValue(param);
		} else if (extensions.size() == 0) {
			 rootObjectExtension.addExtension().setUrl("http://hl7.org/fhir/StructureDefinition/patient-clinicalTrial#NCT").setValue(param);
		} else {
			throw new IllegalStateException("More than one extension specified for this object.");
		}
		return this;
	}

	public Extension getRootObjectExtension() {
		return rootObjectExtension;
	}

	public void setRootObjectExtension(Extension rootObjectExtension) {
		this.rootObjectExtension = rootObjectExtension;
	}

    @LogicalModelChange("Change BaseResource to DomainResource")
    public Extension bindTemplateToParent(DomainResource containingResource){
		rootObjectExtension = new Extension();
		rootObjectExtension.setUrl(uri);
		containingResource.addExtension(rootObjectExtension);
		return rootObjectExtension;
	}
}