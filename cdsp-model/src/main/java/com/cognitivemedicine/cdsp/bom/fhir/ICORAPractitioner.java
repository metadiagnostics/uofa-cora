/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.Practitioner;


public interface ICORAPractitioner extends ICORACognitiveBaseIdentifiable<ICORAPractitioner> {

    public Address addAddress();

    public ICORAPractitioner addAddress(Address param);

    public HumanName addName();

    public ICORAPractitioner addName(HumanName param);



    public Attachment addPhoto();

    public ICORAPractitioner addPhoto(Attachment param);

    public Practitioner.PractitionerQualificationComponent addQualification();

    public ICORAPractitioner addQualification(
            Practitioner.PractitionerQualificationComponent param);

    public ContactPoint addTelecom();

    public ICORAPractitioner addTelecom(ContactPoint param);

    public Boolean getActive();

    public BooleanType getActiveElement();

    public List<Address> getAddress();

    public Address getAddressFirstRep();

    public Date getBirthDate();

    public DateType getBirthDateElement();

    public CodeableConcept getClassification();

    public AdministrativeGender getGender();

    public Enumeration<AdministrativeGender> getGenderElement();

    public List<HumanName> getName();


    public HumanName getNameFirstRep();

    public List<Attachment> getPhoto();


    public Attachment getPhotoFirstRep();

    public List<Practitioner.PractitionerQualificationComponent> getQualification();

    public Practitioner.PractitionerQualificationComponent getQualificationFirstRep();

    public List<ContactPoint> getTelecom();

    public ContactPoint getTelecomFirstRep();

    public ICORAPractitioner setActive(Boolean param);

    public ICORAPractitioner setActiveElement(BooleanType param);

    public ICORAPractitioner setAddress(List<Address> param);

    public ICORAPractitioner setBirthDate(Date param);

    public ICORAPractitioner setBirthDateElement(DateType param);

    public ICORAPractitioner setClassification(CodeableConcept param);

    public ICORAPractitioner setGender(AdministrativeGender param);

    public ICORAPractitioner setGender(String param);

    public ICORAPractitioner setGenderElement(Enumeration<AdministrativeGender> param);

    public ICORAPractitioner setName(List<HumanName> param);

    public ICORAPractitioner setPhoto(List<Attachment> param);


    public ICORAPractitioner setQualification(
            List<Practitioner.PractitionerQualificationComponent> param);

    public ICORAPractitioner setTelecom(List<ContactPoint> param);
}
