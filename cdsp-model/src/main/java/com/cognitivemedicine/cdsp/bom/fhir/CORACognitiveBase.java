/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Resource;

//import org.hl7.fhir.instance.model.api.IBaseResource;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelMissing;
import com.cognitivemedicine.fhir.logicalmodel.Context;

import ca.uhn.fhir.context.FhirContext;

/**
 * @author Jerry Goodnough
 *
 */
public abstract class CORACognitiveBase<T extends Resource, I> implements Serializable {
	private static FhirContext fhirCtx = FhirContext.forDstu3();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@LogicalModelChange("Make Transient to provide default Serialization")
	protected transient T adaptedClass = null;

	@LogicalModelMissing("To be added to IQCore")
	protected Context ctx = new Context();

	public final T getAdaptee() {
		return adaptedClass;
	}

	@LogicalModelAddition("To be added to IQCore")
	public final Context getCORAContext() {
		return this.ctx;
	}

	/**
	 * Always treat de-serialization as a full-blown constructor, by validating
	 * the final state of the de-serialized object.
	 */
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
		// always perform the default de-serialization first
		aInputStream.defaultReadObject();
		String in = (String) aInputStream.readObject();

		adaptedClass = (T) fhirCtx.newJsonParser().parseResource(in);
	}

	public final void setAdaptee(T param) {
		this.adaptedClass = param;
	}

	@SuppressWarnings("unchecked")
	@LogicalModelAddition("To be added to IQCore")
	public final I setCORAContext(Context ctx) {
		this.ctx = ctx;
		return (I) this;
	}

    @LogicalModelChange("Should be get IdElement")
    public IdType getIdElement() {
        return adaptedClass.getIdElement();
    }

    @LogicalModelChange("Should be get Id")
    public String getId() {
        return adaptedClass.getId();
    }

    @SuppressWarnings("unchecked")
    @LogicalModelChange("Should be setIdElement")
    public I setIdElement(IdType id) {
        adaptedClass.setIdElement(id);
        return (I) this;
    }

    @SuppressWarnings("unchecked")
    @LogicalModelChange("Should be setId")
    public I setId(String id) {
        adaptedClass.setId(id);
        return (I) this;
    }
    
    public String getLanguage()
    {
        return adaptedClass.getLanguage();
    }
    public CodeType getLanguageElement()
    {
        return adaptedClass.getLanguageElement();
    }
    public I setLanguage(String param)
    {
        adaptedClass.setLanguage(param);
        return (I) this;
    }
    
    public I setLanguageElement(CodeType code)
    {
        adaptedClass.setLanguageElement(code);
        return (I) this;
    }
   
    
	/**
	 * This is the default implementation of writeObject. Customise if
	 * necessary.
	 */
	private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
		// perform the default serialization for all non-transient, non-static
		// fields
		aOutputStream.defaultWriteObject();

		String out = fhirCtx.newJsonParser().encodeResourceToString((Resource) adaptedClass);
		aOutputStream.writeObject(out);
	}

}
