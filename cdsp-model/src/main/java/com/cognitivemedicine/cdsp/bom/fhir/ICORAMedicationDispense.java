/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.IntegerType;
import org.hl7.fhir.dstu3.model.MedicationDispense;
import org.hl7.fhir.dstu3.model.MedicationDispense.MedicationDispensePerformerComponent;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.Type;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;



@LogicalModelAddition("Extends our base")
public interface ICORAMedicationDispense
        extends ICORACognitiveBaseIdentifiable<ICORAMedicationDispense> {

    public Reference addAuthorizingPrescription();

    public ICORAMedicationDispense addAuthorizingPrescription(Reference param);

    public Reference addDetectedIssue();

    public ICORAMedicationDispense addDetectedIssue(Reference param);

    public Dosage addDosageInstruction();

    public ICORAMedicationDispense addDosageInstruction(Dosage param);

    public Reference addEventHistory();

    public ICORAMedicationDispense addEventHistory(Reference param);

    public Annotation addNote();

    public ICORAMedicationDispense addNote(Annotation param);

    public Reference addPartOf();

    public ICORAMedicationDispense addPartOf(Reference param);

    public MedicationDispensePerformerComponent addPerformer();

    public ICORAMedicationDispense addPerformer(MedicationDispensePerformerComponent param);

    public Reference addSupportingInformation();

    public ICORAMedicationDispense addSupportingInformation(Reference param);

    public CORADosage addWrappedDosageInstruction();

    public ICORAMedicationDispense addWrappedDosageInstruction(CORADosage param);

    public List<Reference> getAuthorizingPrescription();

    public Reference getAuthorizingPrescriptionFirstRep();

    public CodeableConcept getCategory();

    public Reference getContext();

    public SimpleQuantity getDaysSupply();

    public CORALocationAdapter getDestinationResource();

    public List<Reference> getDetectedIssue();

    public Reference getDetectedIssueFirstRep();

    public List<Dosage> getDosageInstruction();

    public Dosage getDosageInstructionFirstRep();

    public List<Reference> getEventHistory();

    public Reference getEventHistoryFirstRep();

    public CodeableConcept getMedicationCodeableConcept();

    public Reference getMedicationReference();

    public boolean getNotDone();

    public BooleanType getNotDoneElement();

    public Type getNotDoneReason();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public List<Reference> getPartOf();

    public Reference getPartOfFirstRep();

    public CORAPatientAdapter getPatientResource();

    public List<MedicationDispensePerformerComponent> getPerformer();

    public MedicationDispensePerformerComponent getPerformerFirstRep();

    public SimpleQuantity getQuantity();

    public IntegerType getRefillsRemaining();

    public MedicationDispense.MedicationDispenseStatus getStatus();

    public Enumeration<MedicationDispense.MedicationDispenseStatus> getStatusElement();

    public Reference getSubject();

    public MedicationDispense.MedicationDispenseSubstitutionComponent getSubstitution();

    public List<Reference> getSupportingInformation();

    public Reference getSupportingInformationFirstRep();

    public CodeableConcept getType();

    public Period getValidityPeriod();

    public Date getWhenHandedOver();

    public DateTimeType getWhenHandedOverElement();

    public Date getWhenPrepared();

    public DateTimeType getWhenPreparedElement();

    public List<CORADosage> getWrappedDosageInstruction();

    public CORADosage getWrappedDosageInstructionFirstRep();

    public ICORAMedicationDispense setAuthorizingPrescription(List<Reference> param);

    public ICORAMedicationDispense setCategory(CodeableConcept param);

    public ICORAMedicationDispense setContext(Reference param);

    public ICORAMedicationDispense setDaysSupply(SimpleQuantity param);

    public ICORAMedicationDispense setDestinationResource(CORALocationAdapter param);

    public ICORAMedicationDispense setDetectedIssue(List<Reference> param);

    public ICORAMedicationDispense setDosageInstruction(List<Dosage> param);

    public ICORAMedicationDispense setEventHistory(List<Reference> param);

    public ICORAMedicationDispense setMedicationCodeableConcept(CodeableConcept param);

    public ICORAMedicationDispense setMedicationReference(Reference param);

    public ICORAMedicationDispense setNotDone(boolean param);

    public ICORAMedicationDispense setNotDoneElement(BooleanType param);

    public ICORAMedicationDispense setNotDoneReason(Type param);

    public ICORAMedicationDispense setNote(List<Annotation> param);

    public ICORAMedicationDispense setPartOf(List<Reference> param);

    public ICORAMedicationDispense setPatientResource(CORAPatientAdapter param);

    public ICORAMedicationDispense setPerformer(List<MedicationDispensePerformerComponent> param);

    public ICORAMedicationDispense setQuantity(SimpleQuantity param);

    public ICORAMedicationDispense setRefillsRemaining(IntegerType param);

    public ICORAMedicationDispense setStatus(MedicationDispense.MedicationDispenseStatus param);

    public ICORAMedicationDispense setStatus(String param);

    public ICORAMedicationDispense setStatusElement(
            Enumeration<MedicationDispense.MedicationDispenseStatus> param);

    public ICORAMedicationDispense setSubject(Reference param);

    public ICORAMedicationDispense setSubstitution(
            MedicationDispense.MedicationDispenseSubstitutionComponent param);


    public ICORAMedicationDispense setSupportingInformation(List<Reference> param);

    public ICORAMedicationDispense setType(CodeableConcept param);

    public ICORAMedicationDispense setValidityPeriod(Period param);

    public ICORAMedicationDispense setWhenHandedOver(Date param);

    public ICORAMedicationDispense setWhenHandedOverElement(DateTimeType param);

    public ICORAMedicationDispense setWhenPrepared(Date param);

    public ICORAMedicationDispense setWhenPreparedElement(DateTimeType param);

    public ICORAMedicationDispense setWrappedDosageInstruction(List<CORADosage> param);


}
