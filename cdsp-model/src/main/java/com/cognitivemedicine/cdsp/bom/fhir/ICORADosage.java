/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Ratio;
import org.hl7.fhir.dstu3.model.SimpleQuantity;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Timing;

public interface ICORADosage {


    public Dosage getAdaptee();

    public List<CodeableConcept> getAdditionalInstructions();

    public Boolean getAsNeededBoolean();

    public BooleanType getAsNeededBooleanElement();

    public CodeableConcept getAsNeededCodeableConcept();

    public SimpleQuantity getDoseQuantity();

    public Range getDoseRange();

    public CodeableConcept getDoseType();

    public String getId();

    public Extension getInfuseOver();

    public Ratio getMaxDeliveryRate();

    public Quantity getMaxDeliveryVolume();

    public SimpleQuantity getMaxDosePerAdministration();

    public SimpleQuantity getMaxDosePerLifetime();

    public Ratio getMaxDosePerPeriod();

    public CodeableConcept getMethod();

    public Ratio getMinDosePerPeriod();

    public String getPatientInstructions();

    public Ratio getRateGoal();

    public Ratio getRateIncrement();

    public Extension getRateIncrementInterval();

    public SimpleQuantity getRateQuantity();

    public Range getRateRange();

    public Ratio getRateRatio();

    public CodeableConcept getRoute();

    public int getSequence();

    public CodeableConcept getSiteCodeableConcept();

    public CodeableConcept getSiteReference();

    public String getText();

    public StringType getTextElement();

    public Timing getTiming();

    public void setAdaptee(Dosage param);

    public CORADosage setAdditionalInstructions(List<CodeableConcept> param);

    public CORADosage setAsNeededBoolean(boolean param);

    public CORADosage setAsNeededBoolean(BooleanType param);

    public CORADosage setAsNeededCodeableConcept(CodeableConcept param);

    public CORADosage setDoseQuantity(SimpleQuantity param);

    public CORADosage setDoseRange(Range param);

    public CORADosage setDoseType(CodeableConcept param);

    public CORADosage setId(String param);

    public CORADosage setInfuseOver(Extension param);

    public CORADosage setMaxDeliveryRate(Ratio param);

    public CORADosage setMaxDeliveryVolume(Quantity param);

    public CORADosage setMaxDosePerAdministration(SimpleQuantity param);

    public CORADosage setMaxDosePerLifetime(SimpleQuantity param);

    public CORADosage setMaxDosePerPeriod(Ratio param);

    public CORADosage setMethod(CodeableConcept param);

    public CORADosage setMinDosePerPeriod(Ratio param);

    public CORADosage setPatientInstructions(String param);

    public CORADosage setRateGoal(Ratio param);

    public CORADosage setRateIncrement(Ratio param);

    public CORADosage setRateIncrementInterval(Extension param);
    
    public CORADosage setRateQuantity(SimpleQuantity param);

    public CORADosage setRateRange(Range param);

    public CORADosage setRateRatio(Ratio param);

    public CORADosage setRoute(CodeableConcept param);


    public CORADosage setSequence(int param);

    public CORADosage setSite(CodeableConcept param);

    public CORADosage setSiteCodeableConcept(CodeableConcept param);

    public CORADosage setText(String param);

    public CORADosage setTextElement(StringType param);

    public CORADosage setTiming(Timing param);
    
}
