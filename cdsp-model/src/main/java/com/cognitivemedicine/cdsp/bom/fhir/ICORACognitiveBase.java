/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.io.Serializable;

import org.hl7.fhir.dstu3.model.CodeType;
import org.hl7.fhir.dstu3.model.IdType;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * @author Jerry Goodnough
 *
 */
public interface ICORACognitiveBase<T> extends Serializable {

	@LogicalModelAddition("To be added to IQCore")
	public Context getCORAContext();

	public String getId();
	
	public IdType getIdElement();

    public String getLanguage();
    
	public CodeType getLanguageElement();
	
	@LogicalModelAddition("To be added to IQCore")
	public T setCORAContext(Context ctx);
	
	public T setId(String id);
	
	public T setIdElement(IdType id);
	
	public T setLanguage(String param);
	
	public T setLanguageElement(CodeType code);
}
