/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Attachment;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hl7.fhir.dstu3.model.DiagnosticReport.DiagnosticReportPerformerComponent;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.InstantType;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.StringType;



public class CORADiagnosticReportAdapter
        extends CORACognitiveBaseIdentifiable<DiagnosticReport, ICORADiagnosticReport>
        implements ICORADiagnosticReport {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORADiagnosticReportAdapter() {
        this.adaptedClass = new DiagnosticReport();
    }

    public CORADiagnosticReportAdapter(DiagnosticReport adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public CodeableConcept addCodedDiagnosis() {
        return adaptedClass.addCodedDiagnosis();
    }

    @Override
    public ICORADiagnosticReport addCodedDiagnosis(CodeableConcept param) {
        adaptedClass.addCodedDiagnosis(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORADiagnosticReport addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }

    @Override
    public DiagnosticReport.DiagnosticReportImageComponent addImage() {
        DiagnosticReport.DiagnosticReportImageComponent item =
                new DiagnosticReport.DiagnosticReportImageComponent();
        adaptedClass.addImage(item);
        return item;
    }

    @Override
    public ICORADiagnosticReport addImage(DiagnosticReport.DiagnosticReportImageComponent param) {
        adaptedClass.addImage(param);
        return this;
    }

    @Override
    public DiagnosticReportPerformerComponent addPerformer() {
        return adaptedClass.addPerformer();
    }

    @Override
    public ICORADiagnosticReport addPerformer(DiagnosticReportPerformerComponent param) {
        adaptedClass.addPerformer(param);
        return this;
    }

    @Override
    public Attachment addPresentedForm() {
        Attachment item = new Attachment();
        adaptedClass.addPresentedForm(item);
        return item;
    }

    @Override
    public ICORADiagnosticReport addPresentedForm(Attachment param) {
        adaptedClass.addPresentedForm(param);
        return this;
    }

    @Override
    public Reference addResult() {
        return adaptedClass.addResult();
    }

    @Override
    public ICORADiagnosticReport addResult(Reference param) {
        adaptedClass.addResult(param);
        return this;
    }

    @Override
    public CodeableConcept getCategory() {
        return adaptedClass.getCategory();
    }

    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }


    @Override
    public List<CodeableConcept> getCodedDiagnosis() {
        return adaptedClass.getCodedDiagnosis();
    }

    @Override
    public CodeableConcept getCodedDiagnosisFirstRep() {
        return adaptedClass.getCodedDiagnosisFirstRep();
    }

    @Override
    public String getConclusion() {
        return adaptedClass.getConclusion();
    }

    @Override
    public StringType getConclusionElement() {
        return adaptedClass.getConclusionElement();
    }

    @Override
    public Reference getContext() {
        return adaptedClass.getContext();
    }

    @Override
    public Date getEffectiveDateTime() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getEffective()).getValue();
        } else {
            return null;
        }
    }


    @Override
    public DateTimeType getEffectiveDateTimeElement() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public Period getEffectivePeriod() {
        if (adaptedClass.getEffective() != null && adaptedClass.getEffective() instanceof Period) {
            return (Period) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public CORAEncounterAdapter getEncounterResource() {

        CORAEncounterAdapter out = null;

        Resource ctx = adaptedClass.getContextTarget();
        if ((ctx != null) && (ctx instanceof Encounter)) {
            out = new CORAEncounterAdapter((Encounter) ctx);
        }
        return out;


    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public List<DiagnosticReport.DiagnosticReportImageComponent> getImage() {
        return adaptedClass.getImage();
    }

    @Override
    public DiagnosticReport.DiagnosticReportImageComponent getImageFirstRep() {
        return adaptedClass.getImageFirstRep();
    }

    @Override
    public Date getIssued() {
        return adaptedClass.getIssued();
    }

    @Override
    public InstantType getIssuedElement() {
        return adaptedClass.getIssuedElement();
    }

    @Override
    public Reference getLocationPerformed() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/diagnosticReport-locationPerformed");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Reference) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for locationPerformed");
        }
    }


    @Override
    public List<DiagnosticReportPerformerComponent> getPerformer() {
        return adaptedClass.getPerformer();
    }

    @Override
    public DiagnosticReportPerformerComponent getPerformerFirstReq() {
        return adaptedClass.getPerformerFirstRep();
    }

    @Override
    public List<Attachment> getPresentedForm() {
        return adaptedClass.getPresentedForm();
    }

    @Override
    public Attachment getPresentedFormFirstRep() {
        return adaptedClass.getPresentedFormFirstRep();
    }

    @Override
    public List<Reference> getResult() {
        return adaptedClass.getResult();
    }

    @Override
    public Reference getResultFirstRep() {
        return adaptedClass.getResultFirstRep();
    }

    @Override
    public DiagnosticReport.DiagnosticReportStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<DiagnosticReport.DiagnosticReportStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Narrative getText() {
        return adaptedClass.getText();
    }


    @Override
    public ICORADiagnosticReport setCategory(CodeableConcept param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setCode(CodeableConcept param) {
        adaptedClass.setCode(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setCodededDiagnosis(List<CodeableConcept> param) {
        adaptedClass.setCodedDiagnosis(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setConclusion(String param) {
        adaptedClass.setConclusion(param);
        return this;
    }


    @Override
    public ICORADiagnosticReport setConclusionElement(StringType param) {
        adaptedClass.setConclusionElement(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setEffectiveDateTime(Date param) {
        adaptedClass.setEffective(new DateTimeType(param));
        return this;
    }


    @Override
    public ICORADiagnosticReport setEffectiveDateTimeElement(DateTimeType param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setEffectivePeriod(Period param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.setContextTarget(param.adaptedClass);
        return this;
    }

    public ICORADiagnosticReport setId(IdType param) {
        adaptedClass.setId(param);
        return this;
    }


    @Override
    public ICORADiagnosticReport setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setImage(
            List<DiagnosticReport.DiagnosticReportImageComponent> param) {
        adaptedClass.setImage(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setIssued(Date param) {
        adaptedClass.setIssued(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setIssuedElement(InstantType param) {
        adaptedClass.setIssuedElement(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setLocationPerformed(Reference param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/diagnosticReport-locationPerformed")
                .setValue(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setPerformer(List<DiagnosticReportPerformerComponent> param) {
        adaptedClass.setPerformer(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setPresentedForm(List<Attachment> param) {
        adaptedClass.setPresentedForm(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setResult(List<Reference> param) {
        adaptedClass.setResult(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setStatus(DiagnosticReport.DiagnosticReportStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setStatusElement(
            Enumeration<DiagnosticReport.DiagnosticReportStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORADiagnosticReport setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public String toString() {
        return "CORADiagnosticReportAdapter [getCategory()=" + getCategory() + ", getCode()="
                + getCode() + ", getCodedDiagnosis()=" + getCodedDiagnosis() + ", getConclusion()="
                + getConclusion() + ", getContext()=" + getContext() + ", getEffectiveDateTime()="
                + getEffectiveDateTime() + ", getEffectivePeriod()=" + getEffectivePeriod()
                + ", getIdentifier()=" + getIdentifier() + ", getImage()=" + getImage()
                + ", getIssued()=" + getIssued() + ", getLocationPerformed()="
                + getLocationPerformed() + ", getPerformer()=" + getPerformer()
                + ", getPresentedForm()=" + getPresentedForm() + ", getResult()=" + getResult()
                + ", getStatus()=" + getStatus() + ", getText()=" + getText()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }
    
    
}
