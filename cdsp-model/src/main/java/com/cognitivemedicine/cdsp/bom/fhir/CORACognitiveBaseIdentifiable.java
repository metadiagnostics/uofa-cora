/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Resource;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;


/**
 * @author Jerry Goodnough
 *
 *         It is presumed that all resources from DomainResource
 */
public abstract class CORACognitiveBaseIdentifiable<T extends DomainResource, I>
        extends CORACognitiveBase<T, I> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DomainResource addContained(Resource param) {
        ((DomainResource) adaptedClass).addContained(param);
        return (DomainResource) param;
    }

    public abstract Identifier addIdentifier();

    public abstract I addIdentifier(Identifier param);

    public List<Resource> getContained() {
        return ((DomainResource) adaptedClass).getContained();
    }

    /**
     * Help method to fetch first identifier by system.
     */
    @LogicalModelAddition("To be added to IQCore")
    public Identifier getFirstIdentiferBySystem(String system) {
        List<Identifier> identifiers = getIdentifier();
        for (Identifier id : identifiers) {
            String idsystem = id.getSystem();
            if (idsystem != null) {
                if (id.getSystem().compareTo(system) == 0) {
                    return id;
                }
            }
        }
        // If there is no identifier in the system well return null?

        return null;
    }

    public abstract List<Identifier> getIdentifier();

    public abstract Identifier getIdentifierFirstRep();

    @LogicalModelAddition("To be added to IQCore")
    public Identifier getPrimaryIdentifer() {
        List<Identifier> identifiers = getIdentifier();
        // Always use the official Id if present.
        for (Identifier id : identifiers) {
            Identifier.IdentifierUse use = id.getUse();
            if (use != null) {
                if (Identifier.IdentifierUse.OFFICIAL == use) {
                    return id;
                }
            }
        }
        // If there is no official Id we default to the first (Yuk)

        return identifiers.size() == 0 ? null : identifiers.get(0);
    }

    public Narrative getText() {
        return ((DomainResource) adaptedClass).getText();
    }

    @SuppressWarnings("unchecked")
    public I setContained(List<Resource> param) {
        ((DomainResource) adaptedClass).setContained(param);
        return (I) this;
    }

    public abstract I setIdentifier(List<Identifier> param);

    @SuppressWarnings("unchecked")
    @LogicalModelAddition("To be added to IQCore")
    public I setPrimaryIdentifer(Identifier identifier) {
        boolean fnd = false;
        List<Identifier> identifiers = getIdentifier();

        identifier.setUse(Identifier.IdentifierUse.OFFICIAL);
        // First we look if there is an official identifier and if found we
        // replace it;
        for (int i = 0; i < identifiers.size(); i++) {
            Identifier id = identifiers.get(i);
            Identifier.IdentifierUse use = id.getUse();
            if (use != null) {
                if (Identifier.IdentifierUse.OFFICIAL == use) {
                    identifiers.set(i, identifier);
                    fnd = true;
                }
            }
        }
        // Add the identifier if not found
        if (fnd == false) {
            identifiers.add(identifier);
        }
        return (I) this;
    }

    public I setText(Narrative param) {
        ((DomainResource) adaptedClass).setText(param);
        return (I) this;
    }
}
