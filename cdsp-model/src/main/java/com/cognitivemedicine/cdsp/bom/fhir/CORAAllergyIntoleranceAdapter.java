/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Age;
import org.hl7.fhir.dstu3.model.AllergyIntolerance;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceCategory;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceClinicalStatus;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceCriticality;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceType;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceVerificationStatus;
import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.exceptions.FHIRException;




public class CORAAllergyIntoleranceAdapter
        extends CORACognitiveBaseIdentifiable<AllergyIntolerance, ICORAAllergyIntolerance>
        implements ICORAAllergyIntolerance {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAAllergyIntoleranceAdapter() {
        this.adaptedClass = new AllergyIntolerance();
    }

    public CORAAllergyIntoleranceAdapter(AllergyIntolerance adaptee) {
        this.adaptedClass = adaptee;
    }


    @Override
    public ICORAAllergyIntolerance addCategory(AllergyIntoleranceCategory param) {
        adaptedClass.addCategory(param);
        return this;
    }
    
    
    @Override
    public Enumeration<AllergyIntoleranceCategory> addCategoyElement()
    {
        return adaptedClass.addCategoryElement();
    }

    @Override
    public Identifier addIdentifier() {
        return adaptedClass.addIdentifier();
    }

    @Override
    public ICORAAllergyIntolerance addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return null;
    }

    @Override
    public AllergyIntolerance.AllergyIntoleranceReactionComponent addReaction() {
        AllergyIntolerance.AllergyIntoleranceReactionComponent item =
                new AllergyIntolerance.AllergyIntoleranceReactionComponent();
        adaptedClass.addReaction(item);
        return item;
    }

    @Override
    public ICORAAllergyIntolerance addReaction(
            AllergyIntolerance.AllergyIntoleranceReactionComponent param) {
        adaptedClass.addReaction(param);
        return this;
    }

    @Override
    public CORAAllergyIntoleranceReaction addWrappedReaction() {
        AllergyIntolerance.AllergyIntoleranceReactionComponent item =
                new AllergyIntolerance.AllergyIntoleranceReactionComponent();
        adaptedClass.addReaction(item);
        return new CORAAllergyIntoleranceReaction(item);
    }

    @Override
    public ICORAAllergyIntolerance addWrappedReaction(CORAAllergyIntoleranceReaction param) {
        if (param != null) {
            adaptedClass.addReaction(param.getAdaptee());
        }
        return this;
    }

    @Override
    public Date getAssertedDate() {
        return adaptedClass.getAssertedDate();
    }

    @Override
    public DateTimeType getAssertedDateElement() {
        return adaptedClass.getAssertedDateElement();
    }

 
    @Override
    public Reference getAssertor() {
        return adaptedClass.getAsserter();
    }

    @Override
    public List<Enumeration<AllergyIntoleranceCategory>> getCategory() {
        return adaptedClass.getCategory();

    }

    @Override
    public AllergyIntoleranceClinicalStatus getClinicalStatus() {
        return adaptedClass.getClinicalStatus();
    }

    @Override
    public Enumeration<AllergyIntoleranceClinicalStatus> getClinicalStatusElement() {
        return adaptedClass.getClinicalStatusElement();
    }

    @Override
    public CodeableConcept getCode() {
        return adaptedClass.getCode();
    }

    @Override
    public AllergyIntoleranceCriticality getCriticality() {
        return adaptedClass.getCriticality();
    }

    @Override
    public Enumeration<AllergyIntoleranceCriticality> getCriticalityElement() {
        return adaptedClass.getCriticalityElement();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        
        return adaptedClass.getIdentifierFirstRep();
    }

    public DateTimeType getLastOccurenceElement() {
        return adaptedClass.getLastOccurrenceElement();
    }

    @Override
    public Date getLastOccurrence() {
        return adaptedClass.getLastOccurrence();

    }

    @Override
    public DateTimeType getLastOccurrenceElement() {
        
        return adaptedClass.getLastOccurrenceElement();
    }

    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstReq() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public Type getOnset() {
        return adaptedClass.getOnset();
    }



    @Override
    public Age getOnsetAge() {
        
        try {
            return adaptedClass.getOnsetAge();
        } catch (FHIRException e) {
            return null;
        }
    }

    @Override
    public DateTimeType getOnsetDateTypeType()  {
        try {
            return adaptedClass.getOnsetDateTimeType();
        } catch (FHIRException e) {
            return null;
        }
    }


    @Override
    public Period getOnsetPeriod() throws FHIRException {
        return adaptedClass.getOnsetPeriod();
    }

    @Override
    public Range getOnsetRange() throws FHIRException {
        return adaptedClass.getOnsetRange();
    }

    @Override
    public StringType getOnsetStringType() throws FHIRException {
        return adaptedClass.getOnsetStringType();
    }

    @Override
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getPatient().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getPatient().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<AllergyIntolerance.AllergyIntoleranceReactionComponent> getReaction() {
        return adaptedClass.getReaction();
    }

    @Override
    public AllergyIntolerance.AllergyIntoleranceReactionComponent getReactionFirstRep() {
        return adaptedClass.getReactionFirstRep();
    }

    @Override
    public CodeableConcept getReasonRefuted() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/allergyintolerance-reasonRefuted");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (CodeableConcept) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for reasonRefuted");
        }
    }

    @Override
    public Reference getRecorder() {
        return adaptedClass.getRecorder();
    }

    @Override
    public Extension getResolutionAge() {
        List<Extension> extensions = adaptedClass.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/allergyintolerance-resolutionAge");
        if (extensions == null || extensions.size() <= 0) {
            return null;
        } else if (extensions.size() == 1) {
            return (Extension) extensions.get(0).getValue();
        } else {
            throw new RuntimeException("More than one extension exists for resolutionAge");
        }
    }

    @Override
    public AllergyIntoleranceType getType() {
        return adaptedClass.getType();
    }

    @Override
    public Enumeration<AllergyIntoleranceType> getTypeElement() {
        return adaptedClass.getTypeElement();
    }

    @Override
    public AllergyIntoleranceVerificationStatus getVerificationStatus() {
        return adaptedClass.getVerificationStatus();
    }

    @Override
    public Enumeration<AllergyIntoleranceVerificationStatus> getVerificationStatusElement() {
        return adaptedClass.getVerificationStatusElement();
    }

    @Override
    public List<CORAAllergyIntoleranceReaction> getWrappedReaction() {
        List<CORAAllergyIntoleranceReaction> items = new java.util.ArrayList<>();
        for (AllergyIntolerance.AllergyIntoleranceReactionComponent type : adaptedClass.getReaction()) {
            items.add(new CORAAllergyIntoleranceReaction(type));
        }
        return items;
    }

    @Override
    public CORAAllergyIntoleranceReaction getWrappedReactionFirstRep() {
        CORAAllergyIntoleranceReaction wrapperItem = new CORAAllergyIntoleranceReaction();
        AllergyIntolerance.AllergyIntoleranceReactionComponent item = adaptedClass.getReactionFirstRep();
        if (item != null) {
            wrapperItem = new CORAAllergyIntoleranceReaction(item);
        }
        return wrapperItem;
    }

    @Override
    public ICORAAllergyIntolerance setAssertedDate(Date param) {
        adaptedClass.setAssertedDate(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setAssertedDateElement(DateTimeType param) {
        adaptedClass.setAssertedDateElement(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setAsserter(Reference param) {
        adaptedClass.setAsserter(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setCategory(List<Enumeration<AllergyIntoleranceCategory>> param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setClinicialStatus(AllergyIntoleranceClinicalStatus value) {
        adaptedClass.setClinicalStatus(value);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setClinicialStatusElement(
            Enumeration<AllergyIntoleranceClinicalStatus> value) {
        adaptedClass.setClinicalStatusElement(value);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setCode(CodeableConcept value) {
        adaptedClass.setCode(value);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setCriticality(AllergyIntoleranceCriticality param) {
        adaptedClass.setCriticality(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setCriticalityElement(
            Enumeration<AllergyIntoleranceCriticality> param) {
        adaptedClass.setCriticalityElement(param);
        return this;
    }


    @Override
    public ICORAAllergyIntolerance setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setLastOccurrence(Date param) {
        adaptedClass.setLastOccurrence(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setLastOccurrenceElement(DateTimeType param) {
        adaptedClass.setLastOccurrenceElement(param);
        return this;
    }


    @Override
    public ICORAAllergyIntolerance setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    public ICORAAllergyIntolerance setOnset(Date param) {
        adaptedClass.setOnset(new DateTimeType(param));
        return this;
    }

    public ICORAAllergyIntolerance setOnset(DateTimeType param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setOnset(Type param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setOnsetAge(Age param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setOnsetDateTypeType(DateTimeType param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setOnsetPeriod(Period param) {
        adaptedClass.setOnset(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setOnsetRange(Range param) {
        adaptedClass.setOnset(param);
        return this;
    }


    @Override
    public ICORAAllergyIntolerance setOnsetStringType(StringType param) {
        adaptedClass.setOnset(param);
        return this;
    }



    @Override
    public ICORAAllergyIntolerance setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getPatient().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setReaction(
            List<AllergyIntolerance.AllergyIntoleranceReactionComponent> param) {
        adaptedClass.setReaction(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setReasonRefuted(CodeableConcept param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/allergyintolerance-reasonRefuted")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setRecorder(Reference param) {
        adaptedClass.setRecorder(param);
        return this;

    }

    @Override
    public ICORAAllergyIntolerance setResolutionAge(Extension param) {
        adaptedClass.addExtension()
                .setUrl("http://hl7.org/fhir/StructureDefinition/allergyintolerance-resolutionAge")
                .setValue(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setType(AllergyIntoleranceType param) {
        adaptedClass.setType(param);
        return this;
    }


    @Override
    public ICORAAllergyIntolerance setTypeElement(Enumeration<AllergyIntoleranceType> param) {
        adaptedClass.setTypeElement(param);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setVerificationStatus(
            AllergyIntoleranceVerificationStatus value) {
        adaptedClass.setVerificationStatus(value);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setVerificationStatusElement(
            Enumeration<AllergyIntoleranceVerificationStatus> value) {
        adaptedClass.setVerificationStatusElement(value);
        return this;
    }

    @Override
    public ICORAAllergyIntolerance setWrappedReaction(List<CORAAllergyIntoleranceReaction> param) {
        List<AllergyIntolerance.AllergyIntoleranceReactionComponent> items = new java.util.ArrayList<>();
        for (CORAAllergyIntoleranceReaction item : param) {
            items.add(item.getAdaptee());
        }
        adaptedClass.setReaction(items);
        return this;
    }

    @Override
    public String toString() {
        return "CORAAllergyIntoleranceAdapter [getAssertedDate()=" + getAssertedDate()
                + ", getAssertor()=" + getAssertor() + ", getCategory()=" + getCategory()
                + ", getClinicalStatus()=" + getClinicalStatus() + ", getCode()=" + getCode()
                + ", getCriticality()=" + getCriticality() + ", getIdentifier()=" + getIdentifier()
                + ", getLastOccurrence()=" + getLastOccurrence() + ", getNote()=" + getNote()
                + ", getOnset()=" + getOnset() + ", getReaction()=" + getReaction()
                + ", getReasonRefuted()=" + getReasonRefuted() + ", getRecorder()=" + getRecorder()
                + ", getResolutionAge()=" + getResolutionAge() + ", getType()=" + getType()
                + ", getVerificationStatus()=" + getVerificationStatus()
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }


}
