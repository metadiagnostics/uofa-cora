/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Specimen;
import org.hl7.fhir.dstu3.model.Specimen.SpecimenStatus;

/**
 * DTSU3/HAPI2.4/BOM Specimen
 * 
 * @author Jerry Goodnough
 *
 */

public interface ICORASpecimen extends ICORACognitiveBaseIdentifiable<ICORASpecimen> {

    public Specimen.SpecimenContainerComponent addContainer();

    public ICORASpecimen addContainer(Specimen.SpecimenContainerComponent param);

    public Annotation addNote();

    public ICORASpecimen addNote(Annotation param);

    public Specimen.SpecimenProcessingComponent addProcessing();

    public ICORASpecimen addProcessing(Specimen.SpecimenProcessingComponent param);

    public Reference addRequest();

    public ICORASpecimen addRequest(Reference param);

    public CORASpecimenContainer addWrappedContainer();

    public ICORASpecimen addWrappedContainer(CORASpecimenContainer param);

    public Identifier getAccessionIdentifier();

    public Specimen.SpecimenCollectionComponent getCollection();

    public List<Specimen.SpecimenContainerComponent> getContainer();

    public Specimen.SpecimenContainerComponent getContainerFirstRep();

    public List<Annotation> getNote();

    public Annotation getNoteFirstRep();

    public List<Specimen.SpecimenProcessingComponent> getProcessing();

    public Specimen.SpecimenProcessingComponent getProcessingFirstRep();

    public Date getReceivedTime();

    public DateTimeType getReceivedTimeElement();

    public List<Reference> getRequest();

    public Reference getRequestFirstRep();

    public SpecimenStatus getStatus();

    public Enumeration<SpecimenStatus> getStatusElement();

    public Reference getSubject();

    public CodeableConcept getType();

    public List<CORASpecimenContainer> getWrappedContainer();

    public CORASpecimenContainer getWrappedContainerFirstRep();

    public ICORASpecimen setAccessionIdentifier(Identifier param);

    public ICORASpecimen setCollection(Specimen.SpecimenCollectionComponent param);

    public ICORASpecimen setContainer(List<Specimen.SpecimenContainerComponent> param);

    public ICORASpecimen setNote(List<Annotation> param);

    public ICORASpecimen setProcessing(List<Specimen.SpecimenProcessingComponent> param);

    public ICORASpecimen setReceivedTime(Date param);

    public ICORASpecimen setReceivedTimeElement(DateTimeType param);

    public ICORASpecimen setRequest(List<Reference> param);

    public ICORASpecimen setStatus(SpecimenStatus param);

    public ICORASpecimen setStatus(String param);

    public ICORASpecimen setStatusElement(Enumeration<SpecimenStatus> param);

    public ICORASpecimen setSubject(Reference param);

    public ICORASpecimen setType(CodeableConcept param);

    public ICORASpecimen setWrappedContainer(List<CORASpecimenContainer> param);
}
