/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Substance;
import org.hl7.fhir.dstu3.model.Substance.FHIRSubstanceStatus;


/**
 * DSTU3/HAPI2.4/BOM Substance
 *
 * @author Jerry Goodnough
 *
 */
public interface ICORASubstance extends ICORACognitiveBaseIdentifiable<ICORASubstance> {

    public Substance.SubstanceIngredientComponent addIngredient();

    public ICORASubstance addIngredient(Substance.SubstanceIngredientComponent param);

    public Substance.SubstanceInstanceComponent addInstance();

    public ICORASubstance addInstance(Substance.SubstanceInstanceComponent param);

    public CodeableConcept getCode();

    public String getDescription();

    public StringType getDescriptionElement();

    public List<Substance.SubstanceIngredientComponent> getIngredient();

    public Substance.SubstanceIngredientComponent getIngredientFirstRep();

    public List<Substance.SubstanceInstanceComponent> getInstance();

    public Substance.SubstanceInstanceComponent getInstanceFirstRep();

    public FHIRSubstanceStatus getStatus();

    public ICORASubstance setCode(CodeableConcept param);

    public ICORASubstance setDescription(String param);

    public ICORASubstance setDescriptionElement(StringType param);

    public ICORASubstance setIngredient(List<Substance.SubstanceIngredientComponent> param);

    public ICORASubstance setInstance(List<Substance.SubstanceInstanceComponent> param);

    public ICORASubstance setStatus(FHIRSubstanceStatus param);
}
