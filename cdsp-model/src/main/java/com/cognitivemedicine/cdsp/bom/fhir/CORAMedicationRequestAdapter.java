/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.MedicationRequest.MedicationRequestIntent;
import org.hl7.fhir.dstu3.model.MedicationRequest.MedicationRequestRequesterComponent;
import org.hl7.fhir.dstu3.model.MedicationRequest.MedicationRequestStatus;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;



public class CORAMedicationRequestAdapter
        extends CORACognitiveBaseIdentifiable<MedicationRequest, ICORAMedicationRequest>
        implements ICORAMedicationRequest {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAMedicationRequestAdapter() {
        this.adaptedClass = new MedicationRequest();
    }

    public CORAMedicationRequestAdapter(MedicationRequest adaptee) {
        this.adaptedClass = adaptee;
    }

    @Override
    public Reference addBasedOn() {

        return adaptedClass.addBasedOn();
    }

    @Override
    public ICORAMedicationRequest addBasedOn(Reference param) {
        adaptedClass.addBasedOn();
        return this;
    }

    @Override
    public Reference addDefinition() {

        return adaptedClass.addDefinition();
    }

    @Override
    public ICORAMedicationRequest addDefinition(Reference param) {
        adaptedClass.addDefinition();
        return this;
    }

    @Override
    public Reference addDetectedIssue() {

        return adaptedClass.addSupportingInformation();
    }

    @Override
    public ICORAMedicationRequest addDetectedIssue(Reference param) {
        adaptedClass.addDetectedIssue();
        return this;
    }


    @Override
    public Dosage addDosageInstruction() {

        return adaptedClass.addDosageInstruction();
    }

    @Override
    public ICORAMedicationRequest addDosageInstruction(Dosage param) {
        adaptedClass.addDosageInstruction(param);
        return this;
    }

    @Override
    public Reference addEventHistory() {
        return adaptedClass.addEventHistory();
    }

    @Override
    public ICORAMedicationRequest addEventHistory(Reference param) {
        adaptedClass.addEventHistory(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {
        Identifier item = new Identifier();
        adaptedClass.addIdentifier(item);
        return item;
    }

    @Override
    public ICORAMedicationRequest addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }


    @Override
    public Annotation addNote() {
        return adaptedClass.addNote();
    }

    @Override
    public ICORAMedicationRequest addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }


    @Override
    public CodeableConcept addReasonCode() {
        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAMedicationRequest addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {
        return adaptedClass.addReasonReference();
    }



    @Override
    public ICORAMedicationRequest addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }



    @Override
    public Reference addSupportingInformation() {

        return adaptedClass.addSupportingInformation();
    }

    @Override
    public ICORAMedicationRequest addSupportingInformation(Reference param) {
        adaptedClass.addSupportingInformation(param);
        return this;
    }


    @Override
    public CORADosage addWrappedDosageInstruction() {

        Dosage item = adaptedClass.addDosageInstruction();
        return new CORADosage(item);
    }

    @Override
    public ICORAMedicationRequest addWrappedDosageInstruction(CORADosage param) {
        if (param != null) {
            adaptedClass.addDosageInstruction(param.getAdaptee());
        }
        return this;
    }

    @Override
    public Date getAuthoredOn() {
        return adaptedClass.getAuthoredOn();
    }

    @Override
    public DateTimeType getAuthoredOnElement() {
        return adaptedClass.getAuthoredOnElement();
    }


    @Override
    public List<Reference> getBasedOn() {
        return adaptedClass.getBasedOn();
    }

    @Override
    public Reference getBasedOnFirstRep() {

        return adaptedClass.getBasedOnFirstRep();
    }


    @Override
    public Reference getContext() {
        return adaptedClass.getContext();

    }

    @Override
    public List<Reference> getDefinition() {
        return adaptedClass.getDefinition();
    }


    @Override
    public Reference getDefinitionFirstRep() {

        return adaptedClass.getDefinitionFirstRep();
    }

    @Override
    public List<Reference> getDetectedIssue() {
        return adaptedClass.getDetectedIssue();
    }

    @Override
    public Reference getDetectedIssueFirstRep() {

        return adaptedClass.getDetectedIssueFirstRep();
    }



    @Override
    public MedicationRequest.MedicationRequestDispenseRequestComponent getDispenseRequest() {
        return adaptedClass.getDispenseRequest();
    }


    @Override
    public List<Dosage> getDosageInstruction() {
        return adaptedClass.getDosageInstruction();
    }

    @Override
    public Dosage getDosageInstructionFirstRep() {
        return adaptedClass.getDosageInstructionFirstRep();
    }

    @Override
    @LogicalModelAlias("getContext")
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getContext().getResource() instanceof Encounter) {
            CORAEncounterAdapter profiledType = new CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getContext().getResource());
            return profiledType;
        } else {
            return null;
        }
    }


    @Override
    public List<Reference> getEventHistory() {
        return adaptedClass.getEventHistory();
    }



    @Override
    public Reference getEventHistoryFirstRep() {
        return adaptedClass.getEventHistoryFirstRep();
    }

    @Override
    public Identifier getGroupIdentifier() {
        return adaptedClass.getGroupIdentifier();
    }


    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public MedicationRequestIntent getIntent() {
        return adaptedClass.getIntent();
    }

    @Override
    public CodeableConcept getMedicationCodeableConcept() {
        if (adaptedClass.getMedication() != null
                && adaptedClass.getMedication() instanceof CodeableConcept) {
            return (CodeableConcept) adaptedClass.getMedication();
        } else {
            return null;
        }
    }

    @Override
    public Reference getMedicationReference() {
        if (adaptedClass.getMedication() != null
                && adaptedClass.getMedication() instanceof Reference) {
            return (Reference) adaptedClass.getMedication();
        } else {
            return null;
        }
    }

    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getSubject().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public MedicationRequest getPriorPrescriptionResource() {
        return adaptedClass.getPriorPrescriptionTarget();
    }

    @Override
    public List<CodeableConcept> getReasonCode() {
        return adaptedClass.getReasonCode();

    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {
        return adaptedClass.getReasonCodeFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {
        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstRep() {
        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public MedicationRequestRequesterComponent getRequester() {
        return adaptedClass.getRequester();
    }

    @Override
    public MedicationRequestStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<MedicationRequestStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }


    @Override
    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public MedicationRequest.MedicationRequestSubstitutionComponent getSubstitution() {
        return adaptedClass.getSubstitution();
    }

    @Override
    public List<Reference> getSupportingInformation() {
        return adaptedClass.getSupportingInformation();
    }

    @Override
    public Reference getSupportingInformationFirstRep() {

        return adaptedClass.getSupportingInformationFirstRep();
    }

    @Override
    public List<CORADosage> getWrappedDosageInstruction() {
        List<CORADosage> items = new java.util.ArrayList<>();
        for (Dosage type : adaptedClass.getDosageInstruction()) {
            items.add(new CORADosage(type));
        }
        return items;
    }

    @Override
    public CORADosage getWrappedDosageInstructionFirstRep() {
        CORADosage wrapperItem = new CORADosage();
        Dosage item = adaptedClass.getDosageInstructionFirstRep();
        if (item != null) {
            wrapperItem = new CORADosage(item);
        }
        return wrapperItem;
    }

    @Override
    public ICORAMedicationRequest setAuthoredOn(Date param) {
        adaptedClass.setAuthoredOn(param);
        return this;
    }


    @Override
    public ICORAMedicationRequest setAuthoredOnElement(DateTimeType param) {
        adaptedClass.setAuthoredOnElement(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setBasedOn(List<Reference> param) {
        adaptedClass.setBasedOn(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    public ICORAMedicationRequest setDateEnded(DateTimeType param) {
        adaptedClass.setAuthoredOnElement(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setDefinition(List<Reference> param) {
        adaptedClass.setDefinition(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setDetectedIssue(List<Reference> param) {
        adaptedClass.setDetectedIssue(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setDispenseRequest(
            MedicationRequest.MedicationRequestDispenseRequestComponent param) {
        adaptedClass.setDispenseRequest(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setDosageInstruction(List<Dosage> param) {
        adaptedClass.setDosageInstruction(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getContext().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedicationRequest setEventHistory(List<Reference> param) {
        adaptedClass.setEventHistory(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setGroupIdentifier(Identifier param) {
        adaptedClass.setGroupIdentifier(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setIntent(MedicationRequestIntent param) {
        adaptedClass.setIntent(param);
        return this;

    }

    @Override
    public ICORAMedicationRequest setMedicationCodeableConcept(CodeableConcept param) {
        adaptedClass.setMedication(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setMedicationReference(Reference param) {
        adaptedClass.setMedication(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getSubject().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedicationRequest setPriorPrescriptionResource(MedicationRequest param) {
        adaptedClass.getPriorPrescription().setResource(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setRequester(MedicationRequestRequesterComponent param) {
        adaptedClass.setRequester(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setStatus(MedicationRequestStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setStatus(String param) {
        adaptedClass.setStatus(MedicationRequestStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAMedicationRequest setStatusElement(Enumeration<MedicationRequestStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setSubstitution(
            MedicationRequest.MedicationRequestSubstitutionComponent param) {
        adaptedClass.setSubstitution(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setSupportingInformation(List<Reference> param) {
        adaptedClass.setSupportingInformation(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setText(Narrative param) {
        adaptedClass.setText(param);
        return this;
    }

    @Override
    public ICORAMedicationRequest setWrappedDosageInstruction(List<CORADosage> param) {
        List<Dosage> items = new java.util.ArrayList<>();
        for (CORADosage item : param) {
            items.add(item.getAdaptee());
        }
        adaptedClass.setDosageInstruction(items);
        return this;
    }

    @Override
    public String toString() {
        final int maxLen = 10;
        return "CORAMedicationRequestAdapter [getBasedOn()="
                + (getBasedOn() != null
                        ? getBasedOn().subList(0, Math.min(getBasedOn().size(), maxLen)) : null)
                + ", getContext()=" + getContext() + ", getDefinition()="
                + (getDefinition() != null
                        ? getDefinition().subList(0, Math.min(getDefinition().size(), maxLen))
                        : null)
                + ", getDetectedIssue()="
                + (getDetectedIssue() != null
                        ? getDetectedIssue().subList(0, Math.min(getDetectedIssue().size(), maxLen))
                        : null)
                + ", getDispenseRequest()=" + getDispenseRequest() + ", getDosageInstruction()="
                + (getDosageInstruction() != null ? getDosageInstruction().subList(0,
                        Math.min(getDosageInstruction().size(), maxLen)) : null)
                + ", getEventHistory()="
                + (getEventHistory() != null
                        ? getEventHistory().subList(0, Math.min(getEventHistory().size(), maxLen))
                        : null)
                + ", getGroupIdentifier()=" + getGroupIdentifier() + ", getIntent()=" + getIntent()
                + ", getMedicationCodeableConcept()=" + getMedicationCodeableConcept()
                + ", getMedicationReference()=" + getMedicationReference() + ", getNote()="
                + (getNote() != null ? getNote().subList(0, Math.min(getNote().size(), maxLen))
                        : null)
                + ", getPriorPrescriptionResource()=" + getPriorPrescriptionResource()
                + ", getReasonCode()="
                + (getReasonCode() != null
                        ? getReasonCode().subList(0, Math.min(getReasonCode().size(), maxLen))
                        : null)
                + ", getReasonReference()="
                + (getReasonReference() != null ? getReasonReference().subList(0,
                        Math.min(getReasonReference().size(), maxLen)) : null)
                + ", getRequester()=" + getRequester() + ", getStatus()=" + getStatus()
                + ", getSubject()=" + getSubject() + ", getSubstitution()=" + getSubstitution()
                + ", getSupportingInformation()="
                + (getSupportingInformation() != null ? getSupportingInformation().subList(0,
                        Math.min(getSupportingInformation().size(), maxLen)) : null)
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }


}
