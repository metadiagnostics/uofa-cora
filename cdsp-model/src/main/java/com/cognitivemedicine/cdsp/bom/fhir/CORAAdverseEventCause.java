/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Extension;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;

public class CORAAdverseEventCause {

    public static final String uri =
            "http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-cause";
    private Extension adaptedClass = null;
    private Extension rootObjectExtension = new Extension(uri);

    public CORAAdverseEventCause() {
        this.adaptedClass = new Extension();
    }

    public CORAAdverseEventCause(Extension adaptee) {
        this.adaptedClass = adaptee;
    }

    @LogicalModelChange("Change BaseResource to DomainResource")
    public Extension bindTemplateToParent(DomainResource containingResource) {
        rootObjectExtension = new Extension();
        rootObjectExtension.setUrl(uri);
        containingResource.addExtension(rootObjectExtension);
        return rootObjectExtension;
    }

    public Extension getAdaptee() {
        return adaptedClass;
    }

    public CodeableConcept getCertainty() {
        CodeableConcept returnValue;
        java.util.List<Extension> extensions = rootObjectExtension.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-cause#certainty");
        if (extensions.size() == 1) {
            returnValue = (CodeableConcept) extensions.get(0).getValue();
        } else if (extensions.size() == 0) {
            returnValue = null;
        } else {
            throw new IllegalStateException("More than one extension specified for this object.");
        }
        return returnValue;
    }

    public void getItem() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public Extension getRootObjectExtension() {
        return rootObjectExtension;
    }

    public void setAdaptee(Extension param) {
        this.adaptedClass = param;
    }

    public CORAAdverseEventCause setCertainty(CodeableConcept param) {
        java.util.List<Extension> extensions = rootObjectExtension.getExtensionsByUrl(
                "http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-cause#certainty");
        if (extensions.size() == 1) {
            extensions.get(0).setValue(param);
        } else if (extensions.size() == 0) {
            rootObjectExtension.addExtension()
                    .setUrl("http://hl7.org/fhir/StructureDefinition/QICore-adverseevent-cause#certainty")
                    .setValue(param);
        } else {
            throw new IllegalStateException("More than one extension specified for this object.");
        }
        return this;
    }

    public CORAAdverseEventCause setItem() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setRootObjectExtension(Extension rootObjectExtension) {
        this.rootObjectExtension = rootObjectExtension;
    }
}
