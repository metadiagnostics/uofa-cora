/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.List;

import org.hl7.fhir.dstu3.model.DomainResource;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Resource;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelUpdate;

/*
 * import Identifier;
 */

/**
 * @author Jerry Goodnough
 *
 */
public interface ICORACognitiveBaseIdentifiable<T> extends ICORACognitiveBase<T> {

    @LogicalModelAddition("To be added to IQCore")
    public Identifier getPrimaryIdentifer();

    @LogicalModelAddition("To be added to IQCore")
    public T setPrimaryIdentifer(Identifier param);

    public List<Identifier> getIdentifier();

    public T setIdentifier(List<Identifier> param);

    public T addIdentifier(Identifier param);

    public Identifier addIdentifier();

    public Identifier getIdentifierFirstRep();

    public Identifier getFirstIdentiferBySystem(String system);

    @LogicalModelUpdate("Updated for DSTU3 to return a list")
    public List<Resource> getContained();
    
    @LogicalModelUpdate("Updated for DSTU3 to use a list")
    public T setContained(List<Resource> param);
    
    @LogicalModelUpdate("Updated for DSTU3")
    public DomainResource addContained(Resource param);
    
    public Narrative getText();
    
    public T setText(Narrative param);
    
    

}
