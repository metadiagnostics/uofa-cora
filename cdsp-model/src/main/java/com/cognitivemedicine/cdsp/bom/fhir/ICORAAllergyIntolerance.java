/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Age;
import org.hl7.fhir.dstu3.model.AllergyIntolerance;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceCategory;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceClinicalStatus;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceCriticality;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceType;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceVerificationStatus;
import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Extension;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.exceptions.FHIRException;


public interface ICORAAllergyIntolerance
        extends ICORACognitiveBaseIdentifiable<ICORAAllergyIntolerance> {


    public ICORAAllergyIntolerance addCategory(AllergyIntoleranceCategory param);
    
    public Enumeration<AllergyIntoleranceCategory> addCategoyElement();

    public AllergyIntolerance.AllergyIntoleranceReactionComponent addReaction();

    public ICORAAllergyIntolerance addReaction(
            AllergyIntolerance.AllergyIntoleranceReactionComponent param);

    public CORAAllergyIntoleranceReaction addWrappedReaction();

    public ICORAAllergyIntolerance addWrappedReaction(CORAAllergyIntoleranceReaction param);

    public Date getAssertedDate();

    public DateTimeType getAssertedDateElement();

    public Reference getAssertor();

    public List<Enumeration<AllergyIntoleranceCategory>> getCategory();

     public AllergyIntoleranceClinicalStatus getClinicalStatus();

    public Enumeration<AllergyIntoleranceClinicalStatus> getClinicalStatusElement();

    public CodeableConcept getCode();

    public AllergyIntoleranceCriticality getCriticality();

    public Enumeration<AllergyIntoleranceCriticality> getCriticalityElement();

    public Date getLastOccurrence();

    public DateTimeType getLastOccurrenceElement();

    public List<Annotation> getNote();

    public Annotation getNoteFirstReq();

    public Type getOnset();

    public Age getOnsetAge() throws FHIRException;

    public DateTimeType getOnsetDateTypeType() throws FHIRException;

    public Period getOnsetPeriod() throws FHIRException;

    public Range getOnsetRange() throws FHIRException;

    public StringType getOnsetStringType() throws FHIRException;

    public CORAPatientAdapter getPatientResource();

    public List<AllergyIntolerance.AllergyIntoleranceReactionComponent> getReaction();

    public AllergyIntolerance.AllergyIntoleranceReactionComponent getReactionFirstRep();

    public CodeableConcept getReasonRefuted();

    public Reference getRecorder();

    public Extension getResolutionAge();

    public AllergyIntoleranceType getType();

    public Enumeration<AllergyIntoleranceType> getTypeElement();

    public AllergyIntoleranceVerificationStatus getVerificationStatus();

    public Enumeration<AllergyIntoleranceVerificationStatus> getVerificationStatusElement();

    public List<CORAAllergyIntoleranceReaction> getWrappedReaction();

    public CORAAllergyIntoleranceReaction getWrappedReactionFirstRep();

    public ICORAAllergyIntolerance setAssertedDate(Date param);

    public ICORAAllergyIntolerance setAssertedDateElement(DateTimeType param);

    public ICORAAllergyIntolerance setAsserter(Reference param);

    public ICORAAllergyIntolerance setCategory(List<Enumeration<AllergyIntoleranceCategory>> param);

    public ICORAAllergyIntolerance setClinicialStatus(AllergyIntoleranceClinicalStatus value);

    public ICORAAllergyIntolerance setClinicialStatusElement(
            Enumeration<AllergyIntoleranceClinicalStatus> value);

    public ICORAAllergyIntolerance setCode(CodeableConcept value);

    public ICORAAllergyIntolerance setCriticality(AllergyIntoleranceCriticality param);

    public ICORAAllergyIntolerance setCriticalityElement(Enumeration<AllergyIntoleranceCriticality> param);

    public ICORAAllergyIntolerance setLastOccurrence(Date param);

    public ICORAAllergyIntolerance setLastOccurrenceElement(DateTimeType param);

    public ICORAAllergyIntolerance setNote(List<Annotation> param);

    public ICORAAllergyIntolerance setOnset(Type param);

    public ICORAAllergyIntolerance setOnsetAge(Age param);

    public ICORAAllergyIntolerance setOnsetDateTypeType(DateTimeType param);

    public ICORAAllergyIntolerance setOnsetPeriod(Period param);

    public ICORAAllergyIntolerance setOnsetRange(Range param);

    public ICORAAllergyIntolerance setOnsetStringType(StringType param);

    public ICORAAllergyIntolerance setPatientResource(CORAPatientAdapter param);

    public ICORAAllergyIntolerance setReaction(
            List<AllergyIntolerance.AllergyIntoleranceReactionComponent> param);

    public ICORAAllergyIntolerance setReasonRefuted(CodeableConcept param);

    public ICORAAllergyIntolerance setRecorder(Reference param);

    public ICORAAllergyIntolerance setResolutionAge(Extension param);


    public ICORAAllergyIntolerance setType(AllergyIntoleranceType param);

    public ICORAAllergyIntolerance setTypeElement(Enumeration<AllergyIntoleranceType> param);

    public ICORAAllergyIntolerance setVerificationStatus(
            AllergyIntoleranceVerificationStatus value);

    public ICORAAllergyIntolerance setVerificationStatusElement(
            Enumeration<AllergyIntoleranceVerificationStatus> value);

    public ICORAAllergyIntolerance setWrappedReaction(List<CORAAllergyIntoleranceReaction> param);
}
