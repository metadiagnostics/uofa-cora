/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Age;
import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.dstu3.model.FamilyMemberHistory;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Range;
import org.hl7.fhir.dstu3.model.Reference;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;

/**
 * DTSU3 / BOM Familu History
 * 
 * @author Jerry Goodnough
 * 
 *         Missing Elements definition
 * 
 *
 */
// TODO - Look at how to make the Conditions also fluent

@LogicalModelAddition("Extends our base")
public interface ICORAFamilyMemberHistory
        extends ICORACognitiveBaseIdentifiable<ICORAFamilyMemberHistory> {

    public FamilyMemberHistory.FamilyMemberHistoryConditionComponent addCodition(
            FamilyMemberHistory.FamilyMemberHistoryConditionComponent param);

    public FamilyMemberHistory.FamilyMemberHistoryConditionComponent addCondition();

    public CodeableConcept addReasonCode();

    public ICORAFamilyMemberHistory addReasonCode(CodeableConcept param);

    public Reference addReasonReference();

    public ICORAFamilyMemberHistory addReasonReference(Reference param);


    // Age
    public Age getAgeAge();

    public Range getAgeRange();

    public String getAgeString();

    public DateType getBornDate();

    public Period getBornPeriod();

    public String getBornString();

    public List<FamilyMemberHistory.FamilyMemberHistoryConditionComponent> getCondition();

    public FamilyMemberHistory.FamilyMemberHistoryConditionComponent getConditionFirstRep();

    public Date getDate();

    public DateTimeType getDateElement();

    public Age getDeceasedAge();

    // deceased;
    public Boolean getDeceasedBoolean();

    public DateType getDeceasedDate();


    public Range getDeceasedRange();

    public String getDeceasedString();

    public boolean getEstimatedAge();

    public AdministrativeGender getGender();

    public Enumeration<AdministrativeGender> getGenderElement();

    public String getName();

    public boolean getNotDone();

    public CodeableConcept getNotDoneReason();

    public CORAPatientAdapter getPatientResource();

    public List<CodeableConcept> getReasonCode();

    public CodeableConcept getReasonCodeFirstRep();

    public List<Reference> getReasonReference();

    public Reference getReasonReferenceFirstRep();

    public CodeableConcept getRelationship();

    public FamilyMemberHistory.FamilyHistoryStatus getStatus();

    public Enumeration<FamilyMemberHistory.FamilyHistoryStatus> getStatusElement();

    public ICORAFamilyMemberHistory setAgeQuantity(Age param);

    public ICORAFamilyMemberHistory setAgeRange(Range param);

    public ICORAFamilyMemberHistory setAgeString(String param);

    public ICORAFamilyMemberHistory setBornDate(DateType param);

    public ICORAFamilyMemberHistory setBornPeriod(Period param);

    public ICORAFamilyMemberHistory setBornString(String param);

    public ICORAFamilyMemberHistory setCondition(
            List<FamilyMemberHistory.FamilyMemberHistoryConditionComponent> param);

    public ICORAFamilyMemberHistory setDate(Date date);

    public ICORAFamilyMemberHistory setDateElement(DateTimeType date);

    public ICORAFamilyMemberHistory setDeceasedBoolean(Boolean param);

    public ICORAFamilyMemberHistory setDeceasedDate(DateType param);

    public ICORAFamilyMemberHistory setDeceasedQuantity(Age param);

    public ICORAFamilyMemberHistory setDeceasedRange(Range param);

    public ICORAFamilyMemberHistory setDeceasedString(String param);

    public ICORAFamilyMemberHistory setEstimatedAge(boolean param);

    public ICORAFamilyMemberHistory setGender(AdministrativeGender param);

    public ICORAFamilyMemberHistory setGender(String param);

    public ICORAFamilyMemberHistory setGenderElement(Enumeration<AdministrativeGender> param);

    public ICORAFamilyMemberHistory setName(String name);

    public ICORAFamilyMemberHistory setNotDone(boolean param);

    public ICORAFamilyMemberHistory setNotDoneReason(CodeableConcept param);

    public ICORAFamilyMemberHistory setPatientResource(CORAPatientAdapter param);

    public ICORAFamilyMemberHistory setReasonCode(List<CodeableConcept> param);

    public ICORAFamilyMemberHistory setReasonReference(List<Reference> param);

    public ICORAFamilyMemberHistory setRelationship(CodeableConcept param);

    public ICORAFamilyMemberHistory setStatus(FamilyMemberHistory.FamilyHistoryStatus param);

    public ICORAFamilyMemberHistory setStatusElement(
            Enumeration<FamilyMemberHistory.FamilyHistoryStatus> param);


}
