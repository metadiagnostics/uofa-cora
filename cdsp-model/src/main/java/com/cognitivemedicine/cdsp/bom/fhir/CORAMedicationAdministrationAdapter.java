/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.bom.fhir;

import java.util.Date;
import java.util.List;

import org.hl7.fhir.dstu3.model.Annotation;
import org.hl7.fhir.dstu3.model.BooleanType;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Enumeration;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Period;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Type;
import org.hl7.fhir.dstu3.model.MedicationAdministration.MedicationAdministrationPerformerComponent;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAlias;



public class CORAMedicationAdministrationAdapter extends
        CORACognitiveBaseIdentifiable<MedicationAdministration, ICORAMedicationAdministration>
        implements ICORAMedicationAdministration {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CORAMedicationAdministrationAdapter() {
        this.adaptedClass = new MedicationAdministration();
    }

    public CORAMedicationAdministrationAdapter(MedicationAdministration adaptee) {
        this.adaptedClass = adaptee;
    }


    @Override
    public Reference addDefinition() {
        return adaptedClass.addDefinition();
    }


    @Override
    public ICORAMedicationAdministration addDefinition(Reference param) {
        adaptedClass.addDefinition(param);
        return this;
    }

    @Override
    public Reference addEventHistory() {
        return adaptedClass.addEventHistory();
    }

    @Override
    public ICORAMedicationAdministration addEventHistory(Reference param) {
        adaptedClass.addEventHistory(param);
        return this;
    }

    @Override
    public Identifier addIdentifier() {

        return adaptedClass.addIdentifier();
    }

    @Override
    public ICORAMedicationAdministration addIdentifier(Identifier param) {
        adaptedClass.addIdentifier(param);
        return this;
    }


    @Override
    public Annotation addNote() {
        return adaptedClass.addNote();
    }

    @Override
    public ICORAMedicationAdministration addNote(Annotation param) {
        adaptedClass.addNote(param);
        return this;
    }

    @Override
    public Reference addPartOf() {
        return adaptedClass.addPartOf();
    }

    @Override
    public ICORAMedicationAdministration addPartOf(Reference param) {
        adaptedClass.addPartOf(param);
        return this;
    }

    @Override
    public MedicationAdministrationPerformerComponent addPerformer() {
        return adaptedClass.addPerformer();
    }

    @Override
    public ICORAMedicationAdministration addPerformer(
            MedicationAdministrationPerformerComponent param) {
        adaptedClass.addPerformer(param);
        return this;
    }

    @Override
    public CodeableConcept addReasonCode() {
        return adaptedClass.addReasonCode();
    }

    @Override
    public ICORAMedicationAdministration addReasonCode(CodeableConcept param) {
        adaptedClass.addReasonCode(param);
        return this;
    }

    @Override
    public Reference addReasonReference() {
        return adaptedClass.addReasonReference();
    }


    @Override
    public ICORAMedicationAdministration addReasonReference(Reference param) {
        adaptedClass.addReasonReference(param);
        return this;
    }

    @Override
    public Reference addSupportingInformation() {
        return adaptedClass.addSupportingInformation();
    }

    @Override
    public ICORAMedicationAdministration addSupportingInformation(Reference param) {
        adaptedClass.addSupportingInformation(param);
        return this;
    }

    @Override
    public CodeableConcept getCategory() {
        return adaptedClass.getCategory();
    }

    @Override
    public Reference getContext() {
        return adaptedClass.getContext();
    }

    @Override
    public List<Reference> getDefinition() {
        return adaptedClass.getDefinition();
    }

    @Override
    public Reference getDefinitionFirstRep() {
        return adaptedClass.getDefinitionFirstRep();
    }

    @Override
    public MedicationAdministration.MedicationAdministrationDosageComponent getDosage() {
        return adaptedClass.getDosage();
    }


    @Override
    public Type getEffective() {
        return adaptedClass.getEffective();
    }

    @Override
    @LogicalModelAlias("getEffective")
    public Date getEffectiveTimeDateTime() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return ((DateTimeType) adaptedClass.getEffective()).getValue();
        } else {
            return null;
        }
    }


    @Override
    @LogicalModelAlias("getEffective")
    public DateTimeType getEffectiveTimeDateTimeElement() {
        if (adaptedClass.getEffective() != null
                && adaptedClass.getEffective() instanceof DateTimeType) {
            return (DateTimeType) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    @LogicalModelAlias("getEffective")
    public Period getEffectiveTimePeriod() {
        if (adaptedClass.getEffective() != null && adaptedClass.getEffective() instanceof Period) {
            return (Period) adaptedClass.getEffective();
        } else {
            return null;
        }
    }

    @Override
    public CORAEncounterAdapter getEncounterResource() {
        if (adaptedClass.getContext().getResource() instanceof Encounter) {
            com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterAdapter profiledType =
                    new com.cognitivemedicine.cdsp.bom.fhir.CORAEncounterAdapter();
            profiledType.setAdaptee((Encounter) adaptedClass.getContext().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<Reference> getEventHistory() {
        return adaptedClass.getEventHistory();
    }

    @Override
    public Reference getEventHistoryFirstRep() {
        return adaptedClass.getEventHistoryFirstRep();
    }

    @Override
    public List<Identifier> getIdentifier() {
        return adaptedClass.getIdentifier();
    }

    @Override
    public Identifier getIdentifierFirstRep() {
        return adaptedClass.getIdentifierFirstRep();
    }

    @Override
    public CodeableConcept getMedicationCodeableConcept() {
        if (adaptedClass.getMedication() != null
                && adaptedClass.getMedication() instanceof CodeableConcept) {
            return (CodeableConcept) adaptedClass.getMedication();
        } else {
            return null;
        }
    }

    @Override
    public Reference getMedicationReference() {
        if (adaptedClass.getMedication() != null
                && adaptedClass.getMedication() instanceof Reference) {
            return (Reference) adaptedClass.getMedication();
        } else {
            return null;
        }
    }

    @Override
    public List<Annotation> getNote() {
        return adaptedClass.getNote();
    }

    @Override
    public Annotation getNoteFirstRep() {
        return adaptedClass.getNoteFirstRep();
    }

    @Override
    public Boolean getNotGiven() {
        return adaptedClass.getNotGiven();
    }

    @Override
    public BooleanType getNotGivenElement() {
        return adaptedClass.getNotGivenElement();
    }

    @Override
    public List<Reference> getPartOf() {
        return adaptedClass.getPartOf();
    };

    @Override
    public Reference getPartOfFirstRep() {
        return adaptedClass.getPartOfFirstRep();
    }

    @Override
    @LogicalModelAlias("getSubject")
    public CORAPatientAdapter getPatientResource() {
        if (adaptedClass.getSubject().getResource() instanceof Patient) {
            CORAPatientAdapter profiledType = new CORAPatientAdapter();
            profiledType.setAdaptee((Patient) adaptedClass.getSubject().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<MedicationAdministrationPerformerComponent> getPerformer() {
        return adaptedClass.getPerformer();
    }

    @Override
    public MedicationAdministrationPerformerComponent getPerformerFirstRep() {
        return adaptedClass.getPerformerFirstRep();
    }

    @Override
    public CORAMedicationRequestAdapter getPrescriptionResource() {
        if (adaptedClass.getPrescription().getResource() instanceof MedicationRequest) {
            CORAMedicationRequestAdapter profiledType = new CORAMedicationRequestAdapter();
            profiledType
                    .setAdaptee((MedicationRequest) adaptedClass.getPrescription().getResource());
            return profiledType;
        } else {
            return null;
        }
    }

    @Override
    public List<CodeableConcept> getReasonCode() {
        return adaptedClass.getReasonCode();
    }

    @Override
    public CodeableConcept getReasonCodeFirstRep() {
        return adaptedClass.getReasonCodeFirstRep();
    }

    @Override
    public List<Reference> getReasonReference() {
        return adaptedClass.getReasonReference();
    }

    @Override
    public Reference getReasonReferenceFirstReq() {
        return adaptedClass.getReasonReferenceFirstRep();
    }

    @Override
    public MedicationAdministration.MedicationAdministrationStatus getStatus() {
        return adaptedClass.getStatus();
    }

    @Override
    public Enumeration<MedicationAdministration.MedicationAdministrationStatus> getStatusElement() {
        return adaptedClass.getStatusElement();
    }

    @Override
    public Reference getSubject() {
        return adaptedClass.getSubject();
    }

    @Override
    public List<Reference> getSupportingInformation() {
        return adaptedClass.getSupportingInformation();
    }


    @Override
    public Reference getSupportingInformationFirstRep() {
        return adaptedClass.getSupportingInformationFirstRep();
    }

    @Override
    public ICORAMedicationAdministration setCategory(CodeableConcept param) {
        adaptedClass.setCategory(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setContext(Reference param) {
        adaptedClass.setContext(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setDefinition(List<Reference> param) {
        adaptedClass.setDefinition(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setDosage(
            MedicationAdministration.MedicationAdministrationDosageComponent param) {
        adaptedClass.setDosage(param);
        return this;
    }


    @Override
    public ICORAMedicationAdministration setEffective(Type param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setEffective")
    public ICORAMedicationAdministration setEffectiveTimeDateTime(Date param) {
        adaptedClass.setEffective(new DateTimeType(param));
        return this;
    }

    @Override
    @LogicalModelAlias("setEffective")
    public ICORAMedicationAdministration setEffectiveTimeDateTime(DateTimeType param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setEffective")
    public ICORAMedicationAdministration setEffectiveTimePeriod(Period param) {
        adaptedClass.setEffective(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setEncounterResource(CORAEncounterAdapter param) {
        adaptedClass.getContext().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedicationAdministration setEventHistory(List<Reference> param) {
        adaptedClass.setEventHistory(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setIdentifier(List<Identifier> param) {
        adaptedClass.setIdentifier(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setMedicationCodeableConcept(CodeableConcept param) {
        adaptedClass.setMedication(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setMedicationReference(Reference param) {
        adaptedClass.setMedication(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setNote(List<Annotation> param) {
        adaptedClass.setNote(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setNotGiven(boolean param) {
        adaptedClass.setNotGiven(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setNotGivenElement(BooleanType param) {
        adaptedClass.setNotGivenElement(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setPartOf(List<Reference> param) {
        adaptedClass.setPartOf(param);
        return this;
    }

    @Override
    @LogicalModelAlias("setSubject")
    public ICORAMedicationAdministration setPatientResource(CORAPatientAdapter param) {
        adaptedClass.getSubject().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedicationAdministration setPerformer(
            List<MedicationAdministrationPerformerComponent> param) {
        adaptedClass.setPerformer(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setPrescriptionResource(
            CORAMedicationRequestAdapter param) {
        adaptedClass.getPrescription().setResource(param.getAdaptee());
        return this;
    }

    @Override
    public ICORAMedicationAdministration setReasonCode(List<CodeableConcept> param) {
        adaptedClass.setReasonCode(param);
        return this;
    }


    @Override
    public ICORAMedicationAdministration setReasonReference(List<Reference> param) {
        adaptedClass.setReasonReference(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setStatus(
            MedicationAdministration.MedicationAdministrationStatus param) {
        adaptedClass.setStatus(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setStatus(String param) {
        adaptedClass
                .setStatus(MedicationAdministration.MedicationAdministrationStatus.valueOf(param));
        return this;
    }

    @Override
    public ICORAMedicationAdministration setStatusElement(
            Enumeration<MedicationAdministration.MedicationAdministrationStatus> param) {
        adaptedClass.setStatusElement(param);
        return this;
    }

    @Override
    public ICORAMedicationAdministration setSubject(Reference param) {
        adaptedClass.setSubject(param);
        return this;
    }


    @Override
    public ICORAMedicationAdministration setSupportingInformation(List<Reference> param) {
        adaptedClass.setSupportingInformation(param);
        return this;
    }

    @Override
    public String toString() {
        final int maxLen = 10;
        return "CORAMedicationAdministrationAdapter [getCategory()=" + getCategory()
                + ", getContext()=" + getContext() + ", getDefinition()="
                + (getDefinition() != null
                        ? getDefinition().subList(0, Math.min(getDefinition().size(), maxLen))
                        : null)
                + ", getDosage()=" + getDosage() + ", getEffective()=" + getEffective()
                + ", getEventHistory()="
                + (getEventHistory() != null
                        ? getEventHistory().subList(0, Math.min(getEventHistory().size(), maxLen))
                        : null)
                + ", getIdentifier()="
                + (getIdentifier() != null
                        ? getIdentifier().subList(0, Math.min(getIdentifier().size(), maxLen))
                        : null)
                + ", getMedicationCodeableConcept()=" + getMedicationCodeableConcept()
                + ", getMedicationReference()=" + getMedicationReference() + ", getNote()="
                + (getNote() != null ? getNote().subList(0, Math.min(getNote().size(), maxLen))
                        : null)
                + ", getNotGiven()=" + getNotGiven() + ", getPartOf()="
                + (getPartOf() != null
                        ? getPartOf().subList(0, Math.min(getPartOf().size(), maxLen)) : null)
                + ", getPerformer()="
                + (getPerformer() != null
                        ? getPerformer().subList(0, Math.min(getPerformer().size(), maxLen)) : null)
                + ", getPrescriptionResource()=" + getPrescriptionResource() + ", getReasonCode()="
                + (getReasonCode() != null
                        ? getReasonCode().subList(0, Math.min(getReasonCode().size(), maxLen))
                        : null)
                + ", getReasonReference()="
                + (getReasonReference() != null ? getReasonReference().subList(0,
                        Math.min(getReasonReference().size(), maxLen)) : null)
                + ", getStatus()=" + getStatus() + ", getSubject()=" + getSubject()
                + ", getSupportingInformation()="
                + (getSupportingInformation() != null ? getSupportingInformation().subList(0,
                        Math.min(getSupportingInformation().size(), maxLen)) : null)
                + ", getPrimaryIdentifer()=" + getPrimaryIdentifer() + ", getCORAContext()="
                + getCORAContext() + "]";
    }



}
