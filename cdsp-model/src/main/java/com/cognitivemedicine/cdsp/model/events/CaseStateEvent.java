/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.model.events;

import com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.constant.Constants;

public class CaseStateEvent extends Event {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static enum CaseTypeEnum {
        IN01(Constants.COG_EVENT_CODE_SYSTEM, "IN01", "Case State")
        ;
        
        private final CodingDt coding;

        private CaseTypeEnum(String codeSystem, String code, String display) {
            this.coding = new CodingDt(codeSystem, code);
            this.coding.setDisplay(display);
        }

        public CodingDt getCoding() {
            return coding;
        }
        
    }
    
    
    public static enum CaseStateEnum {
        CASE_OPEN(Constants.COG_CASE_STATE_CODE_SYSTEM, "O", "Case Open"),
        CASE_CLOSE(Constants.COG_CASE_STATE_CODE_SYSTEM, "C", "Case Closed"),
        START_RECORD(Constants.COG_CASE_STATE_CODE_SYSTEM, "-3", "Start Record"),
        END_RECORD(Constants.COG_CASE_STATE_CODE_SYSTEM, "-4", "End Record"),
        SURGERY_START(Constants.COG_CASE_STATE_CODE_SYSTEM, "-9", "Surgery Start"),
        SURGERY_END(Constants.COG_CASE_STATE_CODE_SYSTEM, "-10", "Surgery Stop")
        ;
        
        private final CodingDt coding;

        private CaseStateEnum(String codeSystem, String code, String display) {
            this.coding = new CodingDt(codeSystem, code);
            this.coding.setDisplay(display);
        }
        
        public static CaseStateEnum fromCodingDt(CodingDt code){
            for (CaseStateEnum state : CaseStateEnum.values()) {
                if (state.isEquals(code)){
                    return state;
                }
            }
            throw new IllegalArgumentException("Unknown State for Coding "+code);
        }
        
        public static boolean existsForCoding(CodingDt code){
            try{
                fromCodingDt(code);
                return true;
            } catch(IllegalArgumentException e){
                return false;
            }
        }

        public CodingDt getCoding() {
            return coding;
        }
        
        public boolean isEquals(CodingDt other){
            return 
                this.coding.getSystem().equals(other.getSystem()) &&
                this.coding.getCode().equals(other.getCode());
        }
        
    }
    
	private String caseId;
	
	private CodingDt caseState;
	
	private String patientId;
	
	private String workstationId;
    
    private CORAPatientAdapter patient;

	public CaseStateEvent(String caseId, CaseStateEnum caseState, String patientId, String workstationId) {
		this.caseId = caseId;
		this.caseState = caseState.getCoding();
		this.patientId = patientId;
		this.workstationId = workstationId;
		this.setType(CaseTypeEnum.IN01.getCoding());
	}
    
	public CaseStateEvent(String caseId, CaseStateEnum caseState, String patientId, String workstationId, CORAPatientAdapter patient) {
        this(caseId, caseState, patientId, workstationId);
        this.patient = patient;
    }

	public String getCaseId() {
		return caseId;
	}

	public CodingDt getCaseState() {
		return caseState;
	}

	public String getPatientId() {
		return patientId;
	}

	public String getWorkstationId() {
		return workstationId;
	}

    public CORAPatientAdapter getPatient() {
        return patient;
    }

    @Override
    public String toString() {
        return "CaseStateEvent{" + "caseId=" + caseId + ", caseState=" + caseState + ", patientId=" + patientId + ", workstationId=" + workstationId + ", time= "+getTime()+", patient=" + (patient == null ? null : patient.hashCode()) + '}';
    }
    
    
}
