/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.model.events;

import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;

public class EligibilityEvent extends ContextAwareEvent {
    
    private static final long serialVersionUID = 1L;
    
    private boolean eligible = true; //default to true
    private String eligibilityCode;
    private IdentifierDt generatedBy;
    
    public boolean isEligible() {
        return eligible;
    }
    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }
    public String getEligibilityCode() {
        return eligibilityCode;
    }
    public void setEligibilityCode(String eligibilityCode) {
        this.eligibilityCode = eligibilityCode;
    }
    public IdentifierDt getGeneratedBy() {
        return generatedBy;
    }
    public void setGeneratedBy(IdentifierDt generatedBy) {
        this.generatedBy = generatedBy;
    }
    @Override
    public String toString() {
        return "EligibilityEvent [eligible=" + eligible + ", eligibilityCode=" + eligibilityCode + ", generatedBy=" + generatedBy
                + ", getCtx()=" + getCtx() + ", getTime()=" + getTime() + ", getType()=" + getType() + ", isMock()=" + isMock()
                + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }

}
