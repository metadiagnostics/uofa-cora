/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.model.measure;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;

/**
 * StatusDetail entity represents an status of a {@link MeasureStatus} part. This part can be a detail or an applicability detail.
 * @author calcacuervo
 *
 */
@Embeddable
public class StatusDetail {

    /**
     * The code to represent this status. CodingDt class is useful was it has a code, and a display.
     */
    @Embedded
    private CodingDt code;

    /**
     * The met status of this condition. This can be true, false or null, which is used to represent unknown.
     */
    private Boolean status;

    public StatusDetail() {}

    public StatusDetail(final CodingDt code) {
        this.code = code;
    }

    public StatusDetail(final CodingDt code, final Boolean status) {
        this.code = code;
        this.status = status;
    }

    public CodingDt getCode() {
        return code;
    }

    public void setCode(CodingDt code) {
        this.code = code;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "StatusDetail [code=" + code + ", status=" + status + "]";
    }

}
