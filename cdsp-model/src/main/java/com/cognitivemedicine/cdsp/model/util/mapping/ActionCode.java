/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.model.util.mapping;

public enum ActionCode {

    ACK("1", "Acknowledgement Action", "Acknowledge"),
    REJECT("2", "Reject Action", "Reject"),
    READ("3", "Read Action", "Read"),
    SNOOZE("4", "Snooze Action", "Snooze"),
    DISMISS("5", "Dismiss Action", "Dismiss");
    private String code;
    private String display;
    private String description;

    private ActionCode(String code, String display, String description) {
        this.code = code;
        this.display = display;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getDisplay() {
        return display;
    }
    
    public static ActionCode getByCode(String code) {
        for(ActionCode action : ActionCode.values()) {
            if (code.equals(action.getCode())) {
                return action;
            }
        }
        throw new IllegalArgumentException("Cannot find action with code " + code);
    }
}
