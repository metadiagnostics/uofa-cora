/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.model.events;

import java.io.Serializable;

import ca.uhn.fhir.model.primitive.DateTimeDt;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;

/**
 * Represents an event in the system.
 * @author dcalcaprina
 *
 */
public class Event implements Serializable {

	private CodingDt type;

	private DateTimeDt time;
    
    /**
     * This flag specifies whether this Event was created as part of a test or 
     * demo run. 
     * The semantics of this flag are not specified here but in the libraries,
     * ktds, code that is actually using it.
     */
    private boolean mock;

	public void setTime(DateTimeDt time) {
		this.time = time;
	}

	public void setType(CodingDt type) {
		this.type = type;
	}

	public DateTimeDt getTime() {
		return time;
	}

	public CodingDt getType() {
		return type;
	}

    public boolean isMock() {
        return mock;
    }

    public void setMock(boolean mock) {
        this.mock = mock;
    }
    
}
