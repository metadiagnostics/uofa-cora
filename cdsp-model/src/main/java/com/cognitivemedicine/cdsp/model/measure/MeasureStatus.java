/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.model.measure;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.cognitivemedicine.fhir.logicaldatatypes.DateTimeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.Attribution;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * This class represents the status of a measure at a given time.
 * @author calcacuervo
 *
 */
@Entity
public class MeasureStatus {

    @Id
    @GeneratedValue
    @Column(name = "dbid")
    private Long dbid;

    /**
     * The different applicability statuses of a measure.
     * @author calcacuervo
     *
     */
    public enum ApplicablityStatus {
        Yes,
        No,
        Excluded,
        Unknown
    };

    /**
     * The {@link Context} of the measure
     */
    @Embedded
    private Context context;

    /**
     * The last modified time of the status.
     */
    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "timeZone", column = @Column(name = "recorded_timeZone")),
            @AttributeOverride(name = "precision", column = @Column(name = "recorded_precision")),
            @AttributeOverride(name = "value", column = @Column(name = "recorded_value"))})
    @Temporal(TemporalType.TIMESTAMP)
    private DateTimeDt recorded;

    /**
     * An identifier of a measure.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private IdentifierDt identifier;

    /**
     * The {@link ApplicablityStatus} of the measure. This means whether the measure is applicable to the context or not.
     */
    @Enumerated(EnumType.STRING)
    private ApplicablityStatus applicability;

    /**
     * The details of why the applicability field is field with the current value.
     * This list is ordered so that validations can be done in an easier way.
     */
    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @OrderBy("code.code")
    private List<StatusDetail> applicabilityDetail;

    /**
     * This field indicates of the measure has been met or not.
     */
    @Column(name="met")
    private Boolean met;
    
    /**
     * Indicates if this measure status case is under an exception or not
     */
    @Column(name="denominatorException")
    private Boolean denominatorException;

    /**
     * The details of why the denominatorException field holds with the current value.
     * This list is ordered so that validations can be done in an easier way.
     */
    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @OrderBy("code.code")
    private List<StatusDetail> denominatorExceptionDetail;

    /**
     * This holds the explanation of why the measure has been met or not. This can hold the history of details while the status is changing.
     * This list is ordered so that validations can be done in an easier way.
     */
    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @OrderBy("code.code")
    private List<StatusDetail> detail;


    /**
     * A group identifier for the measure.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private IdentifierDt groupIdentifier;

    /**
     * An attribution of the measure.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private Attribution attribution;

    /**
     * Who created the measure status.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private IdentifierDt createdBy;

    public MeasureStatus() {
        // needed by json.
    }

    public MeasureStatus(Context context, IdentifierDt identifier, IdentifierDt groupIdentifier, Attribution attribution,
            IdentifierDt createdBy, DateTimeDt recorded) {
        this.context = context;
        this.identifier = identifier;
        this.groupIdentifier = groupIdentifier;
        this.attribution = attribution;
        this.createdBy = createdBy;
        this.recorded = recorded;
    }

    public Context getContext() {
        return context;
    }

    public DateTimeDt getRecorded() {
        return recorded;
    }

    public IdentifierDt getIdentifier() {
        return identifier;
    }

    public ApplicablityStatus getApplicability() {
        return applicability;
    }

    public List<StatusDetail> getApplicabilityDetail() {
        return applicabilityDetail;
    }

    public Boolean getMet() {
        return met;
    }

    public List<StatusDetail> getDetail() {
        return detail;
    }

    public IdentifierDt getGroupIdentifier() {
        return groupIdentifier;
    }

    public Attribution getAttribution() {
        return attribution;
    }

    public IdentifierDt getCreatedBy() {
        return createdBy;
    }

    public void setDbid(Long dbid) {
        this.dbid = dbid;
    }

    public Long getDbid() {
        return dbid;
    }
    
    public Boolean getDenominatorException() {
        return denominatorException;
    }
    
    public List<StatusDetail> getDenominatorExceptionDetail() {
        return denominatorExceptionDetail;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setRecorded(DateTimeDt recorded) {
        this.recorded = recorded;
    }

    public void setIdentifier(IdentifierDt identifier) {
        this.identifier = identifier;
    }

    public void setApplicability(ApplicablityStatus applicablity) {
        this.applicability = applicablity;
    }

    public void setApplicabilityDetail(List<StatusDetail> applicabilityDetail) {
        this.applicabilityDetail = applicabilityDetail;
    }

    public void setMet(Boolean met) {
        this.met = met;
    }

    public void setDetail(List<StatusDetail> detail) {
        this.detail = detail;
    }

    public void setGroupIdentifier(IdentifierDt groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    public void setAttribution(Attribution attribution) {
        this.attribution = attribution;
    }

    public void setCreatedBy(IdentifierDt createdBy) {
        this.createdBy = createdBy;
    }
    
    public void setDenominatorException(Boolean denominatorException) {
        this.denominatorException = denominatorException;
    }
    
    public void setDenominatorExceptionDetail(List<StatusDetail> denominatorExceptionDetail) {
        this.denominatorExceptionDetail = denominatorExceptionDetail;
    }
    
    @PreUpdate
    void onUpdate() {
        //every time the entity is updated, the recorded field is updated.
        this.recorded.setValue(new Date());
    }

    @Override
    public String toString() {
        return "MeasureStatus [dbid=" + dbid + ", context=" + context + ", recorded=" + recorded + ", identifier=" + identifier
                + ", applicability=" + applicability + ", applicabilityDetail=" + applicabilityDetail + ", met=" + met + ", detail="
                + detail + ", groupIdentifier=" + groupIdentifier + ", attribution=" + attribution + ", createdBy=" + createdBy + "]";
    }

}
