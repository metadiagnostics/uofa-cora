/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel.constant;

/**
 *
 * @author esteban
 */
public class Constants {
    
    public final static String COG_PATIENT_ID_SYSTEM = "com.cognitivemedicine";
    
    public final static String COG_EVENT_CODE_SYSTEM = "com.cognitivemedicine.codesystem.events";
    public final static String COG_CASE_STATE_CODE_SYSTEM = "com.cognitivemedicine.codesystem.casestate";
    
    public final static String CORA_PATIENT_ID_SYSTEM = "cora.patient.id";
    public final static String CORA_CASE_ID_SYSTEM = "cora.case.id";
    public final static String SMOKING_STATUS_CODE_SYSTEM = "SMOKING_STATUS";

    public static final String CORE_MOCK_PATIENT_CODE_SYSTEM = "cora.mock.patient.id";

    public static final String CORA_TAG_SYSTEM = "http://cognitivemedicine.com/tag";
    public static final String CORA_MOCK_TAG_CODE = "mock";
    
    public static final String FHIR_HTTP_PUSH_SUSCRIPTION_NAME = "HTTP-PUSH-SUBSCRIPTION-ENABLED";
    
    public static final String UNKNOWN = "Unk";
    
}
