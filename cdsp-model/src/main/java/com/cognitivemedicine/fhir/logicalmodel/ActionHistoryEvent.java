/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToOne;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;

/**
 * The ActionHistroyEvent Object is intended to be used record when actions are
 * taken in specific resources. For example to record when a communications
 * request is acknowledged.
 * 
 * @author dcalcaprina
 *
 */
@Entity
public class ActionHistoryEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Date eventDate;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Base resource;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CodingDt action;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private IdentifierDt user;
    
    @ElementCollection(fetch=FetchType.EAGER)
    @JoinTable(name="ActionHistoryMetadata", joinColumns=@JoinColumn(name="ID"))
    @MapKeyColumn (name="KEY")
    @Column(name="VALUE")
    private Map<String, String> actionHistoryMetadata = new HashMap<String, String>();
    
    public CodingDt getAction() {
        return action;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public Base getResource() {
        return resource;
    }

    public void setAction(CodingDt action) {
        this.action = action;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public void setResource(Base resource) {
        this.resource = resource;
    }

    public void setUser(IdentifierDt user) {
        this.user = user;
    }

    public IdentifierDt getUser() {
        return user;
    }
    
    public Map<String,String> getActionHistoryMetadata() {
        return actionHistoryMetadata;
    }
    public void setActionHistoryMetadata(Map<String,String> actionHistoryMetadata) {
        this.actionHistoryMetadata = actionHistoryMetadata;
    } 
    

    @Override
    public String toString() {
        return "ActionHistoryEvent [id=" + id + ", eventDate=" + eventDate + ", resource="
                + resource + ", action=" + action + ", user=" + user + "]";
    }
}
