/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import java.util.List;

import javax.persistence.Entity;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DurationDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicaldatatypes.NarrativeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.PeriodDt;
import com.cognitivemedicine.fhir.logicalmodel.oldinterfaces.ILMEncounter;



/**
 * Stub Implementation - TODO Fill out and add nested relationships
 *
 * 
 * @author Jerry Goodnough
 * 
 */
@Entity
public class Encounter extends Base  {

	private static final long serialVersionUID = 1L;

	public CodeableConceptDt getReasonCancelled() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setReasonCancelled(CodeableConceptDt param) {
		// TODO Auto-generated method stub
		
	}
	public CodeDt getLanguage() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setLanguage(CodeDt param) {
		// TODO Auto-generated method stub
		
	}
	
	public String getPriority() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setPriority(String param) {
		// TODO Auto-generated method stub
		
	}
	public PeriodDt getPeriod() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setPeriod(PeriodDt param) {
		// TODO Auto-generated method stub
		
	}
	public List<IdentifierDt> getIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setIdentifier(List<IdentifierDt> param) {
		// TODO Auto-generated method stub
		
	}
	public ILMEncounter addIdentifier(IdentifierDt param) {
		// TODO Auto-generated method stub
		return null;
	}
	public IdentifierDt addIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}
	public IdentifierDt getIdentifierFirstRep() {
		// TODO Auto-generated method stub
		return null;
	}
	public NarrativeDt getText() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setText(NarrativeDt param) {
		// TODO Auto-generated method stub
		
	}
	public String getClassElement() {
		// TODO Auto-generated method stub
		return null;
	}
	public DurationDt getLength() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setLength(DurationDt param) {
		// TODO Auto-generated method stub
		
	}
	public String getStatus() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setStatus(String param) {
		// TODO Auto-generated method stub
		
	}

	
	
}
