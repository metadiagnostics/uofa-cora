/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel.oldinterfaces;

import java.util.Date;
import java.util.List;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelAddition;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelMissing;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelQuestion;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelRemoval;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateTimeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicaldatatypes.NarrativeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.PeriodDt;
import com.cognitivemedicine.fhir.logicalmodel.Action;
import com.cognitivemedicine.fhir.logicalmodel.Attribution;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;
import com.cognitivemedicine.fhir.logicalmodel.Patient;
import com.cognitivemedicine.fhir.logicalmodel.Recipient;
import com.cognitivemedicine.fhir.logicalmodel.Sender;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Deprecated
public interface ILMCommunicationRequest extends ILMBase {

	@LogicalModelRemoval("Removed IdentiferDt addIndentifer() since the method made no sense")
	public ILMCommunicationRequest addIdentifier(IdentifierDt param);

	public ILMCommunicationRequest addPayload(CommunicationRequest.Payload param);

	@LogicalModelAddition("Added Action")
	public ILMCommunicationRequest addAction(Action action);

	@LogicalModelAddition("Added Attribution")
	public ILMCommunicationRequest addAttribution(Attribution attribution);

	@LogicalModelMissing("Add Recipent and changed to a type")
//	public ILMCommunicationRequest addRecipent(ILMRecipient param);
	public ILMCommunicationRequest addRecipient(Recipient param);

	@LogicalModelAddition("Added Action")
//	public List<ILMAction> getAction();
	public List<Action> getAction();

	@LogicalModelAddition("Added Attribution")
	public List<Attribution> getAttribution();

	public CodeableConceptDt getCategory();
	@LogicalModelRemoval("Temp removal - Serialization related")
	//public ContainedDt getContained();

	@LogicalModelChange("Change to an Encounter or and Encounter Resource")
	public ILMEncounter getEncounter();

	public List<IdentifierDt> getIdentifier();

	/**
	 * Used to track interated
	 * 
	 * @return
	 */
	@LogicalModelAddition("Added TopicIdentifier to support relating messsage")
	public IdentifierDt getTopicIdentifier();

	@LogicalModelAddition("Added TopicIdentifier to support relating messsage")
	public void setTopicIdentifier(IdentifierDt topicId);
	
	@JsonIgnore
	@LogicalModelAddition("Added TopicIdentifier to support relating messsage")
	public void setTopicIdentifier(String topicId);
	
	public IdentifierDt getIdentifierFirstRep();

	public CodeDt getLanguage();

	@LogicalModelMissing("Medium was missing")
	public CodeableConceptDt getMedium();

	public List<CommunicationRequest.Payload> getPayload();

	@JsonIgnore
	public CommunicationRequest.Payload getPayloadFirstRep();

	public CodeableConceptDt getPriority();

	public CodeableConceptDt getReasonRejected();

	@LogicalModelMissing("Add Recipent and changed to a type")
//	public List<ILMRecipient> getRecipent();
	public List<Recipient> getRecipient();
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public Date getRequestedOn();

	@LogicalModelQuestion("How is the different from requestedOn ?")
	public DateTimeDt getRequestedOnElement();
	@JsonIgnore
	@XmlTransient
	@Transient
	public Date getScheduledDateTime();

	@LogicalModelQuestion("How is the different from ScheduledDateTime ?")
	public DateTimeDt getScheduledDateTimeElement();

	public PeriodDt getScheduledPeriod();

	@LogicalModelChange("Change to Subject from Subject Identifer")
//	public ILMSender getSender();
	public Sender getSender();

	@JsonIgnore
	@XmlTransient
	@Transient
	public String getStatus();

	public String getStatusElement();
	
	@LogicalModelChange("Change to Subject from Subject Resource")
//	public ILMPatient getSubject();
	public Patient getSubject();

	public NarrativeDt getText();

	@LogicalModelAddition("Added Action")
//	public void setAction(List<ILMAction> action);
	public void setAction(List<Action> action);

	@LogicalModelAddition("Added Attribution")
	public void setAttribution(List<Attribution> attribution);

	@LogicalModelChange("Change all setter from Fluent")
	public void setCategory(CodeableConceptDt param);

	@LogicalModelRemoval("Temp removal - Serialization related")
	//public void setContained(ContainedDt param);

	@LogicalModelChange("Change to an Encounter or and Encounter Resource")
	public void setEncounter(ILMEncounter param);

	public void setIdentifier(List<IdentifierDt> param);

	public void setLanguage(CodeDt param);

	@LogicalModelMissing("Medium was missing")
	public void setMedium(CodeableConceptDt param);

	public void setPayload(List<CommunicationRequest.Payload> param);

	public void setPriority(CodeableConceptDt param);

	@LogicalModelMissing("Add Recipent and changed to a type")
//	public void setRecipent(List<ILMRecipient> recipent);
	public void setRecipient(List<Recipient> recipient);

	public void setReasonRejected(CodeableConceptDt param);
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setRequestedOn(Date param);
	@LogicalModelChange("Make name beans/propetry semetric")
	public void setRequestedOnElement(DateTimeDt param);
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setScheduledDateTime(Date param);

	public void setScheduledDateTime(DateTimeDt param);

	public void setScheduledPeriod(PeriodDt param);

	@LogicalModelMissing("Add sender and changed to a type")
	//public void setSender(ILMSender sender);
	public void setSender(Sender sender);

	@JsonIgnore
	@XmlTransient
	@Transient
	public void setStatusElement(String param);
	

	public void setStatus(String param);

	@LogicalModelChange("Change to Subject from Subject Resource")
//	public void setSubject(ILMPatient param);
	public void setSubject(Patient param);

	public void setText(NarrativeDt param);

	/**
	 * Inital Payload interface - Needs to be extend to deal with other
	 * container related issues and typing
	 * 
	 * @author Jerry Goodnough TODO: Revist this definition in detail
	 */
	interface Payload {
		public String getContent();

		public void setContent(String content);
		
		public String getTitle();

		public void setTitle(String content);
	}
}