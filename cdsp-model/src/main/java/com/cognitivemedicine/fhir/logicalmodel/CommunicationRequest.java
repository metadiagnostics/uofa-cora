/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelQuestion;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateTimeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicaldatatypes.NarrativeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.PeriodDt;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Jerry Goodnough
 *
 */
@JsonAutoDetect
@Entity
public class CommunicationRequest extends Base {

    private static final long serialVersionUID = 1L;

    @Entity
    public static class Payload {

        @Id
        @GeneratedValue
        @Column(name = "dbid")
        private Long dbid;

        @Lob
        // Creates a longtext despite specified length
        @Column(name = "payload_content", length = 65000)
        private String myContent;
        private String title;

        // TODO Insure that serialization can handle larger bodies

        /*
         * (non-Javadoc)
         * 
         * @see com.cognitivemedicine.fhir.logicalmodel.ILMCommunicationRequest.
         * Payload#getContect()
         */
        public String getContent() {
            return myContent;
        }

        public String getTitle() {
            return title;
        }

        public void setContent(String content) {
            myContent = content;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public String toString() {
            return "Payload [dbid=" + dbid + ", myContent=" + myContent + ", title=" + title + "]";
        }

    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CommunicationRequest_Action", joinColumns = @JoinColumn(name = "dbid"))
    // To solve this exception: Caused by: org.hibernate.loader.MultipleBagFetchException: cannot
    // simultaneously fetch multiple bags
    @Fetch(FetchMode.SELECT)
    private List<Action> action = new LinkedList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CommunicationRequest_Attribution", joinColumns = @JoinColumn(name = "dbid"))
    @Fetch(FetchMode.SELECT)
    private List<Attribution> attribution = new LinkedList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CommunicationRequest_Identifiers", joinColumns = @JoinColumn(name = "dbid"))
    @Fetch(FetchMode.SELECT)
    private List<IdentifierDt> identifiers = new LinkedList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CommunicationRequest_Payloads", joinColumns = @JoinColumn(name = "dbid"))
    @Fetch(FetchMode.SELECT)
    private List<CommunicationRequest.Payload> payload = new LinkedList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CommunicationRequest_Recipient", joinColumns = @JoinColumn(name = "dbid"))
    @OrderBy
    // We should have been using Set but since we are using List, Hibernate does not allow for more
    // than two EAGER Fetches.
    @Fetch(value = FetchMode.SELECT)
    private List<Recipient> recipient = new LinkedList<>();

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CodeableConceptDt category = new CodeableConceptDt();

    // @OneToOne(cascade = CascadeType.ALL)
    @LogicalModelQuestion("Are we missing attributes in Encounter?")
    @Transient
    private Encounter encounter;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "value", column = @Column(
            name = "language_value"))})
    private CodeDt language;

    @LogicalModelQuestion("Are we missing ContainedDt?")
    // private ContainedDt contained;
    @OneToOne(cascade = CascadeType.ALL)
    private CodeableConceptDt medium;

    @OneToOne(cascade = CascadeType.ALL)
    private CodeableConceptDt priority;

    @OneToOne(cascade = CascadeType.ALL)
    private CodeableConceptDt reasonRejected;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "timeZone", column = @Column(name = "requestedon_timeZone")),
            @AttributeOverride(name = "precision", column = @Column(name = "requestedon_precision")),
            @AttributeOverride(name = "value", column = @Column(name = "requestedon_value"))})
    private DateTimeDt requestedOnElement = null;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "timeZone", column = @Column(name = "scheduled_timeZone")),
            @AttributeOverride(name = "precision", column = @Column(name = "scheduled_precision")),
            @AttributeOverride(name = "value", column = @Column(name = "scheduled_value"))})
    private DateTimeDt scheduledDateTimeElement = null;

    @OneToOne(cascade = CascadeType.ALL)
    private PeriodDt scheduledPeriod;

    @OneToOne(cascade = CascadeType.ALL)
    private Sender sender = new Sender();

    @Transient
    private List<RequestStatus> statuses;

    @LogicalModelQuestion("Are we missing additional attributes in Patient?")
    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private Patient subject = new Patient();

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "status", column = @Column(name = "narrative_status")),
            @AttributeOverride(name = "text", column = @Column(name = "narrative_tex"))})
    private NarrativeDt text = new NarrativeDt();

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private IdentifierDt topicIdentifier;

    // public Long getDbid() {
    // return dbid;
    // }
    //
    // public void setDbid(Long dbid) {
    // this.dbid = dbid;
    // }

    public CommunicationRequest addAction(Action action) {
        this.action.add(action);
        return this;
    }

    public CommunicationRequest addAttribution(Attribution attribution) {
        this.attribution.add(attribution);
        return this;
    }

    public CommunicationRequest addIdentifier(IdentifierDt param) {
        identifiers.add(param);
        return this;
    }

    public CommunicationRequest addPayload(CommunicationRequest.Payload param) {
        payload.add(param);
        return this;
    }

    public CommunicationRequest addRecipient(Recipient param) {
        this.recipient.add(param);
        return this;
    }

    public List<Action> getAction() {
        return action;
    }

    public List<Attribution> getAttribution() {
        return this.attribution;
    }

    public CodeableConceptDt getCategory() {
        return category;
    }

    public Encounter getEncounter() {
        return encounter;
    }

    public List<IdentifierDt> getIdentifier() {
        return identifiers;
    }


    @JsonIgnore
    @XmlTransient
    @Transient
    public IdentifierDt getIdentifierFirstRep() {
        return identifiers.size() == 0 ? null : identifiers.get(0);
    }

    public CodeDt getLanguage() {
        return language;
    }

    public CodeableConceptDt getMedium() {

        return medium;
    }

    public List<CommunicationRequest.Payload> getPayload() {

        return payload;
    }

    @JsonIgnore
    @XmlTransient
    @Transient
    public CommunicationRequest.Payload getPayloadFirstRep() {
        return payload.size() == 0 ? null : payload.get(0);
    }

    public CodeableConceptDt getPriority() {
        return priority;
    }

    public CodeableConceptDt getReasonRejected() {
        return reasonRejected;
    }

    public List<Recipient> getRecipient() {
        return this.recipient;
    }

    @JsonIgnore
    @XmlTransient
    @Transient
    public Date getRequestedOn() {
        return requestedOnElement == null ? null : requestedOnElement.getValue();
    }

    public DateTimeDt getRequestedOnElement() {
        return this.requestedOnElement;
    }

    @JsonIgnore
    @XmlTransient
    @Transient
    public Date getScheduledDateTime() {
        return scheduledDateTimeElement == null ? null : scheduledDateTimeElement.getValue();
    }

    public DateTimeDt getScheduledDateTimeElement() {
        return scheduledDateTimeElement;
    }

    public PeriodDt getScheduledPeriod() {
        return scheduledPeriod;
    }

    public Sender getSender() {
        return sender;
    }

    public List<RequestStatus> getStatuses() {
        return statuses;
    }

    @Transient
    public List<RequestStatus> getStatusElement() {
        return statuses;
    }

    public Patient getSubject() {
        return subject;
    }

    public NarrativeDt getText() {
        return text;
    }

    public IdentifierDt getTopicIdentifier() {
        return topicIdentifier;
    }

    public void setAction(List<Action> action) {
        this.action = action;
    }

    public void setAttribution(List<Attribution> attributions) {
        this.attribution = attributions;
    }

    public void setCategory(CodeableConceptDt param) {
        this.category = param;
    }

    public void setEncounter(Encounter param) {
        this.encounter = param;
    }

    public void setIdentifier(List<IdentifierDt> param) {
        this.identifiers = param;
    }

    public void setLanguage(CodeDt param) {
        this.language = param;
    }

    public void setMedium(CodeableConceptDt param) {
        this.medium = param;
    }

    public void setPayload(List<CommunicationRequest.Payload> param) {
        payload = param;
    }

    public void setPriority(CodeableConceptDt param) {
        this.priority = param;
    }

    public void setReasonRejected(CodeableConceptDt param) {
        this.reasonRejected = param;
    }

    public void setRecipient(List<Recipient> recipients) {
        this.recipient = recipients;
    }

    @JsonIgnore
    @XmlTransient
    @Transient
    public void setRequestedOn(Date param) {
        if (requestedOnElement == null) {
            requestedOnElement = new DateTimeDt(param);
        } else {
            this.requestedOnElement.setValue(param);
        }

    }

    public void setRequestedOnElement(DateTimeDt param) {
        this.requestedOnElement = param;
    }

    @JsonIgnore
    @XmlTransient
    @Transient
    public void setScheduledDateTime(Date param) {
        if (scheduledDateTimeElement == null) {
            scheduledDateTimeElement = new DateTimeDt(param);
        } else {
            scheduledDateTimeElement.setValue(param);
        }
    }

    public void setScheduledDateTime(DateTimeDt param) {
        this.scheduledDateTimeElement = param;
    }

    public void setScheduledPeriod(PeriodDt param) {
        this.scheduledPeriod = param;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public void setStatuses(List<RequestStatus> param) {
        statuses = param;
    }
    
    @JsonIgnore
    public void setStatus(String reqStatus) {
        if (statuses == null) {
            statuses = new ArrayList<>();
        }
        statuses.add(new RequestStatus(new Date(), new CodingDt("", reqStatus)));
    }

    @Transient
    @JsonIgnore
    @XmlTransient
    public void setStatusElement(List<RequestStatus> param) {
        this.statuses = param;
    }

    public void setSubject(Patient param) {
        this.subject = param;
    }

    public void setText(NarrativeDt param) {
        this.text = param;
    }

    @JsonProperty
    public void setTopicIdentifier(IdentifierDt topicId) {
        topicIdentifier = topicId;
    }

    @JsonIgnore
    @XmlTransient
    @Transient
    public void setTopicIdentifier(String topicId) {
        topicIdentifier = new IdentifierDt();
        topicIdentifier.setValue(topicId);
    }

    @Override
    public String toString() {
        return "CommunicationRequest [action=" + action + ", attribution=" + attribution
                + ", identifiers=" + identifiers + ", payload=" + payload + ", recipient="
                + recipient + ", category=" + category + ", encounter=" + encounter + ", language="
                + language + ", medium=" + medium + ", priority=" + priority + ", reasonRejected="
                + reasonRejected + ", requestedOnElement=" + requestedOnElement
                + ", scheduledDateTimeElement=" + scheduledDateTimeElement + ", scheduledPeriod="
                + scheduledPeriod + ", sender=" + sender + ", status=" + statuses + ", subject="
                + subject + ", text=" + text + ", topicIdentifier=" + topicIdentifier + "]";
    }

}
