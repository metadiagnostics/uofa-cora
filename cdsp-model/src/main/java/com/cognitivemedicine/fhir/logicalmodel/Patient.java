/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hl7.fhir.dstu3.model.Identifier.IdentifierUse;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateTimeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.HumanNameDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicaldatatypes.NarrativeDt;
import com.fasterxml.jackson.annotation.JsonIgnore;



/**
 * @author Jerry Goodnough
 *
 */
@Entity
public class Patient extends Base {

	private static final long serialVersionUID = 1L;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "Patient_Identifiers", joinColumns = @JoinColumn(name = "dbid"))
    @Fetch(FetchMode.SELECT)
	private List<IdentifierDt> identifiers = new LinkedList<>();
    
	public List<IdentifierDt> getIdentifier() {	
		return identifiers;
	}

	public void setIdentifier(List<IdentifierDt> param) {
		identifiers = param;
	}

	public Patient addIdentifier(IdentifierDt param) {
		identifiers.add(param);
		return this;
	}

	
	@JsonIgnore
	@XmlTransient
	@Transient
	public IdentifierDt getIdentifierFirstRep() {
	
		return identifiers.size() == 0? null:identifiers.get(0);
	}



	
	@JsonIgnore
	@XmlTransient
	@Transient
	public IdentifierDt getPrimaryIdentifer() {
		
		//Always use the official Id if present.
		for (IdentifierDt id : identifiers )
		{
			if (id.getUse().compareTo(IdentifierUse.OFFICIAL.name())==0)
			{
				return id;
			}
		}
		//If there is no official Id we default to the first (Yuk)

		return identifiers.size()==0?null:identifiers.get(0);
	}


	
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setPrimaryIdentifer(IdentifierDt identifier) {
		boolean fnd = false;
		identifier.setUse(IdentifierUse.OFFICIAL.name());
		//First we look if there is an official identifier and if found we replace it;
		for (int i = 0; i < identifiers.size(); i++)
		{
			IdentifierDt id = identifiers.get(i);
			if (id.getUse().compareTo(IdentifierUse.OFFICIAL.name())==0)
			{
				identifiers.set(i, identifier);
				fnd = true;
			}
		}
		//Add the identifier if not found 
		if (fnd == false)
		{
			identifiers.add(identifier);
		}
		
		//TODO Update the context too.
	}

	public CodeableConceptDt getReligion() {
		// TODO Auto-generated method stub
		return null;
	}


	public void setReligion(CodeableConceptDt param) {
		// TODO Auto-generated method stub
		
	}

	public DateDt getBirthDateElement() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public Date getBirthDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public void setBirthDate(Date param) {
		// TODO Auto-generated method stub
		
	}

	public void setBirthDateElement(DateDt param) {
		// TODO Auto-generated method stub
		
	}

	public Boolean getActive() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setActive(Boolean param) {
		// TODO Auto-generated method stub
		
	}

	public CodeableConceptDt getRace() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setRace(CodeableConceptDt param) {
		// TODO Auto-generated method stub
		
	}

	public CodeDt getLanguage() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setLanguage(CodeDt param) {
		// TODO Auto-generated method stub
		
	}

	public DateTimeDt getBirthTime() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setBirthTime(DateTimeDt param) {
		// TODO Auto-generated method stub
		
	}

	public List<CodeableConceptDt> getDisability() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDisability(List<CodeableConceptDt> param) {
		// TODO Auto-generated method stub
		
	}

	public String getMaritalStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setMaritalStatus(String param) {
		// TODO Auto-generated method stub
		
	}

	public List<HumanNameDt> getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getMultipleBirthInteger() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setMultipleBirthInteger(Integer param) {
		// TODO Auto-generated method stub
		
	}

	public CodeableConceptDt getMilitaryService() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setMilitaryService(CodeableConceptDt param) {
		// TODO Auto-generated method stub
		
	}

	public String getGender() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setGender(String param) {
		// TODO Auto-generated method stub
		
	}

	public NarrativeDt getText() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setText(NarrativeDt param) {
		// TODO Auto-generated method stub
		
	}

	public Boolean getDeceasedBoolean() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDeceasedBoolean(Boolean param) {
		// TODO Auto-generated method stub
		
	}

	public DateTimeDt getDeceasedDateTimeElement() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public Date getDeceasedDateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDeceasedDateTimeElement(DateTimeDt param) {
		// TODO Auto-generated method stub
		
	}

	@JsonIgnore
	@XmlTransient
	@Transient
	public void setDeceasedDateTime(Date param) {
		// TODO Auto-generated method stub
		
	}

	public CodeableConceptDt getEthnicity() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setEthnicity(CodeableConceptDt param) {
		// TODO Auto-generated method stub
		
	}

	public Boolean getCadavericDonor() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCadavericDonor(Boolean param) {
		// TODO Auto-generated method stub
		
	}
	

}
