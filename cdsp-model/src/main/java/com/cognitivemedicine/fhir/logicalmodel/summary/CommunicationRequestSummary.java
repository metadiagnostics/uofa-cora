/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel.summary;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateTimeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.RequestStatus;

public class CommunicationRequestSummary {
	public static class Element {
		Long msgId;
		Date date;
		String priority;
		List<RequestStatus> status;
		String patientId;
		String topicIdentifier;
		String title;

		// default constructor for Serialization
		public Element() {
		}

		public Element(long msgId, DateTimeDt date, IdentifierDt topicIdentifier, CodeableConceptDt priority, String patientId, String title) {
			super();
			initializeElement(msgId, date.getValue(), topicIdentifier.getValue(), priority.getCodingFirstRep().getDisplay(), patientId, title);
		}

		public Element(Long msgId, Date date, String topicIdentifier, String priority, String patientId, String title) {
			super();
			initializeElement(msgId, date, topicIdentifier, priority, patientId, title);
		}

		public void initializeElement(Long msgId, Date date, String topicIdentifier, String priority, String patientId, String title) {
			this.msgId = msgId;
			this.date = date;
			this.title = title;
			this.priority = priority;
			this.patientId = patientId;
			this.topicIdentifier = topicIdentifier;
		}

		public Long getMsgId() {
			return msgId;
		}

		public void setMsgId(Long msgId) {
			this.msgId = msgId;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public String getTitle() {
			return title;
		}

		public String getTopicIdentifier() {
			return topicIdentifier;
		}

		public String getPriority() {
			return priority;
		}

		public void setPriority(String priority) {
			this.priority = priority;
		}

		public List<RequestStatus> getStatus() {
			return status;
		}

		public void setStatus(List<RequestStatus> status) {
			this.status = status;
		}
		public void setPatientId(String patientId) {
			this.patientId = patientId;
		}
		public String getPatientId() {
			return patientId;
		}

	}

	private int pageSize;
	private int pageNumber;
	private List<Element> elements;

	public CommunicationRequestSummary() {
		elements = new ArrayList<>();
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public List<Element> getElements() {
		return elements;
	}

	public void setElements(List<Element> elements) {
		this.elements = elements;
	}

	public void addElement(Long msgId, Date date, String topicIdentifier, String priority, String patientId, String title) {
		if (getElements() == null) {
			elements = new ArrayList<>();
		}
		elements.add(new Element(msgId, date, topicIdentifier, priority, patientId, title));
	}
	
	public void addElement(Element element) {
		elements.add(element);
	}

}
