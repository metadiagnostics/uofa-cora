/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel.oldinterfaces;

import java.util.Date;
import java.util.List;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;
import com.cognitivemedicine.cdsp.model.annotation.LogicalModelRemoval;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DateTimeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.HumanNameDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicaldatatypes.NarrativeDt;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Initial incomplete representation
 * 
 * @author Jerry Goodnough
 *
 */
@Deprecated
public interface ILMPatient extends ILMBase {

	// public List<Patient.Contact> getContact();

	// public void setContact(List<Patient.Contact> param);

	// public ILMPatient addContact(Patient.Contact param);

	// public Patient.Contact addContact();

	// public Patient.Contact getContactFirstRep();

	// public List<QICorePatientAddress> getWrappedAddress();

	// public void setWrappedAddress(List<QICorePatientAddress> param);

	// public ILMPatient addWrappedAddress(QICorePatientAddress param);

	// public QICorePatientAddress addWrappedAddress();

	// public QICorePatientAddress getWrappedAddressFirstRep();

	// public List<AddressDt> getAddress();

	// public void setAddress(List<AddressDt> param);

	// public ILMPatient addAddress(AddressDt param);

	// public AddressDt addAddress();

	// public AddressDt getAddressFirstRep();

	// public List<QICorePatientTelecom> getWrappedTelecom();

	// public void setWrappedTelecom(List<QICorePatientTelecom> param);

	// public ILMPatient addWrappedTelecom(QICorePatientTelecom param);

	// public QICorePatientTelecom addWrappedTelecom();

	// public QICorePatientTelecom getWrappedTelecomFirstRep();

	// public List<ContactPointDt> getTelecom();

	// public void setTelecom(List<ContactPointDt> param);

	// public ILMPatient addTelecom(ContactPointDt param);

	// public ContactPointDt addTelecom();

	// public ContactPointDt getTelecomFirstRep();

	// public Patient.Animal getAnimal();

	// public void setAnimal(Patient.Animal param);

	public CodeableConceptDt getReligion();

	public void setReligion(CodeableConceptDt param);

	// public List<Patient.Link> getLink();

	// public void setLink(List<Patient.Link> param);

	// public ILMPatient addLink(Patient.Link param);

	// public Patient.Link addLink();

	// public Patient.Link getLinkFirstRep();

	// public List<QICorePatientClinicalTrial> getClinicalTrial();

	// public void setClinicalTrial(
	// List<QICorePatientClinicalTrial> param);

	public DateDt getBirthDateElement();

	@JsonIgnore
	@XmlTransient
	@Transient
	public Date getBirthDate();

	@JsonIgnore
	@XmlTransient
	@Transient
	public void setBirthDate(Date param);

	@LogicalModelChange("renamed to make bean name semetiric")
	public void setBirthDateElement(DateDt param);

	// public AddressDt getBirthPlace();

	// public void setBirthPlace(AddressDt param);

	// public List<QICorePatientNationality> getNationality();

	// public void setNationality(List<QICorePatientNationality> param);

	// public BooleanDt getActiveElement();

	public Boolean getActive();

	public void setActive(Boolean param);

	// public void setActive(BooleanDt param);
	@LogicalModelRemoval("Temp removal - Serialization related")
	// public ContainedDt getContained();

	// public void setContained(ContainedDt param);

	public CodeableConceptDt getRace();

	public void setRace(CodeableConceptDt param);

	public CodeDt getLanguage();

	public void setLanguage(CodeDt param);

	// public List<Patient.Communication> getCommunication();

	// public void setCommunication(List<Patient.Communication> param);

	// public ILMPatient addCommunication(Patient.Communication param);

	// public Patient.Communication addCommunication();

	// public Patient.Communication getCommunicationFirstRep();

	public DateTimeDt getBirthTime();

	public void setBirthTime(DateTimeDt param);

	public List<IdentifierDt> getIdentifier();

	public void setIdentifier(List<IdentifierDt> param);

	@LogicalModelRemoval("Removed IdentiferDt addIndentifer() since the method made no sense")
	public ILMPatient addIdentifier(IdentifierDt param);

	public IdentifierDt getIdentifierFirstRep();

	public IdentifierDt getPrimaryIdentifer();

	public void setPrimaryIdentifer(IdentifierDt identifer);

	public List<CodeableConceptDt> getDisability();

	public void setDisability(List<CodeableConceptDt> param);

	// public QICoreOrganizationAdapter getManagingOrganizationResource();

	// public void setManagingOrganizationResource(
	// QICoreOrganizationAdapter param);

	public String getMaritalStatus();

	public void setMaritalStatus(String param);

	public List<HumanNameDt> getName();

	// public void setName(List<HumanNameDt> param);

	// public ILMPatient addName(HumanNameDt param);

	// public HumanNameDt addName();

	// public HumanNameDt getNameFirstRep();

	// public List<AttachmentDt> getPhoto();

	// public void setPhoto(List<AttachmentDt> param);

	// public ILMPatient addPhoto(AttachmentDt param);

	// public AttachmentDt addPhoto();

	// public AttachmentDt getPhotoFirstRep();

	// public BooleanDt getMultipleBirthBooleanElement();

	// public Boolean getMultipleBirthBoolean();

	// public void setMultipleBirthBoolean(BooleanDt param);

	// public void setMultipleBirthBoolean(Boolean param);

	public Integer getMultipleBirthInteger();

	public void setMultipleBirthInteger(Integer param);

	public CodeableConceptDt getMilitaryService();

	public void setMilitaryService(CodeableConceptDt param);

	public String getGender();

	public void setGender(String param);

	public NarrativeDt getText();

	public void setText(NarrativeDt param);

	public Boolean getDeceasedBoolean();

	public void setDeceasedBoolean(Boolean param);

	public DateTimeDt getDeceasedDateTimeElement();

	@JsonIgnore
	@XmlTransient
	@Transient
	public Date getDeceasedDateTime();

	@LogicalModelChange("Make Bean Semetric")
	public void setDeceasedDateTimeElement(DateTimeDt param);

	@JsonIgnore
	@XmlTransient
	@Transient
	public void setDeceasedDateTime(Date param);

	public CodeableConceptDt getEthnicity();

	public void setEthnicity(CodeableConceptDt param);

	public Boolean getCadavericDonor();

	public void setCadavericDonor(Boolean param);
}
