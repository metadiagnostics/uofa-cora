/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.oldinterfaces.ILMRequester;



/**
 * @author Jerry Goodnough
 *
 */
@Entity
public class Requester implements ILMRequester { //extends Base 

	private static final long serialVersionUID = 1L;

	@Embedded
	@AttributeOverrides({
	@AttributeOverride(name = "system", column=@Column(name="requester_system")),
	@AttributeOverride(name = "type", column=@Column(name="requester_type")),
	@AttributeOverride(name = "use", column=@Column(name="requester_use")),
	@AttributeOverride(name = "value", column=@Column(name="requester_value")),
	})
	private IdentifierDt identifier = new IdentifierDt();
	
	@Embedded
	@AttributeOverrides({
	@AttributeOverride(name = "id", column=@Column(name="codeableconcept_id"))})
	private CodeableConceptDt type = new CodeableConceptDt();
	
	/* (non-Javadoc)
	 * @see com.cognitivemedicine.fhir.logicalmodel.ILMRecipient#getIdentifier()
	 */
	@Override
	public IdentifierDt getIdentifier() {
		
		return identifier;
	}

	/* (non-Javadoc)
	 * @see com.cognitivemedicine.fhir.logicalmodel.ILMRecipient#getType()
	 */
	@Override
	public CodeableConceptDt getType() {
		
		return type;
	}

	/* (non-Javadoc)
	 * @see com.cognitivemedicine.fhir.logicalmodel.ILMRecipient#setIdentifier(ca.uhn.fhir.model.dstu2.composite.IdentifierDt)
	 */
	@Override
	public void setIdentifier(IdentifierDt id) {
		this.identifier = id;

	}

	/* (non-Javadoc)
	 * @see com.cognitivemedicine.fhir.logicalmodel.ILMRecipient#setType(ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt)
	 */
	@Override
	public void setType(CodeableConceptDt type) {
		this.type = type;
	}

}
