/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;

/**
 * @author Jerry Goodnough
 *
 */
@Entity
public class Action extends Base {
	
	private static final long serialVersionUID = 1L;

	private String description;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private CodeableConceptDt action;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private CodeableConceptDt type;
	
	private Boolean hidden;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CodeableConceptDt getAction() {
		return action;
	}
	
	public void setAction(CodeableConceptDt action) {
		this.action = action;
	}
	
	public CodeableConceptDt getType() {
		return type;
	}
	
	public void setType(CodeableConceptDt type) {
		this.type = type;
	}
	
	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}
	
	public Boolean getHidden() {
		return hidden;
	}
	
}
