/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;

public class RequestStatus {

    private Date time;

    private CodingDt code;

    private Map<RequestStatusMetadataKey, String> metadata;

    public RequestStatus() {
        // needed for JSON serialization
    }

    public RequestStatus(Date time, CodingDt code) {
        this.time = time;
        this.code = code;
        this.metadata = new HashMap<RequestStatus.RequestStatusMetadataKey, String>();
    }

    public CodingDt getCode() {
        return code;
    }

    public Map<RequestStatusMetadataKey, String> getMetadata() {
        return metadata;
    }

    public Date getTime() {
        return time;
    }

    public void setCode(CodingDt code) {
        this.code = code;
    }

    public void setMetadata(Map<RequestStatusMetadataKey, String> metadata) {
        this.metadata = metadata;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void addMetadata(RequestStatusMetadataKey key, String value) {
        this.metadata.put(key, value);
    }

    public static enum RequestStatusMetadataKey {
        REJECT_REASON;
    }
}
