/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel.oldinterfaces;

import java.util.List;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelRemoval;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.DurationDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicaldatatypes.NarrativeDt;
import com.cognitivemedicine.fhir.logicaldatatypes.PeriodDt;





/**
 * Stub interface for an encounter - Incomplete.
 * 
 * @author Jerry Goodnough
 *
 */
@Deprecated
public interface ILMEncounter extends ILMBase {

	   //public Encounter.Hospitalization getHospitalization();

	   //public void setHospitalization(Encounter.Hospitalization param);

	   public CodeableConceptDt getReasonCancelled();

	   public void setReasonCancelled(CodeableConceptDt param);

	   public CodeDt getLanguage();

	   public void setLanguage(CodeDt param);

	   //public List<Encounter.Location> getLocation();

	   //public void setLocation(List<Encounter.Location> param);

	   //public ILMEncounter addLocation(Encounter.Location param);

	   //public Encounter.Location addLocation();

	   //public Encounter.Location getLocationFirstRep();

	   //public List<Encounter.StatusHistory> getStatusHistory();

	   //public void setStatusHistory(List<Encounter.StatusHistory> param);

	   //public ILMEncounter addStatusHistory(Encounter.StatusHistory param);

	   //public Encounter.StatusHistory addStatusHistory();

	   //public Encounter.StatusHistory getStatusHistoryFirstRep();

	   //public QICorePatientAdapter getPatientResource();

	   //public void setPatientResource(QICorePatientAdapter param);

	   //public List<Encounter.Participant> getParticipant();

	   //public void setParticipant(List<Encounter.Participant> param);

	   //public ILMEncounter addParticipant(Encounter.Participant param);

	   //public Encounter.Participant addParticipant();

	   //public Encounter.Participant getParticipantFirstRep();

	   public String getPriority();

	   public void setPriority(
	         String param);

	   public PeriodDt getPeriod();

	   public void setPeriod(PeriodDt param);

	   public List<IdentifierDt> getIdentifier();

	   public void setIdentifier(List<IdentifierDt> param);

	   public ILMEncounter addIdentifier(IdentifierDt param);

	   public IdentifierDt addIdentifier();

	   public IdentifierDt getIdentifierFirstRep();

	   public NarrativeDt getText();

	   public void setText(NarrativeDt param);

	   //public QICoreOrganizationAdapter getServiceProviderResource();

	   //public void setServiceProviderResource(
	   //      QICoreOrganizationAdapter param);

	   @LogicalModelRemoval("Temp removal - Serialization related")
	   //public ContainedDt getContained();
	   //public void setContained(ContainedDt param);

	   public String getClassElement();

	   //public void setClassElement(String param);

	   //public void setClassElement(EncounterClassEnum param);

	   //public String getClassElementElement();

	   //public List<QICoreEncounterRelatedCondition> getRelatedCondition();

	   //public void setRelatedCondition(
	   //      List<QICoreEncounterRelatedCondition> param);

	   //public Appointment getAppointmentResource();

	   //public void setAppointmentResource(Appointment param);

	   public DurationDt getLength();

	   public void setLength(DurationDt param);

	   public String getStatus();

	   public void setStatus(String param);

	   //public void setStatus(EncounterStateEnum param);

	   //public String getStatusElement();

}
