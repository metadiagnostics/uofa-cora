/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicalmodel;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class provides context to IQCore Logical Model classes.
 * 
 * @author nbashir
 *
 *         TODO - Add property listener contact to allow hooking
 */
@Embeddable
public class Context implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
	private IdentifierDt subjectId; // used to represent patient ID
	
	@OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
	private IdentifierDt caseId; // used to represent Drager case ID

	@JsonProperty("subjectId")
	public IdentifierDt getSubjectId() {
		return subjectId;
	}
	
	@JsonProperty("subjectId")
	public void setSubjectId(IdentifierDt newId) {
		this.subjectId = newId;
	}

	public IdentifierDt getCaseId() {
		return caseId;
	}

	public void setCaseId(IdentifierDt caseId) {
		this.caseId = caseId;
	}
	
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setSubject(String id) {
		subjectId = new IdentifierDt();
		subjectId.setValue(id);
	}
}
