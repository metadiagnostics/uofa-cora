/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hl7.fhir.dstu3.model.Coding;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Jerry Goodnough
 *
 */
@Entity
public class CodingDt implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "dbid")
	private Long dbid;
	
	@Embedded
	@AttributeOverrides({
	@AttributeOverride(name = "value", column=@Column(name="code_value"))})
	private CodeDt code;

	private String display = "";
	private String system = "";
	private String version = "";
	private Boolean userSelected = Boolean.FALSE;
	

	/**
	 * Constructor
	 */
	public CodingDt() {
		// nothing
	}

	/**
	 * HAPI FHIR based copy constructor
	 */
	public CodingDt(Coding src) {
		code = new CodeDt(src.getCode());
		system = src.getSystem();
		version = src.getVersion();
		display = src.getDisplay();
		userSelected = src.getUserSelected();

	}

	/**
	 * Get HAPI FHIR data type eqv.
	 * 
	 * @return
	 */
	public Coding toHAPIFHIR() {
		Coding out = new  Coding();
		
		if (code != null) {
			out.setCodeElement(code.toHAPIFHIR());
		}

		out.setDisplay(display);
		out.setSystem(system);
		out.setUserSelected(userSelected.booleanValue());
		out.setVersion(version);
		return out;
	}

	/**
	 * Creates a new Coding with the given system and code
	 */
	public CodingDt(String theSystem, String theCode) {
		setSystem(theSystem);
		setCode(theCode);
	}

	/**
	 * Copy constructor: Creates a new Coding with the system and code copied
	 * out of the given coding
	 */
	public CodingDt(CodingDt theCoding) {
		this(theCoding.getSystem(), theCoding.getCode());
	}
	
	
	/**
	 * Gets the value(s) for <b>system</b> (). creating it if it does not exist.
	 * Will not return <code>null</code>.
	 *
	 * <p>
	 * <b>Definition:</b> The identification of the code system that defines the
	 * meaning of the symbol in the code.
	 * </p>
	 */

	public String getSystem() {
		return system;
	}

	/**
	 * Sets the value(s) for <b>system</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> The identification of the code system that defines the
	 * meaning of the symbol in the code.
	 * </p>
	 */
	public void setSystem(String theValue) {
		system = theValue;

	}

	/**
	 * Gets the value(s) for <b>version</b> (). creating it if it does not
	 * exist. Will not return <code>null</code>.
	 *
	 * <p>
	 * <b>Definition:</b> The version of the code system which was used when
	 * choosing this code. Note that a well-maintained code system does not need
	 * the version reported, because the meaning of codes is consistent across
	 * versions. However this cannot consistently be assured. and when the
	 * meaning is not guaranteed to be consistent, the version SHOULD be
	 * exchanged
	 * </p>
	 */
	public String getVersion() {
		return this.version;
	}

	/**
	 * Sets the value(s) for <b>version</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> The version of the code system which was used when
	 * choosing this code. Note that a well-maintained code system does not need
	 * the version reported, because the meaning of codes is consistent across
	 * versions. However this cannot consistently be assured. and when the
	 * meaning is not guaranteed to be consistent, the version SHOULD be
	 * exchanged
	 * </p>
	 */
	public void setVersion(String theValue) {
		version = theValue;
	}

	/**
	 * Gets the value(s) for <b>code</b> (). creating it if it does not exist.
	 * Will not return <code>null</code>.
	 *
	 * <p>
	 * <b>Definition:</b> A symbol in syntax defined by the system. The symbol
	 * may be a predefined code or an expression in a syntax defined by the
	 * coding system (e.g. post-coordination)
	 * </p>
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public CodeDt getCodeElement() {
		if (code == null) {
			code = new CodeDt();
		}
		return code;
	}

	/**
	 * Gets the value(s) for <b>code</b> (). creating it if it does not exist.
	 * Will not return <code>null</code>.
	 *
	 * <p>
	 * <b>Definition:</b> A symbol in syntax defined by the system. The symbol
	 * may be a predefined code or an expression in a syntax defined by the
	 * coding system (e.g. post-coordination)
	 * </p>
	 */
	public String getCode() {
		if (code == null) {
			code = new CodeDt();
		}
		return code.getValue();
	}

	/**
	 * Sets the value(s) for <b>code</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> A symbol in syntax defined by the system. The symbol
	 * may be a predefined code or an expression in a syntax defined by the
	 * coding system (e.g. post-coordination)
	 * </p>
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setCodeElement(CodeDt theValue) {
		code = theValue;
	}

	/**
	 * Sets the value for <b>code</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> A symbol in syntax defined by the system. The symbol
	 * may be a predefined code or an expression in a syntax defined by the
	 * coding system (e.g. post-coordination)
	 * </p>
	 */
	public void setCode(String theCode) {
		code = new CodeDt(theCode);
	}

	/**
	 * Gets the value(s) for <b>display</b> (). creating it if it does not
	 * exist. Will not return <code>null</code>.
	 *
	 * <p>
	 * <b>Definition:</b> A representation of the meaning of the code in the
	 * system, following the rules of the system
	 * </p>
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * Sets the value(s) for <b>display</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> A representation of the meaning of the code in the
	 * system, following the rules of the system
	 * </p>
	 */
	public void setDisplay(String theValue) {
		display = theValue;

	}

	/**
	 * Gets the value(s) for <b>userSelected</b> (). creating it if it does not
	 * exist. Will not return <code>null</code>.
	 *
	 * <p>
	 * <b>Definition:</b> Indicates that this coding was chosen by a user
	 * directly - i.e. off a pick list of available items (codes or displays)
	 * </p>
	 */
	public Boolean getUserSelected() {
		return userSelected;
	}

	/**
	 * Sets the value(s) for <b>userSelected</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> Indicates that this coding was chosen by a user
	 * directly - i.e. off a pick list of available items (codes or displays)
	 * </p>
	 */
	public void setUserSelected(Boolean theValue) {
		userSelected = theValue;

	}

	/**
	 * Sets the value for <b>userSelected</b> ()
	 *
	 * <p>
	 * <b>Definition:</b> Indicates that this coding was chosen by a user
	 * directly - i.e. off a pick list of available items (codes or displays)
	 * </p>
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setUserSelected(boolean theBoolean) {
		userSelected = new Boolean(theBoolean);

	}

    @Override
    public String toString() {
        return "CodingDt{system='" + system  + "', code='" + code + "', display='" + display +"'}";
    }
    
    

}
