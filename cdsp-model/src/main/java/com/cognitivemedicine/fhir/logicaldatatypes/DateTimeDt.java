/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Embeddable;

import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.TemporalPrecisionEnum;




/**
 * @author Jerry Goodnough
 *
 */
@Embeddable
public class DateTimeDt implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TimeZone timeZone;
	private String precision  = TemporalPrecisionEnum.SECOND.name();
	private Date value;
	
	public DateTimeDt() {
		super();
	}
	/**
	 * Create a new DateTimeDt with seconds precision and the local time zone
	 */
	public DateTimeDt(Date theDate) {
		value = theDate;
		precision = TemporalPrecisionEnum.SECOND.name();
		timeZone = TimeZone.getDefault();
	}
	
	public DateTimeDt(Date theDate, String thePrecision) {
		
		value = theDate;
		precision = thePrecision;
		timeZone = TimeZone.getDefault();
	}
	
	public DateTimeDt(Date theDate, String thePrecision, TimeZone zone) {
		
		value = theDate;
		precision = thePrecision;
		timeZone = zone;
	}
	/**
	 * HAPI FHIR Based copy constructor
	 * @param src
	 */
	public DateTimeDt(DateTimeType src)
	{
		value = src.getValue();
		timeZone = src.getTimeZone();
		precision = src.getPrecision().name();
	}

	/**
	 * Get DateTimeDT as a HAPI FHIR Object
	 * @return
	 */
	public DateTimeType toHAPIFHIR()
	{
	    DateTimeType out = new DateTimeType();
		out.setValue(value);
		out.setPrecision(TemporalPrecisionEnum.valueOf(precision));
		out.setTimeZone(timeZone);
		return out;
	}
	
	/**
	 * @return the timeZone
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}
	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}
	/**
	 * @return the precision
	 */
	public String getPrecision() {
		return precision;
	}
	/**
	 * @param precision the precision to set
	 */
	public void setPrecision(String precision) {
		this.precision = precision;
	}
	/**
	 * @return the value
	 */
	public Date getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Date value) {

		this.value = value;
	}
	/**
	 * Returns a new instance of DateTimeDt with the current system time and SECOND precision and the system local time
	 * zone
	 */
	public static DateTimeDt withCurrentTime() {
		
		return new DateTimeDt(new Date(), TemporalPrecisionEnum.SECOND.name());
	}
}
