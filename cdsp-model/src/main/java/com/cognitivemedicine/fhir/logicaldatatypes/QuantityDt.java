/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hl7.fhir.dstu3.model.Quantity;

import com.cognitivemedicine.cdsp.model.annotation.LogicalModelChange;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jerry Goodnough
 *
 */
@Embeddable
public class QuantityDt implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Constructor
	 */
	public QuantityDt() {
		// nothing
	}

 
	/**
	 * Constructor
	 */
	public QuantityDt( double theValue) {
		setValue(theValue);
	}

	/**
	 * Constructor
	 */
	public QuantityDt( long theValue) {
		setValue(theValue);
	}
	
	/**
	 * HAPI FHIR Based copy constructor
	 * @param src
	 */
	public QuantityDt(Quantity src)
	{
		value  = new DecimalDt(src.getValueElement());
		unit = src.getUnit();
		system = src.getSystem();
		codeElement = new CodeDt(src.getCodeElement());
	}

	/**
	 * Get QuantityDt as a HAPI FHIR Object
	 * @return
	 */
	public Quantity toHAPIFHIR()
	{
	    Quantity out = new Quantity();
		out.setCodeElement(codeElement.toHAPIFHIR());
		out.setSystem(system);
		out.setUnit(unit);
		out.setValueElement(value.toHAPIFHIR());
		return out;
	}
	
	private DecimalDt value;


	private String unit;
	
	private String system;
	
		
	private CodeDt codeElement;
	

	/**
	 * Gets the value(s) for <b>value</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The value of the measured amount. The value includes an implicit precision in the presentation of the value
     * </p> 
	 */
	//TODO - Fix for XML serialization
	@Transient
	@JsonProperty("value")
	public DecimalDt getValueElement() {  
		if (value == null) {
			value = new DecimalDt();
		}
		return value;
	}

	
	/**
	 * Gets the value(s) for <b>value</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The value of the measured amount. The value includes an implicit precision in the presentation of the value
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public BigDecimal getValue() {  
		return getValueElement().getValue();
	}

	/**
	 * Sets the value(s) for <b>value</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The value of the measured amount. The value includes an implicit precision in the presentation of the value
     * </p> 
	 */
	//TODO - Fix for XML serialization
	@JsonProperty("value")
	@Transient
	public void setValue(DecimalDt theValue) {
		value = theValue;
		
	}
	
	

 	/**
	 * Sets the value for <b>value</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The value of the measured amount. The value includes an implicit precision in the presentation of the value
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setValue( long theValue) {
		value = new DecimalDt(theValue); 
		
	}

	/**
	 * Sets the value for <b>value</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The value of the measured amount. The value includes an implicit precision in the presentation of the value
     * </p> 
	 */

	@JsonIgnore
	@XmlTransient
	@Transient
	public void setValue( double theValue) {
		value = new DecimalDt(theValue); 
		
	}

	/**
	 * Sets the value for <b>value</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The value of the measured amount. The value includes an implicit precision in the presentation of the value
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setValue( java.math.BigDecimal theValue) {
		value = new DecimalDt(theValue); 
		
	}

 
	
	/**
	 * Gets the value(s) for <b>unit</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * A human-readable form of the unit
     * </p> 
	 */
	public String getUnit() {  
		return unit;
	}

	/**
	 * Sets the value(s) for <b>unit</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * A human-readable form of the unit
     * </p> 
	 */
	public void setUnit(String theValue)
	{
		unit = theValue;
	}
		
	
	

	
	/**
	 * Gets the value(s) for <b>system</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The identification of the system that provides the coded form of the unit
     * </p> 
	 */
	public String getSystem() {  
		return system;
	}

	/**
	 * Sets the value(s) for <b>system</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The identification of the system that provides the coded form of the unit
     * </p> 
	 */
	public void setSystem(String theValue) {
		system = theValue;
	}
	
	


 
	/**
	 * Gets the value(s) for <b>code</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * A computer processable form of the unit in some unit representation system
     * </p> 
	 */
	public CodeDt getCodeElement() {  
		if (codeElement == null) {
			codeElement = new CodeDt();
		}
		return codeElement;
	}

	
	/**
	 * Gets the value(s) for <b>code</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * A computer processable form of the unit in some unit representation system
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public String getCode() {  
		return getCodeElement().getValue();
	}

	/**
	 * Sets the value(s) for <b>code</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * A computer processable form of the unit in some unit representation system
     * </p> 
     * 
	 */
	@LogicalModelChange("Changed for be bean/property consistent")
	public void setCodeElement(CodeDt theValue) {
		codeElement = theValue;
	}
	
	

 	/**
	 * Sets the value for <b>code</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * A computer processable form of the unit in some unit representation system
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public QuantityDt setCode( String theCode) {
		codeElement = new CodeDt(theCode); 
		return this; 
	}

}
