/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;

import javax.persistence.Embeddable;

import org.hl7.fhir.dstu3.model.CodeType;

/**
 * @author Jerry Goodnough
 *
 *
 * Do we really need this type - We could simplify CodeDt to String
 */
@Embeddable
public class CodeDt implements Serializable{

	private static final long serialVersionUID = 1L;

	private String value;

	/**
	 * Constructor
	 */
	public CodeDt()
	{}
	

	/**
	 * Simple HAPI FHIR based copy constructor
	 * @param src
	 */
	public CodeDt(CodeType src)
	{
		value = src.getValueAsString();
	}
	
	/**
	 * Returns the HAPI FHIR version of this data type.
	 * @return
	 */
	public CodeType toHAPIFHIR()
	{
		CodeType out = new CodeType();
		out.setValue(value);
		return out;	
	}
	
	

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}


	public CodeDt(String theCode) {
		value = theCode;
	}



}
