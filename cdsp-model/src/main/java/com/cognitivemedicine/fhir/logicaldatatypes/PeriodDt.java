/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hl7.fhir.dstu3.model.Period;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Jerry Goodnough
 *
 */
@Entity
public class PeriodDt implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "dbid")
	private Long dbid;

	@Embedded
	@AttributeOverrides({
	@AttributeOverride(name = "timeZone", column=@Column(name="startdate_timeZone")),
	@AttributeOverride(name = "precision", column=@Column(name="startdate_precision")),
	@AttributeOverride(name = "value", column=@Column(name="startdate_value"))
	})
	private DateTimeDt start;

	@Embedded
	@AttributeOverrides({
	@AttributeOverride(name = "timeZone", column=@Column(name="enddate_timeZone")),
	@AttributeOverride(name = "precision", column=@Column(name="enddate_precision")),
	@AttributeOverride(name = "value", column=@Column(name="enddate_value"))
	})
	private DateTimeDt end;
	
	public PeriodDt(){};
	
	/**
	 * HAPI FHIR Based copy constructor
	 * @param src
	 */
	public PeriodDt(Period src)
	{
		start = new DateTimeDt(src.getStartElement().getValue());
		end = new DateTimeDt(src.getEndElement().getValue());
		
	}

	/**
	 * Get PeriodDt as a HAPI FHIR Object
	 * @return
	 */
	public Period toHAPIFHIR()
	{
		Period out = new Period();
		out.setStart(start.toHAPIFHIR().getValue());
		out.setEnd(end.toHAPIFHIR().getValue());
		return out;
	}

	/**
	 * Gets the value(s) for <b>start</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The start of the period. The boundary is inclusive.
     * </p> 
	 */

    @JsonProperty("Start")
	public DateTimeDt getStartElement() {  
		if (start == null) {
			start = new DateTimeDt();
		}
		return start;
	}

	
	/**
	 * Gets the value(s) for <b>start</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The start of the period. The boundary is inclusive.
     * </p> 
	 */
	@JsonIgnore
	public Date getStart() {  
		return getStartElement().getValue();
	}

	/**
	 * Sets the value(s) for <b>start</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The start of the period. The boundary is inclusive.
     * </p> 
	 */
    @JsonProperty("Start")
	public void setStart(DateTimeDt theValue) {
		start = theValue;
	
	}
	
	

 	/**
	 * Sets the value for <b>start</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The start of the period. The boundary is inclusive.
     * </p> 
	 */
	@JsonIgnore
	public void setStart( Date theDate,  String thePrecision) {
		start = new DateTimeDt(theDate, thePrecision); 

	}

	/**
	 * Sets the value for <b>start</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The start of the period. The boundary is inclusive.
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setStartWithSecondsPrecision( Date theDate) {
		start = new DateTimeDt(theDate); 

	}

 
	/**
	 * Gets the value(s) for <b>end</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The end of the period. If the end of the period is missing, it means that the period is ongoing. The start may be in the past, and the end date in the future, which means that period is expected/planned to end at that time
     * </p> 
	 */
	@JsonProperty("End")
	public DateTimeDt getEndElement() {  
		if (end == null) {
			end = new DateTimeDt();
		}
		return end;
	}

	
	/**
	 * Gets the value(s) for <b>end</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * The end of the period. If the end of the period is missing, it means that the period is ongoing. The start may be in the past, and the end date in the future, which means that period is expected/planned to end at that time
     * </p> 
	 */
	@JsonIgnore
    public Date getEnd() {  
		return getEndElement().getValue();
	}

	/**
	 * Sets the value(s) for <b>end</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The end of the period. If the end of the period is missing, it means that the period is ongoing. The start may be in the past, and the end date in the future, which means that period is expected/planned to end at that time
     * </p> 
	 */
	@JsonProperty("End")
	public void setEnd(DateTimeDt theValue) {
		end = theValue;
	
	}
	
	

 	/**
	 * Sets the value for <b>end</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The end of the period. If the end of the period is missing, it means that the period is ongoing. The start may be in the past, and the end date in the future, which means that period is expected/planned to end at that time
     * </p> 
	 */
	@JsonIgnore
	public void setEnd( Date theDate,  String thePrecision) {
		end = new DateTimeDt(theDate, thePrecision); 

	}

	/**
	 * Sets the value for <b>end</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * The end of the period. If the end of the period is missing, it means that the period is ongoing. The start may be in the past, and the end date in the future, which means that period is expected/planned to end at that time
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public void setEndWithSecondsPrecision( Date theDate) {
		end = new DateTimeDt(theDate); 

	}

}
