/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.HumanName.NameUse;
import org.hl7.fhir.dstu3.model.StringType;



/**
 * @author Jerry Goodnough
 *
 */
public class HumanNameDt implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private java.util.List<String> family;

    private java.util.List<String> given;

    private PeriodDt period;

    private java.util.List<String> prefix;

    private java.util.List<String> suffix;

    private String text = "";

    private String use = NameUse.USUAL.name();

    public HumanNameDt() {}

    /**
	 * HAPI FHIR Based copy constructor
	 * @param src
	 */
	public HumanNameDt(HumanName src)
	{
		period = new PeriodDt(src.getPeriod());
		text = src.getText();
		use = src.getUse().toString();

		this.family = new ArrayList<String>(1);
		this.family.add(src.getFamily());
		this.given = copyListStringType(src.getGiven());
		this.prefix = copyListStringType(src.getPrefix());
		this.suffix = copyListStringType(src.getSuffix());
	}

    /**
     * Helper to create a HAPI FHIR string list
     * 
     * @param src
     * @return
     */
    private List<StringType> copyListString(List<String> src) {
        ArrayList<StringType> out = null;
        if (src != null) {
            out = new ArrayList<StringType>(src.size());
            Iterator<String> itr = src.iterator();
            while (itr.hasNext()) {
                String val = itr.next();
                out.add(new StringType(val));
            }

        }
        return out;
    }

    /**
     * Helper to create a HAPI FHIR string list
     * 
     * @param src
     * @return
     */
    private List<String> copyListStringType(List<StringType> src) {
        ArrayList<String> out = null;
        if (src != null) {
            out = new ArrayList<String>(src.size());
            Iterator<StringType> itr = src.iterator();
            while (itr.hasNext()) {
                StringType val = itr.next();
                out.add(val.getValue());
            }

        }
        return out;
    }

    /**
     * @return the family
     */
    public java.util.List<String> getFamily() {
        if (family == null) {
            family = new LinkedList<String>();
        }
        return family;
    }

    /**
     * @return the given
     */
    public java.util.List<String> getGiven() {
        if (suffix == null) {
            suffix = new LinkedList<String>();
        }
        return given;
    }

    /**
     * @return the period
     */
    public PeriodDt getPeriod() {
        if (period == null) {
            period = new PeriodDt();
        }
        return period;
    }

    /**
     * @return the prefix
     */
    public java.util.List<String> getPrefix() {
        if (suffix == null) {
            suffix = new LinkedList<String>();
        }
        return prefix;
    }

    /**
     * @return the suffix
     */
    public java.util.List<String> getSuffix() {
        if (suffix == null) {
            suffix = new LinkedList<String>();
        }
        return suffix;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the use
     */
    public String getUse() {
        return use;
    }

    /**
     * @param family the family to set
     */
    public void setFamily(java.util.List<String> family) {
        this.family = family;
    }

    /**
     * @param given the given to set
     */
    public void setGiven(java.util.List<String> given) {
        this.given = given;
    }

    /**
     * @param period the period to set
     */
    public void setPeriod(PeriodDt period) {
        this.period = period;
    }

    /**
     * @param prefix the prefix to set
     */
    public void setPrefix(java.util.List<String> prefix) {
        this.prefix = prefix;
    }

    /**
     * @param suffix the suffix to set
     */
    public void setSuffix(java.util.List<String> suffix) {
        this.suffix = suffix;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @param use the use to set
     */
    public void setUse(String use) {
        this.use = use;
    }

    /**
     * Get HumanNameDt as a HAPI FHIR Object
     * 
     * @return
     */
    public HumanName toHAPIFHIR() {
        HumanName out =
                new HumanName();

        out.setText(text);
        out.setUse(NameUse.valueOf(use));
        if (period != null) {
            out.setPeriod(period.toHAPIFHIR());
        }
        out.setFamily(family.get(0));
        out.setGiven(copyListString(given));
        out.setPrefix(copyListString(prefix));
        out.setSuffix(copyListString(suffix));

        return out;
    }

}
