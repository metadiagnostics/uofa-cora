/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hl7.fhir.dstu3.model.DecimalType;

import com.fasterxml.jackson.annotation.JsonIgnore;



/**
 * @author Jerry Goodnough
 *
 */
@Embeddable
public class DecimalDt implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public DecimalDt() {
		super();
	}

	BigDecimal value;
	
	/**
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

	/**
	 * Constructor
	 */

	public DecimalDt( BigDecimal theValue) {
		value =  theValue;
	}

	/**
	 * Constructor
	 */
	
	public DecimalDt( double theValue) {
		// Use the valueOf here because the constructor gives crazy precision
		// changes due to the floating point conversion
		value = BigDecimal.valueOf(theValue);
	}

	/**
	 * Constructor
	 */
	
	public DecimalDt( long theValue) {
		value = new BigDecimal(theValue);
	}

	/**
	 * Constructor
	 */
	public DecimalDt(String theValue) {
		value = new BigDecimal(theValue);
	}
	/**
	 * HAPI FHIR Based copy constructor
	 * @param src
	 */
	public DecimalDt(DecimalType src)
	{
		value = src.getValue();

	}

	/**
	 * Get DecimalDt as a HAPI FHIR Object
	 * @return
	 */
	public DecimalType toHAPIFHIR()
	{
	    DecimalType out = new DecimalType();
		out.setValue(value);
		
		return out;
	}
	
	
	/**
	 * Gets the value as an integer, using {@link BigDecimal#intValue()}
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public int getValueAsInteger() {
		return value.intValue();
	}
	@JsonIgnore
	@XmlTransient
	@Transient
	public Number getValueAsNumber() {
		return getValue();
	}


	/**
	 * Rounds the value to the given prevision
	 * 
	 * @see MathContext#getPrecision()
	 */
	public void round(int thePrecision) {
		if (getValue() != null) {
			BigDecimal newValue = getValue().round(new MathContext(thePrecision));
			setValue(newValue);
		}
	}

	/**
	 * Rounds the value to the given prevision
	 * 
	 * @see MathContext#getPrecision()
	 * @see MathContext#getRoundingMode()
	 */
	public void round(int thePrecision, RoundingMode theRoundingMode) {
		if (getValue() != null) {
			BigDecimal newValue = getValue().round(new MathContext(thePrecision, theRoundingMode));
			setValue(newValue);
		}
	}

	/**
	 * Sets a new value using an integer
	 */
	@JsonIgnore
	@XmlTransient
	public void setValueAsInteger(int theValue) {
		setValue(new BigDecimal(theValue));
	}

}
