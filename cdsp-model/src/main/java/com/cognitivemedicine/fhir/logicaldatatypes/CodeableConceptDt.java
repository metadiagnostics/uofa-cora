/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;



/**
 * @author Jerry Goodnough
 *
 */
@Entity
public class CodeableConceptDt implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "dbid")
	private Long dbid;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "CodeableConceptDt_CodingDt", joinColumns = @JoinColumn(name = "dbid"))
	// We should have been using Set but since we are using List, Hibernate does not allow for more than two EAGER Fetches.
	@Fetch(value = FetchMode.SELECT)
	private List<CodingDt> coding = new LinkedList<>();

	private String id;
	private String text;

	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Constructor
	 */
	public CodeableConceptDt() {
		// nothing
	}

	/**
	 * Constructor which creates a CodeableConceptDt with one coding repetition, containing
	 * the given system and code
	 */
	public CodeableConceptDt(String theSystem, String theCode) {
		CodingDt code  = addCoding();
		code.setSystem(theSystem);
		code.setCode(theCode);
	}

	
	
	/**
	 * @return the coding
	 */
	public List<CodingDt> getCoding() {
		return coding;
	}

	/**
	 * @param coding the coding to set
	 */
	public void setCoding(List<CodingDt> coding) {
		this.coding = coding;
	}


	/**
	 * Adds a given new value for <b>coding</b> ()
	 *
	 * <p>
	 * <b>Definition:</b>
	 * A reference to a code defined by a terminology system
	 * </p>
	 * @param theValue The coding to add (must not be <code>null</code>)
	 */
	public CodingDt addCoding(CodingDt theValue) {
		if (theValue == null) {
			throw new NullPointerException("theValue must not be null");
		}
		getCoding().add(theValue);
		return theValue;
	}
	
	public CodingDt addCoding() {
	
		CodingDt theValue = new CodingDt();
		getCoding().add(theValue);
		return theValue;
	}
	/**
	 * Gets the first repetition for <b>coding</b> (),
	 * creating it if it does not already exist.
	 *
     * <p>
     * <b>Definition:</b>
     * A reference to a code defined by a terminology system
     * </p> 
	 */
	@JsonIgnore
	@XmlTransient
	@Transient
	public CodingDt getCodingFirstRep() {
		if (getCoding().isEmpty()) {
			return addCoding();
		}
		return getCoding().get(0); 
	}
  
	/**
	 * Gets the value(s) for <b>text</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * A human language representation of the concept as seen/selected/uttered by the user who entered the data and/or which represents the intended meaning of the user
     * </p> 
	 */
	@JsonIgnore
	public String getTextElement() {  
		if (text == null) {
			text = "";
		}
		return text;
	}

	
	/**
	 * Gets the value(s) for <b>text</b> ().
	 * creating it if it does
	 * not exist. Will not return <code>null</code>.
	 *
     * <p>
     * <b>Definition:</b>
     * A human language representation of the concept as seen/selected/uttered by the user who entered the data and/or which represents the intended meaning of the user
     * </p> 
	 */
	public String getText() {  
		return text;
	}

	/**
	 * Sets the value(s) for <b>text</b> ()
	 *
     * <p>
     * <b>Definition:</b>
     * A human language representation of the concept as seen/selected/uttered by the user who entered the data and/or which represents the intended meaning of the user
     * </p> 
	 */
	public void setText(String theValue) {
		text = theValue;
	}
	
	



}
