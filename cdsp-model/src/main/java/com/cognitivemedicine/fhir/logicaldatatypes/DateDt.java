/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.fhir.logicaldatatypes;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Embeddable;

import org.hl7.fhir.dstu3.model.DateType;
import org.hl7.fhir.dstu3.model.TemporalPrecisionEnum;



/**
 * @author Jerry Goodnough
 *
 */
@Embeddable
public class DateDt implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private TimeZone timeZone;
	private String precision  = TemporalPrecisionEnum.DAY.name();
	private Date value;
	
	public DateDt() {};
	
	public DateDt(Date theDate)
	{
		value = theDate;
		timeZone = TimeZone.getDefault();
	}
	
	public DateDt(Date theDate, TimeZone zone)
	{
		value = theDate;
		timeZone = zone;
	}
	
	/**
	 * HAPI FHIR Based copy constructor
	 * @param src
	 */
	public DateDt(DateType src)
	{
		value = src.getValue();
		timeZone = src.getTimeZone();
		precision = src.getPrecision().name();
	}

	/**
	 * Get DateDT as a HAPI FHIR Object
	 * @return
	 */
	public DateType toHAPIFHIR()
	{
		DateType out = new DateType();
		out.setValue(value);
		out.setTimeZone(timeZone);
		out.setPrecision(TemporalPrecisionEnum.valueOf(precision));
		return out;
	}
	
	/**
	 * @return the timeZone
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}
	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}
	/**
	 * @return the precision
	 */
	public String getPrecision() {
		return precision;
	}
	/**
	 * @param precision the precision to set
	 */
	public void setPrecision(String precision) {
		this.precision = precision;
	}
	/**
	 * @return the value
	 */
	public Date getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Date value) {

		this.value = value;
	}
	
}
