package com.cognitivemedicine.ktd.commons;

import com.cognitivemedicine.cdps.ktd.commons.KTDConfigurationProfile;

import com.cognitivemedicine.cdps.ktd.commons.parameters.IntegerExecutionParameter;
import com.cognitivemedicine.cdps.ktd.commons.parameters.StringExecutionParameter;

import com.cognitivemedicine.cdsp.processmanager.runtime.RESTServiceLocator;
import com.cognitivemedicine.cdsp.services.config.client.ConfigurationServiceClient;
import com.cognitivemedicine.cdsp.services.config.model.ConfigurationProfileDTO;
import com.cognitivemedicine.cdsp.processmanager.runtime.InitParameter;
import org.apache.commons.configuration.DataConfiguration;
import com.cognitivemedicine.cdps.ktd.commons.KTDInformation;

import com.cognitivemedicine.cdsp.services.config.client.RESTClient;

import java.util.List;

rule "Configure DataConfiguration"
salience 10
when
    KTDConfigurationProfile($configurationName: configurationName, $profileName: profileName)
    InitParameter($sl: value#RESTServiceLocator, value#RESTServiceLocator.serviceName == RESTServiceLocator.Name.CONFIGURATION_SERVICE)
    $cn: List() from collect( InitParameter ( name == InitParameter.InitParameterConstants.CONFIGURATION_NAME ) ) //we need a collect because we also want to be activated even if there is no InitParameter
    $cp: List() from collect( InitParameter ( name == InitParameter.InitParameterConstants.CONFIGURATION_PROFILE_NAME ) ) //we need a collect because we also want to be activated even if there is no InitParameter
then
    String configurationName = $configurationName;
    String profileName = $profileName;

    //Overwrite default values if InitParameter are present
    if(!$cn.isEmpty()){
        configurationName = (String)((InitParameter)$cn.get(0)).getValue();
    }
    if(!$cp.isEmpty()){
        profileName = (String)((InitParameter)$cp.get(0)).getValue();
    }

    ConfigurationServiceClient csc = new ConfigurationServiceClient(RESTClient.create($sl.getServiceBaseURL()));
    ConfigurationProfileDTO cpd = csc.getConfigurationProfileOrEmpty(configurationName, profileName);
    DataConfiguration config = ConfigurationServiceClient.getProfileParametersAsConfiguration(cpd);
    insert(config);
end

rule "Config Report Parameters"
salience 10
when
    $cfg: DataConfiguration()
then
    insert(new StringExecutionParameter("MONGO_HOST", $cfg.getString("MONGO_HOST", "localhost")));
    insert(new IntegerExecutionParameter("MONGO_PORT", $cfg.getInt("MONGO_PORT", 27017)));
end

rule "Config OpenMQ Parameters"
salience 10
when
    $cfg: DataConfiguration()
then
    insert(new StringExecutionParameter("OPENMQ_ADDRESS_LIST", $cfg.getString("OPENMQ_ADDRESS_LIST", "localhost:7676")));
end

rule "Config CommunicationService Parameters"
salience 10
when
    $cfg: DataConfiguration()
then
    insert(new StringExecutionParameter("CS_URL", $cfg.getString("CS_URL", "http://localhost:8080/cdspwps/services/api/v1")));
end

rule "Config Demo Topic Parameters"
salience 10
when
    $cfg: DataConfiguration()
then
    insert(new StringExecutionParameter("DEMO_TOPIC_NAME", $cfg.getString("DEMO_TOPIC_NAME", "DEMO_DATA")));
    insert(new StringExecutionParameter("DEMO_DISCRIMINATOR_FIELD", $cfg.getString("DEMO_DISCRIMINATOR_FIELD", "CaseId")));
end