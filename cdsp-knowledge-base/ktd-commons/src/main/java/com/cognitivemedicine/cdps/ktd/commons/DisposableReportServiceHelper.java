/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.processmanager.runtime.Disposable;
import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.model.KTIAssessmentReportInformation;
import com.cognitivemedicine.cdsp.report.model.ReportInformation;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Helper for report services.
 * @author dcalcaprina
 *
 */
public class DisposableReportServiceHelper implements ReportService, Disposable {

    private static final Logger LOG = LoggerFactory.getLogger(DisposableReportServiceHelper.class);
    private final ExecutorService executor;
    private final ReportService delegate;

    public DisposableReportServiceHelper(ReportService delegate) {
        this.delegate = delegate;
        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void report(final ReportInformation report) {
        LOG.debug("Going to send a report to be saved asyncronously.");
        delegate.report(report);
    }

    @Override
    public void updateReportField(String className, String idField, String ktiId, String fieldName, Object value) {
        delegate.updateReportField(className, idField, ktiId, fieldName, value);
    }

    @Override
    public void dispose() {
        try {
            executor.shutdownNow();
            if (!executor.awaitTermination(30, TimeUnit.SECONDS)) {
                LOG.info("Timed out waiting for executor to shut down, exiting uncleanly");
            }
        } catch (InterruptedException e) {
            LOG.error("There was an error awaiting for executor termination", e);
        }
    }

    /**
     * Returns an assessment report instance based on a given communication request.
     * @param communicationRequest
     * @param ktdName
     * @param ktiId
     * @return
     */
    public static ReportInformation getAssessmentReport(String ktdName, String ktdVersion, String ktiId, Context context, String message, String alertType) {
        KTIAssessmentReportInformation report =
                new KTIAssessmentReportInformation(context, new Date(), "PMS", ktdName, ktdVersion, ktiId, message, alertType);
        return report;
    }
}
