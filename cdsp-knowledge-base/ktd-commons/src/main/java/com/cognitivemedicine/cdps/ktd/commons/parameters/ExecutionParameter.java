/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons.parameters;

/**
 *
 * @author esteban
 */
public class ExecutionParameter<T> {

    static final long serialVersionUID = 1L;

    private java.lang.String name;
    private T value;

    public ExecutionParameter() {
    }

    public ExecutionParameter(java.lang.String name,
        T value) {
        this.name = name;
        this.value = value;
    }
    
    public java.lang.String getName() {
        return this.name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
