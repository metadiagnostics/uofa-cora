/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons;

/**
 * Holds the KTD Information like the ktd name, and the KTI Id.
 * @author dcalcaprina
 *
 */
public class KTDInformation {

	private String ktdName;
	
	private String ktdVersion;
	
	private String ktiId;

	public KTDInformation(String ktdName, String ktdVersion, String ktiId) {
		this.ktdName = ktdName;
		this.ktiId = ktiId;
		this.ktdVersion = ktdVersion;
	}
	
	public String getKtiId() {
		return ktiId;
	}
	
	public String getKtdName() {
		return ktdName;
	}
	
	public String getKtdVersion() {
		return ktdVersion;
	}
}
