/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons;

import java.util.List;

import org.drools.core.spi.KnowledgeHelper;

import com.cognitivemedicine.cdsp.messaging.api.MessageListener;

public class InsertFactsMessageListener implements MessageListener<Object> {

	private final KnowledgeHelper drools;

	public InsertFactsMessageListener(KnowledgeHelper drools) {
		this.drools = drools;
	}
	@Override
	public void onMessage(Object message) {
		if (message instanceof List<?>) {
			List<?> facts = (List<?>) message;
			for (Object fact : facts) {
				drools.getWorkingMemory().insert(fact);
			}
		}
		else {
			drools.getWorkingMemory().insert(message);
		}
		drools.getWorkingMemory().fireAllRules();
	}
	


}
