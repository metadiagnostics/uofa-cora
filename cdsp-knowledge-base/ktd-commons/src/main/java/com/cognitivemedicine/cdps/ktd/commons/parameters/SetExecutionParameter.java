/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons.parameters;

import java.util.Set;

/**
 * Execution Parameter which holds a {@link Set} value.
 * @author calcacuervo
 *
 */
public class SetExecutionParameter extends ExecutionParameter<Set> {
    static final long serialVersionUID = 1L;

    public SetExecutionParameter(String name, Set value) {
        super(name, value);
    }

    @Override
    public String toString() {
        return "SetExecutionParameter{ name= " + this.getName() + ", value=" +this.getValue()+ '}';
    }
}
