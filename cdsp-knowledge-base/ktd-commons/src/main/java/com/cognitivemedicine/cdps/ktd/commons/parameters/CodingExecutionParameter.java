/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons.parameters;

import org.hl7.fhir.dstu3.model.Coding;

public class CodingExecutionParameter extends ExecutionParameter<Coding> {
    static final long serialVersionUID = 1L;

    public CodingExecutionParameter(String name, CodingWrapper coding) {
        super(name, coding.getCoding());
    }

    @Override
    public String toString() {
        return "CodingExecutionParameter{ name= " + this.getName() + ", value=" +this.getValue()+ '}';
    }
    
    public static class CodingWrapper {
        
        private Coding coding;
        
        public CodingWrapper(Coding coding) {
            this.coding = coding;
        }
        
        public CodingWrapper(final String display, final String system, final String code, final String version) {
            Coding coding = new Coding(system, code, display);
            coding.setVersion(version);
            this.coding = coding;
        }
        public Coding getCoding() {
            return coding;
        }
    }
}
