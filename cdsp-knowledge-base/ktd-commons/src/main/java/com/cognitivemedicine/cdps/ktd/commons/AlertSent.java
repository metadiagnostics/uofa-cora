/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;

import ca.uhn.hl7v2.model.primitive.ID;

/**
 * Alert sent indicator.
 * 
 * @author calcacuervo
 *
 */
public class AlertSent {

    private CommunicationRequest cr;

    private long timestamp;
    private long scheduledDateTime;
    private String title;
    private String alertType;

    public AlertSent(CommunicationRequest cr) {
        this.cr = cr;
        long now = new Date().getTime();
        this.timestamp = cr.getRequestedOn() == null ? now : cr.getRequestedOn().getTime();
        this.scheduledDateTime = cr.getScheduledDateTime() == null ? now : cr.getScheduledDateTime().getTime();
        this.title = cr.getPayloadFirstRep() == null ? "" : cr.getPayloadFirstRep().getTitle();
        this.alertType = getType();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getScheduledDateTime() {
        return scheduledDateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getAlertType() {
        return alertType;
    }

    private String getType() {
        List<IdentifierDt> ids = this.cr.getIdentifier();
        for (IdentifierDt identifierDt : ids) {
            if (identifierDt.getSystem().contains("KTD")) {
                String system = identifierDt.getSystem();
                String alertType = system.substring(system.indexOf("KTD") + "KTD".length() + 1);
                if (!StringUtils.isBlank(alertType)) {
                    return alertType;
                }
            }
        }
        // if no identifier found, the title will be the alert type.
        return getTitle();
    }

    public static AlertSent fromCommunicationRequest(CommunicationRequest cr) {
        return new AlertSent(cr);
    }
}
