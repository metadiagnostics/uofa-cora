/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons;

import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.drools.core.spi.KnowledgeHelper;

import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommunicationsHelper {

    private final static Logger LOG = LoggerFactory.getLogger(CommunicationsHelper.class);
    
	private final String communicationServiceUrl;
	private final KnowledgeHelper helper;
    
    private boolean disabled;
	
	public CommunicationsHelper(String communicationServiceUrl, KnowledgeHelper helper) {
		this.communicationServiceUrl = communicationServiceUrl;
		this.helper = helper;
	}

	public AlertSent sendAlert(CommunicationRequest alertMessage) {
        
        if (disabled){
            LOG.trace("The following Communication Request will not be sent because CommunicationsHelper is disabled: {}", alertMessage);
            return null;
        }
        
        
		HttpClient client = HttpClientBuilder.create().build();
		ObjectMapper mapper = new ObjectMapper();

		try {
			String content = mapper.writeValueAsString(alertMessage);
			StringEntity entity = new StringEntity((String) content);
			entity.setContentType(ContentType.APPLICATION_JSON.getMimeType());
			HttpUriRequest post = RequestBuilder.post().setUri(this.communicationServiceUrl).setEntity(entity).build();
			client.execute(post);
			AlertSent alertSent = AlertSent.fromCommunicationRequest(alertMessage);
			helper.insert(alertSent);
			return alertSent;
		} catch (IOException e) {
			throw new RuntimeException("Could not send alert.", e);
		}
	}
    
    public void setDisabled(boolean disabled){
        this.disabled = disabled;
    }
}
