/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons.logging;

import org.drools.core.WorkingMemory;
import org.drools.core.audit.WorkingMemoryLogger;
import org.drools.core.audit.event.LogEvent;
import org.kie.internal.event.KnowledgeRuntimeEventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default Working Memory Logger, which can log in different Log Levels.
 * @author dcalcaprina
 *
 */
public class DefaultWorkingMemoryLogger extends WorkingMemoryLogger {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultWorkingMemoryLogger.class);
	private LogLevel level;

	public DefaultWorkingMemoryLogger(LogLevel level, WorkingMemory workingMemory) {
		super(workingMemory);
		this.level = level;
	}

	public DefaultWorkingMemoryLogger(LogLevel level, KnowledgeRuntimeEventManager session) {
		super(session);
		this.level = level;
	}

	@Override
	public void logEventCreated(LogEvent logEvent) {
		String log = logEvent.toString();
		switch (level) {
		case TRACE:
			LOG.trace(log);
			break;
		case DEBUG:
			LOG.debug(log);
			break;
		case INFO:
			LOG.info(log);
			break;
		case WARN:
			LOG.warn(log);
			break;
		case ERROR:
			LOG.error(log);
			break;
		}
	}

	public static enum LogLevel {
		TRACE, DEBUG, INFO, WARN, ERROR,
	}
}
