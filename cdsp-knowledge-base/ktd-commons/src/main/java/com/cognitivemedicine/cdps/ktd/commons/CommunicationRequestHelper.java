/*******************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/
package com.cognitivemedicine.cdps.ktd.commons;

import com.cognitivemedicine.cdsp.bom.fhir.ICORAMedicationRequest;
import com.cognitivemedicine.cdsp.fhir.util.FhirUtils;
import com.cognitivemedicine.cdsp.model.util.mapping.ActionCode;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.Action;
import com.cognitivemedicine.fhir.logicalmodel.Attribution;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest.Payload;
import com.cognitivemedicine.fhir.logicalmodel.Context;
import com.cognitivemedicine.fhir.logicalmodel.Patient;
import com.cognitivemedicine.fhir.logicalmodel.Recipient;
import java.util.ArrayList;
import java.util.Collections;
import org.hl7.fhir.dstu3.model.Coding;

/**
 *
 * @author esteban
 */
public class CommunicationRequestHelper {

    public static CommunicationRequest createCommunicationRequest(Context context, String topicIdentifier, String status, String provenance, CodingDt category, CodingDt priority, Payload payload, List<Recipient> recipients, List<Action> actions, String alertType, KTDInformation ktdInformation) {

        //Priority
        CodeableConceptDt priorityCC = new CodeableConceptDt();
        priorityCC.addCoding(priority);

        //Attribution
        Attribution attribution = new Attribution();
        attribution.setDescription(provenance);

        //CommunicationRequest
        CommunicationRequest cr = new CommunicationRequest();
        cr.setId(UUID.randomUUID().toString());
        cr.addPayload(payload);
        cr.setContext(context);
        cr.setTopicIdentifier(topicIdentifier);
        cr.getCategory().setText(category.getDisplay());
        cr.getCategory().addCoding(category);
        cr.setPriority(priorityCC);
        cr.setStatus(status);
        cr.addAttribution(attribution);
        cr.setScheduledDateTime(new Date());

        //add the subject as the context subject
        cr.setSubject(createPatient(context.getSubjectId()));

        cr.setRequestedOn(new Date());

        IdentifierDt ktdIdentifier = new IdentifierDt();
        ktdIdentifier.setSystem("KTD." + alertType);
        ktdIdentifier.setValue(ktdInformation.getKtdName() + ":" + ktdInformation.getKtdVersion());
        cr.addIdentifier(ktdIdentifier);
        if (recipients != null) {
            for (Recipient r : recipients) {
                cr.addRecipient(r);
            }
        }

        cr.setAction(actions);

        return cr;
    }

    public static List<Action> getActions(ActionCode... requestedActions) {
        List<Action> actions = new ArrayList<Action>();
        for (ActionCode actionMapping : requestedActions) {
            actions.add(getAction(actionMapping.getDescription(), getCoding("com.cognitivemedicine.codesystem.actions", actionMapping.getCode(), actionMapping.getDisplay()), getCoding("Type", "1"), false));          
        }
        return actions;
    }
    
    private static CodeableConceptDt getCoding(String system, String code, String display) {
        CodeableConceptDt coding = new CodeableConceptDt(system, code);
        coding.setText(display);
        return coding;
    }
    
    public static List<Action> getActions(String... requestedActions) {
        return Collections.emptyList();
    }

    public static Action getAction(String description, CodeableConceptDt actionCode, CodeableConceptDt typeCode, boolean hidden) {
        Action action = new Action();
        action.setDescription(description);
        action.setAction(actionCode);
        action.setType(typeCode);
        action.setHidden(hidden);
        return action;
    }

    public static List<Action> getDefaultActions() {
        List<Action> actions = new ArrayList<Action>();
        actions.add(getAction("Acknowledge", getCoding("Action", "1"), getCoding("Type", "1"), false));
        actions.add(getAction("Reject", getCoding("Action", "2"), getCoding("Type", "1"), false));
        actions.add(getAction("Read", getCoding("Action", "3"), getCoding("Type", "1"), true));
        return actions;
    }

    public static CodingDt getCodingDt(String system, String code, String display) {
        CodingDt coding = new CodingDt(system, code);
        coding.setDisplay(display);

        return coding;
    }

    private static CodeableConceptDt getCoding(String code, String value) {
        CodeableConceptDt coding = new CodeableConceptDt(code, value);
        return coding;
    }

    public static Payload createPayload(String title, String body) {
        Payload payload = new Payload();
        payload.setTitle(title);
        payload.setContent(body);

        return payload;
    }

    public static Recipient createRecipient(String recipientSystem, String recipientCode, String recipientDisplay, String recipientRoleSystem, String recipientRoleValue) {
        //Recipient
        Recipient recipient = new Recipient();
        CodeableConceptDt recipientType = new CodeableConceptDt();
        CodingDt recipientTypeCode = new CodingDt();
        recipientTypeCode.setSystem(recipientSystem);
        recipientTypeCode.setCode(recipientCode);
        recipientTypeCode.setDisplay(recipientDisplay);
        recipientType.addCoding(recipientTypeCode);
        recipient.setType(recipientType);
        IdentifierDt roleId = new IdentifierDt();
        roleId.setSystem(recipientRoleSystem);
        roleId.setValue(recipientRoleValue);
        recipient.setIdentifier(roleId);

        return recipient;
    }

    public static Patient createPatient(IdentifierDt identifier) {
        Patient patient = new Patient();
        patient.addIdentifier(identifier);
        return patient;
    }

    public static String createMedicationRequestTemplate(ICORAMedicationRequest medicationRequest, String codeSystemToShow) {
        Coding c = FhirUtils.getCodingForCodeSystem(medicationRequest.getMedicationCodeableConcept(), codeSystemToShow);
        return c == null ? "&lt;Unknown&gt;" : c.getDisplay() != null ? c.getDisplay() : medicationRequest.getMedicationCodeableConcept().getText();
    }

}
