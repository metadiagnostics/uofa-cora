/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.util;

import ca.uhn.fhir.model.primitive.BaseDateTimeDt;
import ca.uhn.hl7v2.model.Message;
import com.cognitivemedicine.cdsp.bom.fhir.CORACognitiveBase;
import com.cognitivemedicine.cdsp.fhir.util.BaseDateTimeDtDeserializer;
import com.cognitivemedicine.cdsp.fhir.util.BaseDateTimeDtSerializer;
import com.cognitivemedicine.cdsp.fhir.util.BaseDateTimeTypeDeserializer;
import com.cognitivemedicine.cdsp.fhir.util.BaseDateTimeTypeSerializer;
import com.cognitivemedicine.cdsp.fhir.util.BundleDeserializer;
import com.cognitivemedicine.cdsp.fhir.util.BundleSerializer;
import com.cognitivemedicine.cdsp.fhir.util.CognitiveBaseResourceDeSerializer;
import com.cognitivemedicine.cdsp.fhir.util.CognitiveBaseResourceSerializer;
import com.cognitivemedicine.cdsp.fhir.util.IBaseResourceSerializer;
import com.cognitivemedicine.cdsp.hl7v2.util.MessageDeserializer;
import com.cognitivemedicine.cdsp.hl7v2.util.MessageSerializer;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.hl7.fhir.dstu3.model.BaseDateTimeType;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.instance.model.api.IBaseResource;

/**
 * This class serves as a facade of all the JsonProviders this module contains.
 *
 * @author esteban
 */
public class MultiJsonProvider extends JacksonJaxbJsonProvider {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(MultiJsonProvider.class);

    public MultiJsonProvider() {
        LOG.info("Config MultiJsonProvider with needed customized Serializers");
        this.setMapper(this.createObjectMapper());
    }

    /**
     * Default constructor, usually used when provider is automatically
     * configured to be used with JAX-RS implementation. /** Default
     * constructor, usually used when provider is automatically configured to be
     * used with JAX-RS implementation.
     */

    /**
     * @param annotationsToUse Annotation set(s) to use for configuring data
     * binding
     */
    public MultiJsonProvider(Annotations... annotationsToUse) {
        super(annotationsToUse);
        this.setMapper(this.createObjectMapper());
    }

    /**
     * Constructor to use when a custom mapper (usually components like
     * serializer/deserializer factories that have been configured) is to be
     * used.
     */
    public MultiJsonProvider(ObjectMapper mapper, Annotations[] annotationsToUse) {
        super(annotationsToUse);
        this.setMapper(mapper);
    }

    private ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        SimpleModule module = new SimpleModule();

        //Serializers
        module.addSerializer(Message.class, new MessageSerializer());
        module.addSerializer(Bundle.class, new BundleSerializer());
        module.addSerializer(IBaseResource.class, new IBaseResourceSerializer());
        module.addSerializer(CORACognitiveBase.class, new CognitiveBaseResourceSerializer());
        module.addSerializer(BaseDateTimeType.class, new BaseDateTimeTypeSerializer());
        module.addSerializer(BaseDateTimeDt.class, new BaseDateTimeDtSerializer());

        //Deserializers
        module.addDeserializer(Message.class, new MessageDeserializer());
        module.addDeserializer(Bundle.class, new BundleDeserializer());
        module.addDeserializer(CORACognitiveBase.class, new CognitiveBaseResourceDeSerializer());
        module.addDeserializer(BaseDateTimeType.class, new BaseDateTimeTypeDeserializer());
        module.addDeserializer(BaseDateTimeDt.class, new BaseDateTimeDtDeserializer());

        objectMapper.registerModule(module);
        
        return objectMapper;
    }
}
