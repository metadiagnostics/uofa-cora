/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;

public class MockTerminologyServer {

    private final static Map<String, CodeableConcept> allergiesCodes;
    private final static Map<String, CodeableConcept> proceduresCodes;
    private final static Map<String, CodeableConcept> vitalCodes;
    private final static Map<String, CodeableConcept> relationshipCodes;
    private final static Map<String, CodeableConcept> medicationCodes;
    private final static Map<String, CodeableConcept> devicesCodes;
    private final static Map<String, CodeableConcept> observationCodes;
    private final static CodeableConcept gestationalAgeCode;

    static {
        // use a custom comparator for the keys of this map, allowing a case insensitive string comparison.
        medicationCodes = new TreeMap<>((o1, o2) -> {
            return o1.toString().toLowerCase().compareTo(o2.toString().toLowerCase());
        });
        
        medicationCodes.put("Droperidol", getCodeableConcept(getCoding("Droperidol", "RXNORM", "3648"),
                getCoding("Droperidol", "urn:oid:2.16.840.1.113883.6.88", "3648")));
        medicationCodes.put("Hydroxyzine", getCodeableConcept(getCoding("Hydroxyzine", "RXNORM", "5553"),
                getCoding("Hydroxyzine", "urn:oid:2.16.840.1.113883.6.88", "5553")));
        medicationCodes.put("Metoclopramide", getCodeableConcept(getCoding("Metoclopramide", "RXNORM", "6915"),
                getCoding("Metoclopramide", "urn:oid:2.16.840.1.113883.6.88", "6915")));
        medicationCodes.put("Ondansetron", getCodeableConcept(getCoding("Ondansetron", "RXNORM", "26225"),
                getCoding("Ondansetron", "urn:oid:2.16.840.1.113883.6.88", "26225")));
        medicationCodes.put("Promethazine", getCodeableConcept(getCoding("Promethazine", "RXNORM", "8745"),
                getCoding("Promethazine", "urn:oid:2.16.840.1.113883.6.88", "8745")));
        
        medicationCodes.put("Exp HAL", getCodeableConcept(getCoding("Halothane", "RXNORM", "5095"),
                getCoding("Halothane", "urn:oid:2.16.840.1.113883.6.88", "5095")));
        medicationCodes.put("Exp HAL (mm Hg) (Demo)", getCodeableConcept(getCoding("Halothane", "RXNORM", "5095"),
                getCoding("Halothane", "urn:oid:2.16.840.1.113883.6.88", "5095")));
        medicationCodes.put("Exp HAL (mm Hg) (Demo:1)", getCodeableConcept(getCoding("Halothane", "RXNORM", "5095"),
                getCoding("Halothane", "urn:oid:2.16.840.1.113883.6.88", "5095")));

        medicationCodes.put("Exp ISO", getCodeableConcept(getCoding("Isoflurane", "RXNORM", "6026"),
                getCoding("Isoflurane", "urn:oid:2.16.840.1.113883.6.88", "6026")));
        medicationCodes.put("Exp ISO (mm Hg) (Demo)", getCodeableConcept(getCoding("Isoflurane", "RXNORM", "6026"),
                getCoding("Isoflurane", "urn:oid:2.16.840.1.113883.6.88", "6026")));
        medicationCodes.put("Exp ISO (mm Hg) (Demo:1)", getCodeableConcept(getCoding("Isoflurane", "RXNORM", "6026"),
                getCoding("Isoflurane", "urn:oid:2.16.840.1.113883.6.88", "6026")));

        medicationCodes.put("Exp SEV", getCodeableConcept(getCoding("Sevoflurane", "RXNORM", "36453"),
                getCoding("Sevoflurane", "urn:oid:2.16.840.1.113883.6.88", "36453")));
        medicationCodes.put("Exp SEV (mm Hg) (Demo)", getCodeableConcept(getCoding("Sevoflurane", "RXNORM", "36453"),
                getCoding("Sevoflurane", "urn:oid:2.16.840.1.113883.6.88", "36453")));
        medicationCodes.put("Exp SEV (mm Hg) (Demo:1)", getCodeableConcept(getCoding("Sevoflurane", "RXNORM", "36453"),
                getCoding("Sevoflurane", "urn:oid:2.16.840.1.113883.6.88", "36453")));

        medicationCodes.put("Exp DES", getCodeableConcept(getCoding("Desflurane", "RXNORM", "27340"),
                getCoding("Desflurane", "urn:oid:2.16.840.1.113883.6.88", "27340")));
        medicationCodes.put("Exp DES (mm Hg) (Demo)", getCodeableConcept(getCoding("Desflurane", "RXNORM", "27340"),
                getCoding("Desflurane", "urn:oid:2.16.840.1.113883.6.88", "27340")));
        medicationCodes.put("Exp DES (mm Hg) (Demo:1)", getCodeableConcept(getCoding("Desflurane", "RXNORM", "27340"),
                getCoding("Desflurane", "urn:oid:2.16.840.1.113883.6.88", "27340")));

        medicationCodes.put("FentaNYL", getCodeableConcept(getCoding("FentaNYL", "RXNORM", "4337"),
                getCoding("FentaNYL", "urn:oid:2.16.840.1.113883.6.88", "4337")));
        medicationCodes.put("FentaNYL Infusion", getCodeableConcept(getCoding("FentaNYL Infusion", "RXNORM", "4337"),
                getCoding("FentaNYL Infusion", "urn:oid:2.16.840.1.113883.6.88", "4337")));
        medicationCodes.put("HYDROmorphone", getCodeableConcept(getCoding("HYDROmorphone", "RXNORM", "3423"),
                getCoding("HYDROmorphone", "urn:oid:2.16.840.1.113883.6.88", "3423")));
        medicationCodes.put("Morphine Epidural", getCodeableConcept(getCoding("Morphine Epidural", "RXNORM", "7052"),
                getCoding("Morphine", "urn:oid:2.16.840.1.113883.6.88", "7052")));
        medicationCodes.put("Morphine MPF SAB", getCodeableConcept(getCoding("Morphine MPF SAB", "RXNORM", "7052"),
                getCoding("Morphine", "urn:oid:2.16.840.1.113883.6.88", "7052")));
        medicationCodes.put("Remifentanil", getCodeableConcept(getCoding("Remifentanil", "RXNORM", "73032"),
                getCoding("Remifentanil", "urn:oid:2.16.840.1.113883.6.88", "73032")));
        medicationCodes.put("Remifentanil Infusion", getCodeableConcept(getCoding("Remifentanil Infusion", "RXNORM", "73032"),
                getCoding("Remifentanil", "urn:oid:2.16.840.1.113883.6.88", "73032")));
        medicationCodes.put("SUFentanil", getCodeableConcept(getCoding("SUFentanil", "RXNORM", "56795"),
                getCoding("SUFentanil", "urn:oid:2.16.840.1.113883.6.88", "56795")));
        medicationCodes.put("SUFentanil Infusion", getCodeableConcept(getCoding("SUFentanil Infusion", "RXNORM", "56795"),
                getCoding("SUFentanil", "urn:oid:2.16.840.1.113883.6.88", "56795")));


        relationshipCodes = new HashMap<>();

        relationshipCodes.put("mother", getCodeableConcept("mother", "http://snomed.info/sct", "72705000"));
        relationshipCodes.put("father", getCodeableConcept("father", "http://snomed.info/sct", "66839005"));
        relationshipCodes.put("grand father", getCodeableConcept("grand father", "http://snomed.info/sct", "34871008"));
        relationshipCodes.put("grand mother", getCodeableConcept("grand mother", "http://snomed.info/sct", "113157001"));
        relationshipCodes.put("sister", getCodeableConcept("sister", "http://snomed.info/sct", "27733009"));
        relationshipCodes.put("brother", getCodeableConcept("brother", "http://snomed.info/sct", "70924004"));
        relationshipCodes.put("cousin", getCodeableConcept("cousin", "http://snomed.info/sct", "55538000"));
        relationshipCodes.put("person in family", getCodeableConcept("person in family", "http://snomed.info/sct", "303071001"));

        allergiesCodes = new HashMap<>();
        allergiesCodes.put("Betadine", getCodeableConcept("Beta-lactam antibiotic (substance)", "http://snomed.info/sct", "373297006"));
        allergiesCodes.put("PCN", getCodeableConcept("Beta-lactam antibiotic (substance)", "http://snomed.info/sct", "373297006"));
        allergiesCodes.put("373297006", getCodeableConcept("Beta-lactam antibiotic (substance)", "http://snomed.info/sct", "373297006"));

        proceduresCodes = new HashMap<>();
        proceduresCodes.put("Hysterectomy",
                getCodeableConcept(
                        "Total abdominal hysterectomy (corpus and cervix), with or without removal of tube(s), with or without removal of ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58150"));
        proceduresCodes.put("TOTAL ABDOMINAL HYSTERECT W/WO RMVL TUBE OVARY",
                getCodeableConcept(
                        "Total abdominal hysterectomy (corpus and cervix), with or without removal of tube(s), with or without removal of ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58150"));
        proceduresCodes.put("58150",
                getCodeableConcept(
                        "Total abdominal hysterectomy (corpus and cervix), with or without removal of tube(s), with or without removal of ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58150"));
        proceduresCodes.put("RAD ABDL HYSTERECTOMY W/BI PELVIC LMPHADENECTOMY",
                getCodeableConcept(
                        "Radical abdominal hysterectomy, with bilateral total pelvic lymphadenectomy and para-aortic lymph node sampling (biopsy), with or without removal of tube(s), with or without removal of ovary(s)",
                        "http://www.ama-assn.org/go/cpt", "58210"));
        proceduresCodes.put("58210",
                getCodeableConcept(
                        "Radical abdominal hysterectomy, with bilateral total pelvic lymphadenectomy and para-aortic lymph node sampling (biopsy), with or without removal of tube(s), with or without removal of ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58210"));
        proceduresCodes.put("Vaginal Hysterectomy",
                getCodeableConcept("Vaginal hysterectomy, for uterus 250 g or less; with removal of tube(s), and/or ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58262"));
        proceduresCodes.put("VAG HYST 250 GM/< W/RMVL TUBE&/OVARY",
                getCodeableConcept("Vaginal hysterectomy, for uterus 250 g or less; with removal of tube(s), and/or ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58262"));
        proceduresCodes.put("58262",
                getCodeableConcept("Vaginal hysterectomy, for uterus 250 g or less; with removal of tube(s), and/or ovary(s)",
                        "urn:oid:2.16.840.1.113883.6.12", "58262"));
        
        proceduresCodes.put("67311",
                getCodeableConcept("STRABISMUS RECESSION/RESCJ 1 HRZNTL MUSC",
                        "urn:oid:2.16.840.1.113883.6.12", "67311"));
        proceduresCodes.put("67312",
                getCodeableConcept("STRABISMUS RECESSION/RESCJ 2 HRZNTL MUSC",
                        "urn:oid:2.16.840.1.113883.6.12", "67312"));
        proceduresCodes.put("67314",
                getCodeableConcept("STRABISMUS RECESSION/RESCJ 1 VER MUSC",
                        "urn:oid:2.16.840.1.113883.6.12", "67314"));
        proceduresCodes.put("67316",
                getCodeableConcept("STRABISMUS RECESSION/RESCJ 2/MORE VER MUSC",
                        "urn:oid:2.16.840.1.113883.6.12", "67316"));
        proceduresCodes.put("67318",
                getCodeableConcept("STRABISMUS ANY SUPERIOR OBLIQUE MUSCLE",
                        "urn:oid:2.16.840.1.113883.6.12", "67318"));
        proceduresCodes.put("67331",
                getCodeableConcept("STRABISMUS PREVIOUS EYE X INVOLVE EO MUSC",
                        "urn:oid:2.16.840.1.113883.6.12", "67331"));
        proceduresCodes.put("67332",
                getCodeableConcept("STRABISMUS SCARRING EO MUSC/RSTCV MYOPATHY",
                        "urn:oid:2.16.840.1.113883.6.12", "67332"));
        proceduresCodes.put("67334",
                getCodeableConcept("STRABISMUS POST FIXJ SUTR TQ W/WO MUSC RECESSION",
                        "urn:oid:2.16.840.1.113883.6.12", "67334"));
        proceduresCodes.put("67335",
                getCodeableConcept("PLACEMENT ADJUSTABLE SUTURE STRABISMUS",
                        "urn:oid:2.16.840.1.113883.6.12", "67335"));
        proceduresCodes.put("67340",
                getCodeableConcept("STRABISMUS EXPL/RPR DETACHED EXTROCULAR MUSC",
                        "urn:oid:2.16.840.1.113883.6.12", "67340"));
        
        // 2.16.840.1.113883.6.96 is SNOMED CT OID. For General Anesthesia we are using value sets and using the oid.
        // In the future, we should migrate all codes systems to use value sets.
        proceduresCodes.put("General", getCodeableConcept("General anesthesia", "urn:oid:2.16.840.1.113883.6.96", "50697003"));
        proceduresCodes.put("General anesthesia", getCodeableConcept("General anesthesia", "urn:oid:2.16.840.1.113883.6.96", "50697003"));
        
        proceduresCodes.put("In Situ", getCodeableConcept(getCoding("In Situ", "http://snomed.info/sct", "419991009"),
                getCoding("In Situ", "urn:oid:2.16.840.1.113883.6.96", "419991009")));
        proceduresCodes.put("Mask", getCodeableConcept(getCoding("Mask", "http://snomed.info/sct", "261382003"),
                getCoding("Mask", "urn:oid:2.16.840.1.113883.6.96", "261382003")));
        proceduresCodes.put("Endotracheal Intubation - Direct Laryngoscopy",
                getCodeableConcept(getCoding("Endotracheal Intubation - Direct Laryngoscopy", "http://snomed.info/sct", "78121007"),
                        getCoding("Endotracheal Intubation - Direct Laryngoscopy", "urn:oid:2.16.840.1.113883.6.96", "78121007")));
        proceduresCodes.put("Endotracheal Intubation - Indirect Laryngoscopy",
                getCodeableConcept(getCoding("Endotracheal Intubation - Indirect Laryngoscopy", "http://snomed.info/sct", "673005"),
                        getCoding("Endotracheal Intubation - Indirect Laryngoscopy", "urn:oid:2.16.840.1.113883.6.96", "673005")));
        proceduresCodes.put("Endotracheal Intubation - Intubating LMA",
                getCodeableConcept(getCoding("Endotracheal Intubation - Intubating LMA", "http://snomed.info/sct", "418613003"),
                        getCoding("Endotracheal Intubation - Intubating LMA", "urn:oid:2.16.840.1.113883.6.96", "418613003")));
        proceduresCodes.put("Supraglottic - LMA", getCodeableConcept(getCoding("Supraglottic - LMA", "http://snomed.info/sct", "424979004"),
                getCoding("Supraglottic - LMA", "urn:oid:2.16.840.1.113883.6.96", "424979004")));

        proceduresCodes.put("general procedure", getCodeableConcept(getCoding("general procedure", "http://snomed.info/sct", "71388002"),
                getCoding("general procedure", "urn:oid:2.16.840.1.113883.6.96", "71388002")));
        
        // Holds LOINC Values for Vitals
        vitalCodes = new HashMap<>();
        vitalCodes.put("etCO2", getCodeableConcept("Carbon dioxide^at end expiration", "http://loinc.org/loinc", "19891-1"));
        vitalCodes.put("etCO2 (mm Hg) (Demo)", getCodeableConcept("Carbon dioxide^at end expiration", "http://loinc.org/loinc", "19891-1"));
        vitalCodes.put("ETCO2", getCodeableConcept("Carbon dioxide^at end expiration", "http://loinc.org/loinc", "19891-1"));
        vitalCodes.put("Temperature", getCodeableConcept("Body temperature", "http://loinc.org/loinc", "8310-5"));
        vitalCodes.put("Temp 1 (C) (Demo)", getCodeableConcept("Body temperature", "http://loinc.org/loinc", "8310-5"));
        vitalCodes.put("Pulse", getCodeableConcept("Heart rate", "http://loinc.org/loinc", "8867-4"));
        vitalCodes.put("Heart Rate (bpm) (Demo)", getCodeableConcept("Heart rate", "http://loinc.org/loinc", "8867-4"));
        vitalCodes.put("FGF", getCodeableConcept("Fresh Gas Fluid", "http://snomed.info/sct", "250849000"));
        vitalCodes.put("FG Flow (ml/min) (Demo)", getCodeableConcept("Fresh Gas Fluid", "http://snomed.info/sct", "250849000"));
        vitalCodes.put("CVP Mean", getCodeableConcept("CVP", "http://snomed.info/sct", "71420008"));
        vitalCodes.put("CVP Mean (mm Hg) (Demo)", getCodeableConcept("CVP", "http://snomed.info/sct", "71420008"));
        
        gestationalAgeCode = getCodeableConcept("Gestational Age", "http://snomed.info/sct", "5703600657036006");
        
        devicesCodes = new HashMap<>();
        devicesCodes.put("In Situ", getCodeableConcept(getCoding("In Situ", "http://snomed.info/sct", "26412008"),
                getCoding("In Situ", "urn:oid:2.16.840.1.113883.6.96", "26412008")));
        devicesCodes.put("Mask", getCodeableConcept(getCoding("Mask", "http://snomed.info/sct", "261382003"),
                getCoding("Mask", "urn:oid:2.16.840.1.113883.6.96", "261382003")));
        devicesCodes.put("Endotracheal Intubation - Direct Laryngoscopy",
                getCodeableConcept(getCoding("Endotracheal Intubation - Direct Laryngoscopy", "http://snomed.info/sct", "26412008"),
                        getCoding("Endotracheal Intubation - Direct Laryngoscopy", "urn:oid:2.16.840.1.113883.6.96", "26412008")));
        devicesCodes.put("Endotracheal Intubation - Indirect Laryngoscopy",
                getCodeableConcept(getCoding("Endotracheal Intubation - Indirect Laryngoscopy", "http://snomed.info/sct", "26412008"),
                        getCoding("Endotracheal Intubation - Indirect Laryngoscopy", "urn:oid:2.16.840.1.113883.6.96", "26412008")));
        devicesCodes.put("Endotracheal Intubation - Intubating LMA",
                getCodeableConcept(getCoding("Endotracheal Intubation - Intubating LMA", "http://snomed.info/sct", "26412008"),
                        getCoding("Endotracheal Intubation - Intubating LMA", "urn:oid:2.16.840.1.113883.6.96", "26412008")));
        devicesCodes.put("Supraglottic - LMA", getCodeableConcept(getCoding("Supraglottic - LMA", "http://snomed.info/sct", "257268009"),
                getCoding("Supraglottic - LMA", "urn:oid:2.16.840.1.113883.6.96", "257268009")));

        observationCodes = new HashMap<>();
        observationCodes.put("449868002", getCodeableConcept(getCoding("Current every day smoker", "http://snomed.info/sct", "449868002"),
                getCoding("Current every day smoker", "urn:oid:2.16.840.1.113883.6.96", "449868002")));
        observationCodes.put("428041000124106", getCodeableConcept(getCoding("Current some day smoker", "http://snomed.info/sct", "428041000124106"),
                getCoding("Current some day smoker", "urn:oid:2.16.840.1.113883.6.96", "428041000124106")));
        observationCodes.put("8517006", getCodeableConcept(getCoding("Former smoker", "http://snomed.info/sct", "8517006"),
                getCoding("Former smoker", "urn:oid:2.16.840.1.113883.6.96", "8517006")));
        observationCodes.put("266919005", getCodeableConcept(getCoding("Never", "http://snomed.info/sct", "266919005"),
                getCoding("Never", "urn:oid:2.16.840.1.113883.6.96", "266919005")));
        observationCodes.put("77176002", getCodeableConcept(getCoding("Smoker", "http://snomed.info/sct", "77176002"),
                getCoding("Smoker", "urn:oid:2.16.840.1.113883.6.96", "77176002")));
        observationCodes.put("266927001", getCodeableConcept(getCoding("Unknown", "http://snomed.info/sct", "266927001"),
                getCoding("Unknown", "urn:oid:2.16.840.1.113883.6.96", "266927001")));
        observationCodes.put("428071000124103", getCodeableConcept(getCoding("Current Heavy tobacco smoker", "http://snomed.info/sct", "428071000124103"),
                getCoding("Current Heavy tobacco smoker", "urn:oid:2.16.840.1.113883.6.96", "428071000124103")));
        observationCodes.put("428071000124105", getCodeableConcept(getCoding("Current Light tobacco smoker", "http://snomed.info/sct", "428071000124105"),
                getCoding("Current Light tobacco smoker", "urn:oid:2.16.840.1.113883.6.96", "428071000124105")));
        
        //weight
        observationCodes.put("27113001", getCodeableConcept("Body weight", "http://snomed.info/sct", "27113001"));
    
    }

    private static CodeableConcept getCodeableConcept(String text, String codingSystem, String code) {
        CodeableConcept ccdt = new CodeableConcept();
        List<Coding> codingList = new ArrayList<>();
        Coding coding = new Coding(codingSystem, code, text);
        coding.setUserSelected(false);
        codingList.add(coding);
        ccdt.setCoding(codingList);
        return ccdt;
    }
    
    private static CodeableConcept getCodeableConcept(Coding...codings) {
        CodeableConcept ccdt = new CodeableConcept();
        List<Coding> codingList = new ArrayList<>();
        if (codings != null) {
            for (Coding c : codings) {
                codingList.add(c);
            }
        }
        ccdt.setCoding(codingList);
        return ccdt;
    }

    private static CodeableConcept getCodeableConcept(String originalName, List<Coding> codings) {
        CodeableConcept ccdt = new CodeableConcept();
        ccdt.setText(originalName);
        List<Coding> codingList = new ArrayList<>();
        if (codings != null) {
            for (Coding c : codings) {
                codingList.add(c);
            }
        }
        ccdt.setCoding(codingList);
        return ccdt;
    }
    
    private static CodeableConcept getCodeableConcept(List<Coding> codings) {
        CodeableConcept ccdt = new CodeableConcept();
        List<Coding> codingList = new ArrayList<>();
        if (codings != null) {
            for (Coding c : codings) {
                codingList.add(c);
            }
        }
        ccdt.setCoding(codingList);
        return ccdt;
    }
    
    private static Coding getCoding(String text, String codingSystem, String code) {
        Coding coding = new Coding(codingSystem, code, text);
        coding.setUserSelected(false);
        return coding;
    }

    public static CodeableConcept getAllergyCodeableConcept(String condition) {
        CodeableConcept baseCcdt = allergiesCodes.get(condition);
        if (baseCcdt == null) {
            return null;
        }
        return getCodeableConcept(baseCcdt.getCoding());
    }

    public static CodeableConcept getProcedureCodeableConcept(String procedure) {
        CodeableConcept baseCcdt = proceduresCodes.get(procedure);
        if (baseCcdt == null) {
            return null;
        }
        CodeableConcept copy = getCodeableConcept();
        for (Coding coding : baseCcdt.getCoding()) {
            copy.addCoding(coding);
        }
        return copy;
    }

    public static CodeableConcept getMedicationCodeableConcept(String med) {
        CodeableConcept baseCcdt = medicationCodes.get(med);
        if (baseCcdt == null) {
            return null;
        }
        return getCodeableConcept(med, baseCcdt.getCoding());
    }

    public static CodeableConcept getDeviceCodeableConcept(String device) {
        CodeableConcept baseCcdt = devicesCodes.get(device);
        if (baseCcdt == null) {
            return null;
        }
        return getCodeableConcept(baseCcdt.getCoding());
    }

    public static CodeableConcept getObservationCodeableConcept(String obs) {
        CodeableConcept baseCcdt = observationCodes.get(obs);
        if (baseCcdt == null) {
            return null;
        }
        return getCodeableConcept(baseCcdt.getCoding());
    }

    public static CodeableConcept getVitalCodeableConcept(String vital) {
        CodeableConcept baseCcdt = vitalCodes.get(vital);
        if (baseCcdt == null) {
            return null;
        }
        return getCodeableConcept(vital, baseCcdt.getCoding());
    }

    public static CodeableConcept getRelationshipCodeableConcept(String relationship) {
        CodeableConcept baseCcdt = relationshipCodes.get(relationship);
        if (baseCcdt == null) {
            return null;
        }
        return getCodeableConcept(baseCcdt.getCoding());
    }

    public static String getVitalForConcept(String system, String code) {
        for (Entry<String, CodeableConcept> entry : vitalCodes.entrySet()) {
            CodeableConcept concept = entry.getValue();
            if (system.equals(concept.getCodingFirstRep().getSystem()) && code.equals(concept.getCodingFirstRep().getCode())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static CodeableConcept getGestationalAge() {
        return gestationalAgeCode;
    }

}
