/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;

import java.io.IOException;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hl7.fhir.dstu3.model.BaseDateTimeType;


public class BaseDateTimeTypeSerializer extends JsonSerializer<BaseDateTimeType> {

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(BaseDateTimeType.class);

    
    @Override
    public Class<BaseDateTimeType> handledType() {
        return BaseDateTimeType.class;
    }
    
    @Override
    public void serialize(BaseDateTimeType value, JsonGenerator jgen, SerializerProvider provider)
        throws IOException, JsonProcessingException 
    {
        LOG.debug("IN IResourceSerializer SERIALIZER");
        
        //--------------------------------------------
        // USING HAPI to PARSE a RESOURCE to JSON string
        //--------------------------------------------
      
        Long outstr = value == null || value.getValue() == null ? null : value.getValue().getTime();
        jgen.writeObject(outstr);
        
        //logger.info("SER: \n" + outstr);
    }

}