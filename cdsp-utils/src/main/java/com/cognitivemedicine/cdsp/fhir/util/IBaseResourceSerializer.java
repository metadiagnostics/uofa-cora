/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;

import java.io.IOException;

import org.hl7.fhir.instance.model.api.IBaseResource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;


public class IBaseResourceSerializer extends JsonSerializer<IBaseResource> {

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(IBaseResourceSerializer.class);

    
    @Override
    public Class<IBaseResource> handledType() {
        return IBaseResource.class;
    }
    
    @Override
    public void serialize(IBaseResource value, JsonGenerator jgen, SerializerProvider provider)
        throws IOException, JsonProcessingException 
    {
        LOG.debug("IN IResourceSerializer SERIALIZER");
        
        //--------------------------------------------
        // USING HAPI to PARSE a RESOURCE to JSON string
        //--------------------------------------------
      
        String outstr = FhirUtils.newJsonParser().encodeResourceToString(value);
        jgen.writeRawValue(outstr);
        
        //logger.info("SER: \n" + outstr);
    }

}