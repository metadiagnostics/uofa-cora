/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;

import java.io.IOException;

import org.hl7.fhir.instance.model.api.IBaseResource;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ca.uhn.fhir.parser.IParser;
import com.cognitivemedicine.cdsp.bom.fhir.CORACognitiveBase;


public class CognitiveBaseResourceDeSerializer extends StdDeserializer<CORACognitiveBase> {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CognitiveBaseResourceDeSerializer.class);
    private IParser parser = null;

    // Constructor
    public CognitiveBaseResourceDeSerializer() {
        super(CORACognitiveBase.class);

    }


    @Override
    public CORACognitiveBase deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        LOG.debug("=====> IN Bundle deserialize");

        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        ObjectNode obj = (ObjectNode) mapper.readTree(jp);

        parser = FhirUtils.newJsonParser();

        // GET STRING version of object so HAPI can parse easily to an object.
        // HAPI parsing of object to correct HAPI class (Bundle or IResource)
        String s = mapper.writeValueAsString(obj);
        IBaseResource resource = (IBaseResource) parser.parseResource(s);
        return transformToBOM(resource);
    }

    public CORACognitiveBase transformToBOM(IBaseResource resource){
        String adapterClassName = "com.cognitivemedicine.cdsp.bom.fhir.CORA"+resource.getClass().getSimpleName()+"Adapter";
        try {
            Class<?> target = Class.forName(adapterClassName);
            return (CORACognitiveBase) target.getConstructor(resource.getClass()).newInstance(resource);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unable to transform "+resource.getClass().getName(), ex);
        }
    }
    

}
