/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.hl7.fhir.instance.model.api.IBaseResource;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ca.uhn.fhir.parser.IParser;

public class ObjectDeserializer extends StdDeserializer<Object> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ObjectDeserializer.class);

    // the registry of unique field names to Class types
    private Map<String, Class<? extends Object>> registry = new HashMap<String, Class<? extends Object>>();

    //Constructor
    public ObjectDeserializer() {
    	super(Object.class);
        
    }

    public void register(String uniqueProperty, Class<? extends Object> objectClass) {
        registry.put(uniqueProperty, objectClass);
    }

    @Override
    public Object deserialize(JsonParser jp, DeserializationContext ctxt)
        throws IOException, JsonProcessingException {

        LOG.info("=====> IN OBJECT deserialize");
      
        Class<? extends Object> foundClass = null;

        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        ObjectNode obj = (ObjectNode) mapper.readTree(jp);

        //LOOP THROUGH UNTIL key value found to indicate type.
        Iterator<Entry<String, JsonNode>> elementsIterator = obj.fields();
        while (elementsIterator.hasNext()) {
            Entry<String, JsonNode> element = elementsIterator.next();
            String name = element.getKey();
            if (registry.containsKey(name)) {
                foundClass = registry.get(name);
                break;
            }
        }
        
        // GET STRING version of object so HAPI can parse easily to an object.
        // HAPI parsing of object to correct HAPI class (Bundle or IResource)
        IParser parser = null;
        String s = mapper.writeValueAsString(obj);

        if ((foundClass != null) && foundClass.getSimpleName().equalsIgnoreCase("IBaseResource") ) {
            // HAVE TO USE HAPI TO PARSE foundClass into a IResource object 

            LOG.info("FOUND IRESOURCE OBJECT");
            parser = FhirUtils.newJsonParser();
            IBaseResource iRes = parser.parseResource(s); 
            return iRes;
            
        } else {

            String msg = "No registered unique properties found for polymorphic deserialization";
            LOG.info(msg);

            return obj;

        }
    }

}
