/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.cfg.Annotations;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

/**
 * Customize the Json Provider to control the way(s) our data
 * can be Serialized and DeSerialized
 * 
 * @author tnguyen
 */
public class JsonProvider  extends JacksonJaxbJsonProvider {
    
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(JsonProvider.class);

    public JsonProvider() {
              
        this(JsonUtils.getMapper(), JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
        LOG.debug("Configured JsonProvider with needed customized Serializers");
        
    }
    /**
     * Default constructor, usually used when provider is automatically
     * configured to be used with JAX-RS implementation.
     */

    /**
     * @param annotationsToUse Annotation set(s) to use for configuring
     *    data binding
     */
    public JsonProvider(Annotations... annotationsToUse)
    {
        this(JsonUtils.getMapper(), annotationsToUse);
    }
    
    /**
     * Constructor to use when a custom mapper (usually components
     * like serializer/deserializer factories that have been configured)
     * is to be used.
     */
    public JsonProvider(ObjectMapper mapper, Annotations[] annotationsToUse)
    {
        super(mapper, annotationsToUse);
    }

}
