/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;


import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.cognitivemedicine.fhir.logicaldatatypes.CodeableConceptDt;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;

/**
 * Helper utilities for dealing with FHIR data
 * 
 * @author jgoodnough
 *
 */
public class FhirUtils {
    private static FhirContext fhirCtx;

    /**
     * Constant to define the Data Nature Extension
     */
    public static final String DATANATURE = "http://org.cognitive.cds.invocation.fhir.datanature";
    /**
     * Constant to define the parameter name extension
     */
    public static final String PARAMETERNAME = "http://org.cognitive.cds.invocation.fhir.parametername";

    static {
        fhirCtx = FhirContext.forDstu3();
    }

    /**
     * Get the Standard FHIR Context
     * 
     * @return
     */
    static public FhirContext getContext() {
        return fhirCtx;
    }

    /**
     * Gets a new FHIR JSON parser Note: At this the investigation of if this
     * parser is thread safe has not been done so for now we assume that is is
     * not.
     * 
     * @return
     */
    static public IParser newJsonParser() {
        return fhirCtx.newJsonParser();
    }

    /**
     * Returns the first {@link Coding} found in the passed {@link CodeableConcept}
     * with the specified system.
     * 
     * If the {@link CodeableConcept} doesn't contain any {@link Coding} for
     * the specified system, this method returns null.
     * This method also returns null if any of its parameters is null.
     * 
     * @param cc the {@link CodeableConcept}
     * @param system the Code System
     * @return the first {@link Coding} found in the passed {@link CodeableConcept}
     * with the specified system.
     */
    static public Coding getCodingForCodeSystem(CodeableConcept cc, String system){
        
        if(cc == null || system == null){
            return null;
        }
        
        List<Coding> codings = cc.getCoding();
        
        if (codings == null || codings.isEmpty()){
            return null;
        }
        
        for (Coding coding : codings) {
            if (system.equals(coding.getSystem())){
                return coding;
            }
        }
        
        return null;
    }
    
    /**
     * Returns the code part of the first {@link Coding} found in the 
     * passed {@link CodeableConcept} with the specified system.
     * 
     * If the {@link CodeableConcept} doesn't contain any {@link Coding} for
     * the specified system, this method returns null.
     * 
     * This method also returns null if any of its arguments is null.
     * 
     * @param cc the {@link CodeableConcept}
     * @param system the Code System
     * 
     * @return the code part of the first {@link Coding} found in the passed 
     * {@link CodeableConceptDt} with the specified system.
     */
    static public String getCodeForCodeSystem(CodeableConcept cc, String system){
        Coding codingForCodeSystem = getCodingForCodeSystem(cc, system);
        
        return codingForCodeSystem == null? null : codingForCodeSystem.getCode();
    }
    
    public static boolean areCodingEquals(final Coding c1, final Coding c2) {
        if (c1 == null && c2 == null) {
            return true;
        }
        if (c1 == null || c2 == null) {
            return false;
        }
        String code1 = c1.getCode();
        String code2 = c2.getCode();
        String system1 = c1.getSystem();
        String system2 = c2.getSystem();
        String version1 = c1.getVersion();
        String version2 = c2.getVersion();
        Validate.notNull(code1);
        Validate.notNull(code2);
        Validate.notNull(system1);
        Validate.notNull(system2);
        if (!code1.equals(code2) || !system1.equals(system2)) {
            return false;
        } else {
            if ((version1 == null || version2 == null) ||version1.equals(version2)) {
                return true;
            }
            return false;
        }
    }
}
