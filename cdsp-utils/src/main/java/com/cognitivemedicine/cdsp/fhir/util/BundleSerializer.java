/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.hl7.fhir.dstu3.model.Bundle;

public class BundleSerializer extends JsonSerializer<Bundle> {

	private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(BundleSerializer.class);

//    @Override
//    public Class<Bundle> handledType() {
//        return Bundle.class;
//    }
    
    @Override
    public void serialize(Bundle value, JsonGenerator jgen, SerializerProvider provider)
        throws IOException, JsonProcessingException 
    {
        LOG.debug("IN BundleSerializer SERIALIZER");
        
        //--------------------------------------------
        // USING HAPI to PARSE a BUNDLE to JSON string
        //--------------------------------------------

        String outstr = FhirUtils.newJsonParser().encodeResourceToString(value);
        
        jgen.writeRawValue(outstr);
        //logger.info("==========> SER: \n" + outstr);
    }

}