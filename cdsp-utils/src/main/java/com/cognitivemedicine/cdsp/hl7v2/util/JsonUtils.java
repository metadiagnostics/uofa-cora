/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.hl7v2.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ca.uhn.hl7v2.model.Message;

/**
 * Support Class for JSON
 * 
 * @author esteban
 *
 */
public class JsonUtils {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(JsonUtils.class);

    String ret = null;

    private final static PrivateObjectMapper objectMapper;
    private final static PrivateObjectMapper compactMapper;

    static {
        objectMapper = new PrivateObjectMapper();
        objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.lock();

        compactMapper = new PrivateObjectMapper();
        compactMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        compactMapper.setSerializationInclusion(Include.NON_NULL);
        compactMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        SimpleModule module = new SimpleModule();

        // -------------------------------------------
        // ADD CUSTOM SERIALIZERS
        // -------------------------------------------
        LOG.trace("=========> register serializers");
        module.addSerializer(Message.class, new MessageSerializer());

        // -------------------------------------------
        // ADD CUSTOM DESERIALIZER and
        // REGISTER UNIQUE KEYs FOR each DESER CLASS TYPEs
        // -------------------------------------------
        LOG.trace("=========> register deserializers");
        module.addDeserializer(Message.class, new MessageDeserializer());

        objectMapper.registerModule(module);
        compactMapper.registerModule(module);
    }

    static public ObjectMapper getMapper() {
        return objectMapper;
    }

    public static String toJsonString(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    public static String toJsonStringCompact(Object obj) throws JsonProcessingException {
        return compactMapper.writeValueAsString(obj);
    }

}
