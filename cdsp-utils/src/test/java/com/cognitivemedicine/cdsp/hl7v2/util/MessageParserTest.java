/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.hl7v2.util;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.CanonicalModelClassFactory;
import ca.uhn.hl7v2.parser.PipeParser;

/**
 *
 * @author esteban
 */
public class MessageParserTest {
	protected final static Logger LOG = LoggerFactory.getLogger(MessageParserTest.class);
	
    @Test
    public void testGenericParser() throws HL7Exception {

        String message = "MSH|^~\\&|SENDAPP|SENDFACILITY|RECAPP|RECFACILITY|20160530084200+0000||ADT^A01|MyUniqueMessageControlID1111|MyUniqueProcessingID1111|2.6\r"
            + "EVN|A01|20160530071534|||EventFacilityWhereOriginalEventOccurred\r"
            + "PID|1||1008|0123456789999|MOTHERSLASTNAME^BABYBOY|MOMSMAIDENNAME|20151104103101|M|||123 GOLD STREET^^SAN DIEGO^CA^92130^USA^HOME\r"
            + "NK1|1|MOMFIRST^MOTHERSLASTNAME|72705000^Mother^SCT^10417-6^Mother^LN|123 GOLD STREET^^SAN DIEGO^CA^92130^USA^HOME|^555^5551234\r"
            + "PV1|1|I|NI|||||||NICU";

        LOG.debug("Pre-message: \n" + message);
        

        HapiContext context = new DefaultHapiContext();
        CanonicalModelClassFactory mcf = new CanonicalModelClassFactory("2.6");
        context.setModelClassFactory(mcf);

        PipeParser parser = context.getPipeParser();
        Message messageObject = parser.parse(message);

        message = parser.encode(messageObject);
        LOG.debug("Post-message: \n" + message);
       
    }

}
