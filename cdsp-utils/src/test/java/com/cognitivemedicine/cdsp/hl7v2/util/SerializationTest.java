/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.hl7v2.util;

import ca.uhn.fhir.model.primitive.DateTimeDt;
import com.cognitivemedicine.cdsp.bom.fhir.CORAPatientAdapter;
import com.cognitivemedicine.cdsp.model.events.CaseStateEvent;
import com.cognitivemedicine.cdsp.util.MultiJsonProvider;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import javax.ws.rs.core.MediaType;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class SerializationTest {
    
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SerializationTest.class);

    @Test
    public void serializeCaseStateEventTest() throws IOException {
        
        String caseId = "321";
        String patientId = "123";
        String workstationId = "200";
        CaseStateEvent.CaseStateEnum caseType = CaseStateEvent.CaseStateEnum.CASE_OPEN;
        
        CodingDt caseTypeCode = new CodingDt("some-system", "some-code");
        Date now = new Date();
        
        CORAPatientAdapter patient = new CORAPatientAdapter();
        
        CaseStateEvent evt = new CaseStateEvent(caseId, caseType, patientId, workstationId, patient);
        evt.setType(caseTypeCode);
        evt.setTime(new DateTimeDt(now));

        ObjectMapper om = new MultiJsonProvider().locateMapper(CaseStateEvent.class, MediaType.APPLICATION_JSON_TYPE);
        
        //Serialization
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        om.writerWithDefaultPrettyPrinter().writeValue(baos, evt);
        
        String json = new String(baos.toByteArray());
        
        LOG.debug("Serialized JSON: {}", json);
        
    }

}
