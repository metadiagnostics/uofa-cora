/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.fhir.util;

import org.hl7.fhir.dstu3.model.Coding;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for {@link FhirUtils} class.
 * @author calcacuervo
 *
 */
public class FhirUtilTest {

    @Test
    public void testCodingEquals() {
        Assert.assertTrue(FhirUtils.areCodingEquals(coding("1", "S"), coding("1", "S")));
        Assert.assertTrue(FhirUtils.areCodingEquals(coding("1", "S", "V"), coding("1", "S", "V")));
        Assert.assertTrue(FhirUtils.areCodingEquals(coding("1", "S", "V", "Display"), coding("1", "S", "V", "A display")));
        Assert.assertFalse(FhirUtils.areCodingEquals(coding("1", "S", "V1"), coding("1", "S", "V2")));
        Assert.assertTrue(FhirUtils.areCodingEquals(coding("1", "S", null), coding("1", "S", "V2")));
        Assert.assertTrue(FhirUtils.areCodingEquals(coding("1", "S", "V1"), coding("1", "S", null)));
        Assert.assertFalse(FhirUtils.areCodingEquals(coding("1", "S1", "V"), coding("1", "S2", "V")));
        Assert.assertFalse(FhirUtils.areCodingEquals(coding("1", "S", "V"), coding("2", "S", "V")));
    }

    private Coding coding(String code, String system, String version, String display) {
        Coding coding = new Coding(system, code, display);
        coding.setVersion(version);
        return coding;
    }

    private Coding coding(String code, String system, String version) {
        return coding(code, system, version, null);
    }

    private Coding coding(String code, String system) {
        return coding(code, system, null, null);
    }
}
