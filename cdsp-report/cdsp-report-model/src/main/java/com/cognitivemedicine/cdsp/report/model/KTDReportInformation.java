/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;

import org.mongodb.morphia.annotations.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Adds information about a KTD in a report information.
 * @author dcalcaprina
 *
 */
@Entity("report")
public class KTDReportInformation extends ReportInformation {

    /**
     * Instances of KTD Report information already knows its Coding type.
     */
    // TODO consider moving this to a properties file
    private static final String KTD_REPORT_INFORMATION_TYPE_CODE = "KTD";
    private static final CodingDt KTD_REPORT_INFORMATION_TYPE;

    static {
        KTD_REPORT_INFORMATION_TYPE =
                new CodingDt(ReportInformation.REPORT_INFORMATION_CODING_SYSTEM_TYPE, KTD_REPORT_INFORMATION_TYPE_CODE);
    }

    /**
     * The version of the ktd.
     */
    private String ktdVersion;

    /**
     * Indicates some identifier of the KTI.
     */
    private String ktdId;

    private Date initTime;

    private Date finishTime;
    
    private Boolean ended = false;

    public KTDReportInformation(Context context, Date time, String originator, CodingDt reportedAction, String ktdId, String ktdVersion,
            Date initTime) {
        super(context, time, originator, KTD_REPORT_INFORMATION_TYPE, reportedAction);
        this.ktdId = ktdId;
        this.ktdVersion = ktdVersion;
        this.initTime = initTime;
    }

    public Date getInitTime() {
        return initTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getKtdId() {
        return ktdId;
    }

    public String getKtdVersion() {
        return ktdVersion;
    }
    
    public Boolean getEnded() {
        return ended;
    }
    
    public void setEnded(Boolean ended) {
        this.ended = ended;
    }
}
