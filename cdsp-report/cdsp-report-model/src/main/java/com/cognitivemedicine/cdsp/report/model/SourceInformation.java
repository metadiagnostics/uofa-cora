/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

public class SourceInformation {

    private String sourceName;

    private String sourceVersion;

    public SourceInformation() {
    }
    
    public SourceInformation(String sourceName, String sourceVersion) {
        this.sourceName = sourceName;
        this.sourceVersion = sourceVersion;
    }

    public SourceInformation(String sourceString) {
        String[] informationParts = sourceString.split(":");
        if (informationParts.length == 3) {
            this.sourceName = informationParts[0] + ":" +  informationParts[1];
            this.sourceVersion = informationParts[2];
        } else {
            this.sourceName = sourceString;
        }
    }
    public String getSourceName() {
        return sourceName;
    }
    
    public String getSourceVersion() {
        return sourceVersion;
    }
}
