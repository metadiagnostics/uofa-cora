/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;

import org.mongodb.morphia.annotations.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Adds information about a KTI action in a report information.
 * @author dcalcaprina
 *
 */
@Entity("report")
public class KTILifecycleReportInformation extends KTIReportInformation {

    /**
     * Instances of KTI Report information already knows its Coding type.
     */
    // TODO consider moving this to a properties file
    private static final String KTI_REPORT_INFORMATION_ACTION_CODE = "LIFECYCLE";
    private static final CodingDt KTI_REPORT_INFORMATION_ACTION;

    static {
        KTI_REPORT_INFORMATION_ACTION =
                new CodingDt(ReportInformation.REPORT_INFORMATION_ACTION_CODING_SYSTEM_TYPE,
                        KTI_REPORT_INFORMATION_ACTION_CODE);
    }
    /**
     * Indicates the message
     */
    private Date startTime;
    private Date endTime;

    public KTILifecycleReportInformation() {
    }

    public KTILifecycleReportInformation(Context context, Date time, String originator,
            String ktdName, String ktdVersion, String ktiId, Date startTime) {
        super(context, time, originator, KTI_REPORT_INFORMATION_ACTION, ktdName, ktdVersion, ktiId);
        this.startTime = startTime;
    }
    
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }
}
