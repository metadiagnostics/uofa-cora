/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;
import java.util.Map;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;
import com.cognitivemedicine.fhir.logicalmodel.Context;


/**
 * Report Information which gives information about an Action applied to a {@link CommunicationRequest}.
 * @author dcalcaprina
 *
 */
@Entity("report")
public class CommunicationActionReportInformation extends CommunicationReportInformation {

	private static final String COMMUNICATION_ACTION_REPORT_INFORMATION_ACTION_CODE = "ACTION";
	private static final CodingDt COMMUNICATION_ACTION_REPORT_INFORMATION_ACTION;

	static {
		COMMUNICATION_ACTION_REPORT_INFORMATION_ACTION = new CodingDt(ReportInformation.REPORT_INFORMATION_ACTION_CODING_SYSTEM_TYPE,
				COMMUNICATION_ACTION_REPORT_INFORMATION_ACTION_CODE);
	}

	private CodingDt action;
	/**
	 * The user which made the action.
	 */
	private String user;
	
	/**
	 * The roles of the user.
	 */
	private String roles;
	
	@Embedded
	private Map<String, String> metadata;

	public CommunicationActionReportInformation() {
    }

	public CommunicationActionReportInformation(Context context, Date time, String originator, long communicationId, CommunicationRequest cr, CodingDt action, Map<String, String> metadata, String user, String roles) {
		super(context, time, originator, COMMUNICATION_ACTION_REPORT_INFORMATION_ACTION, communicationId, cr);
		this.action = action;
		this.metadata = metadata;
		this.user = user;
		this.roles = roles;
	}
	
	public CommunicationActionReportInformation(Context context, Date time, String originator, long communicationId, CommunicationRequest cr, CodingDt action, Map<String, String> metadata, String user) {
        this(context, time, originator, communicationId, cr, action, metadata, user, null);
	}

	public CodingDt getAction() {
		return action;
	}

	public Map<String, String> getMetadata() {
        return metadata;
    }
	
	public String getUser() {
        return user;
    }
	
	public String getRoles() {
        return roles;
    }
}
