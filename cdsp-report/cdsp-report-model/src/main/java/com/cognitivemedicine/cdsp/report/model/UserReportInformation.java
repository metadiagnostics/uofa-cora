/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;
import java.util.List;

import org.mongodb.morphia.annotations.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Report Information which gives data about user activity.
 * @author dcalcaprina
 *
 */
@Entity("user_report")
public class UserReportInformation extends ReportInformation {

    private static final String USER_REPORT_INFORMATION_TYPE_CODE = "USER";
    private static final CodingDt USER_REPORT_INFORMATION_TYPE;

    private List<String> roles;

    static {
        USER_REPORT_INFORMATION_TYPE =
                new CodingDt(ReportInformation.REPORT_INFORMATION_CODING_SYSTEM_TYPE, USER_REPORT_INFORMATION_TYPE_CODE);
    }

    public UserReportInformation() {
    }

    public UserReportInformation(Context context, Date time, String originator, CodingDt reportedAction, List<String> roles) {
        super(context, time, originator, USER_REPORT_INFORMATION_TYPE, reportedAction);
        this.roles = roles;
    }

    public List<String> getRoles() {
        return roles;
    }

}
