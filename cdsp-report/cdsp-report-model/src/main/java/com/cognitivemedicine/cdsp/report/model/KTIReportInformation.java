/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;

import org.mongodb.morphia.annotations.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Adds information about a KTI in a report information.
 * @author dcalcaprina
 *
 */
@Entity("report")
public abstract class KTIReportInformation extends ReportInformation {

	/**
	 * Instances of KTI Report information already knows its Coding type.
	 */
	// TODO consider moving this to a properties file
	private static final String KTI_REPORT_INFORMATION_TYPE_CODE = "KTI";
	private static final CodingDt KTI_REPORT_INFORMATION_TYPE;
	
	static {
		KTI_REPORT_INFORMATION_TYPE = new CodingDt(ReportInformation.REPORT_INFORMATION_CODING_SYSTEM_TYPE, KTI_REPORT_INFORMATION_TYPE_CODE);
	}
	
	/**
	 * Indicates some identifier of the KTD.
	 */
	private String ktdId;
	
	/**
	 * The version of the ktd.
	 */
	private String ktdVersion;
	
	/**
	 * Indicates some identifier of the KTI.
	 */
	private String ktiId;

	public KTIReportInformation() {
    }

	public KTIReportInformation(Context context, Date time, String originator, CodingDt reportedAction, String ktdId, String ktdVersion, String ktiId) {
		super(context, time, originator, KTI_REPORT_INFORMATION_TYPE, reportedAction);
		this.ktiId = ktiId;
		this.ktdId = ktdId;
		this.ktdVersion = ktdVersion;
	}

	public String getKtiId() {
		return ktiId;
	}
	
	public String getKtdId() {
		return ktdId;
	}
	
	public String getKtdVersion() {
		return ktdVersion;
	}
}
