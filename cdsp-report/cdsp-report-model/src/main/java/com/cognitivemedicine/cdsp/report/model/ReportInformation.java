/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Base Report Information Class. This contains all basic report information
 * data, such us an id, a Context, time, etc. Subclasses of this can add more
 * concrete data.
 * 
 * @author dcalcaprina
 *
 */
@Entity("report")
public class ReportInformation {

	protected static final String REPORT_INFORMATION_CODING_SYSTEM_TYPE = "ReportInformation";
	protected static final String REPORT_INFORMATION_ACTION_CODING_SYSTEM_TYPE = "ReportInformationAction";

	/**
	 * Unique id for this report information.
	 */
	@Id
	private String id;

	/**
	 * The context where this Report Information refers to.
	 */
	@Embedded
	private Context context;

	/**
	 * Time for the report information.
	 */
	@Property
	private Date time;

	/**
	 * Some identifier of the source of the report information.
	 */
	@Property
	private String originator;

	/**
	 * This contains the code for the type of the report information
	 */
	@Embedded
	private CodingDt type;

	/**
	 * This contains the action of the report information. For this report
	 * information indicates a NEW instance of some event.
	 */
	@Embedded
	private CodingDt reportedAction;

	public ReportInformation() {
    }

	public ReportInformation(Context context, Date time, String originator, CodingDt type, CodingDt reportedAction) {
		this.context = context;
		this.time = time;
		this.originator = originator;
		this.type = type;
		this.reportedAction = reportedAction;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public Context getContext() {
		return context;
	}

	public Date getTime() {
		return time;
	}

	public String getOriginator() {
		return originator;
	}

	public CodingDt getType() {
		return type;
	}

	public CodingDt getReportedAction() {
		return reportedAction;
	}

}
