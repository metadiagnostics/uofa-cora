/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.model;

import java.util.Date;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicaldatatypes.IdentifierDt;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;
import com.cognitivemedicine.fhir.logicalmodel.Context;

/**
 * Report Information which gives data about a {@link CommunicationRequest}.
 * @author dcalcaprina
 *
 */
@Entity("report")
public class CommunicationReportInformation extends ReportInformation {

    /**
     * Instances of Communication Report information already knows its Coding type.
     */
    // TODO consider moving this to a properties file
    private static final String COMMUNICATION_REPORT_INFORMATION_TYPE_CODE = "COMMUNICATION";
    private static final CodingDt COMMUNICATION_REPORT_INFORMATION_TYPE;

    static {
        COMMUNICATION_REPORT_INFORMATION_TYPE =
                new CodingDt(ReportInformation.REPORT_INFORMATION_CODING_SYSTEM_TYPE, COMMUNICATION_REPORT_INFORMATION_TYPE_CODE);
    }

    private long communicationId;
    private String alertType;
    
    @Embedded
    private SourceInformation sourceInformation;
    
    @Embedded
    private CommunicationRequest cr;

    public CommunicationReportInformation() {
        // needed by morphia
    }

    public CommunicationReportInformation(Context context, Date time, String originator, CodingDt reportedAction, long communicationId, CommunicationRequest cr) {
        super(context, time, originator, COMMUNICATION_REPORT_INFORMATION_TYPE, reportedAction);
        this.communicationId = communicationId;
        this.cr = cr;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public long getCommunicationId() {
        return communicationId;
    }

    public String getAlertType() {
        return alertType;
    }
    
    public CommunicationRequest getCr() {
        return cr;
    }
    
    public void setSourceInformation(SourceInformation sourceInformation) {
        this.sourceInformation = sourceInformation;
    }
    
    public SourceInformation getSourceInformation() {
        return sourceInformation;
    }

}
