/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.mongo.query;

import java.util.Date;
import java.util.List;

import com.cognitivemedicine.cdsp.model.util.mapping.ActionCode;
import com.cognitivemedicine.cdsp.report.model.CommunicationActionReportInformation;
import com.cognitivemedicine.cdsp.report.model.CommunicationReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTILifecycleReportInformation;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;

/**
 * Report service to access report data.
 * @author calcacuervo
 *
 */
public interface ReportQueryService {

    /**
     * Return all ktds with reporting information between two dates for a given version.
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @param version. The ktd version. Optional. If not present it will not be a constraint on version.
     * @return
     */
    List<String> getAllKtds(Date from, Date to, String version);

    /**
     * Get the unique instantiations count for a ktd. Unique means for different patients.
     * @param ktd to get the count.
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    long getUniqueInstancesCount(String ktd, String version, Date from, Date to);

    /**
     * Gets the sent assessments count for a ktd.
     * @param ktd
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    long getAssessmentsCount(String ktd, String version, Date from, Date to);

    /**
     * Gets the versions which have been reported for the given ktd and dates.
     * @param ktds the ktdId of interest.
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    List<String> getVersions(String ktdId, Date from, Date to);

    /**
     * Returns the actions count applied for a user and an specified action.
     * @param user the user of the action. Mandatory.
     * @param actionCode the code of the action. Mandatory.
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    long getActionsCount(String user, String actionCode, Date from, Date to);

    /**
     * Returns the instantiations given for a ktdId and version, taking into account date ranges.
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @param ktdId the ktd id. Mandatory.
     * @param version the ktd version. Mandatory.
     * @return
     */
    List<KTILifecycleReportInformation> getInstantiations(Date from, Date to, String ktdId, String version);

    /**
     * Get all the ktds versions between 2 dates. It will return a list of arrays with values like (ktdId, ktdVersion).
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    List<String[]> getAllKtdsVersions(Date from, Date to);

    /**
     * Returns the assessments reports related to a given ktdId, ktdVersion and alertType
     * @param ktdId
     * @param ktdVersion
     * @param alertType
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    List<CommunicationReportInformation> getAssessmentsForKtd(String ktdId, String ktdVersion, String alertType, Date from, Date to);

    /**
     * For a given ktdId and ktdVersion pair, it will return the different alert types sent by the ktd between given dates.
     * @param ktdId
     * @param ktdVersion
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    List<String> getAlertTypesForKtd(String ktdId, String ktdVersion, Date from, Date to);

    /**
     * Gets the number of a given action has been sent by a ktd alert type (identified by ktdId, ktdVersion and alertType) between given dates.
     * @param ktdId
     * @param ktdVersion
     * @param actionCode the code of the action. Please refer to {@link ActionCode#getCode()}
     * @param alertType
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    long getActionsCountForKtd(String ktdId, String ktdVersion, String actionCode, String alertType, Date from, Date to);

    /**
     * Given a list of communication ids, it will return which of them has been actioned in a given period of time.
     * @param communications the list of communication ids. It cannot be null.
     * @param from, date from which records are of interest. If null, no from date will be taken in range.
     * @param to, limit date of which records are of interest. If null, no to date will be taken in range.
     * @return
     */
    long getActionedCount(List<Long> communications, Date from, Date to);

    /**
     * Returns the action report for a given communication id and specific action code.
     * @param communicationId the communication request id. Please refer to {@link CommunicationRequest#getId()}
     * @param actionCode the code of the action. Please refer to {@link ActionCode#getCode()}
     * @return the {@link CommunicationActionReportInformation} related to the given communication id and action code. Null if this action was not applied to the given communication id.
     */
    CommunicationActionReportInformation getAction(long communicationId, String actionCode);

    List<CommunicationActionReportInformation> getActions(String actionCode, Date from, Date to, int offset, int amount, String sortField,
            boolean ascending);
}
