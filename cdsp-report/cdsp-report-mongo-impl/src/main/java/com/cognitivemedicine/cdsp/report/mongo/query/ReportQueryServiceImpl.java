/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.mongo.query;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;

import com.cognitivemedicine.cdsp.report.model.CommunicationActionReportInformation;
import com.cognitivemedicine.cdsp.report.model.CommunicationReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTIAssessmentReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTILifecycleReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTIReportInformation;
import com.cognitivemedicine.cdsp.report.model.ReportInformation;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

/**
 * Report Service which uses MongoDB as data store.
 * @author dcalcaprina
 *
 */
public class ReportQueryServiceImpl extends BasicDAO<ReportInformation, String> implements ReportQueryService {

    public static final String DB_NAME = "report";
    public static final int DEFAULT_PORT = 27017;

    protected ReportQueryServiceImpl(String host, int port, String reportName) {
        super(new Morphia().createDatastore(new MongoClient(host, port), reportName));
    }
    
    protected ReportQueryServiceImpl(MongoClient mongo, String dbName) {
        super(new Morphia().createDatastore(mongo, dbName));
    }

    public ReportQueryServiceImpl(String host, int port) {
        this(host, port, DB_NAME);
    }

    public ReportQueryServiceImpl(String endpoint) {
        super(new Morphia().createDatastore(new MongoClient(endpoint.split(":")[0], Integer.parseInt(endpoint.split(":")[1])), DB_NAME));
    }

    @Override
    public List<String[]> getAllKtdsVersions(Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);

        BasicDBObject match = new BasicDBObject();
        BasicDBObject condition = new BasicDBObject();
        condition.append("className", KTILifecycleReportInformation.class.getName());
        appendTimeConstraints(from, to, condition);
        match.append("$match", condition);

        BasicDBObject group = new BasicDBObject();
        BasicDBObject groupId = new BasicDBObject();
        group.append("$group", groupId);
        BasicDBObject sub = new BasicDBObject();
        groupId.append("_id", sub);
        sub.append("ktdId", "$ktdId").append("ktdVersion", "$ktdVersion");

        BasicDBObject project = new BasicDBObject();
        BasicDBObject projectSub = new BasicDBObject();
        project.append("$project", projectSub);
        projectSub.append("_id", 0).append("ktdId", "$_id.ktdId").append("ktdVersion", "$_id.ktdVersion");

        // TODO append date constraints!
        AggregationOutput out = getDatastore().getCollection(KTIReportInformation.class).aggregate(match, group, project);

        return StreamSupport.stream(out.results().spliterator(), false)
                .map(o -> new String[] {o.get("ktdId").toString(), o.get("ktdVersion").toString()}).collect(Collectors.toList());
    }

    @Override
    public List<String> getAllKtds(Date from, Date to, String version) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.append("className", KTILifecycleReportInformation.class.getName());
        if (!StringUtils.isBlank(version)) {
            dbObject.append("ktdVersion", version);
        }
        appendTimeConstraints(from, to, dbObject, "time");
        return getDatastore().getCollection(KTIReportInformation.class).distinct("ktdId", dbObject);
    }

    @Override
    public List<KTILifecycleReportInformation> getInstantiations(Date from, Date to, String ktdId, String version) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<KTILifecycleReportInformation> query = getDatastore().createQuery(KTILifecycleReportInformation.class).disableValidation()
                .filter("className", KTILifecycleReportInformation.class.getName()).filter("reportedAction.code.value", "LIFECYCLE")
                .filter("ktdId", ktdId).filter("ktdVersion", version);
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        return query.asList();
    }

    @Override
    public List<String> getVersions(String ktdId, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        BasicDBObject dbObject = new BasicDBObject();
        if (!StringUtils.isBlank(ktdId)) {
            dbObject.append("ktdId", ktdId);
        }
        dbObject.append("className", KTILifecycleReportInformation.class.getName());
        appendTimeConstraints(from, to, dbObject, "time");
        return getDatastore().getCollection(KTILifecycleReportInformation.class).distinct("ktdVersion", dbObject);
    }
    
    @Override
    public List<CommunicationReportInformation> getAssessmentsForKtd(String ktdId, String ktdVersion, String alertType, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<CommunicationReportInformation> query = getDatastore().createQuery(CommunicationReportInformation.class)
                .disableValidation().filter("className", CommunicationReportInformation.class.getName())
                .filter("originator", ktdId + ":" + ktdVersion).filter("alertType", alertType);
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        return query.asList();
    }

    @Override
    public long getActionsCount(String user, String actionCode, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<CommunicationActionReportInformation> query = getDatastore().createQuery(CommunicationActionReportInformation.class)
                .disableValidation().filter("className", CommunicationActionReportInformation.class.getName())
                .filter("user", user).filter("action.code.value", actionCode);
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        return query.countAll();
    }

    @Override
    public long getActionsCountForKtd(String ktdId, String ktdVersion, String alertType, String actionCode, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<CommunicationActionReportInformation> query = getDatastore().createQuery(CommunicationActionReportInformation.class)
                .disableValidation().filter("className", CommunicationActionReportInformation.class.getName())
                .filter("originator", ktdId + ":" + ktdVersion).filter("action.code.value", actionCode).filter("alertType", alertType);
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        return query.countAll();
    }
    
    @Override
    public CommunicationActionReportInformation getAction(long communicationId, String actionCode) {
        Query<CommunicationActionReportInformation> query = getDatastore().createQuery(CommunicationActionReportInformation.class)
                .disableValidation().filter("className", CommunicationActionReportInformation.class.getName())
                .filter("communicationId",communicationId).filter("action.code.value", actionCode);
        return query.get();
    }
    
    @Override
    public List<String> getAlertTypesForKtd(String ktdId, String ktdVersion, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.append("className", CommunicationReportInformation.class.getName());
        dbObject.append("originator", ktdId + ":" + ktdVersion);
        appendTimeConstraints(from, to, dbObject, "time");
        return getDatastore().getCollection(CommunicationReportInformation.class).distinct("alertType", dbObject);
    }
    
    @Override
    public long getActionedCount(List<Long> communications, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.append("className", CommunicationActionReportInformation.class.getName());
        BasicDBObject in = new BasicDBObject();
        in.append("$in", communications.toArray());
        dbObject.append("communicationId", in);
        appendTimeConstraints(from, to, dbObject, "time");
        return getDatastore().getCollection(CommunicationActionReportInformation.class).distinct("communicationId", dbObject).size();
    }
    
    @Override
    public long getUniqueInstancesCount(String ktd, String version, Date from, Date to) {
        return getUniqueInstancesCount(ktd, version, from, to, true);
    }

    private long getUniqueInstancesCount(String ktd, String version, Date from, Date to, boolean retrieveAll) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        BasicDBObject dbObject = new BasicDBObject();
        dbObject.append("className", KTILifecycleReportInformation.class.getName());
        dbObject.append("reportedAction.code.value", "LIFECYCLE");
        dbObject.append("ktdId", ktd);
        if (version != null) {
            dbObject.append("ktdVersion", version);
        }
        appendTimeConstraints(from, to, dbObject, "startTime");
        DBCollection collection = getDatastore().getCollection(ReportInformation.class);
        if (!retrieveAll) {
            dbObject.append("endTime", new BasicDBObject("$exists", false));
        }
        return collection.distinct("context.subjectId.value", dbObject).size();
    }

    @Override
    public long getAssessmentsCount(String ktd, String version, Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<ReportInformation> query = getDatastore().createQuery(ReportInformation.class).disableValidation()
                .filter("className", KTIAssessmentReportInformation.class.getName()).filter("reportedAction.code.value", "ASSESSMENT")
                .filter("ktdId", ktd);
        if (version != null) {
            query.filter("ktdVersion", version);
        }
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        return query.countAll();
    }

    @Override
    public List<CommunicationActionReportInformation> getActions(String actionCode, Date from, Date to, int offset, int amount, String sortField, boolean ascending) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<CommunicationActionReportInformation> query = getDatastore().createQuery(CommunicationActionReportInformation.class)
                .disableValidation().filter("className", CommunicationActionReportInformation.class.getName())
                .filter("action.code.value", actionCode).offset(offset);
        if (amount > 0) {
            query.limit(amount);
        }
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        if (sortField != null) {
            query.order((ascending ? "" : "-") + sortField + ("time".equals(sortField) ? "" : ",-time"));
        }
        return query.asList();
    }

    /**
     * Sanitizes from dates.
     * 
     * @param from
     *            this will be set to 00:00:00 so that all records from this
     *            date are taken.
     */
    private Date sanitizeFromDate(Date from) {
        if (from != null) {
            from = DateUtils.setHours(from, 0);
            from = DateUtils.setMinutes(from, 0);
            from = DateUtils.setSeconds(from, 0);
        }
        return from;
    }

    /**
     * Sanitizes to dates.
     * @param to
     *            this will be set to 23:59:59 so that all records from this
     *            date are taken.
     */
    private Date sanitizeToDate(Date to) {
        if (to != null) {
            to = DateUtils.setHours(to, 23);
            to = DateUtils.setMinutes(to, 59);
            to = DateUtils.setSeconds(to, 59);
        }
        return to;
    }

    /**
     * Creates the query constraints for the time of the report.
     * @param from
     * @param to
     * @param dbObject
     */
    private void appendTimeConstraints(Date from, Date to, BasicDBObject dbObject, String timeField) {
        if (from != null && to != null) {
            dbObject.append(timeField, new BasicDBObject("$gte", from).append("$lte", to));
        } else if (from != null) {
            dbObject.append(timeField, new BasicDBObject("$gte", from));
        } else if (to != null) {
            dbObject.append(timeField, new BasicDBObject("$lte", to));
        }
    }

    private void appendTimeConstraints(Date from, Date to, BasicDBObject dbObjectd) {
        appendTimeConstraints(from, to, dbObjectd, "time");
    }

}
