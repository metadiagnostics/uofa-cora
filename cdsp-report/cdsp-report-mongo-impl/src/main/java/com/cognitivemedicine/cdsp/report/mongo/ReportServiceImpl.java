/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.mongo;

import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.model.ReportInformation;
import com.mongodb.MongoClient;

/**
 * Report Service which uses MongoDB as data store.
 * @author dcalcaprina
 *
 */
public class ReportServiceImpl extends BasicDAO<ReportInformation, String> implements ReportService {

    public static final String REPORT_COLLECTION_NAME = "report";
    public static final int DEFAULT_PORT = 27017;

    private static MongoClient getFromEnpoint(String endpoint) {
        String host = endpoint;
        int port = DEFAULT_PORT;
        try {
            if (endpoint.contains(":")) {
                String[] split = endpoint.split(":");
                host = split[0];
                port = Integer.parseInt(split[1]);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception parsing the provided endpoint '" + endpoint + "'", e);
        }

        return new MongoClient(host, port);
    }

    public ReportServiceImpl(String host, int port) {
        this(host, port, REPORT_COLLECTION_NAME);
    }
    public ReportServiceImpl(String endpoint) {
        this(getFromEnpoint(endpoint), REPORT_COLLECTION_NAME);
    }

    protected ReportServiceImpl(String host, int port, String reportName) {
        this(new MongoClient(host, port), reportName);
    }

    public ReportServiceImpl(MongoClient client, String dbName) {
        super(new Morphia().createDatastore(client, dbName));
    }

    public ReportServiceImpl(MongoClient client) {
        super(new Morphia().createDatastore(client, REPORT_COLLECTION_NAME));
    }

    @Override
    public void report(ReportInformation report) {
        this.save(report);
    }

    @Override
    public void updateReportField(String className, String idField, String id, String fieldName, Object value) {
        Query<ReportInformation> query =
                getDatastore().createQuery(ReportInformation.class).disableValidation().filter("className", className);
        if (idField != null) {
            query.filter(idField, id);
        }
        UpdateOperations<ReportInformation> ops =
                getDatastore().createUpdateOperations(ReportInformation.class).disableValidation().set(fieldName, value);
        this.update(query, ops);
    }

}
