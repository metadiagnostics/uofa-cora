/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.mongo.query;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.cognitivemedicine.cdsp.report.model.UserReportInformation;
import com.mongodb.MongoClient;

public class UserReportQueryServiceImpl implements UserReportQueryService {
    public static final String DB_NAME = "report";
    public static final int DEFAULT_PORT = 27017;

    private final Datastore dataStore;

    public UserReportQueryServiceImpl(String host, int port) {
        dataStore = new Morphia().createDatastore(new MongoClient(host, port), DB_NAME);
    }

    public UserReportQueryServiceImpl(String endpoint) {
        String host = endpoint;
        int port = DEFAULT_PORT;
        try {
            if (endpoint.contains(":")) {
                String[] split = endpoint.split(":");
                host = split[0];
                port = Integer.parseInt(split[1]);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception parsing the provided endpoint '" + endpoint + "'", e);
        }

        dataStore = new Morphia().createDatastore(new MongoClient(host, port), DB_NAME);
    }

    /* (non-Javadoc)
     * @see com.cognitivemedicine.cdsp.cwf.api.report.user.UserReportQueryService#getLoggedInUsers(java.util.Date, java.util.Date)
     */
    @Override
    public List<UserReportInformation> getLoggedInUsers(Date from, Date to) {
        from = sanitizeFromDate(from);
        to = sanitizeToDate(to);
        Query<UserReportInformation> query = dataStore.createQuery(UserReportInformation.class); 
        if (from != null) {
            query.filter("time >=", from);
        }
        if (to != null) {
            query.filter("time <=", to);
        }
        return query.asList();
    }

    public UserReportQueryServiceImpl(Datastore dataStore) {
        this.dataStore = dataStore;
    }

    public String getDataBaseEndpoint() {
        if (this.dataStore == null) {
            return null;
        }

        return this.dataStore.getMongo().getConnectPoint();
    }

    /**
     * Sanitizes from dates.
     * 
     * @param from
     *            this will be set to 00:00:00 so that all records from this
     *            date are taken.
     */
    private Date sanitizeFromDate(Date from) {
        if (from != null) {
            from = DateUtils.setHours(from, 0);
            from = DateUtils.setMinutes(from, 0);
            from = DateUtils.setSeconds(from, 0);
        }
        return from;
    }

    /**
     * Sanitizes to dates.
     * @param to
     *            this will be set to 23:59:59 so that all records from this
     *            date are taken.
     */
    private Date sanitizeToDate(Date to) {
        if (to != null) {
            to = DateUtils.setHours(to, 23);
            to = DateUtils.setMinutes(to, 59);
            to = DateUtils.setSeconds(to, 59);
        }
        return to;
    }
}
