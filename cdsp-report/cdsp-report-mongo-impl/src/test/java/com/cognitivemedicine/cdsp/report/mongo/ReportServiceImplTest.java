/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.mongo;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.model.CommunicationReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTILifecycleReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTIReportInformation;
import com.cognitivemedicine.cdsp.report.model.ReportInformation;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;
import com.github.fakemongo.Fongo;
import com.mongodb.MongoClient;


/**
 * IT test for the report service.
 * @author dcalcaprina
 *
 */
public class ReportServiceImplTest {

    private static Fongo fongo;
    
	private MongoClient mongo;
	private Datastore datastore;

    @BeforeClass
	public static void setUpClass() throws Exception {
		fongo = new Fongo("Fake Mongo DB");
    }
    
	@Before
	public void setUp() throws Exception {
		mongo = fongo.getMongo();
        
		final Morphia morphia = new Morphia();

		// create the Datastore connecting to the default port on the local host
		datastore = morphia.createDatastore(mongo, ReportServiceImpl.REPORT_COLLECTION_NAME);
	}

	@Test
	public void saveReport() {
		ReportService service = new ReportServiceImpl(mongo);
		Context ctx = new Context();
		ctx.setSubject("test");
		CodingDt action = new CodingDt("ReportedAction", "NEW");

		KTIReportInformation ri = new KTILifecycleReportInformation(ctx, new Date(), "Test", "Test KTD", "1.0", "Test KTI", new Date());
		service.report(ri);

		CommunicationReportInformation ri2 = new CommunicationReportInformation(ctx, new Date(), "Test", action, 1, null);
		service.report(ri2);

		KTIReportInformation ri3 = new KTILifecycleReportInformation(ctx, new Date(), "Test", "Test KTD", "1.0", "Test KTI", new Date());
		service.report(ri3);

		// now, check that they were saved
		Assert.assertEquals(1,
				datastore.createQuery(ReportInformation.class).disableValidation()
						.filter("className", CommunicationReportInformation.class.getName()).countAll());
		Assert.assertEquals(2,
				datastore.createQuery(ReportInformation.class).disableValidation().filter("className", KTILifecycleReportInformation.class.getName())
						.countAll());
	}
}
