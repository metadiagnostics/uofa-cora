/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.report.mongo.query;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.cognitivemedicine.cdsp.model.util.mapping.ActionCode;
import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.model.CommunicationActionReportInformation;
import com.cognitivemedicine.cdsp.report.model.CommunicationReportInformation;
import com.cognitivemedicine.cdsp.report.model.KTILifecycleReportInformation;
import com.cognitivemedicine.cdsp.report.mongo.ReportServiceImpl;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import com.cognitivemedicine.fhir.logicalmodel.Context;
import com.github.fakemongo.Fongo;
import com.mongodb.MongoClient;


/**
 * IT test for the report  queryservice.
 * @author dcalcaprina
 *
 */
public class ReportQueryServiceImplTest {

    private static Fongo fongo;

    private MongoClient mongo;
    private ReportQueryService queryService;
    private ReportService reportService;

    @Before
    public void setUp() throws Exception {
        fongo = new Fongo("Fake Mongo DB");
        mongo = fongo.getMongo();
        queryService = new ReportQueryServiceImpl(mongo, "test");
        reportService = new ReportServiceImpl(mongo, "test");
    }

    @Test
    public void getAllKtdsVersions_oneRecordAndNoTimeConstraints() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record);
        List<String[]> ktds = queryService.getAllKtdsVersions(null, null);
        Assert.assertNotNull(ktds);
        Assert.assertEquals(1, ktds.size());
        Assert.assertArrayEquals(new String[] {record.getKtdId(), record.getKtdVersion()}, ktds.get(0));

    }

    @Test
    public void getAllKtdsVersions_oneRecordAndTimeConstraints() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record);
        List<String[]> ktds = queryService.getAllKtdsVersions(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertNotNull(ktds);
        Assert.assertEquals(1, ktds.size());
        Assert.assertArrayEquals(new String[] {record.getKtdId(), record.getKtdVersion()}, ktds.get(0));
    }

    @Test
    public void getAllKtdsVersions_twoRecordsAndTimeConstraints_oneInsideAndOneOutside() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 = new KTILifecycleReportInformation(new Context(), DateUtils.addDays(recordDate, 2),
                "originator", "ktd1", "1.1", "org.ktd", DateUtils.addDays(recordDate, 2));
        reportService.report(record2);
        List<String[]> ktds = queryService.getAllKtdsVersions(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertNotNull(ktds);
        Assert.assertEquals(1, ktds.size());
        Assert.assertArrayEquals(new String[] {record1.getKtdId(), record1.getKtdVersion()}, ktds.get(0));
    }

    @Test
    public void getAllKtds_twoRecordsAndTimeConstraintsAndNoVersion_oneInsideAndOneOutside() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 = new KTILifecycleReportInformation(new Context(), DateUtils.addDays(recordDate, 2),
                "originator", "ktd1", "1.1", "org.ktd", DateUtils.addDays(recordDate, 2));
        reportService.report(record2);
        List<String> ktds = queryService.getAllKtds(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1), null);
        Assert.assertNotNull(ktds);
        Assert.assertEquals(1, ktds.size());
        Assert.assertEquals(record1.getKtdId(), ktds.get(0));
    }

    @Test
    public void getAllKtds_twoRecordsAndTimeConstraintsAndNoVersion_bothInside() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd2", "1.1", "org.ktd", recordDate);
        reportService.report(record2);
        List<String> ktds = queryService.getAllKtds(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1), null);
        Assert.assertNotNull(ktds);
        Assert.assertEquals(2, ktds.size());
    }
    
    @Test
    public void getInstantiations_twoInstantiationsOfAKTD() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record2);
        List<KTILifecycleReportInformation> ktds = queryService.getInstantiations(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1), "ktd1", "1.0");
        Assert.assertNotNull(ktds);
        Assert.assertEquals(2, ktds.size());
    }
    
    @Test
    public void getInstantiations_twoInstantiationsOfAKTDAndAnotherFromAnotherKTD() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record2);
        KTILifecycleReportInformation record3 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd2", "1.0", "org.ktd", recordDate);
        reportService.report(record3);
        List<KTILifecycleReportInformation> ktds = queryService.getInstantiations(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1), "ktd1", "1.0");
        Assert.assertNotNull(ktds);
        Assert.assertEquals(2, ktds.size());
    }

    @Test
    public void getInstantiations_twoInstantiationsOfAKTDAndAnotherFromSameKTDButDifferentVersion() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record2);
        KTILifecycleReportInformation record3 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.1", "org.ktd", recordDate);
        reportService.report(record3);
        List<KTILifecycleReportInformation> ktds = queryService.getInstantiations(DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1), "ktd1", "1.0");
        Assert.assertNotNull(ktds);
        Assert.assertEquals(2, ktds.size());
    }
    
    @Test
    public void getVersions_twoInstantiationsOfAKTDAndAnotherFromDiferentKTD() {
        Date recordDate = new Date();
        KTILifecycleReportInformation record1 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.0", "org.ktd", recordDate);
        reportService.report(record1);
        KTILifecycleReportInformation record2 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd1", "1.1", "org.ktd", recordDate);
        reportService.report(record2);
        KTILifecycleReportInformation record3 =
                new KTILifecycleReportInformation(new Context(), recordDate, "originator", "ktd2", "1.1", "org.ktd", recordDate);
        reportService.report(record3);
        List<String> versions = queryService.getVersions("ktd1", DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertNotNull(versions);
        Assert.assertEquals(2, versions.size());
        Assert.assertArrayEquals(new String[] {"1.0",  "1.1"}, versions.toArray());
    }
    
    @Test
    public void getAssessmentsForKtd_twoAssesmentsForGivenAlertType() {
        Date recordDate = new Date();
        CommunicationReportInformation record1 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 1, null);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationReportInformation record2 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 2, null);
        record2.setAlertType("type1");
        reportService.report(record2);
        
        List<CommunicationReportInformation> assessments = queryService.getAssessmentsForKtd("ktd1", "1.1", "type1", DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertNotNull(assessments);
        Assert.assertEquals(2, assessments.size());
    }
    
    @Test
    public void getAssessmentsForKtd_twoAssesmentsForGivenAlertTypeAndAnotherForDifferentType() {
        Date recordDate = new Date();
        CommunicationReportInformation record1 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 1, null);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationReportInformation record2 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 2, null);
        record2.setAlertType("type1");
        reportService.report(record2);
        CommunicationReportInformation record3 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 3, null);
        record3.setAlertType("type2");
        reportService.report(record3);
                
        List<CommunicationReportInformation> assessments = queryService.getAssessmentsForKtd("ktd1", "1.1", "type1", DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertNotNull(assessments);
        Assert.assertEquals(2, assessments.size());
    }
    
    @Test
    public void getAssessmentsForKtd_AssesmentsForADifferentTypeThanRequested() {
        Date recordDate = new Date();
        CommunicationReportInformation record1 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 1, null);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationReportInformation record2 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 2, null);
        record2.setAlertType("type1");
        reportService.report(record2);
        CommunicationReportInformation record3 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 3, null);
        record3.setAlertType("type2");
        reportService.report(record3);

        List<CommunicationReportInformation> assessments = queryService.getAssessmentsForKtd("ktd1", "1.1", "type3", DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertNotNull(assessments);
        Assert.assertEquals(0, assessments.size());
    }
    
    @Test
    public void getActionsCount_ManyActionsAppliedButOneForTheGivenAction() {
        Date recordDate = new Date();
        Context ctx = new Context();
        ctx.setSubject("testKtd");
        String user = "testUser";
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, user);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.ACK.getCode()), null, user);
        record1.setAlertType("type1");
        reportService.report(record2);
                
        long actions = queryService.getActionsCount(user, ActionCode.READ.getCode(), DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(1, actions);
    }
    
    @Test
    public void getActionsCount_ManyActionsApplied() {
        Date recordDate = new Date();
        String user = "testUser";
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(new Context(), recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, user);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(new Context(), recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.ACK.getCode()), null, user);
        record1.setAlertType("type1");
        reportService.report(record2);
        CommunicationActionReportInformation record3 = new CommunicationActionReportInformation(new Context(), recordDate, "ktd1:1.1", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, user);
        record3.setAlertType("type1");
        reportService.report(record3);
                
        long actions = queryService.getActionsCount(user, ActionCode.READ.getCode(), DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(2, actions);
    }
    
    @Test
    public void getActionsCountForKtd_ManyActionsApplied() {
        Date recordDate = new Date();
        Context ctx = new Context();
        ctx.setSubject("testUser");
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.ACK.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record2);
        CommunicationActionReportInformation record3 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record3.setAlertType("type1");
        reportService.report(record3);
                
        long actions = queryService.getActionsCountForKtd("ktd1", "1.1", "type1", ActionCode.READ.getCode(), DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(2, actions);
    }
    
    @Test
    public void getActionsCountForKtd_ManyActionsAppliedForDifferentKTDs() {
        Date recordDate = new Date();
        Context ctx = new Context();
        ctx.setSubject("testUser");
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.ACK.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record2);
        CommunicationActionReportInformation record3 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record3.setAlertType("type1");
        reportService.report(record3);
        CommunicationActionReportInformation record4 = new CommunicationActionReportInformation(ctx, recordDate, "ktd2:1.1", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record4.setAlertType("type1");
        reportService.report(record4);
        long actions = queryService.getActionsCountForKtd("ktd1", "1.1", "type1", ActionCode.READ.getCode(), DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(2, actions);
    }

    @Test
    public void getActionsCountForKtd_ManyActionsAppliedForSameKTDsButDifferentVersions() {
        Date recordDate = new Date();
        Context ctx = new Context();
        ctx.setSubject("testUser");
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.2", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record2);
        CommunicationActionReportInformation record3 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record3.setAlertType("type1");
        reportService.report(record3);
        CommunicationActionReportInformation record4 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record4.setAlertType("type1");
        reportService.report(record4);
        long actions = queryService.getActionsCountForKtd("ktd1", "1.1", "type1", ActionCode.READ.getCode(), DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(3, actions);
    }
    
    @Test
    public void getAction_exists() {
        Date recordDate = new Date();
        Context ctx = new Context();
        ctx.setSubject("testUser");
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.2", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record2);
        CommunicationActionReportInformation record3 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 3, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record3.setAlertType("type1");
        reportService.report(record3);
        CommunicationActionReportInformation record4 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 4, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record4.setAlertType("type1");
        reportService.report(record4);
        CommunicationActionReportInformation action = queryService.getAction(1, ActionCode.READ.getCode());
        Assert.assertEquals(record1.getCommunicationId(), action.getCommunicationId());
    }
    
    @Test
    public void getAction_notExists() {
        Date recordDate = new Date();
        Context ctx = new Context();
        ctx.setSubject("testUser");
        CommunicationActionReportInformation record1 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 1, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationActionReportInformation record2 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.2", 2, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record1.setAlertType("type1");
        reportService.report(record2);
        CommunicationActionReportInformation record3 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 3, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record3.setAlertType("type1");
        reportService.report(record3);
        CommunicationActionReportInformation record4 = new CommunicationActionReportInformation(ctx, recordDate, "ktd1:1.1", 4, null, new CodingDt("Actions", ActionCode.READ.getCode()), null, "");
        record4.setAlertType("type1");
        reportService.report(record4);
        CommunicationActionReportInformation action = queryService.getAction(5, ActionCode.READ.getCode());
        Assert.assertNull(action);
        action = queryService.getAction(1, ActionCode.ACK.getCode());
        Assert.assertNull(action);
    }
    
    @Test
    public void getAlertTypesForKtd_threeDifferentVersionInFourReports() {
        Date recordDate = new Date();
        CommunicationReportInformation record1 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 1, null);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationReportInformation record2 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 2, null);
        record2.setAlertType("type3");
        reportService.report(record2);
        CommunicationReportInformation record3 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 3, null);
        record3.setAlertType("type2");
        CommunicationReportInformation record4 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 3, null);
        record4.setAlertType("type2");
        reportService.report(record3);
        List<String> alertType = queryService.getAlertTypesForKtd("ktd1", "1.1", DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(3, alertType.size());
        Assert.assertArrayEquals(new String[] {"type1", "type2", "type3"}, alertType.stream().sorted().toArray());
    }
    
    @Test
    public void getAlertTypesForKtd_NoReportsForGivenKtd() {
        Date recordDate = new Date();
        CommunicationReportInformation record1 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 1, null);
        record1.setAlertType("type1");
        reportService.report(record1);
        CommunicationReportInformation record2 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 2, null);
        record2.setAlertType("type3");
        reportService.report(record2);
        CommunicationReportInformation record3 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 3, null);
        record3.setAlertType("type2");
        CommunicationReportInformation record4 = new CommunicationReportInformation(new Context(), recordDate, "ktd1:1.1", null, 3, null);
        record4.setAlertType("type2");
        reportService.report(record3);
        List<String> alertType = queryService.getAlertTypesForKtd("ktd2", "1.1", DateUtils.addDays(recordDate, -1), DateUtils.addDays(recordDate, +1));
        Assert.assertEquals(0, alertType.size());
    }
}
