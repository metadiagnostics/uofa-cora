/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization;

import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.mock.MockFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.mock.MockListFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.util.TestUtils;
import java.util.List;
import java.util.Map;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class DroolsDecisionNodeAgendaEventListenerTest {

    private final static Logger LOG = LoggerFactory.getLogger(DroolsDecisionNodeAgendaEventListenerTest.class);
    
    @Test
    public void doTest(){
        KieBase kbase = TestUtils.buildKieBase("decision-node-rules/rules1/rules.drl");
        
        KieSession ksession = kbase.newKieSession();
        DroolsDecisionNodeAgendaEventListener listener = new DroolsDecisionNodeAgendaEventListener();
        ksession.addEventListener(listener);
        
        
        KnowledgeTemplateInitContext kcontext = new KnowledgeTemplateInitContext();
        kcontext.getTriggerFacts().put("abc", "abc");
        kcontext.getTriggerFacts().put("def", "def");
        ksession.insert(kcontext);
        
        MockListFact<Object> mockListFact = new MockListFact<>();
        ksession.insert(mockListFact);
        
        MockFact mockFact = new MockFact("123");
        ksession.insert(mockFact);
        
        ksession.getAgenda().getAgendaGroup("PM-INIT").setFocus();
        ksession.fireAllRules();
        
        
        Map<String, List<DecisionNodeExecution>> execs = listener.getNodeExecutions();
        
        List<DecisionNodeExecution> dnes = execs.get("PM-INIT");
        assertThat(dnes.size(), is(1));
        
        
        dnes = execs.get("Init Rule");
        assertThat(dnes.size(), is(2));
        assertThat(dnes.get(0).getContext().size(), is(2));
        assertThat(dnes.get(0).getContext().get("$ctx"), is(kcontext));
        assertThat(dnes.get(0).getContext().get("$o"), anyOf(is("abc"), is("def")));
        assertThat(dnes.get(1).getContext().size(), is(2));
        assertThat(dnes.get(1).getContext().get("$ctx"), is(kcontext));
        assertThat(dnes.get(1).getContext().get("$o"), anyOf(is("abc"), is("def")));
        assertThat(dnes.get(1).getContext().get("$o"), not(dnes.get(0).getContext().get("$o")));
        
        
        dnes = execs.get("Populate List");
        assertThat(dnes.size(), is(1));
        assertThat(dnes.get(0).getContext().size(), is(1));
        assertThat(dnes.get(0).getContext().get("$l"), is(mockListFact));
        
        dnes = execs.get("Some Rule");
        assertThat(dnes.size(), is(1));
        assertThat(dnes.get(0).getContext().size(), is(1));
        assertThat(dnes.get(0).getContext().get("$l"), is(mockFact));
        
        dnes = execs.get("Some Other Rule");
        assertThat(dnes.size(), is(2));
        assertThat(dnes.get(0).getContext().size(), is(2));
        assertThat(dnes.get(0).getContext().get("$l"), is(mockListFact));
        assertThat(dnes.get(0).getContext().get("$s"), anyOf(is("abc"), is("def")));
        assertThat(dnes.get(1).getContext().size(), is(2));
        assertThat(dnes.get(1).getContext().get("$l"), is(mockListFact));
        assertThat(dnes.get(1).getContext().get("$s"), anyOf(is("abc"), is("def")));
        assertThat(dnes.get(1).getContext().get("$s"), not(dnes.get(0).getContext().get("$o")));
        
        
    }

}
