/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime;

import com.cognitivemedicine.cdsp.model.events.CaseStateEvent;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.impl.mongo.MongoProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateDefinitionLifeCycleFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class MongoProcessManagerStoreTest {
    
    private MongoProcessManagerStore store;
    
    @Before
    public void doBefore(){
        store = new MongoProcessManagerStore("localhost", 27017);
        store.cleanup();
    }
    
    @Ignore("This test is used for dev purposes")
    @Test
    public void saveTest(){
        
        KnowledgeTemplateDefinitionLifeCycleFact f1 = new KnowledgeTemplateDefinitionLifeCycleFact();
        f1.setFact("Hello World");
        
        store.saveOrUpdate(f1);
        
        KnowledgeTemplateInstanceReference ref1 = new KnowledgeTemplateInstanceReference();
        ref1.setKtdId("KTD-1");
        ref1.setKtiId("KTI-1");
        
        store.saveOrUpdate(ref1);
        
    }
    
    @Ignore("This test is used for dev purposes")
    @Test
    public void saveKnowledgeTemplateInitContextTest(){
    
        CaseStateEvent event = new CaseStateEvent("Case-1", CaseStateEvent.CaseStateEnum.CASE_OPEN, "1", "2");
        Map<String, Object> map = new HashMap<>();
        map.put("$evt", event);
        map.put("$str", "Some String");
        
        KnowledgeTemplateInitContext ctx = new KnowledgeTemplateInitContext(map);
        
        KnowledgeTemplateInstanceFact initContextFact = new KnowledgeTemplateInstanceFact();
        initContextFact.setKtiId("KTI-1");
        initContextFact.setFact(ctx);
        
        store.saveOrUpdate(initContextFact);
        
        List<KnowledgeTemplateInstanceFact> facts = store.listKnowledgeTemplateInstanceFacts("KTI-1", KnowledgeTemplateInitContext.class.getName());
        
        assertThat(facts.size(), is(1));
    }
    
    @Ignore("This test is used for dev purposes")
    @Test
    public void listKTDIdsWithKnowledgeTemplateDefinitionLifeCycleFactsTest(){
    
        String ktd1Id = "KTD-1";
        String ktd2Id = "KTD-2";
        
        KnowledgeTemplateDefinitionLifeCycleFact f1 = new KnowledgeTemplateDefinitionLifeCycleFact();
        f1.setKtdId(ktd1Id);
        f1.setFact("Something");
        store.saveOrUpdate(f1);
        
        KnowledgeTemplateDefinitionLifeCycleFact f2 = new KnowledgeTemplateDefinitionLifeCycleFact();
        f2.setKtdId(ktd2Id);
        f2.setFact("Something");
        store.saveOrUpdate(f2);
        
        KnowledgeTemplateDefinitionLifeCycleFact f3 = new KnowledgeTemplateDefinitionLifeCycleFact();
        f3.setKtdId(ktd1Id);
        f3.setFact("Something");
        store.saveOrUpdate(f3);
        
        KnowledgeTemplateDefinitionLifeCycleFact f4 = new KnowledgeTemplateDefinitionLifeCycleFact();
        f4.setKtdId(ktd2Id);
        f4.setFact("Something");
        store.saveOrUpdate(f4);
        
        
        Set<String> ktdIds = store.listKTDIdsWithKnowledgeTemplateDefinitionLifeCycleFacts();
        
        assertThat(ktdIds.size(), is(2));
        assertThat(ktdIds.contains(ktd1Id), is(true));
        assertThat(ktdIds.contains(ktd2Id), is(true));
    }
}
