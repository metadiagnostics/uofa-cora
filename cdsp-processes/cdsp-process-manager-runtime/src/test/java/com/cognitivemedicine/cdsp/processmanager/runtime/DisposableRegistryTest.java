/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class DisposableRegistryTest {
	private static final Logger LOG = LoggerFactory.getLogger(DisposableRegistryTest.class);
    @Test
    public void doDisposeAllTest(){
        
        AtomicInteger disposedResources = new AtomicInteger(0);
        
        List<Disposable> disposablesK = Arrays.asList(
            (Disposable) () -> {LOG.info("Disposing DK1"); disposedResources.addAndGet(1);},
            (Disposable) () -> {LOG.info("Disposing DK2"); disposedResources.addAndGet(1);},
            (Disposable) () -> {LOG.info("Disposing DK3"); disposedResources.addAndGet(1);}
        );
        
        List<Disposable> disposablesL = Arrays.asList(
            (Disposable) () -> {LOG.info("Disposing DL1"); disposedResources.addAndGet(1);},
            (Disposable) () -> {LOG.info("Disposing DL2"); disposedResources.addAndGet(1);}
        );
        
        DisposableRegistry registry = new DisposableRegistry();
        
        disposablesK.stream().parallel().forEach(d -> registry.put("K", d));
        disposablesL.stream().parallel().forEach(d -> registry.put("L", d));
        
        assertThat(registry.getInternalRegistry().size(), is(2));
        assertThat(registry.getInternalRegistry().get("K").size(), is(3));
        assertThat(registry.getInternalRegistry().get("L").size(), is(2));
        
        registry.disposeAll();
        
        assertThat(registry.getInternalRegistry().size(), is(0));
        assertThat(registry.getInternalRegistry().get("K"), is(nullValue()));
        assertThat(registry.getInternalRegistry().get("L"), is(nullValue()));
        assertThat(disposedResources.get(), is(5));
    }
    
    
    @Test
    public void doConcurrentTest(){
        
        AtomicInteger disposedResources = new AtomicInteger(0);
        
        List<Disposable> disposablesK = Arrays.asList(
            (Disposable) () -> {LOG.info("Disposing DK1"); disposedResources.addAndGet(1);},
            (Disposable) () -> {LOG.info("Disposing DK2"); disposedResources.addAndGet(1);},
            (Disposable) () -> {LOG.info("Disposing DK3"); disposedResources.addAndGet(1);}
        );
        
        List<Disposable> disposablesL = Arrays.asList(
            (Disposable) () -> {LOG.info("Disposing DL1"); disposedResources.addAndGet(1);},
            (Disposable) () -> {LOG.info("Disposing DL2"); disposedResources.addAndGet(1);}
        );
        
        DisposableRegistry registry = new DisposableRegistry();
        
        disposablesK.stream().parallel().forEach(d -> registry.put("K", d));
        disposablesL.stream().parallel().forEach(d -> registry.put("L", d));
        
        assertThat(registry.getInternalRegistry().size(), is(2));
        assertThat(registry.getInternalRegistry().get("K").size(), is(3));
        assertThat(registry.getInternalRegistry().get("L").size(), is(2));
        
        registry.disposeAll("K");
        assertThat(registry.getInternalRegistry().size(), is(1));
        assertThat(registry.getInternalRegistry().get("K"), is(nullValue()));
        assertThat(registry.getInternalRegistry().get("L").size(), is(2));
        assertThat(disposedResources.get(), is(3));
        
        registry.disposeAll("L");
        assertThat(registry.getInternalRegistry().size(), is(0));
        assertThat(registry.getInternalRegistry().get("K"), is(nullValue()));
        assertThat(registry.getInternalRegistry().get("L"), is(nullValue()));
        assertThat(disposedResources.get(), is(5));
        
    }
}
