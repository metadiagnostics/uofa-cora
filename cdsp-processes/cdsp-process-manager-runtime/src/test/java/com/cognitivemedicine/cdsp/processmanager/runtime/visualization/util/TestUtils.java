/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization.util;

import org.kie.api.KieBase;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class TestUtils {
    
    private final static Logger LOG = LoggerFactory.getLogger(TestUtils.class);
 
    public static KieBase buildKieBase(String... drls){
        
        KieHelper helper = new KieHelper();
        for (String drl : drls) {
            helper.addResource(ResourceFactory.newClassPathResource(drl), ResourceType.DRL);
        }
        
        Results results = helper.verify();
        
        if (results.hasMessages(Message.Level.ERROR)){
            LOG.error("There are errors in the KBase:");
            for (Message message : results.getMessages(Message.Level.ERROR)) {
                LOG.error("\t"+message.toString());
            }
            throw new IllegalStateException("There are errors in the KBase");
        }
        
        return helper.build();
    }
}
