/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization;

import java.util.Map;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DroolsDecisionNodeExtractorTest {
    
    private final static Logger LOG = LoggerFactory.getLogger(DroolsDecisionNodeExtractorTest.class);
    
    @Test
    public void doTest(){
        
        KieBase kbase = buildKieBase("decision-node-rules/rules1/rules.drl");
        
        Map<String, DecisionNode> decisionNodes = DroolsDecisionNodeExtractor.extractDecisionNodes(kbase);
        assertThat(decisionNodes.size(), is(6));
        
        //Agenda Groups
        assertThat(decisionNodes.get("PM-INIT").getName(), is("PM-INIT"));
        assertThat(decisionNodes.get("PM-INIT").getDescription(), is("agenda-group"));
        assertThat(decisionNodes.get("PM-INIT").getTags().size(), is(0));
        
        assertThat(decisionNodes.get("MAIN").getName(), is("MAIN"));
        assertThat(decisionNodes.get("MAIN").getDescription(), is("agenda-group"));
        assertThat(decisionNodes.get("MAIN").getTags().size(), is(0));
        
        
        //Rules
        assertThat(decisionNodes.get("Init Rule").getName(), is("Init Rule"));
        assertThat(decisionNodes.get("Init Rule").getDescription(), is(nullValue()));
        assertThat(decisionNodes.get("Init Rule").getTags().size(), is(0));
        
        assertThat(decisionNodes.get("Populate List").getName(), is("Populate List"));
        assertThat(decisionNodes.get("Populate List").getDescription(), is(nullValue()));
        assertThat(decisionNodes.get("Populate List").getTags().size(), is(0));
        
        assertThat(decisionNodes.get("Some Rule").getName(), is("Some Rule"));
        assertThat(decisionNodes.get("Some Rule").getDescription(), is("This is some Rule"));
        assertThat(decisionNodes.get("Some Rule").getTags().size(), is(0));
        
        assertThat(decisionNodes.get("Some Other Rule").getName(), is("Some Other Rule"));
        assertThat(decisionNodes.get("Some Other Rule").getDescription(), is("This is another Rule"));
        assertThat(decisionNodes.get("Some Other Rule").getTags().size(), is(4));
        assertThat(decisionNodes.get("Some Other Rule").getTags(), hasItems(is("tag1"), is("tag2"), is("tag3"), is("tag4")));
        
    }
    
    private KieBase buildKieBase(String... drls){
        
        KieHelper helper = new KieHelper();
        for (String drl : drls) {
            helper.addResource(ResourceFactory.newClassPathResource(drl), ResourceType.DRL);
        }
        
        Results results = helper.verify();
        
        if (results.hasMessages(Message.Level.ERROR)){
            LOG.error("There are errors in the KBase:");
            for (Message message : results.getMessages(Message.Level.ERROR)) {
                LOG.error("\t"+message.toString());
            }
            throw new IllegalStateException("There are errors in the KBase");
        }
        
        return helper.build();
    }
}
