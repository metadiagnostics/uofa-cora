/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This registry can be used by KTIs as a place to keep track of all the 
 * services and resources than have to be disposed when a KTI is disposed.
 * 
 * Note: the KTIs are responsible of registering and disposing the elements
 * in this registry.
 * 
 * @author esteban
 */
public class DisposableRegistry {
    
    private final static Logger LOG = LoggerFactory.getLogger(DisposableRegistry.class);
    
    private Map<String, List<Disposable>> registry = new HashMap<>();
    
    public synchronized void put(String key, Disposable disposable){
        List<Disposable> disposables = registry.get(key);
        if (disposables == null){
            disposables = new ArrayList<>();
            registry.put(key, disposables);
        }
        disposables.add(disposable);
    }
    
    
    public synchronized void disposeAll(){
        Set<String> keys = new HashSet<>();
        keys.addAll(this.registry.keySet());
        
        for (String key : keys) {
            this.disposeAll(key);
        }
    }
    
    public synchronized void disposeAll(String key){
        List<Disposable> disposables = registry.get(key);
        if (disposables != null){
            for (Disposable disposable : disposables) {
                try{
                    disposable.dispose();
                } catch (Exception e){
                    LOG.warn("Error disposing "+disposable+". Continuing with the nex one.", e);
                }
            }
            registry.remove(key);
        }
    }
    
    /**
     * This method is meant to be used only for testing purposes.
     * @return 
     */
    protected Map<String, List<Disposable>> getInternalRegistry(){
        return this.registry;
    }
}
