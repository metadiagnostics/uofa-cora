/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.store.model;

import java.util.Date;

/**
 *
 * @author esteban
 */
public class KnowledgeTemplateInstanceFact extends BaseFactWrapper {
    
    private String ktiId;
    private String factClassName;
    
    /**
     * When this attribute is true, the fact will be automatically restored
     * as part of the Restart phase of a KTI
     */
    private boolean restoreOnRestart;

    public KnowledgeTemplateInstanceFact() {
    }

    public KnowledgeTemplateInstanceFact(String id) {
        super(id);
    }

    public KnowledgeTemplateInstanceFact(String id, Date timestamp) {
        super(id, timestamp);
    }

    @Override
    public void setFact(Object fact) {
        super.setFact(fact);
        factClassName = fact.getClass().getName();
    }
    
    public String getKtiId() {
        return ktiId;
    }

    public void setKtiId(String ktiId) {
        this.ktiId = ktiId;
    }

    public String getFactClassName() {
        return factClassName;
    }

    public void setFactClassName(String factClassName) {
        this.factClassName = factClassName;
    }

    public boolean isRestoreOnRestart() {
        return restoreOnRestart;
    }

    public void setRestoreOnRestart(boolean restoreOnRestart) {
        this.restoreOnRestart = restoreOnRestart;
    }
    
}
