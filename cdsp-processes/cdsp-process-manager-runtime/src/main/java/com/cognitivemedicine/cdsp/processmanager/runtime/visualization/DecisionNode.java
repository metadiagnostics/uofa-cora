/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static java.util.stream.Collectors.toList;

/**
 * This class is the representation of a "Decision Node" inside a KTD. 
 * 
 * 
 * @author esteban
 */
public class DecisionNode {
   private String name;
   private String description;
   private final List<String> tags = new ArrayList<>();

    public DecisionNode() {
    }

    public DecisionNode(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    public DecisionNode(String name, String description, List<String> tags) {
        this.name = name;
        this.description = description;
        
        if (tags != null){
            this.tags.addAll(tags.stream().map(t -> t.trim()).collect(toList()));
        }
    }
    
    public void addTag(String tag){
        tags.add(tag);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getTags() {
        return Collections.unmodifiableList(tags);
    }
   
}
