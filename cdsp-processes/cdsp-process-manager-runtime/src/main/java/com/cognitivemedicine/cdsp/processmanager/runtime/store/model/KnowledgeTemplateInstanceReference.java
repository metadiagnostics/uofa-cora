/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.store.model;

import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import java.util.Date;
import org.mongodb.morphia.annotations.Serialized;

/**
 * This class represents a KTI and the initial context it was used to instantiate
 * it. The real KTI is not included in this class.
 * @author esteban
 */
public class KnowledgeTemplateInstanceReference extends Base {
    
    private String ktdId;
    private String ktiId;
    
    @Serialized
    private KnowledgeTemplateInitContext knowledgeTemplateInitContext;

    public KnowledgeTemplateInstanceReference() {
    }

    public KnowledgeTemplateInstanceReference(String id) {
        super(id);
    }

    public KnowledgeTemplateInstanceReference(String id, Date timestamp) {
        super(id, timestamp);
    }

    public String getKtdId() {
        return ktdId;
    }

    public void setKtdId(String ktdId) {
        this.ktdId = ktdId;
    }

    public String getKtiId() {
        return ktiId;
    }

    public void setKtiId(String ktiId) {
        this.ktiId = ktiId;
    }

    public KnowledgeTemplateInitContext getKnowledgeTemplateInitContext() {
        return knowledgeTemplateInitContext;
    }

    public void setKnowledgeTemplateInitContext(KnowledgeTemplateInitContext knowledgeTemplateInitContext) {
        this.knowledgeTemplateInitContext = knowledgeTemplateInitContext;
    }
    
}
