/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.store.model;

import java.util.Date;

/**
 *
 * @author esteban
 */
public class KnowledgeTemplateDefinitionLifeCycleFact extends BaseFactWrapper {
    
    private String ktdId;

    public KnowledgeTemplateDefinitionLifeCycleFact() {
    }

    public KnowledgeTemplateDefinitionLifeCycleFact(String id) {
        super(id);
    }

    public KnowledgeTemplateDefinitionLifeCycleFact(String id, Date timestamp) {
        super(id, timestamp);
    }

    public String getKtdId() {
        return ktdId;
    }

    public void setKtdId(String ktdId) {
        this.ktdId = ktdId;
    }
    
}
