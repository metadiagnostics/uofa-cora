/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.drools.core.definitions.rule.impl.RuleImpl;
import org.kie.api.KieBase;

/**
 * Class that extracts {@link DecisionNode DecisionNodes} from a
 * {@link KieBase}.
 *
 * Any rule marked with an annotation matching {@link DecisionNodeConstants#DECISION_NODE_NAME}
 * is considered a {@link DecisionNode}. 
 * The set of all Agenda Groups present in the {@link KieBase} is also used
 * to create individual {@link DecisionNode DecisionNodes}.
 * 
 * @author esteban
 */
public class DroolsDecisionNodeExtractor {

    public static Map<String, DecisionNode> extractDecisionNodes(KieBase kbase) {
        Map<String, DecisionNode> nodes = new HashMap<>();

        kbase.getKiePackages().stream()
            .flatMap(kp -> kp.getRules().stream()).forEach(r -> {
            if (r.getMetaData().containsKey(DecisionNodeConstants.DECISION_NODE_NAME)) {
                String name = r.getMetaData().get(DecisionNodeConstants.DECISION_NODE_NAME).toString();
                String description = (String) r.getMetaData().get(DecisionNodeConstants.DECISION_NODE_DESCRIPTION);
                List<String> tags = r.getMetaData().get(DecisionNodeConstants.DECISION_NODE_TAGS) == null ? Collections.EMPTY_LIST : Arrays.asList(((String) r.getMetaData().get(DecisionNodeConstants.DECISION_NODE_TAGS)).split(","));
                nodes.put(name, new DecisionNode(name, description, tags));
            }
            
            if (r instanceof RuleImpl && (((RuleImpl)r).getAgendaGroup()) != null) {
                String name = ((RuleImpl)r).getAgendaGroup();
                String description = "agenda-group";
                nodes.put(name, new DecisionNode(name, description, Collections.EMPTY_LIST));
            }
        });
        
        return nodes;
    }

}
