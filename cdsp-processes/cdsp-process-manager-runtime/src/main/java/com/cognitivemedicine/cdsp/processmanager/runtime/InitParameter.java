/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime;

import java.io.Serializable;

/**
 * This class represents a KTI Initialization Parameter. These parameters can be
 * sent by he Process Manager when instantiating a KTI as a way to modify its
 * behavior.
 *
 * @author esteban
 */
public class InitParameter implements Serializable {

    private String name;
    private Object value;

    /**
     * This class contains a set of constants that can be used as the name of
     * {@link InitParameter} to avoid having hardcoded Strings in the code.
     *
     * @author esteban
     */
    public static class InitParameterConstants {
        public final static String DROOLS_LOGGING_ENABLED = "DROOLS_LOGGING_ENABLED";
        public final static String CONFIGURATION_NAME = "_CONFIGURATION_NAME_";
        public final static String CONFIGURATION_PROFILE_NAME = "_CONFIGURATION_PROFILE_NAME_";
        public final static String CONFIGURATION_SERVICE_URL = "__CONFIGURATION_SERVICE_URL__";
        public final static String LAST_EXECUTION_DATE_BEFORE_STARTUP = "__LAST_EXECUTION_DATE_BEFORE_STARTUP__";
    }

    public InitParameter() {
    }

    public InitParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "InitParameter{" + "name= '" + name + "', value= '" + value + "'}";
    }

}
