/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is the representation of the execution of a {@link DecisionNode}.
 * 
 * For performance and serialization purposes, there is no link between this
 * class and the {@link DecisionNode} class. The soft link is created by
 * {@link #nodeName} and {@link DecisionNode#name}.
 * 
 * 
 * @author esteban
 */
public class DecisionNodeExecution {
   private String nodeName;
   private long timestamp;
   private Map<String, Object> context = new HashMap<>();

    public DecisionNodeExecution() {
    }

    public DecisionNodeExecution(String nodeName, long timestamp) {
        this.nodeName = nodeName;
        this.timestamp = timestamp;
    }
    
    public DecisionNodeExecution(String nodeName, long timestamp, Map<String, Object> context) {
        this.nodeName = nodeName;
        this.timestamp = timestamp;
        
        if (context != null){
            this.context.putAll(context);
        }
    }
    
    public void addContext(String key, Object value){
        context.put(key, value);
    }

    public String getNodeName() {
        return nodeName;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Map<String, Object> getContext() {
        return Collections.unmodifiableMap(context);
    }

    
}
