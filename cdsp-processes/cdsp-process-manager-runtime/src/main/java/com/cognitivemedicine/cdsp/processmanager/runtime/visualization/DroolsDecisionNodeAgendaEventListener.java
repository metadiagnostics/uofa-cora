/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.visualization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.AgendaGroupPoppedEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;

/**
 * Drools' Agenda Event Listener used to collect information about Decision
 * Nodes being executed.
 * 
 * A new {@link DecisionNodeExecution} is created by this class whenever a 
 * new Agenda Group is popped from the stack or when a rule is executed.
 * 
 * Note that this class keeps the history of execution nodes in memory.
 * 
 * @author esteban
 */
public class DroolsDecisionNodeAgendaEventListener extends DefaultAgendaEventListener {

    private final Map<String, List<DecisionNodeExecution>> nodeExecutions = new HashMap<>();

    /**
     * Whenever an Agenda Group is popped, a new {@link DecisionNodeExecution} is 
     * created. 
     * The context of the generated {@link DecisionNodeExecution} will be empty.
     * 
     * @param event 
     */
    @Override
    public void agendaGroupPopped(AgendaGroupPoppedEvent event) {
        DecisionNodeExecution dne = new DecisionNodeExecution(event.getAgendaGroup().getName(), System.currentTimeMillis());
        nodeExecutions.computeIfAbsent(dne.getNodeName(), k -> new ArrayList()).add(dne);
    }

    /**
     * When a Match is fired, a new {@link DecisionNodeExecution} is created.
     * The context of this {@link DecisionNodeExecution} will contain any
     * bound variable present in the Match itself.
     * 
     * @param event 
     */
    @Override
    public void afterMatchFired(AfterMatchFiredEvent event) {

        if (event.getMatch().getRule().getMetaData().containsKey(DecisionNodeConstants.DECISION_NODE_NAME)) {
            String nodeName = event.getMatch().getRule().getMetaData().get(DecisionNodeConstants.DECISION_NODE_NAME).toString();
            long timestamp = System.currentTimeMillis();
            Map<String, Object> context = event.getMatch().getDeclarationIds().stream()
                .collect(Collectors.toMap(id -> id, id -> event.getMatch().getDeclarationValue(id)));

            DecisionNodeExecution dne = new DecisionNodeExecution(nodeName, timestamp, context);
            nodeExecutions.computeIfAbsent(dne.getNodeName(), k -> new ArrayList()).add(dne);
        }
    }

    public Map<String, List<DecisionNodeExecution>> getNodeExecutions() {
        return nodeExecutions;
    }

}
