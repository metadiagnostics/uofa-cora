/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.store.impl;

import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.Transient;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.Base;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateDefinitionLifeCycleFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * Implementation of {@link ProcessManagerStore} that keeps all the information
 * in-memory. This implementation is useful for testing purposes.
 * 
 * <b>Important:</b> This implementation is not thread-safe!
 * 
 * @author esteban
 */
public class InMemoryProcessManagerStore implements ProcessManagerStore{

    private Map<String, Base> objects = new HashMap<>();
    
    @Override
    public <T extends Base> T saveOrUpdate(T object) {
        //Do not persist a KnowledgeTemplateDefinitionLifeCycleFact if
        //it contains an instance of a transient classes.
        if (object instanceof KnowledgeTemplateDefinitionLifeCycleFact){
            KnowledgeTemplateDefinitionLifeCycleFact f = (KnowledgeTemplateDefinitionLifeCycleFact)object;
            if(f.getFact() != null && f.getFact().getClass().getAnnotation(Transient.class) != null){
                return object;
            }
        }
        
        if (object.getId() == null){
            object.setId(UUID.randomUUID().toString());
        }
        objects.put(object.getId(), object);
        return object;
    }

    @Override
    public List<KnowledgeTemplateDefinitionLifeCycleFact> listKnowledgeTemplateDefinitionLifeCycleFacts(String ktdId) {
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateDefinitionLifeCycleFact && ((KnowledgeTemplateDefinitionLifeCycleFact)o).getKtdId().equals(ktdId))
            .map(k -> (KnowledgeTemplateDefinitionLifeCycleFact)k)
            .collect(toList());
    }
    
    @Override
    public void deleteKnowledgeTemplateDefinitionLifeCycleFacts(String ktdId){
        List<KnowledgeTemplateDefinitionLifeCycleFact> facts = listKnowledgeTemplateDefinitionLifeCycleFacts(ktdId);
        for (KnowledgeTemplateDefinitionLifeCycleFact fact : facts) {
            objects.remove(fact.getId());
        }
    }

    @Override
    public Set<String> listKTDIdsWithKnowledgeTemplateDefinitionLifeCycleFacts(){
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateDefinitionLifeCycleFact)
            .map(k -> ((KnowledgeTemplateDefinitionLifeCycleFact)k).getKtdId())
            .collect(toSet());
    }
    
    @Override
    public List<KnowledgeTemplateInstanceReference> listKnowledgeTemplateInstanceReferences(String ktdId) {
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateInstanceReference && ((KnowledgeTemplateInstanceReference)o).getKtdId().equals(ktdId))
            .map(k -> (KnowledgeTemplateInstanceReference)k)
            .collect(toList());
    }
    
    @Override
    public void deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateInstanceId(String ktiId) {
        objects = objects.values().stream()
            .filter(o -> !(o instanceof KnowledgeTemplateInstanceReference) || !((KnowledgeTemplateInstanceReference)o).getKtiId().equals(ktiId))
            .collect(Collectors.toMap(Base::getId, o -> o));
    }

    @Override
    public void deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateDefinitionId(String ktdId) {
        objects = objects.values().stream()
            .filter(o -> !(o instanceof KnowledgeTemplateInstanceReference) || !((KnowledgeTemplateInstanceReference)o).getKtdId().equals(ktdId))
            .collect(Collectors.toMap(Base::getId, o -> o));
    }
    
    @Override
    public Set<String> listKTDIdsWithKnowledgeTemplateInstanceReferences(){
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateInstanceReference)
            .map(k -> ((KnowledgeTemplateInstanceReference)k).getKtdId())
            .collect(toSet());
    }
    
    @Override
    public KnowledgeTemplateInstanceFact getKnowledgeTemplateInstanceFactById(String id){
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateInstanceFact && ((KnowledgeTemplateInstanceFact)o).getId().equals(id))
            .map(k -> (KnowledgeTemplateInstanceFact)k)
            .findFirst().orElse(null);
    }

    @Override
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId){
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateInstanceFact && ((KnowledgeTemplateInstanceFact)o).getKtiId().equals(ktiId))
            .map(k -> (KnowledgeTemplateInstanceFact)k)
            .collect(toList());
    }
    
    @Override
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId, boolean restoreOnRestart){
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateInstanceFact && ((KnowledgeTemplateInstanceFact)o).getKtiId().equals(ktiId) && ((KnowledgeTemplateInstanceFact)o).isRestoreOnRestart() == restoreOnRestart )
            .map(k -> (KnowledgeTemplateInstanceFact)k)
            .collect(toList());
    }
    
    @Override
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId, String factClassName) {
        return objects.values().stream()
            .filter(o -> o instanceof KnowledgeTemplateInstanceFact && ((KnowledgeTemplateInstanceFact)o).getKtiId().equals(ktiId) && ((KnowledgeTemplateInstanceFact)o).getFactClassName().equals(factClassName))
            .map(k -> (KnowledgeTemplateInstanceFact)k)
            .collect(toList());
    }
    
    @Override
    public <T extends Base> List<T> listByType(Class<T> clazz) {
        return objects.values().stream()
            .filter(o -> clazz.isAssignableFrom(o.getClass()))
            .map(k -> (T)k)
            .collect(toList());
    }
    
    @Override
    public int getCount(){
        return objects.size();
    }
    
    @Override
    public void cleanup(){
        objects = new HashMap<>();
    }
    
}
