/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.registration;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a registration request to some data source.
 * 
 * @author dcalcaprina
 *
 */
public class DataSourceRegistrationRequest {
	
	/**
	 * The list of data sources to register to.
	 */
	private List<String> dataSourceIds;

	public DataSourceRegistrationRequest() {
		this.dataSourceIds = new ArrayList<String>();
	}
	
	public void addDataSource(String dataSourceId) {
		this.dataSourceIds.add(dataSourceId);
	}

	public List<String> getDataSourceIds() {
		return dataSourceIds;
	}
}
