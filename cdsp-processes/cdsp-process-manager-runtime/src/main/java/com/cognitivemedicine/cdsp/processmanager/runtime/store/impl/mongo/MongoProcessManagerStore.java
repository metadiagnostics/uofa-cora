/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.store.impl.mongo;

import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.Transient;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.Base;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateDefinitionLifeCycleFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceReference;
import com.mongodb.MongoClient;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import static java.util.stream.Collectors.toSet;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 *
 * @author esteban
 */
public class MongoProcessManagerStore implements ProcessManagerStore {
    
    public static final int DEFAULT_PORT = 27017;
    private static final String PROCESS_MANAGER_DB_NAME = "process_manager";
    
    private final Datastore mongoStore;

	
	public MongoProcessManagerStore(String endpoint) {
        String host = endpoint;
        int port = DEFAULT_PORT;
        try{
            if(endpoint.contains(":")){
                String[] split = endpoint.split(":");
                host = split[0];
                port = Integer.parseInt(split[1]);
            }
        } catch (Exception e){
            throw new IllegalArgumentException("Exception parsing the provided endpoint '"+endpoint+"'", e);
        }
        
        mongoStore = new Morphia().createDatastore(new MongoClient(host, port), PROCESS_MANAGER_DB_NAME);
    }
    
    
	public MongoProcessManagerStore(String host, int port) {
		this(new MongoClient(host, port));
	}
    
	public MongoProcessManagerStore(MongoClient client) {
		mongoStore = new Morphia().createDatastore(client, PROCESS_MANAGER_DB_NAME);
	}
    
    
    @Override
    public <T extends Base> T saveOrUpdate(T object) {
        //Do not persist a KnowledgeTemplateDefinitionLifeCycleFact if
        //it contains an instance of a transient classes.
        if (object instanceof KnowledgeTemplateDefinitionLifeCycleFact){
            KnowledgeTemplateDefinitionLifeCycleFact f = (KnowledgeTemplateDefinitionLifeCycleFact)object;
            if(f.getFact() != null && f.getFact().getClass().getAnnotation(Transient.class) != null){
                return object;
            }
        }
        
        if (object.getId() == null){
            object.setId(UUID.randomUUID().toString());
        } else {
            mongoStore.findAndDelete(mongoStore.createQuery(object.getClass()).filter("id ==", object.getId()));
        }
        
        mongoStore.save(object);
        
        
        return object;
    }

    @Override
    public List<KnowledgeTemplateDefinitionLifeCycleFact> listKnowledgeTemplateDefinitionLifeCycleFacts(String ktdId) {
        return mongoStore.find(KnowledgeTemplateDefinitionLifeCycleFact.class).filter("ktdId ==", ktdId).asList();
    }

    @Override
    public void deleteKnowledgeTemplateDefinitionLifeCycleFacts(String ktdId) {
        mongoStore.delete(mongoStore.find(KnowledgeTemplateDefinitionLifeCycleFact.class).filter("ktdId ==", ktdId));
    }
    
    @Override
    public Set<String> listKTDIdsWithKnowledgeTemplateDefinitionLifeCycleFacts(){
        List<KnowledgeTemplateDefinitionLifeCycleFact> facts = mongoStore.find(KnowledgeTemplateDefinitionLifeCycleFact.class).retrieveKnownFields().asList();
        return facts.stream()
            .map(f -> f.getKtdId())
            .collect(toSet());
    }

    @Override
    public List<KnowledgeTemplateInstanceReference> listKnowledgeTemplateInstanceReferences(String ktdId) {
        return mongoStore.find(KnowledgeTemplateInstanceReference.class).filter("ktdId ==", ktdId).asList();
    }

    @Override
    public void deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateInstanceId(String ktiId) {
        mongoStore.delete(mongoStore.find(KnowledgeTemplateInstanceReference.class).filter("ktiId ==", ktiId));
    }

    @Override
    public void deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateDefinitionId(String ktdId) {
        mongoStore.delete(mongoStore.find(KnowledgeTemplateInstanceReference.class).filter("ktdId ==", ktdId));
    }
    
    @Override
    public Set<String> listKTDIdsWithKnowledgeTemplateInstanceReferences(){
        List<KnowledgeTemplateInstanceReference> references = mongoStore.find(KnowledgeTemplateInstanceReference.class).retrieveKnownFields().asList();
        return references.stream()
            .map(r -> r.getKtdId())
            .collect(toSet());
    }
    
    @Override
    public KnowledgeTemplateInstanceFact getKnowledgeTemplateInstanceFactById(String id){
        return mongoStore.find(KnowledgeTemplateInstanceFact.class).filter("id ==", id).limit(1).get();
    }
    
    @Override
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId){
        return mongoStore.find(KnowledgeTemplateInstanceFact.class).filter("ktiId ==", ktiId).asList();
    }
    
    @Override
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId, boolean restoreOnRestart){
        return mongoStore.find(KnowledgeTemplateInstanceFact.class).filter("ktiId ==", ktiId).filter("restoreOnRestart ==", restoreOnRestart).asList();
    }
    
    @Override
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId, String factClassName) {
        return mongoStore.find(KnowledgeTemplateInstanceFact.class).filter("ktiId ==", ktiId).filter("factClassName ==", factClassName).asList();
    }
    
    @Override
    public <T extends Base> List<T> listByType(Class<T> clazz) {
        return mongoStore.find(clazz).asList();
    }
    
    @Override
    public int getCount(){
        int count = 0;
        for (String collectionName : mongoStore.getDB().getCollectionNames()) {
            if ("system.indexes".equals(collectionName)){
                continue;
            }
            count += mongoStore.getDB().getCollection(collectionName).count();
        }
        
        return count;
    }
    
    @Override
    public void cleanup(){
        for (String collectionName : mongoStore.getDB().getCollectionNames()) {
            if ("system.indexes".equals(collectionName)){
                continue;
            }
            mongoStore.getDB().getCollection(collectionName).drop();
        }
    }
    
}
