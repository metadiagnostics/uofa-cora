/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.runtime.store.api;

import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.Base;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateDefinitionLifeCycleFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceReference;
import java.util.List;
import java.util.Set;

/**
 *
 * @author esteban
 */
public interface ProcessManagerStore {
    
    public <T extends Base> T saveOrUpdate(T object);
    
    public List<KnowledgeTemplateDefinitionLifeCycleFact> listKnowledgeTemplateDefinitionLifeCycleFacts(String ktdId);
    
    public void deleteKnowledgeTemplateDefinitionLifeCycleFacts(String ktdId);
    
    public Set<String> listKTDIdsWithKnowledgeTemplateDefinitionLifeCycleFacts();
    
    public List<KnowledgeTemplateInstanceReference> listKnowledgeTemplateInstanceReferences(String ktdId);
    
    public void deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateInstanceId(String ktiId);
    
    public void deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateDefinitionId(String ktdId);
    
    public Set<String> listKTDIdsWithKnowledgeTemplateInstanceReferences();
    
    public KnowledgeTemplateInstanceFact getKnowledgeTemplateInstanceFactById(String id);
    
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId);
    
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId, boolean restoreOnRestart);
    
    public List<KnowledgeTemplateInstanceFact> listKnowledgeTemplateInstanceFacts(String ktiId, String factClassName);
    
    public <T extends Base> List<T> listByType(Class<T> clazz);
    
    public int getCount();
    
    public void cleanup();
}
