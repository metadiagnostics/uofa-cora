<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>com.cognitivemedicine.cdsp</groupId>
    <artifactId>cdsp-processes</artifactId>
    <version>0.0.25</version>
  </parent>

  <artifactId>cdsp-process-manager</artifactId>
  <packaging>jar</packaging>
    
  <name>CDSP Process Manager</name>

  <dependencies>
    <dependency>
      <groupId>com.cognitivemedicine.cdsp</groupId>
      <artifactId>cdsp-process-manager-runtime</artifactId>
    </dependency>
    <dependency>
      <groupId>com.cognitivemedicine.cdsp</groupId>
      <artifactId>cdsp-hl7transformer</artifactId>
    </dependency>
    <dependency>
      <groupId>com.cognitivemedicine.cdsp</groupId>
      <artifactId>cdsp-jms</artifactId>
    </dependency>
    <dependency>
      <groupId>com.cognitivemedicine.cdsp</groupId>
      <artifactId>cdsp-model</artifactId>
    </dependency>       
    <dependency>
      <groupId>org.kie</groupId>
      <artifactId>kie-api</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.drools</groupId>
      <artifactId>drools-core</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.drools</groupId>
      <artifactId>drools-compiler</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.kie</groupId>
      <artifactId>kie-ci</artifactId>
      <type>jar</type>
      <exclusions>
        <exclusion>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.jbpm</groupId>
      <artifactId>jbpm-flow</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.jbpm</groupId>
      <artifactId>jbpm-bpmn2</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-api</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>com.google.code.gson</groupId>
      <artifactId>gson</artifactId>
      <type>jar</type>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-slf4j-impl</artifactId>
    </dependency>
    <dependency>
      <groupId>org.antlr</groupId>
      <artifactId>ST4</artifactId>
    </dependency>
    <dependency>
      <groupId>com.cognitivemedicine.cdsp</groupId>
      <artifactId>cdsp-report-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.quartz-scheduler</groupId>
      <artifactId>quartz</artifactId>
    </dependency>
    <dependency>
      <groupId>org.socraticgrid</groupId>
      <artifactId>config-utils</artifactId>
    </dependency>
    <dependency>
      <groupId>com.cognitivemedicine.cdsp</groupId>
      <artifactId>cdsp-report-mongo-impl</artifactId>
      <scope>test</scope>
    </dependency>   
        
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-all</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>com.github.fakemongo</groupId>
      <artifactId>fongo</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>
  <properties>
    <netbeans.hint.license>apache20</netbeans.hint.license>
  </properties>
    
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>${maven.compiler.plugin.version}</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
          <compilerArgument>-Xlint:all</compilerArgument>
          <showWarnings>true</showWarnings>
          <showDeprecation>true</showDeprecation>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <systemProperties>
            <property>
              <name>kie.maven.settings.custom</name>
              <value>src/test/resources/kjars/settings.xml</value>
            </property>
          </systemProperties>
        </configuration>
      </plugin>
    </plugins>
  </build>
    
</project>
