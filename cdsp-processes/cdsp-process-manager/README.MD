Process Manager
===============

This project serves as a platform to run Knowledge Modules called Knowledge 
Template Definitions (KTDs).

A KTD is the definition of a Knowledge Module and it can be implemented using 
different technologies (currently, only drools-based KTDs are supported). 

A KTD is instantiated by the platform whenever a Start Condition (defined by the
same KTD) is met. The instantiation of a KTD creates a Knowledge Template Instance
(KTI). A KTI has a well defined life-cycle that is administered by this platform.

KTDs can be registered on this platform during runtime.

##Drools-based KTD##

Drools-based KTDs are defined by the 
`com.cognitivemedicine.cdsp.processmanager.impl.DroolsKnowledgeTemplateDefinition`
class. 
This kind of KTD references a KJar (kie jar file) containing the knowledge
to be executed when new KTIs are created. 
The current implementation uses kie-ci to resolve the KJar during runtime. This
means that the KJar to be executed is not a dependency of the platform.

In order for a KJar to be used as a KTD it must contain the following characteristics:

* It must contain a file called `ktd-deployment-descriptor.xml` in the source level
(i.e. src/main/resources). An example of this file can be found [here](src/test/resources/samples/sample-ktd-deployment-descriptor.xml).
This file is going to be used by the Process Manager to:
    * Get the list of topics that are related to the life-cycle of the KTIs derived 
from this KTD. The platform will register the necessary subscribers to this topics and
use the incoming information to deal with the life-cycle events related to 
the KTIs of this KTD.
    * Get the conditions for the different life-cycles phases of the KTIs derived 
from this KTD.
* It must define a default KieBase in its `kmodule.xml` file. The current
implementation only allows 1 KieBase per KTD.
* It may contain configuration rules (without LHS) to perform any kind of configuration
required by this KTD such as: the insertion of any data coming from a life-cycle
event, the registration of any required WorkItemHandler, etc.

##Agenda Groups and KTI Life-Cycle##

The init and start phases of a KTI are associated with the following agenda-groups
in Drools:

* init phase -> "PM-INIT", "MAIN"
* start phase -> "PM-START", "MAIN"
* shutdown phase -> "PM-SHUTDOWN", MAIN"
* dispose phase -> "PM-DISPOSE", MAIN"