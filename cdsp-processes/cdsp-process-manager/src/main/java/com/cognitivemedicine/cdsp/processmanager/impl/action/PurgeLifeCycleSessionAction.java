/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl.action;

import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.Transient;

/**
 * This action is used to remove any fact from the life-cycle KB associated to
 * a particular KTD.
 * The action itself doesn't identify the target KTD though. This allows this 
 * action to be broadcasted to multiple KTDs.
 * 
 * @author esteban
 */
@Transient
public class PurgeLifeCycleSessionAction implements LifeCycleKBAction {
    
}
