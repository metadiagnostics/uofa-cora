/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import org.kie.api.runtime.KieSession;

/**
 * This class keeps track of the relationship between a KTD, its Life-Cycle Session
 * and all its KTIs.
 * 
 * @author esteban
 */
public class KnowledgeTemplateDefinitionRegistry {
    
    private class KnowledgeTemplateDefinitionRegistryEntry{
        private final KnowledgeTemplateDefinition definition;
        private final KieSession session;
        private final Map<String, KnowledgeTemplateInstance> instances = Collections.synchronizedMap(new HashMap<>());

        public KnowledgeTemplateDefinitionRegistryEntry(KnowledgeTemplateDefinition definition, KieSession session) {
            this.definition = definition;
            this.session = session;
        }
        
    }
    
    private final Map<String, KnowledgeTemplateDefinitionRegistryEntry> entries = Collections.synchronizedMap(new HashMap<>());
    
    public void addNewKnowledgeTemplateDefinition(KnowledgeTemplateDefinition definition, KieSession session){
        entries.put(definition.getId(), new KnowledgeTemplateDefinitionRegistryEntry(definition, session));
    }
    
    public void removeKnowledgeTemplateDefinition(String knowledgeTemplateDefinitionId){
        entries.remove(knowledgeTemplateDefinitionId);
    }
    
    public List<KnowledgeTemplateDefinition> getKnowledgeTemplateDefinitions(){
        synchronized(entries){
            return entries.values().stream().map(e -> e.definition).collect(toList());
        }
    }
    
    public KnowledgeTemplateDefinition getKnowledgeTemplateDefinitionById(String knowledgeTemplateDefinitionId){
        KnowledgeTemplateDefinitionRegistryEntry entry = getEntryOrDie(knowledgeTemplateDefinitionId);
        return entry.definition;
    }
    
    public List<KieSession> getKnowledgeTemplateDefinitionsSessions(){
        synchronized(entries){
            return entries.values().stream().map(e -> e.session).collect(toList());
        }
    }
    
    public KnowledgeTemplateDefinition getKnowledgeTemplateDefinitionFromKnowledgeTemplateInstanceId(String knowledgeTemplateInstanceId){
        //TODO: should we add a reverse index for kti -> ktd?
        for (KnowledgeTemplateDefinitionRegistryEntry entry : entries.values()) {
            if (entry.instances.containsKey(knowledgeTemplateInstanceId)){
                return entry.definition;
            }
        }

        throw new IllegalArgumentException("There is no Knowledge Template Definition with a Knowledge Template Instance whose id is '"+knowledgeTemplateInstanceId+"'");
    }
    
    public void addNewKnowledgeTemplateInstance(String knowledgeTemplateDefinitionId, KnowledgeTemplateInstance instance){
        KnowledgeTemplateDefinitionRegistryEntry entry = getEntryOrDie(knowledgeTemplateDefinitionId);
        entry.instances.put(instance.getId(), instance);
    }
    
    public void removeKnowledgeTemplateInstance(String knowledgeTemplateDefinitionId, String knowledgeTemplateInstanceId){
        KnowledgeTemplateDefinitionRegistryEntry entry = getEntryOrDie(knowledgeTemplateDefinitionId);
        entry.instances.remove(knowledgeTemplateInstanceId);
    }
    
    public List<KnowledgeTemplateInstance> getKnowledgeTemplateInstances(String knowledgeTemplateDefinitionId){
        KnowledgeTemplateDefinitionRegistryEntry entry = getEntryOrDie(knowledgeTemplateDefinitionId);
        return entry.instances.values().stream().collect(toList());
    }
    
    public KieSession getSession(String knowledgeTemplateDefinitionId){
        KnowledgeTemplateDefinitionRegistryEntry entry = getEntryOrDie(knowledgeTemplateDefinitionId);
        return entry.session;
    }
    
    public boolean exists(String knowledgeTemplateDefinitionId) {
        return entries.containsKey(knowledgeTemplateDefinitionId);
    }
    
    private KnowledgeTemplateDefinitionRegistryEntry getEntryOrDie(String knowledgeTemplateDefinitionId){
        KnowledgeTemplateDefinitionRegistryEntry entry = entries.get(knowledgeTemplateDefinitionId);
        if(entry == null){
            throw new IllegalArgumentException("Unknown Knowledge Template Definition with id '"+knowledgeTemplateDefinitionId+"'");
        }
        return entry;
    }

}
