/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.drools.compiler.kproject.ReleaseIdImpl;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.builder.Message;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.processmanager.BuildInfo;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DroolsDecisionNodeExtractor;
import java.util.Map;

/**
 * Implementation of KnowledgeTemplateDefinition that creates
 * {@link DroolsKnowledgeTemplateInstance} instances based on kie-ci. This class
 * uses a GAV (groupId, artifactId and version) to know which KieBase needs to
 * be instantiated. The current implementation only allows 1 KieBase (the
 * default) per KieContainer.
 *
 * This implementation will take the life-cycle related topics and life-cycle
 * conditions that the ProcessManager will use from a file inside the kjar with
 * the name {@link #DEPLOYMENT_DESCRIPTOR_FILENAME}.
 *
 * @author esteban
 */
public class DroolsKnowledgeTemplateDefinition implements KnowledgeTemplateDefinition {

    private final static Logger LOG = LoggerFactory.getLogger(DroolsKnowledgeTemplateDefinition.class);
    

	private static final String DEPLOYMENT_DESCRIPTOR_FILENAME = "ktd-deployment-descriptor.xml";

	private final ReleaseId releaseId;
	private KieContainer kContainer;
	private DroolsKnowledgeTemplateDeploymentDescriptor deploymentDescriptor;
    
	public DroolsKnowledgeTemplateDefinition(ReleaseId releaseId) {
		this(releaseId.getGroupId(), releaseId.getArtifactId(), releaseId.getVersion());
	}

	public DroolsKnowledgeTemplateDefinition(String groupId, String artifactId, String version) {
		this.releaseId = new ReleaseIdImpl(groupId, artifactId, version);
	}

	private KieContainer getKContainer() {
		if (this.kContainer == null) {
			KieServices ks = KieServices.Factory.get();
			
			long tTotal = 0;
			LOG.trace("Initializing KieContainer for "+this.getId());
			
			long t1 = System.currentTimeMillis();
			this.kContainer = ks.newKieContainer(this.releaseId);
			long t2 = System.currentTimeMillis();
			LOG.trace("Creation of the KieContainer took "+((t2-t1)/1000)+" seg.");
			tTotal+= ((t2-t1)/1000);
			
			
			t1 = System.currentTimeMillis();
			KieScanner scanner = ks.newKieScanner(this.kContainer);
			t2 = System.currentTimeMillis();
			LOG.trace("Creation of KieScanner took "+((t2-t1)/1000)+" seg.");
			tTotal+= ((t2-t1)/1000);
			
			
			t1 = System.currentTimeMillis();
			scanner.scanNow();
			t2 = System.currentTimeMillis();
			LOG.trace("Scanning took "+((t2-t1)/1000)+" seg.");
			tTotal+= ((t2-t1)/1000);

			
			t1 = System.currentTimeMillis();
			Results results = this.kContainer.verify();
			t2 = System.currentTimeMillis();
			LOG.trace("Verification of KieContainer took "+((t2-t1)/1000)+" seg.");
			tTotal+= ((t2-t1)/1000);

			LOG.trace("TTT "+this.releaseId.getArtifactId()+" TOTAL: "+tTotal+" seg.");
			
			results.getMessages().stream().forEach((message) -> {
				String txt = ">> Message ( " + message.getLevel() + " ): " + message.getText();
				LOG.error(txt);
			});

			if (results.hasMessages(Message.Level.ERROR)) {
				throw new IllegalStateException("There are errors in the Knowledge Template.");
			}
            
            LOG.trace("Initialization of KieContainer "+this.getId()+" is complete");
		}

		return this.kContainer;
	}

	@Override
	public KnowledgeTemplateInstance createInstance() {
		return new DroolsKnowledgeTemplateInstance(getKContainer().getKieBase());
	}

    @Override
    public KnowledgeTemplateInstance recreateInstance(String knowledgeTemplateInstanceId) {
        DroolsKnowledgeTemplateInstance kti = new DroolsKnowledgeTemplateInstance(getKContainer().getKieBase());
        kti.setId(knowledgeTemplateInstanceId);
        
		return kti;
    }
    
	@Override
	public String getId() {
		return this.releaseId.toExternalForm();
	}
	
	@Override
	public String getVersion() {
	    return this.releaseId.getVersion();
	}
	
	@Override
	public String getArtifact() {
	    return new StringBuilder(this.releaseId.getGroupId()).append(":").append(this.releaseId.getArtifactId()).toString();
	}

	@Override
	public String calculateKTIId(KnowledgeTemplateInitContext context) {
		LOG.debug("Calculating KTI Id");
		//TODO: different strategies should be configured through the ktd-deployment-descriptor.xml file.

		//TODO: one of the strategies should return the same id given the
		//same context (based on identity at least)
		return UUID.randomUUID().toString();
	}
    
    @Override
    public boolean isRestartable() {
        return this.getDeploymentDescriptor().isRestartable();
    }

	@Override
	public BuildInfo getBuildInfo() {
		return this.getDeploymentDescriptor().getBuildInfo();
	}
	
	@Override
	public ExecutionMode getExecutionMode() {
	   return this.getDeploymentDescriptor().getExecutionMode();
	}

	@Override
	public LifeCycleEventCondition getStartCondition() {
		return this.getDeploymentDescriptor().getStartCondition();
	}

	@Override
	public LifeCycleEventCondition getShutdownCondition() {
		return this.getDeploymentDescriptor().getShutdownCondition();
	}

	@Override
	public List<String> getLifeCycleRelatedTopics() {
		return this.getDeploymentDescriptor().getLifeCycleRelatedTopics();
	}

	@Override
	public List<String> getAdditionalImports() {
		return this.getDeploymentDescriptor().getAdditionalImports();
	}
    
    @Override
    public Map<String, DecisionNode> getDecisionNodes(){
        //TODO cache the map
        Map<String, DecisionNode> nodes = DroolsDecisionNodeExtractor.extractDecisionNodes(getKContainer().getKieBase());
        return nodes;
    }

	private synchronized DroolsKnowledgeTemplateDeploymentDescriptor getDeploymentDescriptor() {
		if (this.deploymentDescriptor == null) {
			InputStream deploymentDescriptorStream = this.getKContainer().getClassLoader().getResourceAsStream(DEPLOYMENT_DESCRIPTOR_FILENAME);
			if (deploymentDescriptorStream == null) {
				throw new IllegalArgumentException("No " + DEPLOYMENT_DESCRIPTOR_FILENAME + " file found in the kjar. We don;t know how to deploy this KTD!");
			}

			this.deploymentDescriptor = this.parseDeploymentDescriptor(deploymentDescriptorStream);
		}
		return this.deploymentDescriptor;
	}

	private DroolsKnowledgeTemplateDeploymentDescriptor parseDeploymentDescriptor(InputStream is) {
		try {

			JAXBContext context = JAXBContext.newInstance(DroolsKnowledgeTemplateDeploymentDescriptor.class);
			Unmarshaller u = context.createUnmarshaller();

			DroolsKnowledgeTemplateDeploymentDescriptor descriptor = (DroolsKnowledgeTemplateDeploymentDescriptor) u.unmarshal(is);

			return descriptor;
		} catch (Exception ex) {
			throw new IllegalArgumentException("Exception parsing " + DEPLOYMENT_DESCRIPTOR_FILENAME + " file.", ex);
		}
	}
    
	@Override
	public String toString() {
		return "DroolsKnowledgeTemplateDefinition{releaseId=" + releaseId + '}';
	}

}
