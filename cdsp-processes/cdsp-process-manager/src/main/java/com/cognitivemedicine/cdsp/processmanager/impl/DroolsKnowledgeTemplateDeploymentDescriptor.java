/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.cognitivemedicine.cdsp.processmanager.BuildInfo;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;

/**
 *
 * @author esteban
 */

@XmlRootElement(name="deployment-descriptor")
@XmlAccessorType(XmlAccessType.FIELD)
public class DroolsKnowledgeTemplateDeploymentDescriptor {
    
    public DroolsKnowledgeTemplateDeploymentDescriptor() {
        executionMode = ExecutionMode.STATEFULL;
    }
    
    @XmlElement(name="execution-mode", type=ExecutionMode.class)
    private ExecutionMode executionMode;
    
    @XmlElement(name="build-info", type=BuildInfo.class)
    private BuildInfo buildInfo;
    
    @XmlElement(name="restartable", defaultValue="false")
    private boolean restartable;
    
    @XmlElement(name="topic")
    @XmlElementWrapper(name="life-cycle-topics")
    private List<String> lifeCycleRelatedTopics;
    
    @XmlElement(name="additional-import")
    @XmlElementWrapper(name="additional-imports")
    private List<String> additionalImports;
    
    @XmlElement(name="life-cycle-start-condition", type=LifeCycleEventConditionImpl.class)
    private LifeCycleEventCondition startCondition;
    
    @XmlElement(name="life-cycle-shutdown-condition", type=LifeCycleEventConditionImpl.class)
    private LifeCycleEventCondition shutdownCondition;

    public BuildInfo getBuildInfo() {
        if (this.buildInfo == null){
            BuildInfo emptyBuildInfo = new BuildInfo();
            emptyBuildInfo.setBuildNumber("UNKNOWN");
            emptyBuildInfo.setBuildBranch("UNKNOWN");
            emptyBuildInfo.setBuildTimestamp(0);
            
            return emptyBuildInfo;
        }
        return buildInfo;
    }

    public void setBuildInfo(BuildInfo buildInfo) {
        this.buildInfo = buildInfo;
    }

    public boolean isRestartable() {
        return restartable;
    }

    public void setRestartable(boolean restartable) {
        this.restartable = restartable;
    }
    
    public List<String> getLifeCycleRelatedTopics() {
        return lifeCycleRelatedTopics;
    }

    public void setLifeCycleRelatedTopics(List<String> lifeCycleRelatedTopics) {
        this.lifeCycleRelatedTopics = lifeCycleRelatedTopics;
    }

    public LifeCycleEventCondition getStartCondition() {
        return startCondition;
    }

    public void setStartCondition(LifeCycleEventCondition startCondition) {
        this.startCondition = startCondition;
    }

    public LifeCycleEventCondition getShutdownCondition() {
        return shutdownCondition;
    }

    public void setShutdownCondition(LifeCycleEventCondition shutdownCondition) {
        this.shutdownCondition = shutdownCondition;
    }

    public List<String> getAdditionalImports() {
        return additionalImports;
    }
    
    public ExecutionMode getExecutionMode() {
        return executionMode;
    }
    
    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

}
