/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class contains some build information for KTDs.
 * @author esteban
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BuildInfo {
    
    @XmlElement(name="build-number")
    private String buildNumber;
    
    @XmlElement(name="build-branch")
    private String buildBranch;
    
    @XmlElement(name="build-timestamp")
    private long buildTimestamp;

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildBranch() {
        return buildBranch;
    }

    public void setBuildBranch(String buildBranch) {
        this.buildBranch = buildBranch;
    }

    public long getBuildTimestamp() {
        return buildTimestamp;
    }

    public void setBuildTimestamp(long buildTimestamp) {
        this.buildTimestamp = buildTimestamp;
    }

    @Override
    public String toString() {
        return "BuildInfo{" + "buildNumber= '" + buildNumber + "', buildBranch= '" + buildBranch + "', buildTimestamp= '" + buildTimestamp + "'}";
    }
    
}
