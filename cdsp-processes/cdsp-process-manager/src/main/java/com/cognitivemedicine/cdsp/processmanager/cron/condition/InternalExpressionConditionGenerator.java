/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron.condition;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This expression condition generator will expect an input expression with the following format:
 * ${<<input>>}
 * If this format is matched, it will take the internal input and check in order with its generators for the first it can handle the internal input.
 * The first one that can handle it will be delegated to generate the expression.
 * @author calcacuervo
 *
 */
public class InternalExpressionConditionGenerator implements ConditionExpressionGenerator {

    private static final String DEFAULT_REGEX = "\\$\\{(.*?)\\}";
    private String regex;
    private Pattern pattern;
    private ConditionExpressionGenerator[] delegates;

    public InternalExpressionConditionGenerator(String regex, ConditionExpressionGenerator... delegates) {
        this.delegates = delegates;
        this.regex = regex;
        pattern = Pattern.compile(regex);
    }

    public InternalExpressionConditionGenerator(ConditionExpressionGenerator... delegates) {
        this(DEFAULT_REGEX, delegates);
    }
    
    @Override
    public boolean accepts(String source) {
        return source.matches(regex);
    }

    @Override
    public String generateExpression(String source) {
        Matcher matcher = pattern.matcher(source);
        if (matcher.find()) {
            String internalCondition = matcher.group(1);
            for (ConditionExpressionGenerator conditionGenerator : delegates) {
                if (conditionGenerator.accepts(internalCondition)) {
                    return conditionGenerator.generateExpression(internalCondition);
                }
            }
        }
        throw new IllegalArgumentException("Could not generate start condition from given definition: " + source);
    }

}
