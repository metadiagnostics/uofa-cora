/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl.model;

import java.io.Serializable;

/**
 * This class is used to generate start and shutdown conditions for CRON based
 * KTDs.
 * An instance of this class is inserted by an scheduler into the life cycle 
 * session of the corresponding KTD whenever the CRON expression determines it.
 * 
 * @author esteban
 */
public class CronBasedCondition implements Serializable {
    
    public static enum Type {
        START_CONDITION,
        SHUTDOWN_CONDITION,
        OTHER
    }
    
    private long timestamp;
    private Type type;
    
    public CronBasedCondition() {
        this(System.currentTimeMillis());
    }

    public CronBasedCondition(long timestamp) {
        this(timestamp, Type.OTHER);
    }

    public CronBasedCondition(long timestamp, Type type) {
        this.timestamp = timestamp;
        this.type = type;
    }
    
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
}
