/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import com.cognitivemedicine.cdsp.messaging.api.MessageSubscriber;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.report.api.ReportService;
import java.util.concurrent.Executor;
import org.quartz.Scheduler;


public class ProcessManagerConfigurationBuilder {

    private MessageSubscriber messageSubscriber;
    private ProcessManagerStore processManagerStore;
    private ReportService reportService;
    private Executor executor;
    private Scheduler scheduler;
    private ConditionExpressionGenerator conditionExpressionGenerator;
    
    public ProcessManagerConfigurationBuilder() {
    }

    public ProcessManagerConfigurationBuilder setMessageSubscriber(MessageSubscriber messageSubscriber) {
        this.messageSubscriber = messageSubscriber;
        return this;
    }

    public ProcessManagerConfigurationBuilder setConditionExpressionGenerator(ConditionExpressionGenerator conditionExpressionGenerator) {
        this.conditionExpressionGenerator = conditionExpressionGenerator;
        return this;
    }

    public ProcessManagerConfigurationBuilder setProcessManagerStore(ProcessManagerStore processManagerStore) {
        this.processManagerStore = processManagerStore;
        return this;
    }
    
    public ProcessManagerConfigurationBuilder setReportService(ReportService reportService) {
        this.reportService = reportService;
        return this;
    }

    public ProcessManagerConfigurationBuilder setExecutor(Executor executor) {
        this.executor = executor;
        return this;
    }

    public ProcessManagerConfigurationBuilder setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
        return this;
    }

    public ProcessManagerConfiguration createProcessManagerConfiguration() {
        
        if (messageSubscriber == null){
            throw new IllegalStateException("The messageSubscriber can't be null");
        }
        if (processManagerStore == null){
            throw new IllegalStateException("The processManagerStore can't be null");
        }
        if (reportService == null){
            throw new IllegalStateException("The reportService can't be null");
        }
        if (executor == null){
            throw new IllegalStateException("The executor can't be null");
        }
        if (scheduler == null){
            throw new IllegalStateException("The scheduler can't be null");
        }
        if (conditionExpressionGenerator == null){
            throw new IllegalStateException("The expression generator can't be null");
        }        
        return new ProcessManagerConfiguration(messageSubscriber, processManagerStore, reportService, executor, scheduler, conditionExpressionGenerator);
    }
    
}
