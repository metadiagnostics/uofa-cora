/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron;

import com.cognitivemedicine.cdsp.processmanager.impl.KnowledgeTemplateLifeCycleKB;
import com.cognitivemedicine.cdsp.processmanager.impl.model.CronBasedCondition;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.listeners.JobListenerSupport;

/**
 *
 * @author esteban
 */
public class CronBasedKnowledgeTemplateDefinitionJobListener extends JobListenerSupport{

    private final KnowledgeTemplateLifeCycleKB kb;

    public CronBasedKnowledgeTemplateDefinitionJobListener(KnowledgeTemplateLifeCycleKB kb) {
        this.kb = kb;
    }
    
    @Override
    public String getName() {
        return CronBasedKnowledgeTemplateDefinitionJobListener.class.getName();
    }

    /**
     * When a job is executed, a new {@link CronBasedCondition} is created
     * and notified to the corresponding life-cycle session.
     * 
     * @param context
     * @param jobException 
     */
    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        Object result = context.getResult();
        
        if (!(result instanceof CronBasedKnowledgeTemplateDefinitionJobResult)){
            throw new IllegalArgumentException(String.format(
                "The Job result is not of the expected type. Expected %s but found %s.", 
                CronBasedKnowledgeTemplateDefinitionJobResult.class.getName(), 
                result == null? "null" : result.getClass().getName()
            ));
        }
        
        //Create the CronBasedCondition fact
        CronBasedKnowledgeTemplateDefinitionJobResult jobResult = (CronBasedKnowledgeTemplateDefinitionJobResult)result;
        CronBasedCondition fact = new CronBasedCondition(jobResult.getTimestamp(), toType(jobResult.getCondition()));

        //Notify the life-cycle session.
        kb.notify(jobResult.getKtdId(), fact);
    }
    
    private CronBasedCondition.Type toType(CronBasedKnowledgeTemplateDefinitionJob.Condition condition){
        switch (condition){
            case START:
                return CronBasedCondition.Type.START_CONDITION;
            case SHUTDOWN:
                return CronBasedCondition.Type.SHUTDOWN_CONDITION;
            default:
                return CronBasedCondition.Type.OTHER;
        }
    }
    
}
