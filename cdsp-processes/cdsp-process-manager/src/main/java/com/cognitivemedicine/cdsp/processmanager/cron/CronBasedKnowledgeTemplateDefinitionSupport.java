/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron;

import org.quartz.CronScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.impl.KnowledgeTemplateLifeCycleKB;

/**
 * This class in in charge of the coordination of all the required features and
 * tasks to support CRON based KTDs.
 *
 * @author esteban
 */
public class CronBasedKnowledgeTemplateDefinitionSupport {

    private final Scheduler scheduler;
    private final KnowledgeTemplateLifeCycleKB kb;
    private ConditionExpressionGenerator expressionGenerator;

    public CronBasedKnowledgeTemplateDefinitionSupport(Scheduler scheduler, KnowledgeTemplateLifeCycleKB kb, ConditionExpressionGenerator expressionGenerator) {
        this.scheduler = scheduler;
        this.kb = kb;
        this.expressionGenerator = expressionGenerator;
    }

    /**
     * Register the listener in the scheduler that will process the results of
     * the jobs created for the CRON based KTDs.
     *
     */
    public void configureScheduler() {
        try {
            //Register the listener in the scheduler that will process the results
            //of the jobs created for the CRON based KTDs.
            scheduler.getListenerManager().addJobListener(
                new CronBasedKnowledgeTemplateDefinitionJobListener(kb),
                (key) -> CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP.equals(key.getGroup())
            );
        } catch (SchedulerException ex) {
            throw new IllegalStateException("Exception registering linster to scheduler", ex);
        }
    }

    /**
     * If the provided KTD contains {@link LifeCycleEventCondition.ConditionType#CRON}
     * conditions, this method creates and schedules the necessary jobs.
     * 
     * @param ktd 
     */
    public void createKTDJobs(KnowledgeTemplateDefinition ktd) {
        try {

            if (ktd.getStartCondition() != null && ktd.getStartCondition().getConditionType() == LifeCycleEventCondition.ConditionType.CRON) {
                scheduler.scheduleJob(
                    CronBasedKnowledgeTemplateDefinitionJob.createJobDetail(ktd, CronBasedKnowledgeTemplateDefinitionJob.Condition.START),
                    TriggerBuilder.newTrigger()
                        .startNow()
                        .withIdentity(
                            CronBasedKnowledgeTemplateDefinitionJob.getTriggerKey(ktd, CronBasedKnowledgeTemplateDefinitionJob.Condition.START)
                        )
                        .withSchedule(CronScheduleBuilder.cronSchedule(
                                expressionGenerator.generateExpression(ktd.getStartCondition().getCondition().trim())
                        )).build()
                );
            }
            
            if (ktd.getShutdownCondition() != null && ktd.getShutdownCondition().getConditionType() == LifeCycleEventCondition.ConditionType.CRON) {
                scheduler.scheduleJob(
                    CronBasedKnowledgeTemplateDefinitionJob.createJobDetail(ktd, CronBasedKnowledgeTemplateDefinitionJob.Condition.SHUTDOWN),
                    TriggerBuilder.newTrigger()
                        .startNow()
                        .withIdentity(
                            CronBasedKnowledgeTemplateDefinitionJob.getTriggerKey(ktd, CronBasedKnowledgeTemplateDefinitionJob.Condition.SHUTDOWN)
                        )
                        .withSchedule(CronScheduleBuilder.cronSchedule(
                                expressionGenerator.generateExpression(ktd.getShutdownCondition().getCondition().trim())
                        )).build()
                );
            }
        } catch (SchedulerException ex) {
            throw new IllegalStateException("Exception registering linster to scheduler", ex);
        }
    }

    public void removeKTDJobs(KnowledgeTemplateDefinition ktd){
        try {
            if (ktd.getStartCondition() != null && ktd.getStartCondition().getConditionType() == LifeCycleEventCondition.ConditionType.CRON){
                scheduler.unscheduleJob(CronBasedKnowledgeTemplateDefinitionJob.getTriggerKey(ktd, CronBasedKnowledgeTemplateDefinitionJob.Condition.START));
            }
            if (ktd.getShutdownCondition() != null && ktd.getShutdownCondition().getConditionType() == LifeCycleEventCondition.ConditionType.CRON){
                scheduler.unscheduleJob(CronBasedKnowledgeTemplateDefinitionJob.getTriggerKey(ktd, CronBasedKnowledgeTemplateDefinitionJob.Condition.SHUTDOWN));
            }
        } catch (SchedulerException ex) {
            throw new IllegalStateException("Exception unscheduling CRON based KTD's jobs", ex);
        }
    }
}
