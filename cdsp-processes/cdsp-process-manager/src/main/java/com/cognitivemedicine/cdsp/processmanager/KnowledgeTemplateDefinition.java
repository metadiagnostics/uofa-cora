/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager;

import java.util.List;

import com.cognitivemedicine.cdsp.processmanager.impl.ExecutionMode;
import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import java.util.Map;

/**
 * This interface represents the definition of a KnowledgeTemplate.
 * Different implementations of this interface can be used to different runtimes
 * like:
 * <ul>
 *  <li>Local Drools Runtime</li>
 *  <li>Remote Kie-Server</li>
 *  <li>Java-based solution</li>
 *  <li>Other</li>
 * </ul>
 * @author esteban
 */
public interface KnowledgeTemplateDefinition {
    public String getId();
    public String getArtifact();
    public String getVersion();
    public String calculateKTIId(KnowledgeTemplateInitContext context);
    public boolean isRestartable();
    public KnowledgeTemplateInstance createInstance();
    public KnowledgeTemplateInstance recreateInstance(String knowledgeTemplateInstanceId);
    public BuildInfo getBuildInfo();
    public List<String> getLifeCycleRelatedTopics();
    public List<String> getAdditionalImports();
    public LifeCycleEventCondition getStartCondition();
    public LifeCycleEventCondition getShutdownCondition();
    public ExecutionMode getExecutionMode();
    public Map<String, DecisionNode> getDecisionNodes();
}
