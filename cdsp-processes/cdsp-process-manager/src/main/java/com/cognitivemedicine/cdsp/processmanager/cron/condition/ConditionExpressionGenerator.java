/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron.condition;

/**
 * Generates a cron expression from a given input.
 * @author calcacuervo
 *
 */
public interface ConditionExpressionGenerator {

    /**
     * Decides if this generator wants to handle the source.
     * @param source
     * @return true if the generator can handle the source. Otherwise false.
     */
    boolean accepts(String source);

    /**
     * Generates the cron expression for the given source. It is recommended to call {@link ConditionExpressionGenerator#accepts(String)} first,
     * and ONLY invoke this method is this returned true.
     * @param source
     * @return
     */
    String generateExpression(String source);
}
