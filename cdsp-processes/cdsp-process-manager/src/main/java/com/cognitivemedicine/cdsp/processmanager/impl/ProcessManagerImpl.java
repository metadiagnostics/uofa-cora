/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.ProcessManager;
import com.cognitivemedicine.cdsp.processmanager.impl.action.DisposeKnowledgeTemplateInstanceAction;
import com.cognitivemedicine.cdsp.processmanager.impl.exception.KTDAlreadyRegisteredException;
import com.cognitivemedicine.cdsp.processmanager.impl.model.DefaultKTIContextParameters;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNodeExecution;
import com.cognitivemedicine.cdsp.report.model.KTDReportInformation;
import com.cognitivemedicine.fhir.logicaldatatypes.CodingDt;
import java.util.concurrent.CompletableFuture;
import static java.util.stream.Collectors.toList;

/**
 * This class serves as a registry of KnowledgeTemplateDefinitions (KTDs). This
 * class is also in charge of the execution of a KnowledgeTemplateInstance (KTI)
 * life cycle. This class register itself as a subscriber of one or more topics
 * and make any information available there available to the life cycle events
 * hooks of the registered KTDs.
 *
 * @author esteban
 */
public class ProcessManagerImpl implements ProcessManager {

	private final static Logger LOG = LoggerFactory.getLogger(ProcessManagerImpl.class);

	private KnowledgeTemplateLifeCycleKB lifeCycleKB;
	private List<String> dataSourceRegistrationIds;
	private ProcessManagerConfiguration configuration;
	private Date lastExecutionDateBeforeStartup;
	private boolean isInitialized;

	public ProcessManagerImpl(ProcessManagerConfiguration configuration) {
		this(configuration, new HashMap<>());
	}

	public ProcessManagerImpl(ProcessManagerConfiguration configuration,
			Map<String, Object> defaultKTIContextParameters) {
		this.dataSourceRegistrationIds = new ArrayList<>();
		this.configuration = configuration;
		this.lifeCycleKB = new KnowledgeTemplateLifeCycleKB(configuration.getProcessManagerStore(),
				configuration.getScheduler(), new DefaultKTIContextParameters(defaultKTIContextParameters),
				configuration.getConditionGenerator());
	}

	@Override
	public boolean initializationComplete() {
		return this.isInitialized;
	};

	@Override
	public void registerKnowledgeTemplateDefinition(KnowledgeTemplateDefinition knowledgeTemplateDefinition) {
		CompletableFuture.runAsync(() -> {
			try {
				internalRegisterKnowledgeTemplateDefinition(knowledgeTemplateDefinition);
			} catch (KTDAlreadyRegisteredException ex) {
				LOG.warn("KTD Already Registered", ex);
			} catch (Exception e) {
				LOG.error("There was an error registering the following KTD: {}.", knowledgeTemplateDefinition, e);
				// let's try to cleanup if something went wrong.
				try {
					LOG.debug("Trying to cleanup the following KTD: {}", knowledgeTemplateDefinition);
					unregisterKnowledgeTemplateDefinition(knowledgeTemplateDefinition.getId(), true);
				} catch (Exception ex) {
					LOG.error("There was an error cleaning up the following KTD: {}.", knowledgeTemplateDefinition, ex);
				}
			}
		}, configuration.getExecutor());
	}

	@Override
	public void registerKnowledgeTemplateDefinitions(List<KnowledgeTemplateDefinition> knowledgeTemplateDefinitions,
			boolean initializing) {
		List<CompletableFuture<Void>> futureTasks = knowledgeTemplateDefinitions.stream()
				.map(ktd -> CompletableFuture.runAsync(() -> {
					try {
						internalRegisterKnowledgeTemplateDefinition(ktd);
					} catch (Exception e) {
						LOG.error("There was an error registering the following KTD: {}.", ktd, e);
						// let's try to cleanup if something went wrong.
						try {
							LOG.debug("Trying to cleanup the following KTD: {}", ktd);
							unregisterKnowledgeTemplateDefinition(ktd.getId(), true);
						} catch (Exception ex) {
							LOG.error("There was an error cleaning up the following KTD: {}.", ktd, ex);
						}
					}
				}, configuration.getExecutor())).collect(toList());

		// After all the KTDs are deployed, the KB is cleaned up.
		CompletableFuture.allOf(futureTasks.toArray(new CompletableFuture[0])).thenRun(() -> {
			lifeCycleKB.cleanupStore();
			if (initializing) {
				isInitialized = true;
			}
		});
	}

	@Override
	public void unregisterKnowledgeTemplateDefinition(String knowledgeTemplateDefinitionId, boolean disposeInstances) {
		LOG.debug("Unregistering KTD id: {} DisposeInstances: {}", knowledgeTemplateDefinitionId, disposeInstances);
		String artifact = lifeCycleKB.getKnowledgeTemplateDefinition(knowledgeTemplateDefinitionId).getArtifact();
		lifeCycleKB.remove(knowledgeTemplateDefinitionId, disposeInstances, true);
		configuration.getReportService().updateReportField(KTDReportInformation.class.getName(), "ktdId", artifact,
				"finishTime", new Date());
		configuration.getReportService().updateReportField(KTDReportInformation.class.getName(), "ktdId", artifact,
				"ended", true);
	}

	@Override
	public Collection<KnowledgeTemplateDefinition> getRegisteredKnowledgeTemplateDefinitions() {
		return lifeCycleKB.getKnowledgeTemplateDefinitions();
	}

	@Override
	public List<KnowledgeTemplateInstance> getKnowledgeTemplateInstances(String knowledgeTemplateId) {
		return this.lifeCycleKB.getKnowledgeTemplateInstancesByKnowledgeTemplateId(knowledgeTemplateId);
	}

	@Override
	public void dispose() {
		LOG.info("Disposing ProcessManager.");
		for (String dataSourceRegistrationId : this.dataSourceRegistrationIds) {
			try {
				configuration.getMessageSubscriber().unregisterSubscriber(dataSourceRegistrationId);
			} catch (Exception ex) {
				LOG.warn("Exception unregistering from topic with registration id " + dataSourceRegistrationId, ex);
			}
		}

		try {
			configuration.getMessageSubscriber().dispose();
		} catch (Exception ex) {
			LOG.warn("Exception disposing Services Facade", ex);
		}

		try {
			this.lifeCycleKB.dispose();
		} catch (Exception ex) {
			LOG.warn("Exception disposing Life-Cycle Knowledge Base", ex);
		}
		for (KnowledgeTemplateDefinition ktd : lifeCycleKB.getKnowledgeTemplateDefinitions()) {
			String artifact = ktd.getArtifact();
			configuration.getReportService().updateReportField(KTDReportInformation.class.getName(), "ktdId", artifact,
					"finishTime", new Date());
			configuration.getReportService().updateReportField(KTDReportInformation.class.getName(), "ktdId", artifact,
					"ended", true);
		}
	}

	@Override
	public void purgeKnowledgeTemplateDefinitionLifeCycleSession(String knowledgeTemplateDefinitionId) {
		LOG.info("About to purge Life-Cycle Session for KnowledgeTemplateDefinition {}.",
				knowledgeTemplateDefinitionId);
		this.lifeCycleKB.purgeKnowledgeTemplateDefinitionLifeCycleSession(knowledgeTemplateDefinitionId);
	}

	@Override
	public void addKnowledgeTemplateDefinitionInitContextParameter(String knowledgeTemplateDefinitionId, String name,
			Object value) {
		LOG.info("About to add a new Init Context Parameter for KTD {}: {} -> {}.", knowledgeTemplateDefinitionId, name,
				value);
		this.lifeCycleKB.addKnowledgeTemplateDefinitionInitContextParameter(knowledgeTemplateDefinitionId, name, value);
	}

	@Override
	public Map<String, Object> getKnowledgeTemplateDefinitionInitContextParameter(
			String knowledgeTemplateDefinitionId) {
		return this.lifeCycleKB.getKnowledgeTemplateDefinitionInitContextParameter(knowledgeTemplateDefinitionId);
	}

	@Override
	public List<Object> getKnowledgeTemplateDefinitionLifeCycleSessionFacts(String knowledgeTemplateDefinitionId) {
		return this.lifeCycleKB.getKnowledgeTemplateDefinitionLifeCycleSessionFacts(knowledgeTemplateDefinitionId);
	}

	@Override
	public Map<String, DecisionNode> getKnowledgeTemplateDefinitionDecisionNodes(String knowledgeTemplateDefinitionId) {
		return this.lifeCycleKB.getKnowledgeTemplateDefinitionDecisionNodes(knowledgeTemplateDefinitionId);
	}

	@Override
	public Map<String, List<DecisionNodeExecution>> getKnowledgeTemplateInstanceDecisionNodeExecutions(
			String knowledgeTemplateDefinitionId, String knowledgeTemplateInstanceId) {
		return this.lifeCycleKB.getKnowledgeTemplateInstanceDecisionNodeExecutions(knowledgeTemplateDefinitionId,
				knowledgeTemplateInstanceId);
	}

	@Override
	public void terminateKnowledgeTemplateInstance(String knowledgeTemplateInstanceId) {
		LOG.info("About to manually terminate KnowledgeTemplateInstance {}.", knowledgeTemplateInstanceId);
		KnowledgeTemplateDefinition ktd = this.lifeCycleKB
				.getKnowledgeTemplateDefinitionFromKnowledgeTemplateInstanceId(knowledgeTemplateInstanceId);
		this.lifeCycleKB.notify(ktd.getId(), new DisposeKnowledgeTemplateInstanceAction(knowledgeTemplateInstanceId));
	}

	@Override
	public void setLastExecutionDateBeforeStartup(Date lastExecutionDateBeforeStartup) {
		this.lastExecutionDateBeforeStartup = lastExecutionDateBeforeStartup;
	}

	private void internalRegisterKnowledgeTemplateDefinition(KnowledgeTemplateDefinition knowledgeTemplateDefinition) {
		LOG.debug("Registering KTD {} to KnowledgeTemplateLifeCycleKB.", knowledgeTemplateDefinition);
		this.lifeCycleKB.registerNew(knowledgeTemplateDefinition, lastExecutionDateBeforeStartup);

		LOG.debug("KTD {} Build Information:", knowledgeTemplateDefinition);
		LOG.debug("\t{}", knowledgeTemplateDefinition.getBuildInfo());

		// Register any subscriber the KTD requires for its life-cycle events.
		// Please note that this class is not doing any verification whereas
		// a previous registration to a topic already exist. The implementation
		// of ServicesRuntimeFacade must deal with this and avoid unnecessary
		// subscribers.
		if (knowledgeTemplateDefinition.getLifeCycleRelatedTopics() != null) {
			LOG.debug("Registering KTD's life-cycle related topics.");
			for (String lifeCycleRelatedTopic : knowledgeTemplateDefinition.getLifeCycleRelatedTopics()) {
				LOG.debug("Registering to topic {}.", lifeCycleRelatedTopic);
				dataSourceRegistrationIds.add(configuration.getMessageSubscriber()
						.registerSubscriber(lifeCycleRelatedTopic, (Object event) -> {
							lifeCycleKB.notify(knowledgeTemplateDefinition.getId(), event);
						}));
			}
		}

		// report a new ktd started
		KTDReportInformation startReport = new KTDReportInformation(null, new Date(),
				ProcessManagerImpl.class.getName(), new CodingDt("KTD", "START"),
				knowledgeTemplateDefinition.getArtifact(), knowledgeTemplateDefinition.getVersion(), new Date());
		configuration.getReportService().report(startReport);
	}
}
