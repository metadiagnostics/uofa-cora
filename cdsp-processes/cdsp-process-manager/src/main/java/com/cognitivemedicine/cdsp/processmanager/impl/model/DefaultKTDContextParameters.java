/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl.model;

import com.cognitivemedicine.cdsp.processmanager.runtime.InitParameter;
import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class represents parameters that will be sent in the {@link KnowledgeTemplateInitContext}
 * to all the new KTIs of a particular KTD.
 * 
 * The elements of this class will be converted into {@link InitParameter}s before
 * being sent to the new KTI.
 * 
 * @author esteban
 */
public class DefaultKTDContextParameters extends HashMap<String, Object> implements Unpurgeable {
    
    private final String knowledgeTemplateDefinitionId;

    public DefaultKTDContextParameters(String knowledgeTemplateDefinitionId) {
        this(knowledgeTemplateDefinitionId, new HashMap<>());
    }
    
    public DefaultKTDContextParameters(String knowledgeTemplateDefinitionId, Map<String, Object> defaultKTIContextParameters) {
        this.putAll(defaultKTIContextParameters);
        this.knowledgeTemplateDefinitionId = knowledgeTemplateDefinitionId;
    }
    
    public Map<String, InitParameter> toInitParameters(){
        return this.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, e-> new InitParameter(e.getKey(), e.getValue())));
    }

    public String getKnowledgeTemplateDefinitionId() {
        return knowledgeTemplateDefinitionId;
    }
    
}
