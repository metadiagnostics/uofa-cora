/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron.condition;

import java.util.List;

/**
 * A simple composite expression generator which will delegate the generation of the expression to a list of {@link ConditionExpressionGenerator}.
 * It will iterate the list in order, and the first one which accepts the input expression will be in charge of generating it.
 * @author calcacuervo
 *
 */
public class DelegatingExpressionGenerator implements ConditionExpressionGenerator {


    private List<ConditionExpressionGenerator> generators;

    public DelegatingExpressionGenerator(List<ConditionExpressionGenerator> generators) {
        this.generators = generators;
        // add a  default condition generator so that, if no one accepts it, it will return it as it is.
        this.generators.add(new DefaultConditionGenerator());
    }
    /**
     * This will go in order in each generator in the generators list,
     * and it will delegate the generation to the first generator that accepts the expression.
     * At least the last generator should accept it.
     * @param input the source condition to fetch the expression. 
     * @return
     */
    @Override
    public String generateExpression(String input) {
        for (ConditionExpressionGenerator generator : generators) {
            if (generator.accepts(input)) {
                return generator.generateExpression(input);
            }
        }
        throw new IllegalArgumentException("Could not generate condition from given definition: " + input);
    }

    /**
     * This will go in order in each generator in the generators list, and return true if there is some generator
     * that accepts the input expression.
     */
    @Override
    public boolean accepts(String input) {
        for (ConditionExpressionGenerator generator : generators) {
            if (generator.accepts(input)) {
                return true;
            }
        }
        return false;
    }


}
