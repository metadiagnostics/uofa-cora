/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.TriggerKey;

/**
 * This class represents the job that is scheduled for CRON based KTDs.
 * 
 * @author esteban
 */
public class CronBasedKnowledgeTemplateDefinitionJob implements Job {

    public static final String TRIGGER_GROUP = "CRON-BASED-KTD-TRIGGER-GROUP";
    public static final String JOB_GROUP = "CRON-BASED-KTD-JOB-GROUP";
    
    public static final String JOB_DATA_KTD_ID = "ktdId";
    public static final String JOB_DATA_CONDITION = "condition";
    
    public static enum Condition {
        START,
        SHUTDOWN
    }
    
    public static TriggerKey getTriggerKey(KnowledgeTemplateDefinition ktd, Condition condition){
        String name = ktd.getId()+"-"+condition+"-TRIGGER";
        return new TriggerKey(name, TRIGGER_GROUP);
    }
    
    public static JobDetail createJobDetail(KnowledgeTemplateDefinition ktd, Condition condition){
        return JobBuilder.newJob(CronBasedKnowledgeTemplateDefinitionJob.class)
            .withIdentity(new JobKey(ktd.getId()+"-"+condition+"-CRON-JOB", JOB_GROUP))
            .usingJobData(JOB_DATA_KTD_ID, ktd.getId())
            .usingJobData(JOB_DATA_CONDITION, condition.name())
            .build();
    }
    
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String ktdId = context.getMergedJobDataMap().getString(JOB_DATA_KTD_ID);
        Condition condition = Condition.valueOf(context.getMergedJobDataMap().getString(JOB_DATA_CONDITION));
        context.setResult(new CronBasedKnowledgeTemplateDefinitionJobResult(context.getJobRunTime(), ktdId, condition));
    }
    
}
