/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager;

import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNodeExecution;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author esteban
 */
public interface ProcessManager {

    /**
     * This method is used no notify the Process Manager the last time it was successfully used.
     * Implementations of this interface can use this information when, for example, restarting KTIs
     * that were running the last time the PM was running.
     * 
     * @param lastExecutionDateBeforeStartup
     */
    void setLastExecutionDateBeforeStartup(Date lastExecutionDateBeforeStartup);

    /**
     * Registers a new KTD in this process manager instance. After a KTD is registered it could be
     * instantiated by satisfying it's life-cycle-start-condition condition.
     * 
     * @param knowledgeTemplateDefinition The KTD to be registered.
     */
    void registerKnowledgeTemplateDefinition(
            KnowledgeTemplateDefinition knowledgeTemplateDefinition);

    /**
     * Registers a list of KTDs in this process manager instance. After a KTD is registered it could
     * be instantiated by satisfying it's life-cycle-start-condition condition. KTDs from the list
     * that can't be registered should not affect the registration of the other KTDs.
     * 
     * @param knowledgeTemplateDefinitions The list of KTDs to be registered.
     * @param initialize Set to true if being called from initialization of the Process Manager
     */
    void registerKnowledgeTemplateDefinitions(
            List<KnowledgeTemplateDefinition> knowledgeTemplateDefinitions, boolean initializing);

    /**
     * Unregisters a previously registered KTD from this process manager instance. This method can
     * optionally dispose any KTI that was instantiated from the KTD being disposed.
     * 
     * @param knowledgeTemplateDefinitionId The id of the KTD to be disposed.
     * @param disposeInstances Whether any KTI instantiated from the KTD being disposed should also
     *        being disposed.
     */
    void unregisterKnowledgeTemplateDefinition(String knowledgeTemplateDefinitionId,
            boolean disposeInstances);

    /**
     * Lists all the registered KTDs in this process manager instance.
     * 
     * @return the collection of registered KTDs in this process manager instance.
     */
    Collection<KnowledgeTemplateDefinition> getRegisteredKnowledgeTemplateDefinitions();

    /**
     * This method cleans up the Life-Cycle session associated to a KTD. The Life-Cycle session of a
     * KTD is where all the information coming from the life-cycle-topics is stored.
     * 
     * @param knowledgeTemplateDefinitionId The id of the KTD.
     */
    void purgeKnowledgeTemplateDefinitionLifeCycleSession(String knowledgeTemplateDefinitionId);

    /**
     * This method can be used to add parameters that are going to be passed to the KTI's
     * {@link KnowledgeTemplateInitContext} when a new instance is created. Different KTDs could
     * have different init parameters for their KTIs.
     * 
     * @param knowledgeTemplateDefinitionId
     * @param name
     * @param value
     */
    void addKnowledgeTemplateDefinitionInitContextParameter(String knowledgeTemplateDefinitionId,
            String name, Object value);

    /**
     * Returns the Map of Parameters that are going to be passed to the KTI's
     * {@link KnowledgeTemplateInitContext} when a new instance is created.
     * 
     * @param knowledgeTemplateDefinitionId
     * @return
     */
    Map<String, Object> getKnowledgeTemplateDefinitionInitContextParameter(
            String knowledgeTemplateDefinitionId);


    /**
     * Returns the current list of facts in the KTD's life-cycle session.
     * 
     * @param knowledgeTemplateInstanceId
     * @return
     */
    List<Object> getKnowledgeTemplateDefinitionLifeCycleSessionFacts(
            String knowledgeTemplateInstanceId);

    /**
     * Returns all the {@link DecisionNode DecisionNodes} for a KTD.
     * 
     * @param knowledgeTemplateDefinitionId
     * @return
     */
    Map<String, DecisionNode> getKnowledgeTemplateDefinitionDecisionNodes(
            String knowledgeTemplateDefinitionId);

    /**
     * Return all the {@link DecisionNodeExecution DecisionNodeExecutions} of a particular KTI.
     * 
     * @param knowledgeTemplateDefinitionId
     * @param knowledgeTemplateInstanceId
     * @return
     */
    Map<String, List<DecisionNodeExecution>> getKnowledgeTemplateInstanceDecisionNodeExecutions(
            String knowledgeTemplateDefinitionId, String knowledgeTemplateInstanceId);

    /**
     * Lists all the KTIs for a specific KTD.
     * 
     * @param knowledgeTemplateId The id of the KTD.
     * @return The collection of KTIs associated to the specified KTD.
     */
    Collection<KnowledgeTemplateInstance> getKnowledgeTemplateInstances(String knowledgeTemplateId);

    /**
     * Programmatically terminates a KTI. A KTI is normally terminated when its
     * life-cycle-shutdown-condition is met, but it could also be programmatically terminated if
     * needed.
     * 
     * @param knowledgeTemplateInstanceId The id of the KTI to be terminated.
     */
    void terminateKnowledgeTemplateInstance(String knowledgeTemplateInstanceId);

    /**
     * Disposes this process manager instance.
     */
    void dispose();

    /**
     * Returns true if all KTD's specified in the /classes/config-sample/ktds.config are deployed.
     * 
     */
    boolean initializationComplete();


}
