/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron.condition;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cognitivemedicine.config.utils.ConfigUtils;

/**
 * This generator looks the cron expression using config utils. It will expect the source expression to be in the following format:
 * CFG_UTILS:<<contextName>>:<<key>>
 * Where context name is the name of the config utils context, and key is the property to find in the given context.
 * @author calcacuervo
 *
 */
public class ConfigUtilsConditionGenerator implements ConditionExpressionGenerator {

    private static final String DEFAULT_REGEX = "CFG_UTILS\\:(.*)\\:(.*)";
    private static final Pattern PATTERN = Pattern.compile(DEFAULT_REGEX);

    @Override
    public boolean accepts(String source) {
        return source.matches(DEFAULT_REGEX);
    }

    @Override
    public String generateExpression(String source) {
        Matcher matcher = PATTERN.matcher(source);
        if(matcher.find()) {
            String contextName = matcher.group(1);
            String property = matcher.group(2);
            ConfigUtils config = ConfigUtils.getInstance(contextName);
            return config.getString(property);
        }
        throw new IllegalArgumentException("Could not parse expression");

    }
}
