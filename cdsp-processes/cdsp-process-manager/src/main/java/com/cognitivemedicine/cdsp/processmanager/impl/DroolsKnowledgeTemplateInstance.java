/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNodeExecution;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DroolsDecisionNodeAgendaEventListener;
import java.util.List;
import java.util.Map;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.TimedRuleExectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link KnowledgeTemplateInstance} based on a local
 * Drools' KieBase.
 * @author esteban
 */
public class DroolsKnowledgeTemplateInstance implements KnowledgeTemplateInstance {

    private final static Logger LOG = LoggerFactory.getLogger(DroolsKnowledgeTemplateInstance.class);
    
    public final static String KSESSION_KTI_ID_GLOBAL_NAME = "ktiId";
    
    private final KieBase kBase;
    private KieSession ksession;
    private DroolsDecisionNodeAgendaEventListener decisionNodeAgendaEventListener;
    private KnowledgeTemplateInitContext initContext;
    
    private String id;

    public DroolsKnowledgeTemplateInstance(KieBase kBase) {
        this.kBase = kBase;
    }

    @Override
    public void init(String id, KnowledgeTemplateInitContext initContext) {
        
        this.id = id;
        
        this.initContext = initContext;
        
        ksession = createSession(id);
        
        ksession.insert(this.initContext);
        
        ksession.fireAllRules();
        ksession.getAgenda().getAgendaGroup("PM-INIT").setFocus();
        ksession.fireAllRules();
        
    }

    @Override
    public void start() {
        ksession.getAgenda().getAgendaGroup("PM-START").setFocus();
        ksession.fireAllRules();
    }
    
    @Override
    public void restart(KnowledgeTemplateInitContext context) {
        ksession = createSession(id);

        initContext = context;
        if (initContext != null){
            ksession.insert(context);
        }
        
        ksession.fireAllRules();
        ksession.getAgenda().getAgendaGroup("PM-INIT").setFocus();
        ksession.fireAllRules();
        
        
        ksession.getAgenda().getAgendaGroup("PM-RESTART").setFocus();
        ksession.fireAllRules();
        
        ksession.getAgenda().getAgendaGroup("PM-POST-RESTART").setFocus();
        ksession.fireAllRules();
    }
    
    @Override
    public void shutdown() {
        ksession.getAgenda().getAgendaGroup("PM-SHUTDOWN").setFocus();
        ksession.fireAllRules();
    }
    
    @Override
    public void dispose() {
        ksession.getAgenda().getAgendaGroup("PM-DISPOSE").setFocus();
        ksession.fireAllRules();
        ksession.dispose();
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public KnowledgeTemplateInitContext getInitContext(){
        return this.initContext;
    }

    @Override
    public Map<String, List<DecisionNodeExecution>> getDecisionNodeExecutions() {
        return decisionNodeAgendaEventListener.getNodeExecutions();
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    private KieSession createSession(String id){
        KieSessionConfiguration ksconf = KieServices.Factory.get().newKieSessionConfiguration();
        ksconf.setOption(TimedRuleExectionOption.YES);

        KieSession session = kBase.newKieSession(ksconf, null);
        
        try{
            session.setGlobal(KSESSION_KTI_ID_GLOBAL_NAME, this.id);
        } catch(Exception e){
            LOG.warn(String.format("Error setting value for global {}. This KTI will not know what it's id is ({}).", KSESSION_KTI_ID_GLOBAL_NAME, this.id), e);
        }
        
        //TODO: if the session is being restored, this event listener will
        //be initialized whithout the previous execution history.
        //An idea is to pass the sessionId to the listener and make the listener
        //responsible about persisting and restoring any data.
        decisionNodeAgendaEventListener = new DroolsDecisionNodeAgendaEventListener();
        session.addEventListener(decisionNodeAgendaEventListener);
        
        return session;
    }
}
