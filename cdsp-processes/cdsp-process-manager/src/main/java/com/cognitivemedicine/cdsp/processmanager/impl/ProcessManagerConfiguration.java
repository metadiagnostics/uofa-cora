/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import java.util.concurrent.Executor;

import org.quartz.Scheduler;

import com.cognitivemedicine.cdsp.messaging.api.MessageSubscriber;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.report.api.ReportService;

/**
 *
 * @author esteban
 */
public class ProcessManagerConfiguration {
    private final MessageSubscriber messageSubscriber;
    private final ProcessManagerStore processManagerStore;
    private final ReportService reportService;
    private final Executor executor;
    private final Scheduler scheduler;
    private ConditionExpressionGenerator conditionGenerator;

    protected ProcessManagerConfiguration(MessageSubscriber messageSubscriber, ProcessManagerStore processManagerStore, ReportService reportService, Executor executor, Scheduler scheduler, ConditionExpressionGenerator conditionExpressionGenerator) {
        this.messageSubscriber = messageSubscriber;
        this.processManagerStore = processManagerStore;
        this.reportService = reportService;
        this.executor = executor;
        this.scheduler = scheduler;
        this.conditionGenerator = conditionExpressionGenerator;
    }

    public MessageSubscriber getMessageSubscriber() {
        return messageSubscriber;
    }

    public ProcessManagerStore getProcessManagerStore() {
        return processManagerStore;
    }

    public ReportService getReportService() {
        return reportService;
    }

    public Executor getExecutor() {
        return executor;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }
    
    public ConditionExpressionGenerator getConditionGenerator() {
        return conditionGenerator;
    }
    
}
