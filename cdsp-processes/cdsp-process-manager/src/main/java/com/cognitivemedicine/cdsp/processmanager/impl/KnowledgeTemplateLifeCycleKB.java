/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import static java.util.stream.Collectors.toSet;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.kie.api.runtime.rule.Row;
import org.kie.api.runtime.rule.ViewChangedEventListener;
import org.kie.internal.utils.KieHelper;
import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STErrorListener;
import org.stringtemplate.v4.STGroupString;
import org.stringtemplate.v4.misc.STMessage;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.cron.CronBasedKnowledgeTemplateDefinitionSupport;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.impl.action.DisposeKnowledgeTemplateInstancesAction;
import com.cognitivemedicine.cdsp.processmanager.impl.action.PurgeLifeCycleSessionAction;
import com.cognitivemedicine.cdsp.processmanager.impl.exception.KTDAlreadyRegisteredException;
import com.cognitivemedicine.cdsp.processmanager.impl.model.DefaultKTDContextParameters;
import com.cognitivemedicine.cdsp.processmanager.impl.model.DefaultKTIContextParameters;
import com.cognitivemedicine.cdsp.processmanager.runtime.InitParameter;
import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateDefinitionLifeCycleFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceReference;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNodeExecution;
import java.util.Optional;

/**
 * This class contains individual Knowledge Bases for each KTD. These Knowledge
 * Bases will be in charge of the execution of any life cycle hook.
 *
 * @author esteban
 */
public class KnowledgeTemplateLifeCycleKB {

    private final static Logger LOG = LoggerFactory.getLogger(KnowledgeTemplateLifeCycleKB.class);
    private final static Logger SESSION_LOG = LoggerFactory.getLogger("life-cycle-session");

    private final static char LIFE_CYCLE_TEMPLATE_VAR_DELIMITER = '~';

    private final KnowledgeTemplateDefinitionRegistry registry = new KnowledgeTemplateDefinitionRegistry();
    private final ProcessManagerStore processManagerStore;
    private final CronBasedKnowledgeTemplateDefinitionSupport cronSupport;

    /**
     * These parameters will be sent to the {@link KnowledgeTemplateInitContext}
     * of all the KTIs initiated by this object (no matter what KTD they belong
     * to).
     */
    private final DefaultKTIContextParameters defaultKTIContextParameters;

    /**
     * These parameters will be sent to the {@link KnowledgeTemplateInitContext}
     * of all the KTIs initiated by this object (no matter what KTD they belong
     * to).
     */
    private final Map<String, DefaultKTDContextParameters> defaultContextParametersByKnowledgeTemplateInstanceId = new HashMap<>();

    public KnowledgeTemplateLifeCycleKB(ProcessManagerStore processManagerStore, Scheduler scheduler, DefaultKTIContextParameters defaultKTIContextParameters, ConditionExpressionGenerator conditionGenerator) {
        this.processManagerStore = processManagerStore;
        this.defaultKTIContextParameters = defaultKTIContextParameters;
        
        this.cronSupport = new CronBasedKnowledgeTemplateDefinitionSupport(scheduler, this, conditionGenerator);
        this.cronSupport.configureScheduler();
    }

    public void registerNew(KnowledgeTemplateDefinition ktd, Date lastExecutionDateBeforeStartup) {
        if (registry.exists(ktd.getId())) {
            throw new KTDAlreadyRegisteredException("KTD with the given id {} already exists. Please unregister it first.");
        }
        KieHelper helper = new KieHelper();
        helper.addContent(this.createLifeCycleRules(ktd), ResourceType.DRL);

        Results results = helper.verify();
        results.getMessages().stream().forEach((message) -> {
            LOG.error(">> Message ({}): {}", message.getLevel(), message.getText());
        });

        if (results.hasMessages(Message.Level.ERROR)) {
            throw new IllegalStateException("There are errors in the Life Cycle KB.");
        }

        //Create an empty DefaultKTDContextParameters for the future instances 
        //of this KTD.
        DefaultKTDContextParameters defaultKTDContextParameters = new DefaultKTDContextParameters(ktd.getId());
        this.defaultContextParametersByKnowledgeTemplateInstanceId.put(ktd.getId(), defaultKTDContextParameters);

        KieSession ksession = helper.build().newKieSession();
        ksession.setGlobal("kb", this);
        ksession.setGlobal("ktd", ktd);
        ksession.setGlobal("LOG", SESSION_LOG);

        registry.addNewKnowledgeTemplateDefinition(ktd, ksession);

        //Register a Live Query Listener to keep track of the generated 
        //KnowledgeTemplateInstances
        ksession.openLiveQuery("getKnowledgeTemplateInstances", new Object[0], new ViewChangedEventListener() {
            @Override
            public void rowInserted(Row row) {
                KnowledgeTemplateInstance kti = (KnowledgeTemplateInstance) row.get("$kti");
                LOG.debug("New KnowledgeTemplateInstance detected: {}", kti);

                registry.addNewKnowledgeTemplateInstance(ktd.getId(), kti);

                if (ktd.isRestartable()) {
                    KnowledgeTemplateInstanceReference ktir = new KnowledgeTemplateInstanceReference(ktd.getId() + "/" + kti.getId());
                    ktir.setKtdId(ktd.getId());
                    ktir.setKtiId(kti.getId());
                    ktir.setKnowledgeTemplateInitContext(kti.getInitContext());

                    //Persist the KTI Reference
                    processManagerStore.saveOrUpdate(ktir);
                }

                if (LOG.isDebugEnabled()) {
                    LOG.debug("Number of active KTIs for KTD {} is {}", ktd.getId(), registry.getKnowledgeTemplateInstances(ktd.getId()).size());
                }
            }

            @Override
            public void rowDeleted(Row row) {
                KnowledgeTemplateInstance kti = (KnowledgeTemplateInstance) row.get("$kti");
                LOG.debug("KnowledgeTemplateInstance {} is no longer active.", kti);

                registry.removeKnowledgeTemplateInstance(ktd.getId(), kti.getId());
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Number of active KTIs for KTD {} is {}", ktd.getId(), registry.getKnowledgeTemplateInstances(ktd.getId()).size());
                }

                if (ktd.isRestartable()) {
                    processManagerStore.deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateInstanceId(kti.getId());
                }
            }

            @Override
            public void rowUpdated(Row row) {
            }
        });

        ksession.insert(this.defaultKTIContextParameters);
        ksession.insert(defaultKTDContextParameters);
        ksession.fireAllRules();

        //Restore any restartable KTI for this KTD
        if (ktd.isRestartable()) {
            for (KnowledgeTemplateInstanceReference ktiRef : processManagerStore.listKnowledgeTemplateInstanceReferences(ktd.getId())) {

                KnowledgeTemplateInstance kti = ktd.recreateInstance(ktiRef.getKtiId());

                //Add the information about the lastExecutionDateBeforeStartup 
                //to the KnowledgeTemplateInitContext
                ktiRef.getKnowledgeTemplateInitContext().getTriggerFacts().put(
                    InitParameter.InitParameterConstants.LAST_EXECUTION_DATE_BEFORE_STARTUP,
                    new InitParameter(InitParameter.InitParameterConstants.LAST_EXECUTION_DATE_BEFORE_STARTUP, lastExecutionDateBeforeStartup)
                );

                //Trigger the restart pahse of the KTI
                kti.restart(ktiRef.getKnowledgeTemplateInitContext());

                ksession.insert(kti);
                ksession.fireAllRules();
            }

            //Restore any fact from the life-cycle session that was previoulsy
            //persisted.
            //TODO: pre-existing facts can cause re-instantiate (and immediately terminate) 
            //KTIs that were no longer running. Before inserting these facts
            //we need to prevent new KTI instances to be created.
            for (KnowledgeTemplateDefinitionLifeCycleFact factWrapper : processManagerStore.listKnowledgeTemplateDefinitionLifeCycleFacts(ktd.getId())) {
                ksession.insert(factWrapper.getFact());
                ksession.fireAllRules();
            }
        }
        
        //Register any necessary CRON job
        cronSupport.createKTDJobs(ktd);

    }

    public void remove(String knowledgeTemplateDefinitionId, boolean disposeInstances, boolean cleanupStore) {
        LOG.debug("Removing KDT {}", knowledgeTemplateDefinitionId);
        LOG.debug(disposeInstances ? "All the KTIs of this KTD will be disposed." : "KTIs of this KTD will NOT be disposed");

        KieSession ksession = registry.getSession(knowledgeTemplateDefinitionId);

        if (disposeInstances) {
            //Give the life cycle session the chance to dispose the corresponding
            //KnowledgeTemplateInstances.
            ksession.insert(new DisposeKnowledgeTemplateInstancesAction());
            ksession.fireAllRules();
            ksession.dispose();
        }

        KnowledgeTemplateDefinition ktdToBeRemoved = registry.getKnowledgeTemplateDefinitionById(knowledgeTemplateDefinitionId);
        registry.removeKnowledgeTemplateDefinition(knowledgeTemplateDefinitionId);

        if (cleanupStore) {
            LOG.debug("Cleaning up KTD's Life Cycle Facts");
            processManagerStore.deleteKnowledgeTemplateDefinitionLifeCycleFacts(knowledgeTemplateDefinitionId);
        }
        
        cronSupport.removeKTDJobs(ktdToBeRemoved);
    }

    /**
     * Broadcasts the passed Object to all the life-cycle sessions of all the
     * registered KTDs.
     *
     * @param o the Object to be notified.
     */
    public void notify(Object o) {
        for (KnowledgeTemplateDefinition ktd : registry.getKnowledgeTemplateDefinitions()) {
            this.notify(ktd.getId(), o);
        }
    }

    /**
     * Inserts the passed Object into the life-cycle sessions of the specified
     * KTD.
     *
     * @param knowledgeTemplateDefinitionId the KTD id.
     * @param o the Object to be notified.
     */
    public void notify(String knowledgeTemplateDefinitionId, Object o) {
        try {
            LOG.debug("Notifying Life Cycle Session for KTD {} about {}", knowledgeTemplateDefinitionId, o);

            KnowledgeTemplateDefinition ktd = registry.getKnowledgeTemplateDefinitionById(knowledgeTemplateDefinitionId);
            KieSession ksession = registry.getSession(knowledgeTemplateDefinitionId);

            if (Collection.class.isAssignableFrom(o.getClass())) {
                LOG.debug("The incoming object is a Collection. The individual elemnts will be notified.");
                Collection col = (Collection) o;
                for (Object object : col) {
                    insertFactIntoLifeCycleSession(ktd, ksession, object);
                }
            } else {
                insertFactIntoLifeCycleSession(ktd, ksession, o);
            }
            ksession.fireAllRules();
        } catch (Exception e) {
            LOG.error("Error notifying life cycle Session about Object " + o + ".", e);
        }
    }

    public List<KnowledgeTemplateDefinition> getKnowledgeTemplateDefinitions() {
        return registry.getKnowledgeTemplateDefinitions();
    }

    public KnowledgeTemplateDefinition getKnowledgeTemplateDefinition(String knowledgeTemplateDefinitionId) {
        return registry.getKnowledgeTemplateDefinitionById(knowledgeTemplateDefinitionId);
    }

    public List<KnowledgeTemplateInstance> getKnowledgeTemplateInstancesByKnowledgeTemplateId(String knowledgeTemplateId) {
        return registry.getKnowledgeTemplateInstances(knowledgeTemplateId);
    }

    public KnowledgeTemplateDefinition getKnowledgeTemplateDefinitionFromKnowledgeTemplateInstanceId(String knowledgeTemplateInstanceId) {
        return registry.getKnowledgeTemplateDefinitionFromKnowledgeTemplateInstanceId(knowledgeTemplateInstanceId);
    }

    public List<Object> getKnowledgeTemplateDefinitionLifeCycleSessionFacts(String knowledgeTemplateDefinitionId) {
        KieSession ksession = registry.getSession(knowledgeTemplateDefinitionId);
        if (ksession == null) {
            LOG.warn("Unknown KTD with id '" + knowledgeTemplateDefinitionId + "'");
            return Collections.EMPTY_LIST;
        }

        QueryResults queryResults = ksession.getQueryResults("listFacts");
        if (queryResults == null || queryResults.size() <= 0) {
            LOG.warn("We didn't receive any result back from query.");
            return Collections.EMPTY_LIST;
        }

        QueryResultsRow row = queryResults.iterator().next();
        List result = (List) row.get("$result");

        return result;
    }
    
    public Map<String, DecisionNode> getKnowledgeTemplateDefinitionDecisionNodes(String knowledgeTemplateDefinitionId) {
        return registry.getKnowledgeTemplateDefinitionById(knowledgeTemplateDefinitionId).getDecisionNodes();
    }
    
    public Map<String, List<DecisionNodeExecution>> getKnowledgeTemplateInstanceDecisionNodeExecutions(String knowledgeTemplateDefinitionId, String knowledgeTemplateInstanceId) {
        Optional<KnowledgeTemplateInstance> kti = registry.getKnowledgeTemplateInstances(knowledgeTemplateDefinitionId).stream()
            .filter(k -> k.getId().equals(knowledgeTemplateInstanceId))
            .findFirst();
        
        if (!kti.isPresent()){
            LOG.warn("Unknown KTI with id '" + knowledgeTemplateInstanceId + "'");
            return Collections.EMPTY_MAP;
        }
        
        return kti.get().getDecisionNodeExecutions();
    }

    public void addKnowledgeTemplateDefinitionInitContextParameter(String knowledgeTemplateDefinitionId, String name, Object value) {
        this.defaultContextParametersByKnowledgeTemplateInstanceId.get(knowledgeTemplateDefinitionId).put(name, value);
    }

    public Map<String, Object> getKnowledgeTemplateDefinitionInitContextParameter(String knowledgeTemplateDefinitionId) {
        return defaultContextParametersByKnowledgeTemplateInstanceId.get(knowledgeTemplateDefinitionId).entrySet()
            .stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue()));
    }

    public void purgeKnowledgeTemplateDefinitionLifeCycleSession(String knowledgeTemplateDefinitionId) {
        KnowledgeTemplateDefinition ktd = registry.getKnowledgeTemplateDefinitionById(knowledgeTemplateDefinitionId);

        notify(knowledgeTemplateDefinitionId, new PurgeLifeCycleSessionAction());

        //if the KTD is restartable, cleanup any life cycle fact present in the store
        if (ktd.isRestartable()) {
            processManagerStore.deleteKnowledgeTemplateDefinitionLifeCycleFacts(knowledgeTemplateDefinitionId);
        }
    }

    /**
     * Removes any element from the store that is associated with KTDs that are
     * not currently registered in this KB.
     */
    public void cleanupStore() {

        //Collect the information from the store
        Set<String> ktdsInStore = processManagerStore.listKTDIdsWithKnowledgeTemplateDefinitionLifeCycleFacts();
        ktdsInStore.addAll(processManagerStore.listKTDIdsWithKnowledgeTemplateInstanceReferences());

        //Collect the information about the registered KTDs
        Set<String> existingKTDs = registry.getKnowledgeTemplateDefinitions().stream()
            .map(ktd -> ktd.getId())
            .collect(toSet());

        //Remove from the original set the KTDs that are already registered
        ktdsInStore.removeAll(existingKTDs);

        //Delete from the store the KTDs that are not currently registered.
        for (String ktdId : ktdsInStore) {
            LOG.debug("There are entries in the Process Manager Store for a KTD with id {} that is no longer registered. These entries will be deleted from the store.", ktdId);
            processManagerStore.deleteKnowledgeTemplateDefinitionLifeCycleFacts(ktdId);
            processManagerStore.deleteKnowledgeTemplateInstanceReferenceByKnowledgeTemplateDefinitionId(ktdId);
        }
    }

    /**
     * This method disposes any Life Cycle Session it contains and any KTI that
     * was created.
     *
     */
    public void dispose() {
        LOG.info("Disposing KnowledgeTemplateLifeCycleKB");

        //Cleanup sessions first
        this.notify(new PurgeLifeCycleSessionAction());

        //Dispose the KTD
        for (KnowledgeTemplateDefinition ktd : registry.getKnowledgeTemplateDefinitions()) {
            LOG.debug("Disposing Life Cycle KB for KTD {}", ktd.getId());
            this.remove(ktd.getId(), false, false);
        }
    }

    private void insertFactIntoLifeCycleSession(KnowledgeTemplateDefinition knowledgeTemplateDefinition, KieSession ksession, Object object) {
        ksession.insert(object);
        if (knowledgeTemplateDefinition.isRestartable()) {
            KnowledgeTemplateDefinitionLifeCycleFact fact = new KnowledgeTemplateDefinitionLifeCycleFact();
            fact.setKtdId(knowledgeTemplateDefinition.getId());
            fact.setFact(object);
            processManagerStore.saveOrUpdate(fact);
        }
    }

    private String createLifeCycleRules(KnowledgeTemplateDefinition ktd) {

        //If any of the conditions of the KTD is of type CRON, an ad-hoc
        //condition is generated.
        String originalStartCondition = null;
        if (ktd.getStartCondition() != null && ktd.getStartCondition().getConditionType() == LifeCycleEventCondition.ConditionType.CRON) {
            String condition = applyTemplate("/templates/cron-based-start-conditions.template", "conditions", null);
            LOG.debug("Auto-generated Start Condition: {}", condition);
            originalStartCondition = ktd.getStartCondition().getCondition();
            ktd.getStartCondition().setCondition(condition);
        }
        String originalShutdownCondition = null;
        if (ktd.getShutdownCondition()!= null && ktd.getShutdownCondition().getConditionType() == LifeCycleEventCondition.ConditionType.CRON) {
            String condition = applyTemplate("/templates/cron-based-shutdown-conditions.template", "conditions", null);
            LOG.debug("Auto-generated Shutdown Condition: {}", condition);
            originalShutdownCondition = ktd.getShutdownCondition().getCondition();
            ktd.getShutdownCondition().setCondition(condition);
        }

        //Create the life-cycle rules
        Map<String, Object> params = new HashMap<>();
        params.put("ktd", ktd);
        String drl = applyTemplate("/templates/life-cycle-rules.template", "rules", params);
        
        LOG.debug("DRL: {}", drl);
        
        if (originalStartCondition != null){
            ktd.getStartCondition().setCondition(originalStartCondition);
        }
        if (originalShutdownCondition != null){
            ktd.getShutdownCondition().setCondition(originalShutdownCondition);
        }
        
        return drl;
    }

    private String applyTemplate(String templateFile, String templateName, Map<String, Object> params) {
        try {
            STGroupString stg = new STGroupString(
                "kb-template",
                IOUtils.toString(KnowledgeTemplateLifeCycleKB.class.getResourceAsStream(templateFile), Charset.forName("UTF-8")),
                LIFE_CYCLE_TEMPLATE_VAR_DELIMITER,
                LIFE_CYCLE_TEMPLATE_VAR_DELIMITER
            );

            List<String> errors = new ArrayList<>();
            stg.setListener(new STErrorListener() {
                @Override
                public void compileTimeError(STMessage msg) {
                    errors.add(msg.toString());
                }

                @Override
                public void runTimeError(STMessage msg) {
                    errors.add(msg.toString());
                }

                @Override
                public void IOError(STMessage msg) {
                    errors.add(msg.toString());
                }

                @Override
                public void internalError(STMessage msg) {
                    errors.add(msg.toString());
                }
            });

            ST rulesTemplate = stg.getInstanceOf(templateName);
            if (params != null){
                params.entrySet().forEach(e -> rulesTemplate.add(e.getKey(), e.getValue()));
            }
            String drl = rulesTemplate.render();

            if (!errors.isEmpty()) {
                String errorMessages = errors.stream().collect(Collectors.joining("\n"));
                throw new IllegalStateException("Error processing life-cycle rules:\n" + errorMessages);
            }
            
            return drl;
        } catch (IOException ex) {
            throw new IllegalStateException("Exception creating the life-cycle Rules", ex);
        }
    }

}
