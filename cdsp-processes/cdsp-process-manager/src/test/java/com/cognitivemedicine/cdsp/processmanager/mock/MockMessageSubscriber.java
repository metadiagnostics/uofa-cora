/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.mock;

import com.cognitivemedicine.cdsp.messaging.api.MessageListener;
import com.cognitivemedicine.cdsp.messaging.api.MessageSubscriber;
import com.cognitivemedicine.fhir.logicalmodel.CommunicationRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MessageSubscriber implementation meant to be used in unit tests. 
 * This class is completely self-contained. It keeps all the registrations and 
 * messages in memory.
 * @author esteban
 */
public class MockMessageSubscriber implements MessageSubscriber {

    private final Map<String, List<MessageListener<Object>>> listenersByTopic = new HashMap<>();
    private final List<CommunicationRequest> sentAlerts = new ArrayList<>();

	@Override
	public String registerSubscriber(String discriminator, MessageListener listener) {
		List<MessageListener<Object>> listeners = this.listenersByTopic.get(discriminator);
        if (listeners == null){
            listeners = new ArrayList<>();
            this.listenersByTopic.put(discriminator, listeners);
        }
        listeners.add(listener);
        
        return "not-important";
	}

	@Override
	public void unregisterSubscriber(String registrationId) {
        //not supported
	}

	
    public void publishData(String discriminator, Serializable data) {
        List<MessageListener<Object>> listeners = this.listenersByTopic.get(discriminator);
        if (listeners != null){
            for (MessageListener<Object> listener : listeners) {
                listener.onMessage(data);
            }
        }
    }

    @Override
    public void dispose() {
		//do nothing
    }
    
}
