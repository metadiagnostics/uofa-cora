/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.mock;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.cognitivemedicine.cdsp.processmanager.BuildInfo;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.impl.ExecutionMode;
import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import java.util.Map;

/**
 * Mock KnowledgeTemplateDefinition implementation meant to be used in tests.
 * @author esteban
 */
public class MockKnowledgeTemplateDefinition implements KnowledgeTemplateDefinition{

    private final String id;
    private final MockKnowledgeTemplateInstance knowledgeTemplateInstance;
    private boolean restartable;
    private final LifeCycleEventCondition startCondition;
    private final LifeCycleEventCondition shutdownCondition;
    private ExecutionMode executionMode = ExecutionMode.STATEFULL;
    private final List<String> lifeCycleRelatedTopics;
    
    public MockKnowledgeTemplateDefinition(String id, MockKnowledgeTemplateInstance knowledgeTemplateInstance, List<String> lifeCycleRelatedTopics, LifeCycleEventCondition startCondition, LifeCycleEventCondition shutdownCondition, ExecutionMode executionMode) {
        this.id = id;
        this.knowledgeTemplateInstance = knowledgeTemplateInstance;
        this.lifeCycleRelatedTopics = lifeCycleRelatedTopics;
        this.startCondition = startCondition;
        this.shutdownCondition = shutdownCondition;
        this.executionMode = executionMode;
    }
    
    @Override
    public String getId() {
        return this.id;
    }
    
    @Override
    public String getVersion() {
        return "1-SNAPSHOT";
    }
    
    @Override
    public String getArtifact() {
        return null;
    }

    @Override
    public String calculateKTIId(KnowledgeTemplateInitContext context) {
        return UUID.randomUUID().toString();
    }

    @Override
    public boolean isRestartable() {
        return this.restartable;
    }
    
    @Override
    public KnowledgeTemplateInstance createInstance() {
        return this.knowledgeTemplateInstance;
    }

    @Override
    public KnowledgeTemplateInstance recreateInstance(String knowledgeTemplateInstanceId) {
        this.knowledgeTemplateInstance.setId(knowledgeTemplateInstanceId);
        return this.knowledgeTemplateInstance;
    }

    @Override
    public BuildInfo getBuildInfo() {
        return null;
    }
    
    @Override
    public LifeCycleEventCondition getStartCondition() {
        return this.startCondition;
    }

    @Override
    public LifeCycleEventCondition getShutdownCondition() {
        return this.shutdownCondition;
    }
    
    @Override
    public List<String> getLifeCycleRelatedTopics() {
        return this.lifeCycleRelatedTopics;
    }

    @Override
    public List<String> getAdditionalImports() {
        return Collections.EMPTY_LIST;
    }

    public void setRestartable(boolean restartable) {
        this.restartable = restartable;
    }

    @Override
    public ExecutionMode getExecutionMode() {
        return executionMode;
    }

    @Override
    public Map<String, DecisionNode> getDecisionNodes() {
        return Collections.EMPTY_MAP;
    }
    
    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }
    
}
