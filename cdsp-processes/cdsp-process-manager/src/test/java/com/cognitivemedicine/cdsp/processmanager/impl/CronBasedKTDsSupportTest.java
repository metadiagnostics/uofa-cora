/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.listeners.JobListenerSupport;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.ProcessManager;
import com.cognitivemedicine.cdsp.processmanager.cron.CronBasedKnowledgeTemplateDefinitionJob;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.DefaultConditionGenerator;
import com.cognitivemedicine.cdsp.processmanager.mock.MockExecutor;
import com.cognitivemedicine.cdsp.processmanager.mock.MockKnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.mock.MockKnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.mock.MockMessageSubscriber;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.impl.mongo.MongoProcessManagerStore;
import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.mongo.ReportServiceImpl;
import com.github.fakemongo.Fongo;

/**
 *
 * @author esteban
 */
public class CronBasedKTDsSupportTest {

    private static Fongo fongo;

    private MockMessageSubscriber messageSubscriber;
    private ProcessManagerStore pmStore;
    private ProcessManager pm;
    private ReportService reportService;
    private Scheduler scheduler;
    private ConditionExpressionGenerator expressionGenerator;

    @BeforeClass
    public static void doBeforeClass() {
        fongo = new Fongo("Fake Mongo DB");
    }

    @Before
    public void doBefore() throws SchedulerException {

        this.messageSubscriber = new MockMessageSubscriber();
        this.pmStore = new MongoProcessManagerStore(fongo.getMongo());
        this.reportService = new ReportServiceImpl(fongo.getMongo());
        this.scheduler = StdSchedulerFactory.getDefaultScheduler();
        this.expressionGenerator = new DefaultConditionGenerator();
        this.pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(pmStore)
                .setReportService(reportService)
                .setScheduler(scheduler)
                .setConditionExpressionGenerator(expressionGenerator)
                .createProcessManagerConfiguration()
        );

        scheduler.start();
        this.pmStore.cleanup();
    }

    @After
    public void doAfter() throws SchedulerException {
        scheduler.shutdown();
        this.pm.dispose();
    }

    /**
     * This test demonstrates how a KTD with a CRON based start condition is
     * scheduled and how the execution of the scheduled job instantiates a KTI.
     * This test is not waiting for the CRON trigger to actually fire. It is
     * just checking that a job is scheduled and that the result of the
     * execution of the scheduled job is a KTI.
     *
     * @throws InterruptedException
     * @throws SchedulerException
     */
    @Test
    public void doStartConditionTest() throws InterruptedException, SchedulerException {

        String ktdId = "1";
        String cronExpression = "0 0/5 * 1/1 * ? *";

        //No Trigger nor Job in the scheduler.
        assertThat(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).isEmpty(), is(true));
        assertThat(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).isEmpty(), is(true));

        //Register a KTD with a CRON based start condition
        KnowledgeTemplateInstance kti = this.createAndRegisterKnowledgeTemplateDefinition(ktdId, cronExpression, null, null);

        //No KTI so far
        assertThat(pm.getKnowledgeTemplateInstances(ktdId).isEmpty(), is(true));

        //We now have 1 Trigger and 1 Job
        assertThat(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).size(), is(1));
        Trigger trigger = scheduler.getTrigger(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).iterator().next());
        assertThat(trigger instanceof CronTrigger, is(true));
        assertThat(((CronTrigger) trigger).getCronExpression(), is(cronExpression));
        assertThat(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).size(), is(1));

        //Manually execute the Job and check that there is a new KTI
        JobDetail job = scheduler.getJobDetail(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).iterator().next());
        triggerJob(job.getKey());
        assertThat(pm.getKnowledgeTemplateInstances(ktdId).size(), is(1));

    }

    @Test
    public void doStartAndShutdownConditionsTest() throws InterruptedException, SchedulerException {

        String ktdId = "1";
        String cronExpression1 = "0 0/5 * 1/1 * ? *";
        String cronExpression2 = "0 0/10 * 1/1 * ? *";

        //No Trigger nor Job in the scheduler.
        assertThat(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).isEmpty(), is(true));
        assertThat(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).isEmpty(), is(true));

        //Register a KTD with CRON based start and shutdown conditions
        KnowledgeTemplateInstance kti = this.createAndRegisterKnowledgeTemplateDefinition(ktdId, cronExpression1, cronExpression2, null);

        //No KTI so far
        assertThat(pm.getKnowledgeTemplateInstances(ktdId).isEmpty(), is(true));

        //We now have 2 Trigger and 2 Job
        Set<TriggerKey> triggerKeys = scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP));
        assertThat(triggerKeys.size(), is(2));
        List<String> cronExpressions = triggerKeys.stream()
            .map(k -> {
                try {
                    return ((CronTrigger) scheduler.getTrigger(k)).getCronExpression();
                } catch (SchedulerException ex) {
                    throw new IllegalStateException(ex);
                }
            })
            .sorted()
            .collect(toList());

        assertThat(cronExpressions, CoreMatchers.hasItems(cronExpression1, cronExpression2));

        Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP));
        assertThat(jobKeys.size(), is(2));
        
        
        //Manually execute the start Job and check that there is a new KTI
        JobDetail startJob = scheduler.getJobDetail(
            jobKeys.stream()
                .filter(jk -> jk.getName().contains("START"))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No START Job found"))
        );
        triggerJob(startJob.getKey());
        assertThat(pm.getKnowledgeTemplateInstances(ktdId).size(), is(1));
        
        //Manually execute the shutdown Job and check that there no longer a KTI
        JobDetail shutdownJob = scheduler.getJobDetail(
            jobKeys.stream()
                .filter(jk -> jk.getName().contains("SHUTDOWN"))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No SHUTDOWN Job found"))
        );
        triggerJob(shutdownJob.getKey());
        assertThat(pm.getKnowledgeTemplateInstances(ktdId).size(), is(0));
        
    }
    
    /**
     * This test demonstrates how a KTD with a CRON based start condition is
     * scheduled with a "stateless" execution mode.
     *
     * @throws InterruptedException
     * @throws SchedulerException
     */
    @Test
    public void doStartConditionTestWithExecutionMode() throws InterruptedException, SchedulerException {

        String ktdId = "1";
        String cronExpression = "0 0/5 * 1/1 * ? *";

        //No Trigger nor Job in the scheduler.
        assertThat(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).isEmpty(), is(true));
        assertThat(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).isEmpty(), is(true));

        //Register a KTD with a CRON based start condition
        KnowledgeTemplateInstance kti = this.createAndRegisterKnowledgeTemplateDefinition(ktdId, cronExpression, null, ExecutionMode.STATELESS);

        //No KTI so far
        assertThat(pm.getKnowledgeTemplateInstances(ktdId).isEmpty(), is(true));

        //We now have 1 Trigger and 1 Job
        assertThat(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).size(), is(1));
        Trigger trigger = scheduler.getTrigger(scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.TRIGGER_GROUP)).iterator().next());
        assertThat(trigger instanceof CronTrigger, is(true));
        assertThat(((CronTrigger) trigger).getCronExpression(), is(cronExpression));
        assertThat(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).size(), is(1));

        //Manually execute the Job and check that there is a new KTI
        JobDetail job = scheduler.getJobDetail(scheduler.getJobKeys(GroupMatcher.jobGroupEquals(CronBasedKnowledgeTemplateDefinitionJob.JOB_GROUP)).iterator().next());
        triggerJob(job.getKey());

    }
    

    /**
     * Synchronously executes a Job given its key.
     *
     * @param key
     * @throws SchedulerException
     * @throws InterruptedException
     */
    private void triggerJob(JobKey key) throws SchedulerException, InterruptedException {

        CountDownLatch latch = new CountDownLatch(1);

        scheduler.getListenerManager().addJobListener(new JobListenerSupport() {
            @Override
            public String getName() {
                return key.getName() + "-" + key.getGroup();
            }

            @Override
            public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
                latch.countDown();
            }

        }, (k) -> k.equals(key));

        scheduler.triggerJob(key);

        if (!latch.await(3, TimeUnit.SECONDS)) {
            throw new IllegalStateException("Timeout waiting for the Job to be executed");
        }

        scheduler.getListenerManager().removeJobListener(key.getName() + "-" + key.getGroup());
    }

    private KnowledgeTemplateInstance createAndRegisterKnowledgeTemplateDefinition(String id, String startCondition, String shutdownCondition, ExecutionMode executionMode) {

        int ktdsBefore = this.pm.getRegisteredKnowledgeTemplateDefinitions().size();

        LifeCycleEventConditionImpl startConditionEvent = new LifeCycleEventConditionImpl(startCondition);
        startConditionEvent.setConditionType(LifeCycleEventCondition.ConditionType.CRON);

        LifeCycleEventConditionImpl shutdownConditionEvent = null;
        if (shutdownCondition != null) {
            shutdownConditionEvent = new LifeCycleEventConditionImpl(shutdownCondition);
            shutdownConditionEvent.setConditionType(LifeCycleEventCondition.ConditionType.CRON);
        }
        
        MockKnowledgeTemplateInstance kti = new MockKnowledgeTemplateInstance();
        KnowledgeTemplateDefinition ktd;
        if(executionMode != null) {
            ktd = new MockKnowledgeTemplateDefinition(id, kti, null, startConditionEvent, shutdownConditionEvent, executionMode);
        } else {
            ktd = new MockKnowledgeTemplateDefinition(id, kti, null, startConditionEvent, shutdownConditionEvent, null);
        }

        this.pm.registerKnowledgeTemplateDefinition(ktd);

        assertThat(this.pm.getRegisteredKnowledgeTemplateDefinitions().size(), is(ktdsBefore + 1));

        return kti;
    }
}
