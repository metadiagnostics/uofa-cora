/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;

/**
 *
 * @author esteban
 */
public class DroolsKnowledgeTemplateDeploymentDescriptorTest {

    @Test
    public void testDeserialization() throws JAXBException {
        DroolsKnowledgeTemplateDeploymentDescriptor dd = parseDeploymentDescriptor("fixed-cron-dd.xml");
        assertThat(dd.getStartCondition(), not(nullValue()));
        assertThat(dd.getStartCondition().getConditionType(), is(LifeCycleEventCondition.ConditionType.CRON));
        assertThat(dd.getShutdownCondition(), not(nullValue()));
        assertThat(dd.getShutdownCondition().getConditionType(), is(LifeCycleEventCondition.ConditionType.DRL));
    }
    
    @Test
    public void testStatelessDeserialization() throws JAXBException {
        DroolsKnowledgeTemplateDeploymentDescriptor dd = parseDeploymentDescriptor("exec-mode-dd.xml");
        assertThat(dd.getStartCondition(), not(nullValue()));
        assertThat(dd.getStartCondition().getConditionType(), is(LifeCycleEventCondition.ConditionType.CRON));
        assertThat(dd.getShutdownCondition(), not(nullValue()));
        assertThat(dd.getShutdownCondition().getConditionType(), is(LifeCycleEventCondition.ConditionType.DRL));
        assertThat(dd.getExecutionMode(), is(ExecutionMode.STATELESS));
    }

    private DroolsKnowledgeTemplateDeploymentDescriptor parseDeploymentDescriptor(String fileName) throws JAXBException {
        InputStream resourceAsStream = DroolsKnowledgeTemplateDeploymentDescriptorTest.class.getResourceAsStream("/deployment-descriptors/" + fileName);

        JAXBContext context = JAXBContext.newInstance(DroolsKnowledgeTemplateDeploymentDescriptor.class);
        Unmarshaller u = context.createUnmarshaller();

        DroolsKnowledgeTemplateDeploymentDescriptor descriptor = (DroolsKnowledgeTemplateDeploymentDescriptor) u.unmarshal(resourceAsStream);

        return descriptor;
    }
}
