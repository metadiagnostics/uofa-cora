/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.util;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.deployment.DeployRequest;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.repository.RepositoryPolicy;
import org.eclipse.aether.resolution.ArtifactDescriptorRequest;
import org.eclipse.aether.resolution.ArtifactDescriptorResult;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.eclipse.aether.util.artifact.SubArtifact;
import org.kie.api.builder.ReleaseId;

/**
 *
 * @author esteban
 */
public class MavenHelper {
    
    public static void deployArtifactAndDependencies(ReleaseId releaseId, File jar, File pom) throws Exception {

        DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
        locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
        locator.addService(TransporterFactory.class, FileTransporterFactory.class);
        locator.addService(TransporterFactory.class, HttpTransporterFactory.class);

        locator.setErrorHandler(new DefaultServiceLocator.ErrorHandler() {
            @Override
            public void serviceCreationFailed(Class<?> type, Class<?> impl, Throwable exception) {
                exception.printStackTrace();
            }
        });

        RepositorySystem system = locator.getService(RepositorySystem.class);

        DefaultRepositorySystemSession session = MavenRepositorySystemUtils.newSession();
        LocalRepository localRepo = new LocalRepository("target/local-repo");
        session.setLocalRepositoryManager(system.newLocalRepositoryManager(session, localRepo));

        Artifact jarArtifact = new DefaultArtifact(releaseId.getGroupId(), releaseId.getArtifactId(), "jar", releaseId.getVersion());
        jarArtifact = jarArtifact.setFile(jar);

        Artifact pomArtifact = new SubArtifact(jarArtifact, "", "pom");
        pomArtifact = pomArtifact.setFile(pom);

        RemoteRepository distRepo
            = new RemoteRepository.Builder("org.eclipse.aether.examples", "default",
                new File("target/dist-repo").toURI().toString()).build();

        DeployRequest deployRequest = new DeployRequest();
        deployRequest.addArtifact(jarArtifact).addArtifact(pomArtifact);
        deployRequest.setRepository(distRepo);

        system.deploy(session, deployRequest);

        String homeDir = System.getProperty("user.home");
        URL localM2URL = new File(homeDir, ".m2/repository").toURI().toURL();
        DefaultRepositorySystemSession localM2Session = MavenRepositorySystemUtils.newSession();
        LocalRepository localM2Repo = new LocalRepository(new File(localM2URL.toURI()));
        localM2Session.setLocalRepositoryManager(system.newLocalRepositoryManager(localM2Session, localM2Repo));

        List<RemoteRepository> remotes = new ArrayList<>();
        remotes.add(new RemoteRepository.Builder("local", "default", localM2URL.toString()).setSnapshotPolicy(new RepositoryPolicy(true, "daily", "warn")).build());
        remotes.add(distRepo);

        ArtifactDescriptorRequest descriptorRequest = new ArtifactDescriptorRequest();
        descriptorRequest.setArtifact(pomArtifact);
        descriptorRequest.setRepositories(remotes);
        ArtifactDescriptorResult descriptorResult = system.readArtifactDescriptor(session, descriptorRequest);

        CollectRequest collectRequest = new CollectRequest();
        collectRequest.setRootArtifact(descriptorResult.getArtifact());
        collectRequest.setDependencies(descriptorResult.getDependencies());
        collectRequest.setManagedDependencies(descriptorResult.getManagedDependencies());
        collectRequest.setRepositories(descriptorRequest.getRepositories());

        DependencyResult resolvedDependencies = system.resolveDependencies(localM2Session, new DependencyRequest(collectRequest, null));

        DeployRequest dependencyDeployRequest = new DeployRequest();
        dependencyDeployRequest.setRepository(distRepo);
        for (ArtifactResult artifactResult : resolvedDependencies.getArtifactResults()) {
            dependencyDeployRequest.addArtifact(artifactResult.getArtifact());
        }
        system.deploy(session, dependencyDeployRequest);
    }
    
}
