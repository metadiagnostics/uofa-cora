/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.ProcessManager;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.DefaultConditionGenerator;
import com.cognitivemedicine.cdsp.processmanager.mock.MockExecutor;
import com.cognitivemedicine.cdsp.processmanager.mock.MockKnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.mock.MockKnowledgeTemplateDefinition2;
import com.cognitivemedicine.cdsp.processmanager.mock.MockKnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.mock.MockMessageSubscriber;
import com.cognitivemedicine.cdsp.processmanager.runtime.InitParameter;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.impl.mongo.MongoProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateDefinitionLifeCycleFact;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.model.KnowledgeTemplateInstanceReference;
import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.mongo.ReportServiceImpl;
import com.github.fakemongo.Fongo;

/**
 *
 * @author esteban
 */
public class ProcessManagerImplTest {

    private static Fongo fongo;
    
    private MockMessageSubscriber messageSubscriber;
    private ProcessManagerStore pmStore;
    private ProcessManager pm;
    private ReportService reportService;
    private Scheduler scheduler;
    private ConditionExpressionGenerator expressionGenerator;
    
    @BeforeClass
    public static void doBeforeClass(){
        fongo = new Fongo("Fake Mongo DB");
    }
    
    
    @Before
    public void doBefore() throws SchedulerException{
        this.messageSubscriber = new MockMessageSubscriber();
        this.pmStore = new MongoProcessManagerStore(fongo.getMongo());
        this.reportService = new ReportServiceImpl(fongo.getMongo());
        this.scheduler = StdSchedulerFactory.getDefaultScheduler();
        this.expressionGenerator = new DefaultConditionGenerator();
        this.pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(pmStore)
                .setReportService(reportService)
                .setScheduler(scheduler)
                .setConditionExpressionGenerator(expressionGenerator)
                .createProcessManagerConfiguration()
        );
        
        this.pmStore.cleanup();
    }
    
    @After
    public void doAfter(){
        this.pm.dispose();
    }
    
    
    /**
     *
     * Shows how KnowledgeTemplateDefinitions (KTDs) are registered in the
     * ProcessManagerImpl and how new KnowledgeTemplateInstances (KTIs) are
     * created from them when the start condition is matched.
     */
    @Test
    public void knowledgeTemplateInstantiationTest() {

        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");

        //Create 2 KTDs that will be triggered by the presence of a String 
        //with a particular value ("KTD-1" and "KTD-2") in the topic "ALL_ADT".
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition("1", false, lifeCycleRelatedTopics, "String(this == \"KTD-1\")", null);
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition("2", false, lifeCycleRelatedTopics, "String(this == \"KTD-2\")", null);

        //So far, we don't have any KTI.
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").isEmpty(), is(true));
        verify(kti1, Mockito.times(0)).init(anyString(), anyObject());
        verify(kti1, Mockito.times(0)).start();
        verify(kti2, Mockito.times(0)).init(anyString(), anyObject());
        verify(kti2, Mockito.times(0)).start();

        //A String with value "KTD-2" is then published into "ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-2");

        //This should have created a new KTI based on ktd2
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").iterator().next(), is(kti2));

        verify(kti1, Mockito.times(0)).init(anyString(), anyObject());
        verify(kti1, Mockito.times(0)).start();
        verify(kti2, Mockito.times(1)).init(anyString(), anyObject());
        verify(kti2, Mockito.times(1)).start();

    }

    @Test
    public void unregisterKnowledgeTemplateDefinitionAndDisposeInstancesTest() {
        this.unregisterKnowledgeTemplateDefinitionTest(true);
    }

    @Test
    public void unregisterKnowledgeTemplateDefinitionWithoutDisposingInstancesTest() {
        this.unregisterKnowledgeTemplateDefinitionTest(false);
    }

    @Test
    public void testPurgeKTDLifeCycleSession() {
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        //Create a KTD that will be triggered by the presence of 2 Strings 
        //with a particular value: "KTD-1-A" and "KTD-1-B".
        KnowledgeTemplateInstance kti = this.createAndRegisterKnowledgeTemplateDefinition("1", false, lifeCycleRelatedTopics, "String(this == \"KTD-1-A\")\nString(this == \"KTD-1-B\")", null);
        
        //So far, we don't have any KTI.
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        
        //A String with value "KTD-1-A" is then published into "ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        //Still no KTI
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        
        //At this point, we will purge the KTD's life-cycle session.
        this.pm.purgeKnowledgeTemplateDefinitionLifeCycleSession("1");
        
        //A String with value "KTD-1-B" is then published into "ALL_ADT" topic.
        //If the KTD's life-cycle session was not purged, this String should 
        //create a new KTI.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-B");
        
        //But it didn't
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        
        //Just to check that everything is running as expected, a "KTD-1-A"
        //is again published
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        //now we do have a KTI
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("1").iterator().next(), is(kti));
    }
    
    /**
     * This test checks that purging a KTD's life-cycle session doesn't
     * affects others KTD's sessions.
     */
    @Test
    public void testPurgeKTDLifeCycleSession2() {
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        //Create 2 KTD that will be triggered by the presence of 2 Strings. 
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition("1", false, lifeCycleRelatedTopics, "String(this == \"KTD-1-A\")\nString(this == \"KTD-1-B\")", null);
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition("2", false, lifeCycleRelatedTopics, "String(this == \"KTD-2-A\")\nString(this == \"KTD-2-B\")", null);
        
        //So far, we don't have any KTI.
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        
        //A String with value "KTD-1-A" is then published into "ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        //A String with value "KTD-2-A" is then published into "ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-2-A");
        
        //Still no KTI
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        
        //At this point, we will purge the KTD 1 life-cycle session.
        this.pm.purgeKnowledgeTemplateDefinitionLifeCycleSession("1");
        
        //A String with value "KTD-1-B" is then published into "ALL_ADT" topic.
        //If the KTD's life-cycle session was not purged, this String should 
        //create a new KTI.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-B");
        //A String with value "KTD-2-B" is then published into "ALL_ADT" topic.
        //This will cause the instantiation of a new KTI for KTD #2
        messageSubscriber.publishData("ALL_ADT", "KTD-2-B");
        
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").iterator().next(), is(kti2));
        
        //Just to check that everything is running as expected, a "KTD-1-A"
        //is again published
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        //now we do have a KTI
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("1").iterator().next(), is(kti1));
    }
    
    @Test
    public void testKnowledgeTemplateInstanceTermination(){
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");

        //Create a KTD that will be triggered by the presence of a String 
        //with a particular value ("KTD-1-[n]" ).
        List<KnowledgeTemplateInstance> ktis = this.createAndRegisterKnowledgeTemplateDefinition("1", lifeCycleRelatedTopics, "String(this matches \"KTD\\\\-1\\\\-[0-9]\")", 2);
        
        //So far, we don't have any KTI.
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));

        //2 Strings with value "KTD-1-1" and "KTD-1-2" are then published into 
        //"ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-1");
        messageSubscriber.publishData("ALL_ADT", "KTD-1-2");

        //This should have created 2 new KTIs based on ktd1
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(2));
        assertThat(this.pm.getKnowledgeTemplateInstances("1"), hasItems(ktis.get(0), ktis.get(1)));
        
        //Let's now terminate KTI #1
        this.pm.terminateKnowledgeTemplateInstance(ktis.get(0).getId());
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("1").iterator().next(), is(ktis.get(1)));
        
        //Verify that the right KTI was disposed.
        verify(ktis.get(0), Mockito.times(1)).dispose();
        verify(ktis.get(0), Mockito.times(1)).shutdown();
        verify(ktis.get(1), Mockito.times(0)).dispose();
        verify(ktis.get(1), Mockito.times(0)).shutdown();
    }
    
    @Test
    public void testDefaultKTIParameters(){
        //We need a customized PM for this test.
        Map<String, Object> defaultKTIParams = new HashMap<>();
        defaultKTIParams.put("PARAM-1", "VALUE-1");
        defaultKTIParams.put("PARAM-2", "VALUE-2");

        this.pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(this.pmStore)
                .setReportService(this.reportService)
                .setScheduler(scheduler)
                .setConditionExpressionGenerator(expressionGenerator)
                .createProcessManagerConfiguration(),
            defaultKTIParams
        );
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        //Create 2 KTD that will be triggered by the presence of a Strings. 
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition("1", false, lifeCycleRelatedTopics, "String(this == \"KTD-1-A\")", null);
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition("2", false, lifeCycleRelatedTopics, "String(this == \"KTD-2-A\")", null);
        
        //2 Strings with value "KTD-1-A" and "KTD-2-A" are then published into 
        //"ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        messageSubscriber.publishData("ALL_ADT", "KTD-2-A");
        
        //This should have created 2 new KTIs based on the 2 KTDs
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("1").iterator().next(), is(kti1));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").iterator().next(), is(kti2));
        
        //All the KTIs, no matter the KTD, should have received the default
        //init parameters.
        assertThat(((InitParameter)kti1.getInitContext().getTriggerFacts().get("PARAM-1")).getValue(), is("VALUE-1"));
        assertThat(((InitParameter)kti1.getInitContext().getTriggerFacts().get("PARAM-2")).getValue(), is("VALUE-2"));
        assertThat(((InitParameter)kti2.getInitContext().getTriggerFacts().get("PARAM-1")).getValue(), is("VALUE-1"));
        assertThat(((InitParameter)kti2.getInitContext().getTriggerFacts().get("PARAM-2")).getValue(), is("VALUE-2"));
        
    }
    
    @Test
    public void testDefaultKTDParameters(){
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        //Create 2 KTD that will be triggered by the presence of a Strings. 
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition("1", false, lifeCycleRelatedTopics, "String(this == \"KTD-1-A\")", null);
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition("2", false, lifeCycleRelatedTopics, "String(this == \"KTD-2-A\")", null);
        
        //Add 2 different Init Parameters for each KTD
        this.pm.addKnowledgeTemplateDefinitionInitContextParameter("1", "param-1-1", "value-A");
        this.pm.addKnowledgeTemplateDefinitionInitContextParameter("1", "param-1-2", "value-B");
        this.pm.addKnowledgeTemplateDefinitionInitContextParameter("2", "param-2-1", "value-C");
        this.pm.addKnowledgeTemplateDefinitionInitContextParameter("2", "param-2-2", "value-D");
        
        //2 Strings with value "KTD-1-A" and "KTD-2-A" are then published into 
        //"ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        messageSubscriber.publishData("ALL_ADT", "KTD-2-A");
        
        //This should have created 2 new KTIs based on the 2 KTDs
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("1").iterator().next(), is(kti1));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").size(), is(1));
        assertThat(this.pm.getKnowledgeTemplateInstances("2").iterator().next(), is(kti2));
        
        //Each KTI should have received only the parameters associated to it
        //KTD.
        assertThat(((InitParameter)kti1.getInitContext().getTriggerFacts().get("param-1-1")).getValue(), is("value-A"));
        assertThat(((InitParameter)kti1.getInitContext().getTriggerFacts().get("param-1-2")).getValue(), is("value-B"));
        assertThat(((InitParameter)kti2.getInitContext().getTriggerFacts().get("param-2-1")).getValue(), is("value-C"));
        assertThat(((InitParameter)kti2.getInitContext().getTriggerFacts().get("param-2-2")).getValue(), is("value-D"));
        
        
        this.pm.purgeKnowledgeTemplateDefinitionLifeCycleSession("1");
        
        //Let's now ovverride one of the parameters of KTD1 and add a new one
        this.pm.addKnowledgeTemplateDefinitionInitContextParameter("1", "param-1-1", "value-A-NEW");
        this.pm.addKnowledgeTemplateDefinitionInitContextParameter("1", "param-1-3", "value-E");
        
        //Create a new KTI for KTD1
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        //This should have created 1 new KTIs based on the KTD1
        assertThat(this.pm.getKnowledgeTemplateInstances("1").size(), is(1));
        KnowledgeTemplateInstance newKTI = this.pm.getKnowledgeTemplateInstances("1").iterator().next();
        
        //The new KTI should have received the updated parameters
        assertThat(((InitParameter)newKTI.getInitContext().getTriggerFacts().get("param-1-1")).getValue(), is("value-A-NEW"));
        assertThat(((InitParameter)newKTI.getInitContext().getTriggerFacts().get("param-1-2")).getValue(), is("value-B"));
        assertThat(((InitParameter)newKTI.getInitContext().getTriggerFacts().get("param-1-3")).getValue(), is("value-E"));
    }
    
    /**
     * 2 KTDs are deployed (one restartable and the other not). When a KTI
     * for a restartable KTD is instantiated, the ProcessManagerStore is used
     * to persist its reference. When the KTI is disposed, the reference is
     * removed from the store.
     */
    @Test
    public void testRestartableKTIPersistence(){
        
        String restartableKTDId = "1";
        String nonRestartableKTDId = "2";
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        assertThat(pmStore.getCount(), is(0));
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition(nonRestartableKTDId, false, lifeCycleRelatedTopics, "$p: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$p\"] == \"KTD-1-A\")");
        assertThat(pmStore.getCount(), is(0));
        
        //2 KTIs are instantiated
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        assertThat(pm.getKnowledgeTemplateInstances(restartableKTDId).size(), is(1));
        assertThat(pm.getKnowledgeTemplateInstances(nonRestartableKTDId).size(), is(1));
        assertThat(pmStore.listByType(KnowledgeTemplateInstanceReference.class).size(), is(1));
        
        List<KnowledgeTemplateInstanceReference> ktiReferences = pmStore.listKnowledgeTemplateInstanceReferences(restartableKTDId);
        assertThat(ktiReferences.size(), is(1));
        assertThat(ktiReferences.get(0).getKtiId(), is(kti1.getId()));
        
        
        //The 2 KTIs are disposed
        messageSubscriber.publishData("ALL_ADT", "KTD-1-T");
        assertThat(pm.getKnowledgeTemplateInstances(restartableKTDId).size(), is(0));
        assertThat(pm.getKnowledgeTemplateInstances(nonRestartableKTDId).size(), is(0));
        assertThat(pmStore.listByType(KnowledgeTemplateInstanceReference.class).size(), is(0));

        //Life Cycle facts live longer than KTIs
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(2));
        
    }
    
    /**
     * Tests how a KTI from a restartable KTD is restored when the Process Manager
     * is recreated.
     */
    @Test
    public void testRestartableKTI(){
        
        String restartableKTDId = "1";
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        
        //1 KTI is instantiated
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");

        //The KTI is persisted in the store
        assertThat(pmStore.listByType(KnowledgeTemplateInstanceReference.class).size(), is(1));
        List<KnowledgeTemplateInstanceReference> ktiReferences = pmStore.listKnowledgeTemplateInstanceReferences(restartableKTDId);
        assertThat(ktiReferences.size(), is(1));
        assertThat(ktiReferences.get(0).getKtiId(), is(kti1.getId()));
        
        //Recreate the PM (the key here is to use the same store)
        this.pm.dispose();
        this.pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(this.pmStore)
                .setReportService(this.reportService)
                .setScheduler(scheduler)
                .setConditionExpressionGenerator(expressionGenerator)
                .createProcessManagerConfiguration()
        );
        
        //Life Cycle facts survive a dispose of the PM
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(1));
        
        //Register the old KTD again (This will restart any existing KTI in the store)
        this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        
        //We still have 1 kti reference in the store (no duplicated entries were created)
        ktiReferences = pmStore.listKnowledgeTemplateInstanceReferences(restartableKTDId);
        assertThat(ktiReferences.size(), is(1));
        assertThat(ktiReferences.get(0).getKtiId(), is(kti1.getId()));
        
        //The KTI was restarted in the PM
        assertThat(pm.getKnowledgeTemplateInstances(restartableKTDId).size(), is(1));
    }
    
    
    /**
     * 2 restartable KTDs are deployed. The life cycle facts for these KTDs
     * are retained in the store until a manual purge for the KTDs life
     * cycle facts is requested.
     */
    @Test
    public void testLifeCycleKTDsPurgedFromStore(){
        
        String restartableKTDId1 = "1";
        String restartableKTDId2 = "2";
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        assertThat(pmStore.getCount(), is(0));
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId1, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId2, true, lifeCycleRelatedTopics, "$p: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$p\"] == \"KTD-1-A\")");
        assertThat(pmStore.getCount(), is(0));
        
        //2 KTIs are instantiated
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        //Each KTD keeps a reference. 
        //@TODO: this could be improved so the object is persisted once and
        //only a reference is kept.
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(2));
        
        //The 2 KTIs are disposed
        messageSubscriber.publishData("ALL_ADT", "KTD-1-T");

        //Life Cycle facts live longer than KTIs
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(4));
        
        
        //Purge the life cycle session of the first KTD
        pm.purgeKnowledgeTemplateDefinitionLifeCycleSession(restartableKTDId1);
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(2));
        
        //Purge the life cycle session of the second KTD
        pm.purgeKnowledgeTemplateDefinitionLifeCycleSession(restartableKTDId2);
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(0));
        
    }
    
    /**
     * 2 restartable KTDs are deployed. The life cycle facts for these KTDs
     * are retained in the store until they are unregistered from PM.
     */
    @Test
    public void testLifeCycleKTDsPurgedFromStoreWhenKTDIsUnregistered(){
        
        String restartableKTDId1 = "1";
        String restartableKTDId2 = "2";
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        assertThat(pmStore.getCount(), is(0));
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId1, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId2, true, lifeCycleRelatedTopics, "$p: String(this == \"KTD-1-A\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$p\"] == \"KTD-1-A\")");
        assertThat(pmStore.getCount(), is(0));
        
        //2 KTIs are instantiated
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        //Each KTD keeps a reference. 
        //@TODO: this could be improved so the object is persisted once and
        //only a reference is kept.
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(2));
        
        //The 2 KTIs are disposed
        messageSubscriber.publishData("ALL_ADT", "KTD-1-T");

        //Life Cycle facts live longer than KTIs
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(4));
        
        
        //Unregister the first KTD
        pm.unregisterKnowledgeTemplateDefinition(restartableKTDId1, true);
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(2));
        
        //Unregister the second KTD
        pm.unregisterKnowledgeTemplateDefinition(restartableKTDId2, true);
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(0));
        
    }
    
    /**
     * 2 KTDs (one restartable and the other don't) are deployed. These KTDs 
     * require 2 facts to be started. One of this facts is restarted, the PM is 
     * then regenerated and the second fact is inserted. The restartable KTD at 
     * this point generates a KTI.
     */
    @Test
    public void testLifeCycleFactsRestored(){
        
        String restartableKTDId = "1";
        String nonRestartableKTDId = "2";
        
        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");
        
        assertThat(pmStore.getCount(), is(0));
        KnowledgeTemplateInstance kti1 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")\nString(this == \"KTD-1-B\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        KnowledgeTemplateInstance kti2 = this.createAndRegisterKnowledgeTemplateDefinition(nonRestartableKTDId, false, lifeCycleRelatedTopics, "$p: String(this == \"KTD-1-A\")\nString(this == \"KTD-1-B\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$p\"] == \"KTD-1-A\")");
        assertThat(pmStore.getCount(), is(0));
        
        //The first triggering fact is published
        messageSubscriber.publishData("ALL_ADT", "KTD-1-A");
        
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(1));
        
        //Recreate the PM (the key here is to use the same store)
        this.pm.dispose();
        this.pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(this.pmStore)
                .setReportService(this.reportService)
                .setScheduler(scheduler)
                .setConditionExpressionGenerator(expressionGenerator)
                .createProcessManagerConfiguration()
        );
        
        //Re-register the old KTDs
        kti1 = this.createAndRegisterKnowledgeTemplateDefinition(restartableKTDId, true, lifeCycleRelatedTopics, "$s: String(this == \"KTD-1-A\")\nString(this == \"KTD-1-B\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$s\"] == \"KTD-1-A\")");
        kti2 = this.createAndRegisterKnowledgeTemplateDefinition(nonRestartableKTDId, false, lifeCycleRelatedTopics, "$p: String(this == \"KTD-1-A\")\nString(this == \"KTD-1-B\")", "String(this == \"KTD-1-T\") $kti: KnowledgeTemplateInstance(\n" +
"            initContext.triggerFacts[\"$p\"] == \"KTD-1-A\")");
        
        //Check that the fact that was previously persisted is not persisted 
        //again as part of the restoration process.
        assertThat(pmStore.listByType(KnowledgeTemplateDefinitionLifeCycleFact.class).size(), is(1));
        
        //The second triggering fact is published
        messageSubscriber.publishData("ALL_ADT", "KTD-1-B");
        
        //The restartable KTD started a KTI but the non-restartable didn't
        assertThat(pm.getKnowledgeTemplateInstances(restartableKTDId).size(), is(1));
        assertThat(pm.getKnowledgeTemplateInstances(nonRestartableKTDId).size(), is(0));
        
    }
    
    
    /**
     * This method creates and registers a new KTD in Process Manager. The mocked
     * KTI instance that will be returned by the created KTD is returned by this
     * method.
     * @param restartable
     * @param lifeCycleRelatedTopics
     * @param startCondition
     * @param stopCondition
     * @return 
     */
    private KnowledgeTemplateInstance createAndRegisterKnowledgeTemplateDefinition(String id, boolean restartable, List<String> lifeCycleRelatedTopics, String startCondition, String stopCondition){
        
        int ktdsBefore = this.pm.getRegisteredKnowledgeTemplateDefinitions().size();
        
        LifeCycleEventCondition startConditionEvent =  startCondition == null ? null : new LifeCycleEventConditionImpl(startCondition);
        LifeCycleEventCondition stopConditionEvent = stopCondition == null ? null : new LifeCycleEventConditionImpl(stopCondition);
        MockKnowledgeTemplateInstance kti = spy(new MockKnowledgeTemplateInstance());
        MockKnowledgeTemplateDefinition ktd = new MockKnowledgeTemplateDefinition(id, kti, lifeCycleRelatedTopics, startConditionEvent, stopConditionEvent, null);
        ktd.setRestartable(restartable);
        
        this.pm.registerKnowledgeTemplateDefinition(ktd);
        
        assertThat(this.pm.getRegisteredKnowledgeTemplateDefinitions().size(), is(ktdsBefore+1));
        
        return kti;
    }
    
    /**
     * This method creates and registers a new KTD in Process Manager. The mocked
     * KTI instances that will be returned by the created KTD are returned by this
     * method.
     * @param lifeCycleRelatedTopics
     * @param startCondition
     * @param numberOfKTIs 
     * @return 
     */
    private List<KnowledgeTemplateInstance> createAndRegisterKnowledgeTemplateDefinition(String id, List<String> lifeCycleRelatedTopics, String startCondition, int numberOfKTIs){
        
        int ktdsBefore = this.pm.getRegisteredKnowledgeTemplateDefinitions().size();
        
        LifeCycleEventCondition startConditionEvent = new LifeCycleEventConditionImpl(startCondition);
        
        List<KnowledgeTemplateInstance> ktis = new ArrayList<>();
        for (int i = 0; i < numberOfKTIs; i++) {
            ktis.add(spy(new MockKnowledgeTemplateInstance()));
        }
        KnowledgeTemplateDefinition ktd = new MockKnowledgeTemplateDefinition2(id, ktis, lifeCycleRelatedTopics, startConditionEvent, null);
        
        this.pm.registerKnowledgeTemplateDefinition(ktd);
        
        assertThat(this.pm.getRegisteredKnowledgeTemplateDefinitions().size(), is(ktdsBefore+1));
        
        return ktis;
    }
    
    private void unregisterKnowledgeTemplateDefinitionTest(boolean disposeInstances) {

        List<String> lifeCycleRelatedTopics = Arrays.asList("ALL_ADT");

        //Create a KTD that will be triggered by the presence of a String 
        //with a particular value ("KTD-1" ).
        KnowledgeTemplateInstance kti = this.createAndRegisterKnowledgeTemplateDefinition("1", false, lifeCycleRelatedTopics, "String(this == \"KTD-1\")", null);

        //So far, we don't have any KTI.
        assertThat(this.pm.getKnowledgeTemplateInstances("1").isEmpty(), is(true));

        //A String with value "KTD-1" is then published into "ALL_ADT" topic.
        messageSubscriber.publishData("ALL_ADT", "KTD-1");

        //This should have created a new KTI based on ktd1
        assertThat(this.pm.getKnowledgeTemplateInstances("1").iterator().next(), is(kti));

        //unregister the KTD
        this.pm.unregisterKnowledgeTemplateDefinition("1", disposeInstances);
        verify(kti, Mockito.times(disposeInstances ? 1 : 0)).dispose();
        assertThat(this.pm.getRegisteredKnowledgeTemplateDefinitions().size(), is(0));
    }
    
}
