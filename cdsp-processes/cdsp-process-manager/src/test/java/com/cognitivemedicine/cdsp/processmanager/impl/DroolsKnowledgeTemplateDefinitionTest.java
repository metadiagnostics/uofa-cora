/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.impl;


import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.drools.compiler.kproject.ReleaseIdImpl;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.builder.ReleaseId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.processmanager.BuildInfo;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.LifeCycleEventCondition;
import com.cognitivemedicine.cdsp.processmanager.mock.MockExecutor;
import com.cognitivemedicine.cdsp.processmanager.mock.MockFact;
import com.cognitivemedicine.cdsp.processmanager.mock.MockListFact;
import com.cognitivemedicine.cdsp.processmanager.mock.MockMessageSubscriber;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.impl.InMemoryProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.util.MavenHelper;
import com.cognitivemedicine.cdsp.report.mongo.ReportServiceImpl;
import com.github.fakemongo.Fongo;
import org.junit.BeforeClass;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author esteban
 */
public class DroolsKnowledgeTemplateDefinitionTest {
	private static final Logger LOG = LoggerFactory.getLogger(DroolsKnowledgeTemplateDefinitionTest.class);
    
    private static Fongo fongo;
    
    @BeforeClass
    public static void doBeforeClass(){
        fongo = new Fongo("Fake Mongo DB");
    }
    
    /**
     * Create and register a DroolsKnowledgeTemplateDefinition based on a 
     * sample kjar located in src/test/resources/kjars/simple-1/kjar-simple-1.jar.
     * 
     * The start condition of this kjar is "MockListFact()".
     * 
     * @throws Exception 
     */
    @Test
    @Ignore("Fix this tests. They where broken by the scanner change")
    public void droolsKnowledgeTemplateDefinitionTest() throws Exception{
        
        //Install kjar-simple-1.jar in a local Maven repository
        String settingsFilePath = ProcessManagerImplTest.class.getResource("/kjars/simple-1/settings.xml").getPath();
        File jarFile = new File(ProcessManagerImplTest.class.getResource("/kjars/simple-1/kjar-simple-1.jar").toURI());
        File pomFile = new File(ProcessManagerImplTest.class.getResource("/kjars/simple-1/pom.xml").toURI());
        ReleaseId releaseId = new ReleaseIdImpl("com.cognitivemedicine.cdsp.sample", "simple-1", "1.0");
        
        System.setProperty("kie.maven.settings.custom", URLDecoder.decode(settingsFilePath, "UTF-8"));
        
        MavenHelper.deployArtifactAndDependencies(releaseId, jarFile, pomFile);

        
        //Create a KTD for the installed kjar
        KnowledgeTemplateDefinition ktd = new DroolsKnowledgeTemplateDefinition(releaseId);
        
        //Register the KTD into a ProcessManagerImpl instance.
        MockMessageSubscriber messageSubscriber = new MockMessageSubscriber();
        ProcessManagerImpl pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(new InMemoryProcessManagerStore())
                .setReportService(new ReportServiceImpl(fongo.getMongo()))
                .setScheduler(StdSchedulerFactory.getDefaultScheduler())
                .createProcessManagerConfiguration()
        );
        pm.registerKnowledgeTemplateDefinition(ktd);
        
        //Publish a new MockListFact into "SOME-TOPIC". Because this object will match
        //the start condition of the KTD, a new KTI will be created and started.
        //The logic of this KTD (check inside kjar-simple-1.jar for a file named 
        //rules.drl) is to populate this object with a String.
        MockListFact<String> list = new MockListFact<>();
        messageSubscriber.publishData("SOME-TOPIC", list);
        
        //Check that the KTI was created and executed by checking the content of
        //the List
        assertThat(list.getList().size(), is(1));
        assertThat(list.getList().get(0), is("The kjar was here and it says hi!"));
    }
    
    /**
     * This tests uses {@link MockFact} instances to initialize and shutdown
     * KTIs.
     * In this test:
     * <ul>
     * <li>A MockFact with str2 == null will instantiate a new KTI</li>
     * <li>A MockFact with str2 != null will shutdown any KTI initiated
     * by another MockFact with str1 equals to this new MockFacts' str2 </li>
     * </ul>
     * 
     * @throws Exception 
     */
    @Test
    @Ignore("Fix this tests. They where broken by the scanner change")
    public void droolsKnowledgeTemplateDefinitionShutdownTest() throws Exception{
        
        //Install kjar-simple-1.jar in a local Maven repository
        String settingsFilePath = ProcessManagerImplTest.class.getResource("/kjars/simple-2/settings.xml").getPath();
        File jarFile = new File(ProcessManagerImplTest.class.getResource("/kjars/simple-2/kjar-simple-2.jar").toURI());
        File pomFile = new File(ProcessManagerImplTest.class.getResource("/kjars/simple-2/pom.xml").toURI());
        ReleaseId releaseId = new ReleaseIdImpl("com.cognitivemedicine.cdsp.sample", "simple-2", "1.0");
        
        System.setProperty("kie.maven.settings.custom", URLDecoder.decode(settingsFilePath, "UTF-8"));
        
        MavenHelper.deployArtifactAndDependencies(releaseId, jarFile, pomFile);

        
        //Create a KTD for the installed kjar
        KnowledgeTemplateDefinition ktd = new DroolsKnowledgeTemplateDefinition(releaseId);
        
        //Register the KTD into a ProcessManagerImpl instance.
        MockMessageSubscriber messageSubscriber = new MockMessageSubscriber();
        ProcessManagerImpl pm = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(new MockExecutor())
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(new InMemoryProcessManagerStore())
                .setReportService(new ReportServiceImpl(fongo.getMongo()))
                .setScheduler(StdSchedulerFactory.getDefaultScheduler())
                .createProcessManagerConfiguration()
        );
        pm.registerKnowledgeTemplateDefinition(ktd);
        
        //Publish a MockFact and a MockListFact into "SOME-TOPIC". These objects 
        //will instantiate a new KTI.
        MockListFact<String> list = new MockListFact<>();
        messageSubscriber.publishData("SOME-TOPIC", list);
        messageSubscriber.publishData("SOME-TOPIC", new MockFact("S1", null, null, null));
        assertThat(pm.getKnowledgeTemplateInstances(ktd.getId()).size(), is(1));
        
        //Publish another MockFact into "SOME-TOPIC". This object will instantiate
        //yet another KTI.
        messageSubscriber.publishData("SOME-TOPIC", new MockFact("S2", null, null, null));
        assertThat(pm.getKnowledgeTemplateInstances(ktd.getId()).size(), is(2));
        
        //Publish now a MockFact with str2 equals to the str1 of the MockFact
        //that instantiated the second KTI. This will shutdown the second KTI.
        //Check the shutdown condition in the kjar.
        messageSubscriber.publishData("SOME-TOPIC", new MockFact(null, "S2", null, null));
        assertThat(pm.getKnowledgeTemplateInstances(ktd.getId()).size(), is(1));
        assertThat(pm.getKnowledgeTemplateInstances(ktd.getId()).get(0).getInitContext().getTriggerFacts().get("$s"), is("S1"));
        
        //Publish now a MockFact with str1 equals to the str1 of the MockFact
        //that instantiated the second KTI. This will shutdown the first KTI.
        messageSubscriber.publishData("SOME-TOPIC", new MockFact(null, "S1", null, null));
        assertThat(pm.getKnowledgeTemplateInstances(ktd.getId()).size(), is(0));
    }
    
    @Test
    public void droolsKnowledgeTemplateDeploymentDescriptorSerializationTest() throws JAXBException{

        String buildNumber = "123";
        String buildBranch = "develop";
        long buildTimestamp = System.currentTimeMillis();
        
        BuildInfo buildInfo = new BuildInfo();
        buildInfo.setBuildNumber(buildNumber);
        buildInfo.setBuildBranch(buildBranch);
        buildInfo.setBuildTimestamp(buildTimestamp);
        
        List<String> topics = new ArrayList<>();
        topics.add("TopicA");
        topics.add("TopicB");
        
        LifeCycleEventCondition startCondition = new LifeCycleEventConditionImpl("$a: com.cognitivemedicine.model.ADT(value > 10)"); 
        LifeCycleEventCondition shutdownCondition = new LifeCycleEventConditionImpl("$a: com.cognitivemedicine.model.Discharge()"); 
        
        DroolsKnowledgeTemplateDeploymentDescriptor descriptor = new DroolsKnowledgeTemplateDeploymentDescriptor();
        descriptor.setBuildInfo(buildInfo);
        descriptor.setLifeCycleRelatedTopics(topics);
        descriptor.setStartCondition(startCondition);
        descriptor.setShutdownCondition(shutdownCondition);
        
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        JAXBContext context = JAXBContext.newInstance(DroolsKnowledgeTemplateDeploymentDescriptor.class);
        Marshaller m = context.createMarshaller();  
        m.setProperty("jaxb.formatted.output", true);
        m.marshal(descriptor, baos);
        
        String xml = new String(baos.toByteArray());
        
        LOG.debug("XML="+xml);
        
        Unmarshaller u = context.createUnmarshaller();
        DroolsKnowledgeTemplateDeploymentDescriptor unmarshal = (DroolsKnowledgeTemplateDeploymentDescriptor) u.unmarshal(new ByteArrayInputStream(baos.toByteArray()));
        
        assertThat(unmarshal.getLifeCycleRelatedTopics().size(), is(descriptor.getLifeCycleRelatedTopics().size()));
        assertThat(unmarshal.getLifeCycleRelatedTopics(), hasItems(descriptor.getLifeCycleRelatedTopics().toArray(new String[0])));
        assertThat(unmarshal.getStartCondition().getCondition(), is(descriptor.getStartCondition().getCondition()));
        assertThat(unmarshal.getShutdownCondition().getCondition(), is(descriptor.getShutdownCondition().getCondition()));
        assertThat(unmarshal.getBuildInfo().toString(), is(descriptor.getBuildInfo().toString()));
        
    }
    
}
