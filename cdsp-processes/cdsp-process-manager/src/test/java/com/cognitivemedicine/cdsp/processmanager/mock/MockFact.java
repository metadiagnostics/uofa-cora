/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.mock;

import java.io.Serializable;

/**
 *
 * @author esteban
 */
public class MockFact implements Serializable{
    
    public static final long serialVersionUID = 1L;
    
    private String str1;
    private String str2;
    private Serializable obj1;
    private Serializable obj2;

    public MockFact() {
    }

    public MockFact(String str1, String str2, Serializable obj1, Serializable obj2) {
        this.str1 = str1;
        this.str2 = str2;
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public Serializable getObj1() {
        return obj1;
    }

    public void setObj1(Serializable obj1) {
        this.obj1 = obj1;
    }

    public Serializable getObj2() {
        return obj2;
    }

    public void setObj2(Serializable obj2) {
        this.obj2 = obj2;
    }
    
}
