/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.cron.condition;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for condition generators.
 * @author calcacuervo
 *
 */
public class ConditionGeneratorTest {

    private DelegatingExpressionGenerator generator;

    @Before
    public void init() {
        List<ConditionExpressionGenerator> generators = new ArrayList<>();
        generators.add(new CronConditionGenerator());
        generators.add(new InternalExpressionConditionGenerator(new ConfigUtilsConditionGenerator()));
        generator = new DelegatingExpressionGenerator(generators);
    }

    @Test
    public void generateStartCondition_cronInput() {
        String input = "0 0 0 1 1 ? 3000";
        Assert.assertEquals(input, generator.generateExpression(input));
    }

    @Test()
    public void generateStartCondition_malFormedcronInput() {
        String input = "0 0 110 1 1 ? 3000";
        Assert.assertEquals(input, generator.generateExpression(input));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void generateStartCondition_malFormedcronInputWithExpression() {
        String input = "${CFG_UTILS:NOTHING}";
        generator.generateExpression(input);
    }

    @Test
    public void generateStartCondition_configUtilsInput() {
        String input = "${CFG_UTILS:condition.sample:cron}";
        Assert.assertEquals("0 0 0 1 1 ? 3000", generator.generateExpression(input));
    }
}
