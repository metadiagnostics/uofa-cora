/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.processmanager.mock;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.runtime.KnowledgeTemplateInitContext;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNodeExecution;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Mock KnowledgeTemplateInstance implementation meant to be used in tests.
 * 
 * @author esteban
 */
public class MockKnowledgeTemplateInstance implements KnowledgeTemplateInstance {

    private String id;
    private KnowledgeTemplateInitContext initContext;

    
    @Override
    public void init(String id, KnowledgeTemplateInitContext initContext) {
        this.id = id;
        this.initContext = initContext;
    }

    @Override
    public void start() {
    }
    
    @Override
    public void restart(KnowledgeTemplateInitContext initContext) {
        this.initContext = initContext;
    }

    @Override
    public void shutdown() {
    }

    @Override
    public void dispose() {
    }

    @Override
    public String getId() {
        return this.id;
    }
    
    @Override
    public KnowledgeTemplateInitContext getInitContext() {
        return initContext;
    }

    @Override
    public Map<String, List<DecisionNodeExecution>> getDecisionNodeExecutions() {
        return Collections.EMPTY_MAP;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
}
