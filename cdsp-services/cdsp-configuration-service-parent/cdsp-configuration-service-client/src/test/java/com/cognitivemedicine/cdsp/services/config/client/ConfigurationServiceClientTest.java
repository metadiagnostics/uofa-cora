/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.config.client;

import java.util.Collection;

import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.DataConfiguration;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.services.config.model.ConfigurationProfileDTO;
import com.cognitivemedicine.cdsp.services.config.model.ConfigurationSummaryDTO;
import com.cognitivemedicine.cdsp.testcategories.ServiceIntegrationTest;

/**
 *
 * @author esteban
 */
@Category(ServiceIntegrationTest.class)
public class ConfigurationServiceClientTest {

    private ConfigurationServiceClient client;
	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationServiceClientTest.class);
	
    @Before
    public void doBefore(){
        RESTClient rc = RESTClient.create("http://localhost:8080/cdspconfig");
        client = new ConfigurationServiceClient(rc);
    }
    
    @Ignore("This test requires a running Configuration Service instance")
    @Test
    public void testListConfigurations() {
        LOG.info("listConfigurations");
        
        Collection<ConfigurationSummaryDTO> list = client.listConfigurations();

    }
    
    @Ignore("This test requires a running Configuration Service instance")
    @Test
    public void testGetConfigurationProfile() {
        LOG.info("getConfigurationProfile");
        
        ConfigurationProfileDTO profile = client.getConfigurationProfile("Config2", "Profile1");
        DataConfiguration cfg = ConfigurationServiceClient.getProfileParametersAsConfiguration(profile);

        LOG.info("Profile = "+ConfigurationUtils.toString(cfg));
      
    }
    
}
