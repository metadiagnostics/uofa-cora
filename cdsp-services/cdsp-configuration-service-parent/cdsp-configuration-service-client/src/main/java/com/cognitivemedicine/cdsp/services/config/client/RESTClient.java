/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.config.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author esteban
 */
public class RESTClient {
    
    public static RESTClient create(String endpoint){
        
        
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.enable(SerializationFeature.INDENT_OUTPUT);
//        
//        JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
//        provider.setMapper(mapper);
        
        Client client = ClientBuilder.newClient(new ClientConfig().register( com.cognitivemedicine.cdsp.fhir.util.JsonProvider.class ));
        WebTarget target = client.target(endpoint);
        
        RESTClient rc = new RESTClient();
        rc.setWebTarget(target);
        
        return rc;
    }
    
    private WebTarget webTarget;
    
    private RESTClient() {
    }

    private void setWebTarget(WebTarget webTarget) {
        this.webTarget = webTarget;
    }
    
    public WebTarget getWebTarget() {
        return webTarget;
    }
    
}
