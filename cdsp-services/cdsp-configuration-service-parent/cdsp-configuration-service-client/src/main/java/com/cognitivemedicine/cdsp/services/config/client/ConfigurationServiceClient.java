/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.config.client;

import com.cognitivemedicine.cdsp.services.config.api.ConfigurationService;
import com.cognitivemedicine.cdsp.services.config.model.ConfigurationProfileDTO;
import com.cognitivemedicine.cdsp.services.config.model.ConfigurationSummaryDTO;
import com.cognitivemedicine.cdsp.healthcheck.model.HealthCheckStatus;
import com.cognitivemedicine.cdsp.healthcheck.model.ServiceStatistics;
import com.cognitivemedicine.cdsp.healthcheck.util.HealthCheckDetailStatusType;

import java.util.Collection;
import java.util.HashMap;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.configuration.DataConfiguration;
import org.apache.commons.configuration.MapConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class ConfigurationServiceClient implements ConfigurationService {
    
    private final static Logger LOG = LoggerFactory.getLogger(ConfigurationServiceClient.class);
    
    private final WebTarget webTarget;

    public ConfigurationServiceClient(RESTClient client) {
         this.webTarget = client.getWebTarget().path("config");
    }
    
    public static DataConfiguration getProfileParametersAsConfiguration(ConfigurationProfileDTO dto){
        return new DataConfiguration(new MapConfiguration(dto.getParameters()));
    }
    
    @Override
    public Collection<ConfigurationSummaryDTO> listConfigurations() {
        GenericType<Collection<ConfigurationSummaryDTO>> entity = new GenericType<Collection<ConfigurationSummaryDTO>>() {};
        Collection<ConfigurationSummaryDTO> dtos =  webTarget.path("configurations").request(MediaType.APPLICATION_JSON).get(entity);
        return dtos;
    }

    @Override
    public ConfigurationSummaryDTO getConfiguration(String name) {
        Response response =  webTarget.path("configurations").path(name).request(MediaType.APPLICATION_JSON).get();
        ConfigurationSummaryDTO dto = response.readEntity(ConfigurationSummaryDTO.class);
        return dto;
    }

    @Override
    public Collection<ConfigurationProfileDTO> listConfigurationProfiles(String configName) {
        GenericType<Collection<ConfigurationProfileDTO>> entity = new GenericType<Collection<ConfigurationProfileDTO>>() {};
        Collection<ConfigurationProfileDTO> dtos =  webTarget.path("configurations").path(configName).path("profiles").request(MediaType.APPLICATION_JSON).get(entity);
        return dtos;
    }

    @Override
    public ConfigurationProfileDTO getConfigurationProfile(String configName, String name) {
        Response response =  webTarget.path("configurations").path(configName).path("profiles").path(name).request(MediaType.APPLICATION_JSON).get();
        ConfigurationProfileDTO dto = response.readEntity(ConfigurationProfileDTO.class);
        return dto;
    }
    
    public ConfigurationProfileDTO getConfigurationProfileOrEmpty(String configName, String name) {
        try{
            Response response =  webTarget.path("configurations").path(configName).path("profiles").path(name).request(MediaType.APPLICATION_JSON).get();
            ConfigurationProfileDTO dto = response.readEntity(ConfigurationProfileDTO.class);
            return dto;
        } catch(Exception e){
            LOG.warn("Exception retrieving Configuration Profile. Returning an empty one.", e.getMessage());
            ConfigurationProfileDTO dto = new ConfigurationProfileDTO();
            dto.setConfigurationName(configName);
            dto.setName(name);
            dto.setParameters(new HashMap<>());
            return dto;
        }
    }
    
	@Override
	public HealthCheckStatus getStatus(int level) {
		HealthCheckStatus status = new HealthCheckStatus();
		status.setHealthy(true);
		status.setStatusCode(HealthCheckDetailStatusType.INFO.getCode());
		status.setStatusDescription("Success");
		return status;
	}

	@Override
	public ServiceStatistics getStatistics() {
		ServiceStatistics statistics = null;
		return statistics;
	}
    
}
