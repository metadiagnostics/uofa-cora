/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.model.visualization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author esteban
 */
public class DecisionNodeDTO {

    private String name;
    private String description;
    private final List<String> tags = new ArrayList<>();

    public DecisionNodeDTO() {
    }
    
    public DecisionNodeDTO(String name, String description, List<String> tags) {
        this.name = name;
        this.description = description;
        
        if (tags != null){
            this.tags.addAll(tags);
        }
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public List<String> getTags() {
        return Collections.unmodifiableList(tags);
    }
    
    public void setTags(List<String> tags){
        this.tags.addAll(tags);
    }
    
}
