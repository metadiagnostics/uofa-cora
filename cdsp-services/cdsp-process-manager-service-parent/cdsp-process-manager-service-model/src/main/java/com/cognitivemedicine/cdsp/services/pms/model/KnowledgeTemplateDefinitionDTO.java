/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author esteban
 */
public class KnowledgeTemplateDefinitionDTO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String clazz;
    private String id;
    private String buildNumber;
    private String buildBranch;
    private long buildTimestamp;
    private boolean restartable;
    private String startCondition;
    private String shutdownCondition;
    private List<String> lifeCycleRelatedTopics;

    public KnowledgeTemplateDefinitionDTO() {
    }

    public KnowledgeTemplateDefinitionDTO(String clazz, String id, String buildNumber, String buildBranch, long buildTimestamp, boolean restartable, String startCondition, String shutdownCondition, List<String> lifeCycleRelatedTopics) {
        this.clazz = clazz;
        this.id = id;
        this.buildNumber = buildNumber;
        this.buildBranch = buildBranch;
        this.buildTimestamp = buildTimestamp;
        this.restartable = restartable;
        this.startCondition = startCondition;
        this.shutdownCondition = shutdownCondition;
        this.lifeCycleRelatedTopics = lifeCycleRelatedTopics;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildBranch() {
        return buildBranch;
    }

    public void setBuildBranch(String buildBranch) {
        this.buildBranch = buildBranch;
    }

    public long getBuildTimestamp() {
        return buildTimestamp;
    }

    public void setBuildTimestamp(long buildTimestamp) {
        this.buildTimestamp = buildTimestamp;
    }

    public boolean isRestartable() {
        return restartable;
    }

    public void setRestartable(boolean restartable) {
        this.restartable = restartable;
    }
    
    public String getStartCondition() {
        return startCondition;
    }

    public void setStartCondition(String startCondition) {
        this.startCondition = startCondition;
    }

    public String getShutdownCondition() {
        return shutdownCondition;
    }

    public void setShutdownCondition(String shutdownCondition) {
        this.shutdownCondition = shutdownCondition;
    }

    public List<String> getLifeCycleRelatedTopics() {
        return lifeCycleRelatedTopics;
    }

    public void setLifeCycleRelatedTopics(List<String> lifeCycleRelatedTopics) {
        this.lifeCycleRelatedTopics = lifeCycleRelatedTopics;
    }
    
}
