/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.model.visualization;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author esteban
 */
public class DecisionNodeExecutionDTO {

    private String nodeName;
    private long timestamp;
    private Map<String, Object> context = new HashMap<>();

    public DecisionNodeExecutionDTO() {
    }

    public DecisionNodeExecutionDTO(String nodeName, long timestamp) {
        this.nodeName = nodeName;
        this.timestamp = timestamp;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public long getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, Object> getContext() {
        return context;
    }

    public void setContext(Map<String, Object> context) {
        this.context = context;
    }
    
    public void addContextElement(String name, Object value) {
        this.context.put(name, value);
    }

}
