/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.api;

import com.cognitivemedicine.cdsp.healthcheck.model.Monitorable;
import com.cognitivemedicine.cdsp.services.pms.model.KnowledgeTemplateDefinitionDTO;
import com.cognitivemedicine.cdsp.services.pms.model.KnowledgeTemplateInstanceDTO;
import com.cognitivemedicine.cdsp.services.pms.model.visualization.DecisionNodeDTO;
import com.cognitivemedicine.cdsp.services.pms.model.visualization.DecisionNodeExecutionDTO;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author esteban
 */
public interface ProcessManagerService extends Monitorable {

    /**
     * @return 
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#getRegisteredKnowledgeTemplateDefinitions()
     */
    public Collection<KnowledgeTemplateDefinitionDTO> listKTDs();

    /**
     * Returns a KTD definition given its id.
     * @param ktdId
     * @return 
     */
    public KnowledgeTemplateDefinitionDTO getKTD(String ktdId);

    /**
     * @param ktdDefinition
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#registerKnowledgeTemplateDefinition(KnowledgeTemplateDefinition)
     */
    public void registerKTD(String ktdDefinition);

    /**
     * @param ktdId
     * @param terminateInstances
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#unregisterKnowledgeTemplateDefinition(String, boolean)
     */
    public void unregisterKTD(String ktdId, Boolean terminateInstances);

    /**
     * @param ktdId
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#purgeKnowledgeTemplateDefinitionLifeCycleSession(String)
     */
    public void purgeKTDLifeCycleSession(String ktdId);

    
    /**
     * @param ktdId
     * @param paramName
     * @param value
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#addKnowledgeTemplateDefinitionInitContextParameter(String, String, Object);
     */
    public void addKnowledgeTemplateDefinitionInitContextParameter(String ktdId, String paramName, String value);

    /**
     * @param knowledgeTemplateDefinitionId
     * @return 
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#getKnowledgeTemplateDefinitionInitContextParameter(String);
     */
    public Map<String, Object> getKnowledgeTemplateDefinitionInitContextParameter(String knowledgeTemplateDefinitionId);

    /**
     * @param knowledgeTemplateInstanceId
     * @return 
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#getKnowledgeTemplateDefinitionLifeCycleSessionFacts(String);
     */
    public List<Object> getKnowledgeTemplateDefinitionLifeCycleSessionFacts(String knowledgeTemplateInstanceId);

    /**
     * 
     * @param knowledgeTemplateDefinitionId
     * @return 
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager.getKnowledgeTemplateDefinitionDecisionNodes(String);
     */
    public Map<String, DecisionNodeDTO> getKnowledgeTemplateDefinitionDecisionNodes(String knowledgeTemplateDefinitionId);
    
    /**
     * @param ktdId
     * @return 
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#getKnowledgeTemplateInstances(String);
     */
    public Collection<KnowledgeTemplateInstanceDTO> listKTIs(String ktdId);

    /**
     * @param ktdId
     * @param ktiId
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#terminateKnowledgeTemplateInstance(String);
     */
    public void terminateKTI(String ktdId, String ktiId);

    /**
     * Returns a KTI definition given its id and the id of the KTD it belongs to.
     * @param ktdId
     * @param ktiId
     * @return 
     */
    public KnowledgeTemplateInstanceDTO getKTI(String ktdId, String ktiId);

    /**
     * 
     * @param ktdId
     * @param ktiId
     * @param excludeContext 
     * @return 
     * @see com.cognitivemedicine.cdsp.processmanager.ProcessManager#getKnowledgeTemplateInstanceDecisionNodeExecutions(String);
     */
    public Map<String, List<DecisionNodeExecutionDTO>> getKnowledgeTemplateInstanceDecisionNodeExecutions(String ktdId, String ktiId, boolean excludeContext);
	   
}
