/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms;

import com.cognitivemedicine.cdsp.services.pms.KnowledgeTemplateDefinitionUnmarshaller;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.impl.DroolsKnowledgeTemplateDefinition;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class KnowledgeTemplateDefinitionUnmarshallerTest {
    
    @Test
    public void singleKTDTest() throws IOException{
        String serialized = "drools:com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT";
        KnowledgeTemplateDefinitionUnmarshaller unmarshaller = new KnowledgeTemplateDefinitionUnmarshaller();
        
        List<KnowledgeTemplateDefinition> ktds = unmarshaller.unmarshall(new ByteArrayInputStream(serialized.getBytes()));
        
        assertThat(ktds.size(), is (1));
        assertThat(ktds.get(0), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        
        DroolsKnowledgeTemplateDefinition dktd = (DroolsKnowledgeTemplateDefinition) ktds.get(0);
        
        assertThat(dktd.getId(), is("com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT"));
    }
    
    @Test
    public void multipleKTDTest() throws IOException{
        String serialized = "";
        serialized+="drools:com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT\n";
        serialized+="drools:com.cognitivemedicine.cdsp:another-artifact:0.2-SNAPSHOT";
        
        KnowledgeTemplateDefinitionUnmarshaller unmarshaller = new KnowledgeTemplateDefinitionUnmarshaller();
        
        List<KnowledgeTemplateDefinition> ktds = unmarshaller.unmarshall(new ByteArrayInputStream(serialized.getBytes()));
        
        assertThat(ktds.size(), is (2));
        assertThat(ktds.get(0), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        assertThat(ktds.get(1), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        
        DroolsKnowledgeTemplateDefinition dktd1 = (DroolsKnowledgeTemplateDefinition) ktds.get(0);
        DroolsKnowledgeTemplateDefinition dktd2 = (DroolsKnowledgeTemplateDefinition) ktds.get(1);
        
        assertThat(dktd1.getId(), is("com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT"));
        
        assertThat(dktd2.getId(), is("com.cognitivemedicine.cdsp:another-artifact:0.2-SNAPSHOT"));
    }
    
    
    @Test
    public void multipleKTDWithCommentsTest() throws IOException{
        String serialized = "";
        serialized+="drools:com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT\n";
        serialized+="#This is a comment\n";
        serialized+="drools:com.cognitivemedicine.cdsp:another-artifact:0.2-SNAPSHOT\n";
        serialized+="#This is another comment";
        
        KnowledgeTemplateDefinitionUnmarshaller unmarshaller = new KnowledgeTemplateDefinitionUnmarshaller();
        
        List<KnowledgeTemplateDefinition> ktds = unmarshaller.unmarshall(new ByteArrayInputStream(serialized.getBytes()));
        
        assertThat(ktds.size(), is (2));
        assertThat(ktds.get(0), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        assertThat(ktds.get(1), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        
        DroolsKnowledgeTemplateDefinition dktd1 = (DroolsKnowledgeTemplateDefinition) ktds.get(0);
        DroolsKnowledgeTemplateDefinition dktd2 = (DroolsKnowledgeTemplateDefinition) ktds.get(1);
        
        assertThat(dktd1.getId(), is("com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT"));
        
        assertThat(dktd2.getId(), is("com.cognitivemedicine.cdsp:another-artifact:0.2-SNAPSHOT"));
    }
    
    @Test
    public void multipleKTDWithErrorsTest() throws IOException{
        String serialized = "";
        serialized+="drools:com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT\n";
        serialized+="java:com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT\n"; //type is unsupported
        serialized+="drools:com.cognitivemedicine.cdsp:another-artifact:0.2-SNAPSHOT\n";
        serialized+="drools:$o:Order()"; //Not enough information
        
        KnowledgeTemplateDefinitionUnmarshaller unmarshaller = new KnowledgeTemplateDefinitionUnmarshaller();
        
        List<KnowledgeTemplateDefinition> ktds = unmarshaller.unmarshall(new ByteArrayInputStream(serialized.getBytes()));
        
        assertThat(ktds.size(), is (2));
        assertThat(ktds.get(0), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        assertThat(ktds.get(1), instanceOf(DroolsKnowledgeTemplateDefinition.class));
        
        DroolsKnowledgeTemplateDefinition dktd1 = (DroolsKnowledgeTemplateDefinition) ktds.get(0);
        DroolsKnowledgeTemplateDefinition dktd2 = (DroolsKnowledgeTemplateDefinition) ktds.get(1);
        
        assertThat(dktd1.getId(), is("com.cognitivemedicine.cdsp:sample-artifact:1.1-SNAPSHOT"));
        
        assertThat(dktd2.getId(), is("com.cognitivemedicine.cdsp:another-artifact:0.2-SNAPSHOT"));
    }
}
