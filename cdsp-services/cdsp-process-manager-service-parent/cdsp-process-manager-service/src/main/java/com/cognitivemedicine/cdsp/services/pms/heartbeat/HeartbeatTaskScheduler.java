/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.heartbeat;

import com.cognitivemedicine.cdsp.services.pms.config.ConfigConstants;
import com.cognitivemedicine.config.utils.ConfigUtils;
import java.io.File;
import java.util.Date;
import javax.annotation.Resource;
import org.quartz.JobBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.TriggerBuilder;

/**
 * Class used to instantiate and schedule a {@link HeartbeatTask} based on
 * config-utils configuration.
 *
 * @author esteban
 */
public class HeartbeatTaskScheduler {

    private final static String JOB_ID = "PMS-Heartbeat-Job";
    private final long period;

    @Resource
    private Scheduler quartzScheduler;

    private final String fileName;

    public HeartbeatTaskScheduler() {

        ConfigUtils config = ConfigUtils.getInstance(ConfigConstants.CONTEXT_NAME);

        period = config.getLong(ConfigConstants.KEY_HEARTBEAT_PERIOD, 5000);
        fileName = config.getString(ConfigConstants.KEY_HEARTBEAT_FILE, "pms-heartbeat");

    }

    public Date getLastExecutionDate() {
        return HeartbeatTask.getLastExecutionDate(new File(fileName));
    }
    
    public void startExecution() {
        try {
            quartzScheduler.scheduleJob(
                JobBuilder.newJob(HeartbeatTask.class)
                    .withIdentity(JOB_ID)
                    .build(),
                TriggerBuilder.newTrigger()
                    .startNow()
                    .usingJobData(HeartbeatTask.JOB_DATA_FILE_NAME, fileName)
                    .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever((int) (period / 1000))).build()
            );
        } catch (SchedulerException ex) {
            throw new IllegalStateException("Exception scheduling Process Manager Service Heartbeat job", ex);
        }
    }

    public void setQuartzScheduler(Scheduler quartzScheduler) {
        this.quartzScheduler = quartzScheduler;
    }

}
