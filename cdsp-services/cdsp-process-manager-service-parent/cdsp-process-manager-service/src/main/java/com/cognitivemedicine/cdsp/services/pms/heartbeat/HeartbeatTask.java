/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.heartbeat;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is in charge of keep a file in the file system with the last time
 * it was executed. This file can then be used to determine the last time the
 * system was running when it is restarted.
 *
 * <b>Important:</b> The {@link #executeTask() method in this class is not
 * thread-safe!
 *
 * @author esteban
 */
public class HeartbeatTask implements Job {

    public final static String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SS Z";
    public final static String JOB_DATA_FILE_NAME = "fileName";

    private static final Logger LOG = LoggerFactory.getLogger(HeartbeatTask.class);

    public static Date getLastExecutionDate(File heartbeatFile) {
        if (!heartbeatFile.exists()) {
            return null;
        }

        try {
            String dateAsString = IOUtils.toString(new FileReader(heartbeatFile));
            LOG.trace("Last time the HeartbeatTask was executed was on {}", dateAsString);

            return new SimpleDateFormat(TIMESTAMP_PATTERN).parse(dateAsString);
        } catch (IOException ex) {
            throw new IllegalArgumentException("Exception reading from heartbeat file: " + heartbeatFile.getPath(), ex);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Exception parsing heartbeat content: " + heartbeatFile.getPath(), ex);
        }
    }
    
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            String fileName = context.getMergedJobDataMap().getString(JOB_DATA_FILE_NAME);

            if (StringUtils.isBlank(fileName)){
                throw new IllegalArgumentException("The heartbeat file was not specified.");
            }
            
            File heartbeatFile = new File(fileName);

            if (heartbeatFile.exists() && heartbeatFile.isDirectory()) {
                throw new IllegalArgumentException("The heartbeat file points to a directory: " + heartbeatFile.getPath());
            }

            if (heartbeatFile.exists() && !heartbeatFile.canWrite()) {
                throw new IllegalArgumentException("Not enough permissions to write into the heartbeat file: " + heartbeatFile.getPath());
            }

            this.writeHeartbeat(heartbeatFile);
        } catch (IOException e) {
            LOG.error("Error executing Heartbeat Task", e);
        }
    }

    private void writeHeartbeat(File heartbeatFile) throws IOException {
        if (!heartbeatFile.exists()) {
            heartbeatFile.createNewFile();
        }

        try (FileWriter fw = new FileWriter(heartbeatFile)) {
            fw.write(new SimpleDateFormat(TIMESTAMP_PATTERN).format(new Date()));
        }
    }

}
