/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.config;

/**
 *
 * @author esteban
 */
public class ConfigConstants {
    
    public static final String CONTEXT_NAME = "process.manager.service";
    
    public static final String KEY_JMS_ENDPOINT = "jms.endpoint";
    public static final String KEY_JMS_TOPIC = "jms.topic";
    public static final String KEY_JMS_HEADER_DISCRIMINATOR_PROPERTY = "jms.header.discriminator.property";
	
    public static final String KEY_COMMUNICATION_SERVICE_URL = "cs.url";

    public static final String KEY_KTD_REGISTRY_FILE = "ktd.registry.file";
    
    public static final String KEY_CONFIGURATUION_SERVICE_URL = "config.service.url";
    
    public static final String KEY_VERSION_MAVEN_VERSION = "version.maven.version"; 
	public static final String KEY_VERSION_BUILD_NUMBER = "version.build.number"; 
	public static final String KEY_VERSION_BUILD_BRANCH = "version.build.branch"; 
	public static final String KEY_VERSION_BUILD_TIMESTAMP = "version.build.timestamp";
    
	public static final String KEY_HEARTBEAT_FILE = "heartbeat.file";
	public static final String KEY_HEARTBEAT_PERIOD = "heartbeat.period";
    
    public static final String KEY_MONGO_ENDPOINT = "mongo.endpoint";
    
}
