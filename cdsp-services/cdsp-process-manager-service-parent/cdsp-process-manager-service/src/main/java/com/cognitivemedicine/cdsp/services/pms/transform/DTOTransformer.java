/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.transform;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNode;
import com.cognitivemedicine.cdsp.processmanager.runtime.visualization.DecisionNodeExecution;
import com.cognitivemedicine.cdsp.services.pms.model.KnowledgeTemplateDefinitionDTO;
import com.cognitivemedicine.cdsp.services.pms.model.KnowledgeTemplateInstanceDTO;
import com.cognitivemedicine.cdsp.services.pms.model.visualization.DecisionNodeDTO;
import com.cognitivemedicine.cdsp.services.pms.model.visualization.DecisionNodeExecutionDTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import static java.util.stream.Collectors.toList;

/**
 * This class is used to convert classes from PM model into DTOs
 * @author esteban
 */
public class DTOTransformer {
    
    public static KnowledgeTemplateDefinitionDTO toKnowledgeTemplateDefinitionDTO(KnowledgeTemplateDefinition ktd){
        KnowledgeTemplateDefinitionDTO dto = new KnowledgeTemplateDefinitionDTO();
        dto.setClazz(ktd.getClass().getName());
        dto.setId(ktd.getId());
        dto.setBuildNumber(ktd.getBuildInfo().getBuildNumber());
        dto.setBuildBranch(ktd.getBuildInfo().getBuildBranch());
        dto.setRestartable(ktd.isRestartable());
        dto.setBuildTimestamp(ktd.getBuildInfo().getBuildTimestamp());
        dto.setLifeCycleRelatedTopics(new ArrayList<>(ktd.getLifeCycleRelatedTopics()));
        dto.setShutdownCondition(ktd.getShutdownCondition() == null ? null : ktd.getShutdownCondition().getCondition());
        dto.setStartCondition(ktd.getStartCondition() == null ? null : ktd.getStartCondition().getCondition());
        return dto;
    }
    
    public static Collection<KnowledgeTemplateDefinitionDTO> toKnowledgeTemplateDefinitionDTO(Collection<KnowledgeTemplateDefinition> ktds){
        return ktds == null ? Collections.EMPTY_LIST : ktds.stream().map(DTOTransformer::toKnowledgeTemplateDefinitionDTO).collect(toList());
    }
    
    public static KnowledgeTemplateInstanceDTO toKnowledgeTemplateInstanceDTO(KnowledgeTemplateInstance kti){
        KnowledgeTemplateInstanceDTO dto = new KnowledgeTemplateInstanceDTO();
        dto.setClazz(kti.getClass().getName());
        dto.setId(kti.getId());
        dto.setTriggerFacts(kti.getInitContext() != null && kti.getInitContext().getTriggerFacts() != null ? new HashMap<>(kti.getInitContext().getTriggerFacts()): null);
        return dto;
    }
    
    public static Collection<KnowledgeTemplateInstanceDTO> toKnowledgeTemplateInstanceDTO(Collection<KnowledgeTemplateInstance> ktis){
        return ktis == null ? Collections.EMPTY_LIST : ktis.stream().map(DTOTransformer::toKnowledgeTemplateInstanceDTO).collect(toList());
    }
    
    public static DecisionNodeDTO toDecisionNodeDTO(DecisionNode decisionNode){
        return new DecisionNodeDTO(decisionNode.getName(), decisionNode.getDescription(), decisionNode.getTags());
    }
    
    public static DecisionNodeExecutionDTO toDecisionNodeDTO(DecisionNodeExecution decisionNodeExecution, boolean excludeContext){
        DecisionNodeExecutionDTO dto = new DecisionNodeExecutionDTO();
        dto.setNodeName(decisionNodeExecution.getNodeName());
        dto.setTimestamp(decisionNodeExecution.getTimestamp());

        if(!excludeContext){
            for (Map.Entry<String, Object> entry : decisionNodeExecution.getContext().entrySet()) {
                dto.addContextElement(entry.getKey(), entry.getValue());
            }
        }
        
        return dto;
    }
}
