/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;

import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.messaging.api.MessageSubscriber;
import com.cognitivemedicine.cdsp.messaging.jms.JMSMessageSubscriber;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.ProcessManager;
import com.cognitivemedicine.cdsp.processmanager.cron.condition.ConditionExpressionGenerator;
import com.cognitivemedicine.cdsp.processmanager.impl.ProcessManagerConfigurationBuilder;
import com.cognitivemedicine.cdsp.processmanager.impl.ProcessManagerImpl;
import com.cognitivemedicine.cdsp.processmanager.runtime.InitParameter;
import com.cognitivemedicine.cdsp.processmanager.runtime.RESTServiceLocator;
import com.cognitivemedicine.cdsp.report.api.ReportService;
import com.cognitivemedicine.cdsp.report.model.KTDReportInformation;
import com.cognitivemedicine.cdsp.report.mongo.ReportServiceImpl;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.api.ProcessManagerStore;
import com.cognitivemedicine.cdsp.processmanager.runtime.store.impl.mongo.MongoProcessManagerStore;

import com.cognitivemedicine.cdsp.services.pms.config.ConfigConstants;
import com.cognitivemedicine.cdsp.services.pms.heartbeat.HeartbeatTask;
import com.cognitivemedicine.cdsp.services.pms.heartbeat.HeartbeatTaskScheduler;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.sun.messaging.ConnectionConfiguration;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.Resource;
import org.springframework.core.task.TaskExecutor;

/**
 *
 * @author esteban
 *
 */
public class ProcessManagerFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessManagerFactory.class);

    @Resource
    private HeartbeatTaskScheduler heartbeatTaskScheduler;
    
    @Resource
    private TaskExecutor processManagerExecutor;
    
    @Resource
    private ConditionExpressionGenerator conditionExpressionGenerator;
    
    @Resource
    private Scheduler quartzScheduler;
    
    public ProcessManager createProcessManager() throws IOException {

        ConfigUtils configUtils = ConfigUtils.getInstance(ConfigConstants.CONTEXT_NAME);

        String jmsEndpoint = configUtils.getString(ConfigConstants.KEY_JMS_ENDPOINT);
        String jmsTopic = configUtils.getString(ConfigConstants.KEY_JMS_TOPIC);
        String jmsHeaderDiscriminatorProperty = configUtils.getString(ConfigConstants.KEY_JMS_HEADER_DISCRIMINATOR_PROPERTY);

        String ktdRegistryFile = configUtils.getString(ConfigConstants.KEY_KTD_REGISTRY_FILE);
        String communicationServiceUrl = configUtils.getString(ConfigConstants.KEY_COMMUNICATION_SERVICE_URL);
        String configurationServiceUrl = configUtils.getString(ConfigConstants.KEY_CONFIGURATUION_SERVICE_URL);
        String mongoEndpoint = configUtils.getString(ConfigConstants.KEY_MONGO_ENDPOINT);

        LOG.info("JMS Endpoint: {}", jmsEndpoint);
        LOG.info("JMS Topic: {}", jmsTopic);
        LOG.info("JMS Discriminator Property: {}", jmsHeaderDiscriminatorProperty);
        LOG.info("KTD Registry File: {}", ktdRegistryFile);
        LOG.info("Communication Service URL: {}", communicationServiceUrl);
        LOG.info("Configuration Service URL: {}", configurationServiceUrl);
        LOG.info("Mongo Endpoint: {}", mongoEndpoint);

        Date lastExecutionDateBeforeStartup = heartbeatTaskScheduler.getLastExecutionDate();
        LOG.info("Last known execution date: {}", lastExecutionDateBeforeStartup == null ? "Unknown" : new SimpleDateFormat(HeartbeatTask.TIMESTAMP_PATTERN).format(lastExecutionDateBeforeStartup));

        com.sun.messaging.ConnectionFactory connectionFactory = new com.sun.messaging.ConnectionFactory();
        try {
            connectionFactory.setProperty(ConnectionConfiguration.imqAddressList, jmsEndpoint);
            connectionFactory.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
        } catch (JMSException e) {
            throw new IllegalStateException("Exception creating Subscription Service", e);
        }

        MessageSubscriber messageSubscriber = new JMSMessageSubscriber(connectionFactory, jmsTopic, jmsHeaderDiscriminatorProperty);

        ProcessManagerStore processManagerStore = new MongoProcessManagerStore(mongoEndpoint);

        //Prepare default KTI parameters
        Map<String, Object> defaultKTIParameters = new HashMap<>();
        defaultKTIParameters.put(InitParameter.InitParameterConstants.CONFIGURATION_SERVICE_URL, new RESTServiceLocator(RESTServiceLocator.Name.CONFIGURATION_SERVICE, configurationServiceUrl));

        ReportService reportService = new ReportServiceImpl(mongoEndpoint);
        //set all ktd reports as ended.
        reportService.updateReportField(KTDReportInformation.class.getName(), null, null, "ended", true);

        ProcessManager processManager = new ProcessManagerImpl(
            new ProcessManagerConfigurationBuilder()
                .setExecutor(processManagerExecutor)
                .setMessageSubscriber(messageSubscriber)
                .setProcessManagerStore(processManagerStore)
                .setReportService(reportService)
                .setConditionExpressionGenerator(conditionExpressionGenerator)
                .setScheduler(quartzScheduler)
                .createProcessManagerConfiguration(),
            defaultKTIParameters);
        
        processManager.setLastExecutionDateBeforeStartup(lastExecutionDateBeforeStartup);

        File ktdRegistry = new File(ktdRegistryFile);
        if (ktdRegistry.exists() && ktdRegistry.isFile() && ktdRegistry.canRead()) {

            List<KnowledgeTemplateDefinition> ktds;
            try (FileInputStream fis = new FileInputStream(ktdRegistry)) {
                ktds = new KnowledgeTemplateDefinitionUnmarshaller().unmarshall(fis);
            }

            if (ktds != null) {
                processManager.registerKnowledgeTemplateDefinitions(ktds, true);
            }
        } else {
            LOG.warn("{} could not be found. Starting an empty Process Manager.", ktdRegistryFile);
        }

        //Start Heartbeat Task
        heartbeatTaskScheduler.startExecution();

        return processManager;
    }
}
