/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms.ws;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PreDestroy;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.healthcheck.model.HealthCheckStatus;
import com.cognitivemedicine.cdsp.healthcheck.model.ServiceStatistics;
import com.cognitivemedicine.cdsp.healthcheck.model.VersionInformation;
import com.cognitivemedicine.cdsp.healthcheck.util.HealthCheckDetailStatusType;
import com.cognitivemedicine.cdsp.healthcheck.util.HealthCheckStatusType;
import com.cognitivemedicine.cdsp.healthcheck.util.StatusHelper;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateInstance;
import com.cognitivemedicine.cdsp.processmanager.ProcessManager;
import com.cognitivemedicine.cdsp.services.pms.KnowledgeTemplateDefinitionUnmarshaller;
import com.cognitivemedicine.cdsp.services.pms.api.ProcessManagerService;
import com.cognitivemedicine.cdsp.services.pms.config.ConfigConstants;
import com.cognitivemedicine.cdsp.services.pms.model.KnowledgeTemplateDefinitionDTO;
import com.cognitivemedicine.cdsp.services.pms.model.KnowledgeTemplateInstanceDTO;
import com.cognitivemedicine.cdsp.services.pms.model.visualization.DecisionNodeDTO;
import com.cognitivemedicine.cdsp.services.pms.model.visualization.DecisionNodeExecutionDTO;
import com.cognitivemedicine.cdsp.services.pms.transform.DTOTransformer;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ws.rs.DefaultValue;

@Path("/")
public class ProcessManagerServiceImpl implements ProcessManagerService {

	private static final Logger LOG = LoggerFactory.getLogger(ProcessManagerServiceImpl.class);

	private ProcessManager processManager;
	private VersionInformation versionInformation;

	public void setProcessManager(ProcessManager processManager) {
		this.processManager = processManager;
	}

	@PostConstruct
	public void postConstruct() {
		LOG.info("Process manager service is starting now...");
		ConfigUtils config = ConfigUtils.getInstance(ConfigConstants.CONTEXT_NAME);

		String version = config.getString(ConfigConstants.KEY_VERSION_MAVEN_VERSION, "Unknown");
		String buildBranch = config.getString(ConfigConstants.KEY_VERSION_BUILD_BRANCH, "Unknown");
		String buildNumber = config.getString(ConfigConstants.KEY_VERSION_BUILD_NUMBER, "Unknown");
		long buildTimestamp = config.getLong(ConfigConstants.KEY_VERSION_BUILD_TIMESTAMP, 0L);

		versionInformation = new VersionInformation(version, buildBranch, buildNumber, buildTimestamp);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Path("/ktds")
	@Override
	public Collection<KnowledgeTemplateDefinitionDTO> listKTDs() {
		return DTOTransformer
				.toKnowledgeTemplateDefinitionDTO(this.processManager.getRegisteredKnowledgeTemplateDefinitions());
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Path("/ktds/{ktdId}")
	@Override
	public KnowledgeTemplateDefinitionDTO getKTD(@NotNull @PathParam("ktdId") String ktdId) {
		LOG.debug("getting KTD id: {}", ktdId);
		for (KnowledgeTemplateDefinition ktd : this.processManager.getRegisteredKnowledgeTemplateDefinitions()) {
			if (ktd.getId().equals(ktdId)) {
				return DTOTransformer.toKnowledgeTemplateDefinitionDTO(ktd);
			}
		}

		return null;
	}

	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	@Path("/ktds")
	@Override
	public void registerKTD(String ktdDefinition) {
		LOG.debug("attempting to registering KTDs from definition.");
		KnowledgeTemplateDefinitionUnmarshaller unmarshaller = new KnowledgeTemplateDefinitionUnmarshaller();
		try {
			List<KnowledgeTemplateDefinition> ktds = unmarshaller
					.unmarshall(new ByteArrayInputStream(ktdDefinition.getBytes()));
			for (KnowledgeTemplateDefinition ktd : ktds) {
				LOG.debug("registering KTD id: {}", ktd.getId());
				this.processManager.registerKnowledgeTemplateDefinition(ktd);
			}
		} catch (IOException ex) {
			throw new IllegalArgumentException("Exception unmarshalling KnowledgeTemplateDefinition", ex);
		}
	}

	@DELETE
	@Path("/ktds/{ktdId}")
	@Override
	public void unregisterKTD(@NotNull @PathParam("ktdId") String ktdId,
			@QueryParam("terminateInstances") Boolean terminateInstances) {
		LOG.debug("unregistering KTD id: {}", ktdId);
		this.processManager.unregisterKnowledgeTemplateDefinition(ktdId,
				terminateInstances == null || terminateInstances);
	}

	@POST
	@Path("/ktds/{ktdId}/purgeLifeCycleSession")
	@Override
	public void purgeKTDLifeCycleSession(@NotNull @PathParam("ktdId") String ktdId) {
		this.processManager.purgeKnowledgeTemplateDefinitionLifeCycleSession(ktdId);
	}

	@POST
	@Path("/ktds/{ktdId}/initParameters/{paramName}")
	@Override
	public void addKnowledgeTemplateDefinitionInitContextParameter(@NotNull @PathParam("ktdId") String ktdId,
			@NotNull @PathParam("paramName") String paramName, String value) {
		this.processManager.addKnowledgeTemplateDefinitionInitContextParameter(ktdId, paramName, value);
	}

	@GET
	@Path("/ktds/{ktdId}/initParameters")
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Override
	public Map<String, Object> getKnowledgeTemplateDefinitionInitContextParameter(
			@NotNull @PathParam("ktdId") String ktdId) {
		return this.processManager.getKnowledgeTemplateDefinitionInitContextParameter(ktdId);
	}

	@GET
	@Path("/ktds/{ktdId}/lifeCycleFacts")
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Override
	public List<Object> getKnowledgeTemplateDefinitionLifeCycleSessionFacts(@NotNull @PathParam("ktdId") String ktdId) {
		List<Object> facts = this.processManager.getKnowledgeTemplateDefinitionLifeCycleSessionFacts(ktdId);
		List<Object> finalFacts = new ArrayList<>();

		// Some facts can be easily serialized. We need to convert them to something
		// more convenient.
		for (Object fact : facts) {
			if (fact instanceof KnowledgeTemplateInstance) {
				KnowledgeTemplateInstance kti = (KnowledgeTemplateInstance) fact;
				finalFacts.add(DTOTransformer.toKnowledgeTemplateInstanceDTO(kti));
			} else {
				finalFacts.add(fact);
			}
		}

		return finalFacts;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Path("/ktds/{ktdId}/decisionNodes")
	@Override
	public Map<String, DecisionNodeDTO> getKnowledgeTemplateDefinitionDecisionNodes(
			@NotNull @PathParam("ktdId") String knowledgeTemplateDefinitionId) {
		return this.processManager.getKnowledgeTemplateDefinitionDecisionNodes(knowledgeTemplateDefinitionId).entrySet()
				.stream()
				.collect(Collectors.toMap(e -> e.getKey(), e -> DTOTransformer.toDecisionNodeDTO(e.getValue())));
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT,
			SerializationFeature.FAIL_ON_SELF_REFERENCES })
	@Path("/ktds/{ktdId}/ktis")
	@Override
	public Collection<KnowledgeTemplateInstanceDTO> listKTIs(@NotNull @PathParam("ktdId") String ktdId) {
		Collection<KnowledgeTemplateInstance> ktis = this.processManager.getKnowledgeTemplateInstances(ktdId);
		return ktis != null ? DTOTransformer.toKnowledgeTemplateInstanceDTO(ktis) : Collections.emptyList();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Path("/ktds/{ktdId}/ktis/{ktiId}")
	@Override
	public KnowledgeTemplateInstanceDTO getKTI(@NotNull @PathParam("ktdId") String ktdId,
			@NotNull @PathParam("ktiId") String ktiId) {
		LOG.debug("getting KTI for KTD id: {} KTI id: {}", ktdId, ktiId);
		Collection<KnowledgeTemplateInstance> ktis = this.processManager.getKnowledgeTemplateInstances(ktdId);
		if (ktis != null) {
			for (KnowledgeTemplateInstance kti : ktis) {
				if (kti.getId().equals(ktiId)) {
					return DTOTransformer.toKnowledgeTemplateInstanceDTO(kti);
				}
			}
		}

		return null;
	}

	@DELETE
	@Path("/ktds/{ktdId}/ktis/{ktiId}")
	@Override
	public void terminateKTI(@NotNull @PathParam("ktdId") String ktdId, @NotNull @PathParam("ktiId") String ktiId) {
		LOG.debug("terminate KTI called - KTD id: {} KTI id: {}", ktdId, ktiId);
		this.processManager.terminateKnowledgeTemplateInstance(ktiId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT })
	@Path("/ktds/{ktdId}/ktis/{ktiId}/decisionNodeExecutions")
	@Override
	public Map<String, List<DecisionNodeExecutionDTO>> getKnowledgeTemplateInstanceDecisionNodeExecutions(
			@NotNull @PathParam("ktdId") String ktdId, @NotNull @PathParam("ktiId") String ktiId,
			@QueryParam("excludeContext") @DefaultValue("false") boolean excludeContext) {
		return this.processManager.getKnowledgeTemplateInstanceDecisionNodeExecutions(ktdId, ktiId).entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey(),
						e -> e.getValue().stream().map(dne -> DTOTransformer.toDecisionNodeDTO(dne, excludeContext))
								.collect(Collectors.toList())));
	}

	/*
	 * This API will reveal if all of the KTDs are deployed into the Process Manager
	 * as specified by /classes/config-sample/ktds.config. This is typically called
	 * by a bash script and used to let Jenkins know if the Process Manager is
	 * ready.
	 * 
	 * To test: curl -i -v -X GET -H "Content-type: application/json" -H
	 * "Accept: application/json" http://localhost:8080/cdsppms/pm/getStatus
	 */
	@Override
	public HealthCheckStatus getStatus(int level) {
		HealthCheckStatus status;
		if (processManager != null && processManager.initializationComplete()) {
			status = new HealthCheckStatus(HealthCheckStatusType.OK.getHealthStatus(),
					HealthCheckStatusType.OK.getCode(), HealthCheckStatusType.OK.getDescription(), null);
		} else {
		    status = StatusHelper.getHealthCheckStatus(HealthCheckStatusType.NOT_INITIALIZED,
                    HealthCheckDetailStatusType.WARN.getCode(), "Service not initialized");
		}
		status.setVersionInformation(versionInformation);
		return status;
	}

	@Override
	public ServiceStatistics getStatistics() {
		ServiceStatistics statistics = null;
		return statistics;
	}

	@PreDestroy
	public void destroy() {
		LOG.info("process manager service is stopping now...");
		if (this.processManager != null) {
			LOG.debug("Disposing Process Manager");
			try {
				this.processManager.dispose();
			} catch (Exception ex) {
				LOG.warn("Exception disposing Process Manager.", ex);
			}
		}
	}

}
