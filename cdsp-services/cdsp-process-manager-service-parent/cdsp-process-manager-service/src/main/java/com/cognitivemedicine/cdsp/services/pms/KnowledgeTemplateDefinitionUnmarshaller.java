/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.services.pms;

import com.cognitivemedicine.cdsp.processmanager.KnowledgeTemplateDefinition;
import com.cognitivemedicine.cdsp.processmanager.impl.DroolsKnowledgeTemplateDefinition;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is in charge of the deserialization of KTDs.
 * @author esteban
 */
public class KnowledgeTemplateDefinitionUnmarshaller {
    
    private static final Logger LOG = LoggerFactory.getLogger(KnowledgeTemplateDefinitionUnmarshaller.class);
    private static final String COMMENT_CHAR = "#";
    private static final String FIELD_SEPARATOR = ":";
    
    public List<KnowledgeTemplateDefinition> unmarshall(InputStream is) throws IOException{
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        
        List<KnowledgeTemplateDefinition> results = new ArrayList<>();
        while ((line = reader.readLine()) != null){
            
            if (line.trim().startsWith(COMMENT_CHAR) || line.trim().isEmpty()){
                continue;
            }
            
            KnowledgeTemplateDefinition ktd;
            
            String type = line.substring(0, line.indexOf(FIELD_SEPARATOR));
            switch(type.toLowerCase()){
                case "drools":
                    ktd = this.unmarshallDroolsKnowledgeTemplateDefinition(line);
                    break;
                default:
                    LOG.warn("Unssuported KTD of type {}. Skipping it.", type);
                    continue;
            }
            
            if (ktd != null){
                results.add(ktd);
            } else {
                LOG.warn("The KTD coudln't be unmarshalled. Skipping.");
            }
        }

        return results;
    }
    
    private KnowledgeTemplateDefinition unmarshallDroolsKnowledgeTemplateDefinition(String line){
        LOG.debug("Unmarshalling a DroolsKnowledgeTemplateDefinition from \"{}\".", line);
        StringTokenizer st = new StringTokenizer(line);
        
        try{
            //skip type
            st.nextToken(FIELD_SEPARATOR);

            String groupId = st.nextToken(FIELD_SEPARATOR);
            String artifactId = st.nextToken(FIELD_SEPARATOR);
            String version = st.nextToken(FIELD_SEPARATOR);

            LOG.debug("GAV: {}:{}:{}", groupId, artifactId, version);

            return new DroolsKnowledgeTemplateDefinition(groupId, artifactId, version);
        } catch (Exception ex){
            LOG.error("Error unmarshalling DroolsKnowledgeTemplateDefinition.", ex);
            return null;
        }
    }
}
