/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.transformer;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Varies;
import ca.uhn.hl7v2.model.v26.datatype.CWE;
import ca.uhn.hl7v2.model.v26.datatype.CX;
import ca.uhn.hl7v2.model.v26.datatype.XPN;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_OBSERVATION;
import ca.uhn.hl7v2.model.v26.message.ADT_A01;
import ca.uhn.hl7v2.model.v26.message.ORM_O01;
import ca.uhn.hl7v2.model.v26.message.ORU_R01;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.segment.NK1;
import ca.uhn.hl7v2.model.v26.segment.OBX;
import ca.uhn.hl7v2.model.v26.segment.PV1;

public class Hl7TransformerTest {
	private static final Logger LOG = LoggerFactory.getLogger(Hl7TransformerTest.class);
	
	@Test
	public void testHctResults() {
		Message hapiMsg = null;
		String filename = "oru-r01-hct.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		for (Message msg : messages) {
			hapiMsg = Hl7Transformer.parseMessage(msg.toString());
			assertTrue(hapiMsg != null);
			LOG.info("name: " + hapiMsg.getName());
			if (hapiMsg.getName().equals("ORU_R01")) {
				analyzeHct(hapiMsg);
			}

		}
		assertTrue(messages != null);
	}

	@Test
	public void testDirectComponentLabResults() {
		Message hapiMsg = null;
		// String filename = "/tmp/hl7/msg_adt_a04.hl7";
		// String filename = "adt-a01-v26.hl7";
		// String filename = "adt-a03-v26.hl7";
		String filename = "adt-a01-1.hl7v26";
		// String filename = "oru-r01-direct-component-1.5.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		for (Message msg : messages) {
			hapiMsg = Hl7Transformer.parseMessage(msg.toString());
			assertTrue(hapiMsg != null);
			LOG.info("name: " + hapiMsg.getName());
			if (hapiMsg.getName().equals("ADT_A01")) {
				handleAdtA01(hapiMsg);
			}
			else {
				if (hapiMsg.getName().equals("ORU_R01")) {
					handleOruR01WithFloat(hapiMsg);
				}
			}
		}
		assertTrue(messages != null);
	}

	@Test
	public void testCoombsLabResults() {
		Message hapiMsg = null;
		String filename = "oru-r01-coombs-positive.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		for (Message msg : messages) {
			hapiMsg = Hl7Transformer.parseMessage(msg.toString());
			assertTrue(hapiMsg != null);
			LOG.info("name: " + hapiMsg.getName());
			if (hapiMsg.getName().equals("ADT_A01")) {
				handleAdtA01(hapiMsg);
			}
			else {
				if (hapiMsg.getName().equals("ORU_R01")) {
					handleOruR01(hapiMsg);
				}
			}
		}
		assertTrue(messages != null);
	}

	@Test
	public void testTsbLabOrders() {
		Message hapiMsg = null;
		String filename = "orm-o01-tsb.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		for (Message msg : messages) {
			hapiMsg = Hl7Transformer.parseMessage(msg.toString());
			assertTrue(hapiMsg != null);
			LOG.info("name: " + hapiMsg.getName());
			if (hapiMsg.getName().equals("ORM_O01")) {
				handleOrmO01(hapiMsg);
			}
		}
		assertTrue(messages != null);
	}

	/**
	 * This method will test parsing a ADT_A04 message which in structure is the same as ADT_A01
	 */
	@Test
	public void testParseAdtA04Message() {

		Message hapiMsg = null;

		String filename = "adt-a04-nicu.msg";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		for (Message msg : messages) {
			hapiMsg = Hl7Transformer.parseMessage(msg.toString());
			LOG.info("name: " + hapiMsg.getName());
			assertTrue(hapiMsg != null);
			handleAdtA01(hapiMsg);
		}
		assertTrue(messages != null);
	}


	private void handleAdtA01(Message hapiMsg) {
		ADT_A01 adtMsg = (ADT_A01) hapiMsg;
		MSH header = adtMsg.getMSH();
		NK1 nk1 = adtMsg.getNK1();
		LOG.info("Identifier: " + nk1.getRelationship().getIdentifier() + " - name: " + nk1.getRelationship().getNameOfCodingSystem()
				+ " - text: " + nk1.getRelationship().getText() + " - Alternate Identifier: " + nk1.getRelationship().getAlternateIdentifier()
				+ " - Alternate name: " + nk1.getRelationship().getNameOfAlternateCodingSystem() + " - Alternate text: "
				+ nk1.getRelationship().getAlternateText());
		// Retrieve some data from the MSH segment
		String msgType = header.getMessageType().getName();
		String msgTrigger = header.getMessageType().getTriggerEvent().getValue();
		LOG.info(msgType + " " + msgTrigger);
		/*
		 * Now let's retrieve the patient's name from the parsed message.
		 *
		 * PN is an HL7 data type consisting of several components, such as family name, given name, etc.
		 */
		XPN[] patientName = adtMsg.getPID().getPatientName();
		for (int i = 0; i < patientName.length; i++) {
			XPN name = patientName[i];
			LOG.info("firstname: " + name.getGivenName().toString());
			LOG.info("lastname: " + name.getFamilyName().getSurname().toString());
		}

		CX[] patientIds = adtMsg.getPID().getPid3_PatientIdentifierList();
		CX pid = patientIds[0];
		LOG.info(pid.getIDNumber().toString());

		LOG.info("PID == yes");
		LOG.info("Patient ID is: " + adtMsg.getPID().getPid3_PatientIdentifierList(0).getIDNumber().toString());
		LOG.info("firstname: " + adtMsg.getPID().getPatientName(0).getGivenName().toString());
		LOG.info("lastname: " + adtMsg.getPID().getPatientName(0).getFamilyName().getSurname().toString());

		PV1 pv1 = adtMsg.getPV1();

		LOG.info("Point of care: " + pv1.getAssignedPatientLocation().getPointOfCare().toString());
		LOG.info("Hospital Service: " + pv1.getHospitalService().toString());

	}

	private void handleOrmO01(Message hapiMsg) {
		if (hapiMsg.getName().equals("ORM_O01")) {
			ORM_O01 orm = (ORM_O01) hapiMsg;
			CWE universalIdentifier = orm.getORDER().getORDER_DETAIL().getOBR().getUniversalServiceIdentifier();
			LOG.info("code id: " + universalIdentifier.getIdentifier());
			LOG.info("code text: " + universalIdentifier.getText());
			LOG.info("code system: " + universalIdentifier.getNameOfCodingSystem());
		}
	}

	private void handleOruR01(Message hapiMsg) {
		if (hapiMsg.getName().equals("ORU_R01")) {
			ORU_R01 oru = (ORU_R01) hapiMsg;
			LOG.info("code id: "
					+ oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx3_ObservationIdentifier().getIdentifier());
			LOG.info("code text: "
					+ oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx3_ObservationIdentifier().getText());
			LOG.info("code system: " + oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX()
					.getObx3_ObservationIdentifier().getNameOfCodingSystem());
			Varies varies = oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx5_ObservationValue(0);
			LOG.info("observation result: " + varies.getData().toString().toLowerCase());
		}
	}

	private void handleOruR01WithFloat(Message hapiMsg) {
		if (hapiMsg.getName().equals("ORU_R01")) {
			ORU_R01 oru = (ORU_R01) hapiMsg;
			LOG.info("code id: "
					+ oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx3_ObservationIdentifier().getIdentifier());
			LOG.info("code text: "
					+ oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx3_ObservationIdentifier().getText());
			LOG.info("code system: " + oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX()
					.getObx3_ObservationIdentifier().getNameOfCodingSystem());

			CWE unit = oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx6_Units();
			LOG.info("Unit read: " + unit.getIdentifier());
			Double dValue = Double.valueOf(
					oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx5_ObservationValue(0).getData().toString());
			if (dValue > 2.0) {
				LOG.info("observation result: " + dValue + " is greater than 2.0");
			}
			else {
				LOG.info("observation result: " + dValue + " is less than 2.0");
			}

		}
	}

	public void analyzeHct(Message hapiMsg) {
		if (hapiMsg.getName().equals("ORU_R01")) {
			ORU_R01 oru = (ORU_R01) hapiMsg;
			LOG.info("code id: "
					+ oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx3_ObservationIdentifier().getIdentifier());
			LOG.info("code text: "
					+ oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx3_ObservationIdentifier().getText());
			LOG.info("code system: " + oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX()
					.getObx3_ObservationIdentifier().getNameOfCodingSystem());

			CX[] patientIds = oru.getPATIENT_RESULT().getPATIENT().getPID().getPid3_PatientIdentifierList();
			CX pid = patientIds[0];
			LOG.info(pid.getIDNumber().toString());
			List<ORU_R01_OBSERVATION> observations;
			try {
				observations = oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATIONAll();
				OBX obx = null;
				for (ORU_R01_OBSERVATION observation : observations) {
					if (observation.getOBX() != null) {
						obx = observation.getOBX();
						LOG.info(" code id :  " + obx.getObservationIdentifier());
						if ((obx.getObservationIdentifier().getNameOfCodingSystem().getValue().equals("LN"))
								&& (obx.getObservationIdentifier().getIdentifier().getValue().equals("57715-5"))) {
							// it must be the birth time
							Varies varies = obx.getObx5_ObservationValue(0);
							LOG.info("birth time: " + varies.getData().toString().toLowerCase());
						}
						else {
							if ((obx.getObservationIdentifier().getNameOfCodingSystem().getValue().equals("LN"))
									&& (obx.getObservationIdentifier().getIdentifier().getValue().equals("57714-8"))) {
								// it must be the the gestational age
								obx = observation.getOBX();
								Varies varies = obx.getObx5_ObservationValue(0);
								LOG.info("gestational age: " + Integer.parseInt(varies.getData().toString()));
								int gestationalAge = Integer.parseInt(varies.getData().toString());
								LOG.info("gestationalAge: " + gestationalAge);
							}
							else {
								if ((obx.getObservationIdentifier().getNameOfCodingSystem().getValue().equals("LN"))
										&& (obx.getObservationIdentifier().getIdentifier().getValue().equals("4544-3"))) {
									// it must be the birth time
									Varies varies = obx.getObx5_ObservationValue(0);
									LOG.info("Hemoatocrit: " + Double.parseDouble(varies.getData().toString()));
									double hctLevel = Double.parseDouble(varies.getData().toString());
									LOG.info("hctLevel: " + hctLevel);
								}
							}
						}
						obx = null;
					}
				}
			}
			catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
