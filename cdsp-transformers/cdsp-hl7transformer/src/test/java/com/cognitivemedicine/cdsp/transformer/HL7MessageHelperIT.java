/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.transformer;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import com.cognitivemedicine.cdsp.transformer.Hl7Transformer;
import ca.uhn.hl7v2.model.v26.message.ADT_A01;

import ca.uhn.hl7v2.model.Message;

/**
 * Test the HL7MessageHelper - Implemented as an integration test since it
 * relies on the HL7Transformer to transform the source files.
 * 
 * @author Jerry Goodnough
 *
 */
public class HL7MessageHelperIT {

	@Test
	public void testGetGoodPatientIdFromMessage() {
		String filename = "helperTest-GoodId.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		
		Message goodMsg = messages.get(0);
		
		String pid = HL7MessageHelper.getPatientIdFromMessage(goodMsg);
		assertNotNull("No PID found when expected",pid);
		assertTrue("PID Found does not match the expected value",pid.compareTo("123456789-1")==0);
		
	}

	@Test
	public void testGetBadPatientIdFromMessage() {
		String filename = "helperTest-BadId.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		
		Message badmsg = messages.get(0);
		
		String pid = HL7MessageHelper.getPatientIdFromMessage(badmsg);
		assertNull("PID found when not expected",pid);
	}

	@Test
	public void testGetPatientIdFromMessageADT_A01() {
		String filename = "helperTest-GoodId.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		
		Message goodMsg = messages.get(0);
		ADT_A01 adt = (ADT_A01) goodMsg;
		String pid = HL7MessageHelper.getPatientIdFromMessage(adt);
		assertNotNull("No PID found when expected",pid);
		assertTrue("PID Found does not match the expected value",pid.compareTo("123456789-1")==0);

	}
	
	@Test
	public void testGetBadPatientIdFromMessageADT_A01() {
		String filename = "helperTest-BadId.hl7v26";
		List<Message> messages = Hl7Transformer.readFromFile(filename);
		
		Message badmsg = messages.get(0);
		ADT_A01 adt = (ADT_A01) badmsg;
		
		String pid = HL7MessageHelper.getPatientIdFromMessage(adt);
		assertNull("PID found when not expected",pid);
	}

}
