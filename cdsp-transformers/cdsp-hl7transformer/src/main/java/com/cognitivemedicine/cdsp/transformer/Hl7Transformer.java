/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.transformer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.util.Hl7InputStreamMessageIterator;

public class Hl7Transformer {

	private static final Logger LOG = LoggerFactory.getLogger("Hl7MessageHandler");

	public static List<Message> readFromFile(String filename) {
		List<Message> hl7Messages = new ArrayList<Message>();

		InputStream inputStream = Hl7Transformer.class.getClassLoader().getResourceAsStream(filename);

		// The following class is a HAPI utility that will iterate over the messages which appear over an InputStream
		Hl7InputStreamMessageIterator iter = new Hl7InputStreamMessageIterator(inputStream);
		while (iter.hasNext()) {
			Message msg = iter.next();
			hl7Messages.add(msg);
		}
		return hl7Messages;
	}

	/**
	 * Parses an HL7 Message. HAPI API is used for parsing.
	 *
	 * @param msg
	 *            an Hl7 message in String format
	 * @return HAPI representation of an HL7 message
	 */
	public static Message parseMessage(String msg) {
		Message hapiMsg = null;
		if (msg != null) {
			LOG.debug("A plain text representation of HL7 message before parsing: " + msg);
			try (HapiContext context = new DefaultHapiContext()) {
				// http://sourceforge.net/p/hl7api/bugs/223/
				context.getExecutorService();
				Parser parser = context.getGenericParser();
				hapiMsg = parser.parse(msg);
				LOG.debug("HAPI representation of HL7 message after parsing: " + msg.toString());
			}
			catch (HL7Exception | IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
		return hapiMsg;
	}
}
