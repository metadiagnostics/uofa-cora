/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.transformer;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Structure;
import ca.uhn.hl7v2.model.v26.datatype.CX;
import ca.uhn.hl7v2.model.v26.message.ADT_A01;
import ca.uhn.hl7v2.model.v26.segment.PID;

/**
 * Helper Functions to work with HL7 Messages
 * 
 * Note: Currently using a V2.6 model - This is not great and we should look at how to get to version indendence
 * 
 * @author Jerry Goodnough
 *
 */
public class HL7MessageHelper {

	public static String getPatientIdFromMessage(Message msg)
	{
		String pid = null;
		//Ok we have a generic message
		//Try to got an PID segment
		
		Structure possPidSeg;
		try {
			possPidSeg = msg.get("PID");
		} catch (HL7Exception e) {
			//No PID segment found
			return null;
		}
		if (possPidSeg != null)
		{
			PID pidSeg = (PID) possPidSeg;
			CX[] patientIds = pidSeg.getPid3_PatientIdentifierList();
			if (patientIds.length>0)
			{
				CX pidInternal = patientIds[0];
				if (pidInternal != null)
				{
					pid = pidInternal.getIDNumber().toString();
				}
			}
		}
		return pid;
	}

	/**
	 * Helper function return a PID from a ADT_01 message  
	 * @param msg an
 	 * @return
	 */
	public static String getPatientIdFromMessage(ADT_A01  msg)
	{
		String pid = null;
		PID pidSeg = msg.getPID();
		if (pidSeg != null)
		{
			CX[] patientIds = msg.getPID().getPid3_PatientIdentifierList();
			if (patientIds.length>0)
			{
				CX pidInternal = patientIds[0];
				if (pidInternal != null)
				{
					pid = pidInternal.getIDNumber().toString();
				}
			}
		}
		return pid;
	}
	
}


