/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.messaging.jms;

import java.io.Serializable;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JMS Publisher.
 * @author calcacuervo
 *
 */
public class JMSMessagePublisher {

	private static final Logger LOG = LoggerFactory.getLogger(JMSMessageSubscriber.class);

	private final Session jmsSession;
	private final String topicName;
	private final String headerDiscriminatorProperty;

	public JMSMessagePublisher(ConnectionFactory connectionFactory, String topicName,
			String headerDiscriminatorProperty) {
		try {
			this.jmsSession = connectionFactory.createConnection().createSession(Session.AUTO_ACKNOWLEDGE);
		} catch (JMSException e) {
			throw new IllegalStateException("Exception creating JMS Connection", e);
		}
		this.topicName = topicName;
		this.headerDiscriminatorProperty = headerDiscriminatorProperty;
	}

	public void publish(Serializable data, String discriminator) {
		try (MessageProducer producer = jmsSession.createProducer(jmsSession.createTopic(topicName))) {
			ObjectMessage message = this.jmsSession.createObjectMessage(data);
			if (headerDiscriminatorProperty != null) {
			    message.setStringProperty(headerDiscriminatorProperty, discriminator);
			}
			producer.send(message);
		} catch (Exception e) {
			LOG.error("Could not send message through the JMS channel.", e);
		}
	}
	
	public void dispose() {
	    if (this.jmsSession != null) {
	        try {
                this.jmsSession.close();
            } catch (JMSException e) {
                LOG.error("Could not Dispose JMS Session.", e);
            }
	    }
	}
}
