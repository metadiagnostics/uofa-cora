/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.messaging.jms;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.messaging.api.MessageListener;
import com.cognitivemedicine.cdsp.messaging.api.MessageSubscriber;

/**
 * This implementation of MessageSubscriber uses JMS as the Messaging 
 * framework.
 * TODO: This class is very similar to JMSClientSubscriptionHandler. Maybe
 * a new module (cdsp-jms?) needs to be introduced.
 * 
 * @author esteban
 */
public class JMSMessageSubscriber implements MessageSubscriber {
	
	private static final Logger LOG = LoggerFactory.getLogger(JMSMessageSubscriber.class);
	
	private Connection connection;

	private Map<String, Session> subscriptionSession;
	private final String topicName;
	private final String headerDiscriminatorProperty;

	public JMSMessageSubscriber(ConnectionFactory connectionFactory, String topicName, String headerDiscriminatorProperty) {
		this.topicName = topicName;
		this.headerDiscriminatorProperty = headerDiscriminatorProperty;
		this.subscriptionSession = new LinkedHashMap<>();
		
		try {
			this.connection = connectionFactory.createConnection();
			this.connection.start();
		} catch (JMSException ex) {
			throw new IllegalStateException("Exception creating JMS Connection", ex);
		}
	}
	
	@Override
	public String registerSubscriber(String discriminator, MessageListener listener) {

		try {
			Session jmsSession = this.connection.createSession(Session.AUTO_ACKNOWLEDGE);
			MessageConsumer jmsConsumer = jmsSession.createConsumer(jmsSession.createTopic(topicName), headerDiscriminatorProperty + " = '" + discriminator+"'");
			jmsConsumer.setMessageListener((m) -> {
				try {
					Object object = m.getBody(Object.class);
					listener.onMessage(object);
				} catch (JMSException ex) {
					LOG.error("Exception notifying listener about message " + m, ex);
				}
			});
			
			String subscriptionId = UUID.randomUUID().toString();
			subscriptionSession.put(subscriptionId, jmsSession);
			
			return subscriptionId;
		} catch (JMSException ex) {
			throw new IllegalStateException("Exception creating the JMS Consumer", ex);
		}
	}

	@Override
	public void unregisterSubscriber(String registrationId) {
		Session jmsSession = subscriptionSession.get(registrationId);
		if (jmsSession != null) {
			try {
				jmsSession.close();
			} catch (Exception ex) {
				LOG.error("Exception closing JMS Consumer for Subscription '" + registrationId + "'", ex);
			}
		}
	}

	@Override
	public void dispose() {
		for (Map.Entry<String, Session> entry : subscriptionSession.entrySet()) {
			try {
				entry.getValue().close();
			} catch (Exception e) {
				LOG.debug("Error closing JMS Consumer for Subscription '" + entry.getKey() + "'", e);
			}
		}

		try {
			this.connection.close();
		} catch (Exception e) {
			LOG.debug("Error closing JMS Connection", e);
		}
	}
	
}
