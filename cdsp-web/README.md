#CDSP WEB Portal#

Careweb Framework (CWF) Source: https://github.com/carewebframework/carewebframework-core

Careweb Framework (CWF) Wiki: https://github.com/carewebframework/carewebframework.github.io/wiki

Careweb Framework (CWF) Blog: https://carewebframework.wordpress.com/

##Modules##

* cdsp-cwf-api-parent: It contains maven modules related to the integration
with external systems or containing business logic.
* cdsp-cwf-ui-parent: It contains the CWF's plugins. Usually, these modules use
the modules from *cdsp-cwf-api-parent*.
* cdsp-cwf-portal: It contians the portal web application.

##Building the repository##
1. Clone carewebframework, see [https://carewebframework.wordpress.com/2014/04/07/running-the-careweb-framework-sample-web-app/]
2. Add the following to your ~/.m2/settings.xml

        <profile>
            <id>zkoss</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <id>zkoss mavensync</id>
                    <url>http://mavensync.zkoss.org/maven2/</url>
                    <snapshots>
                        <enabled>false</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </snapshots>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </releases>
                </repository>
            </repositories>
        </profile>
        <profile>
            <id>sonatype</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <id>sonatype-snapshot</id>
                    <url>https://oss.sonatype.org/content/repositories/snapshots</url>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </snapshots>
                    <releases>
                        <enabled>false</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </releases>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>sonatype-snapshot</id>
                    <url>https://oss.sonatype.org/content/repositories/snapshots</url>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </snapshots>
                    <releases>
                        <enabled>false</enabled>
                        <updatePolicy>daily</updatePolicy>
                    </releases>
                </pluginRepository>
            </pluginRepositories>
        </profile> 

3. `cd ~/carewebframework-core`
4. `mvn clean install`
5. `cd ~/cdsp_web`
6. `mvn clean install`

If everything compiles, a .war file is created in *cdsp-cwf-portal/target*


##Running the Portal##

The .war generated in the previous step can be deployed in any web container, or
a tomcat instance could be started by executing the following commands from the
root directory:


    cd cdsp-cwf-portal
    mvn tomcat:run-war


The application can be accessed using the following URL: http://localhost:8080/healthservicesportal/
