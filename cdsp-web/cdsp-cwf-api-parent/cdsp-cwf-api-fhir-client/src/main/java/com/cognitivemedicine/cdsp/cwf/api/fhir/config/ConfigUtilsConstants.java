/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.cwf.api.fhir.config;

/**
 *
 * @author esteban
 */
public class ConfigUtilsConstants {
    
    public static final String CONTEXT_NAME = "cdsp.cwf.fhir";
    
    public static final String KEY_AUTHENTICATION_TYPE = "authentication.type";
    public static final String KEY_ENCODING = "encoding";
    public static final String KEY_PRETTY_PRINT = "pretty.print";
    public static final String KEY_PROXY = "proxy";
    public static final String KEY_ROOT_URL = "root.url";
    public static final String KEY_VALIDATE_CONFORMANCE = "validate.conformance";
    public static final String KEY_VERSION = "version";
    
}
