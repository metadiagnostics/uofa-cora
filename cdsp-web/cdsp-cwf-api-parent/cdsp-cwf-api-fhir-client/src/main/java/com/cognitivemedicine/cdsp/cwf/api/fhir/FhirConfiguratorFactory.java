/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.cwf.api.fhir;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.server.EncodingEnum;
import com.cognitivemedicine.cdsp.cwf.api.fhir.config.ConfigUtilsConstants;
import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import com.cognitivemedicine.config.utils.ConfigUtils;

/**
 *
 * @author esteban
 */
public class FhirConfiguratorFactory {
    
    public FhirConfigurator createInstance(){
        
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilsConstants.CONTEXT_NAME);
        
        String authenticationType = config.getString(ConfigUtilsConstants.KEY_AUTHENTICATION_TYPE, null);
        EncodingEnum encoding = EncodingEnum.valueOf(config.getString(ConfigUtilsConstants.KEY_ENCODING, "JSON"));
        boolean prettyPrint = config.getBoolean(ConfigUtilsConstants.KEY_PRETTY_PRINT, Boolean.FALSE);
        String proxy = config.getString(ConfigUtilsConstants.KEY_PROXY, null);
        String rootUrl = config.getString(ConfigUtilsConstants.KEY_ROOT_URL, null);
        boolean validateConformance = config.getBoolean(ConfigUtilsConstants.KEY_VALIDATE_CONFORMANCE, Boolean.FALSE);
        FhirVersionEnum version = FhirVersionEnum.valueOf(config.getString(ConfigUtilsConstants.KEY_VERSION, "DSTU3"));

        FhirConfigurator configurator = new FhirConfigurator();
        
        configurator.setAuthenticationType(authenticationType);
        configurator.setEncoding(encoding);
        configurator.setPrettyPrint(prettyPrint);
        configurator.setProxy(proxy);
        configurator.setRootUrl(rootUrl);
        configurator.setValidateConformance(validateConformance);
        configurator.setVersion(version);
        
        return configurator;
    }
    
}
