/*
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License
 */

package com.cognitivemedicine.cdsp.cwf.jms;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.commons.lang.Validate;

import com.cognitivemedicine.cdsp.messaging.jms.JMSMessagePublisher;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.sun.messaging.ConnectionConfiguration;

/**
 * This factory is intended to be use among cwf services, to reuse the way of obtaining JMS services
 * and properties files.
 * 
 * @author calcacuervo
 *
 */
public class JMSServiceFactory {

    /**
     * The context keys to lookup in config utils.
     * 
     * @author calcacuervo
     *
     */
    public static class ContextKeys {
        /**
         * The context name.
         */
        private static final String CONTEXT_NAME = "jms.cwf";
        /**
         * Key to search the jms connect string.
         */
        public static final String JMS_CONNECT = "jms.connect";
        /**
         * Key to search the jms case topic.
         */
        public static final String JMS_CASE_EVENT_TOPIC = "jms.case.topic";
        /**
         * Key to search the jms case topic discriminator property.
         */
        public static final String JMS_CASE_EVENT_DISCRIMINATOR_FIELD = "jms.case.header.discriminator.property";
        /**
         * Key to search the jms case topic discriminator value.
         */
        public static final String JMS_CASE_EVENT_DISCRIMINATOR_VALUE = "jms.case.header.discriminator";
        /**
         * Key to search the jms eligibility topic discriminator value.
         */
        public static final String JMS_ELIGIBILITY_DISCRIMINATOR_VALUE = "jms.eligibility.header.discriminator";
        /**
         * Key to search the jms demo topic.
         */
        public static final String JMS_DEMO_TOPIC = "jms.demo.topic";
        /**
         * Key to search the jms case topic discriminator value.
         */
        public static final String JMS_DEMO_DISCRIMINATOR_VALUE = "jms.demo.header.discriminator.property";
        /**
         * Key to search the jms user context discriminator value.
         */
        public static final String JMS_USER_CONTEXT_DISCRIMINATOR_VALUE =
                "jms.flyout.user.context.discriminator.property";
        /**
         * Key to search the jms flyout topic.
         */
        public static final String JMS_FLYOUT_TOPIC = "jms.flyout.topic";
    }

    /**
     * Config utils instance used to get properties.
     */
    private final ConfigUtils configUtils;

    /**
     * {@link ConnectionFactory} instance to create consumers and producers.
     */
    private final ConnectionFactory connectionFactory;

    /**
     * Creates a new {@link JMSServiceFactory} instance.
     */
    public JMSServiceFactory() {
        configUtils = ConfigUtils.getInstance(ContextKeys.CONTEXT_NAME);
        String jmsConnect = configUtils.getString(ContextKeys.JMS_CONNECT);
        Validate.notEmpty(jmsConnect, "The jms connection string cannot be null.");
        connectionFactory = getConnectionFactory(jmsConnect);
        Validate.notNull(connectionFactory, "The connection factory cannot be null.");
    }

    /**
     * Returns a new {@link JMSMessagePublisher} using the properties from config utils. This will
     * be configured to send case state related data.
     * 
     * @return
     */
    public JMSMessagePublisher getCaseStateMessagePublisher() {
        return getMessagePublisher(ContextKeys.JMS_CASE_EVENT_TOPIC, ContextKeys.JMS_CASE_EVENT_DISCRIMINATOR_FIELD);
    }

    /**
     * Returns a new {@link JMSMessagePublisher} using the properties from config utils. This will
     * be configured to send data to flyout topic.
     * 
     * @return
     */
    public JMSMessagePublisher getFlyoutMessagePublisher() {
        return getMessagePublisher(ContextKeys.JMS_FLYOUT_TOPIC, ContextKeys.JMS_CASE_EVENT_DISCRIMINATOR_FIELD);
    }

    /**
     * Returns a new {@link JMSMessagePublisher} using the properties from config utils. This will
     * be configured to send data to flyout topic.
     * 
     * @return
     */
    public JMSMessagePublisher getDemoDataMessagePublisher() {
        return getMessagePublisher(ContextKeys.JMS_DEMO_TOPIC, ContextKeys.JMS_DEMO_DISCRIMINATOR_VALUE);
    }

    /**
     * Return this config utils data.
     * 
     * @return the config utils instance, never null.
     */
    public ConfigUtils getConfigUtils() {
        return configUtils;
    }

    /**
     * Returns a new {@link JMSMessagePublisher} for the given topic and discriminator field, using this class connection factory.
     * @param topicNameKey the topic name key to get from , which cannot be null.
     * @param jmsDiscriminatorFieldKey optional.
     * @return
     * @throws IllegalArgumentException if topic property is not found for the given topicNameKey param.
     */
    private JMSMessagePublisher getMessagePublisher(final String topicNameKey, final String jmsDiscriminatorFieldKey) {
        Validate.notEmpty(topicNameKey, "The jms topic key used for the publisher cannot be null.");
        
        String topic = getConfigUtils().getString(topicNameKey);
        String jmsDiscriminatorField = jmsDiscriminatorFieldKey == null ? null : getConfigUtils().getString(jmsDiscriminatorFieldKey);
        Validate.notNull(topic, "The topic for the publisher cannot be null");
        return new JMSMessagePublisher(this.connectionFactory, topic, jmsDiscriminatorField);
    }

    /**
     * Creates a new {@link com.sun.messaging.ConnectionFactory} using the configured jms connect
     * string, with reconnect enabled.
     * 
     * @param jmsConnect the connection string.
     * @return a new {@link ConnectionFactory}, and throws an {@link IllegalStateException} is there
     *         is an error creating the connection factory.
     */
    private ConnectionFactory getConnectionFactory(final String jmsConnect) {
        try {
            Validate.notNull(jmsConnect);
            com.sun.messaging.ConnectionFactory connectionFactory = new com.sun.messaging.ConnectionFactory();
            connectionFactory.setProperty(ConnectionConfiguration.imqAddressList, jmsConnect);
            connectionFactory.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
            return connectionFactory;
        } catch (JMSException e) {
            throw new IllegalStateException("There was an error configure JMS, it will not be configured", e);
        }
    }


}
