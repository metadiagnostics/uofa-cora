/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.definition;

import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.WaitStepInstance;
import java.util.concurrent.TimeUnit;

/**
 * This class represents the definition of a Wait StepDefinition.
 * @author esteban
 */
public class WaitStepDefinition extends AbstractStepDefinition {
    private long time;
    private TimeUnit timeUnit = TimeUnit.SECONDS;

    @Override
    public StepInstance createInstance() {
        return new WaitStepInstance(this);
    }
    
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Override
    public String toString() {
        return "WaitStepDefinition{ name= "+ getName() + ", time=" + time + ", timeUnit=" + timeUnit + '}';
    }
    
}
