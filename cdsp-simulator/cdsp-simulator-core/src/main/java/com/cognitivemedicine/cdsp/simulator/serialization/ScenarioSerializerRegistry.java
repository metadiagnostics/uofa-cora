/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.serialization;

import java.util.ArrayList;
import java.util.List;

/**
 * This class serves as a registry for multiple {@link ScenarioSerializer}.
 * Once multiple serializers are registered into an instance of this class, 
 * the proper serializer for a specific filename can be retrieved using its
 * {@link #getSerializerForFilename(java.lang.String) } method.
 * 
 * By default, this class automatically registers an instance of
 * {@link YamlScenarioSerializer} serializer.
 * 
 * @author esteban
 */
public class ScenarioSerializerRegistry {
    
    private final List<ScenarioSerializer> serializers = new ArrayList<>();

    public ScenarioSerializerRegistry() {
        this.registerSerializer(new YamlScenarioSerializer());
    }
    
    
    /**
     * Registers a new {@link ScenarioSerializer} into this registry.
     * @param serializer 
     */
    public final void registerSerializer(ScenarioSerializer serializer){
        serializers.add(0, serializer);
    }
    
    /**
     * Returns a {@link ScenarioSerializer} that knows how to deal with the
     * specified filename. 
     * If multiple serializers can handle the provided filename, the one
     * that was registered last will be returned. This allows to override
     * previously registered serializers in an easy way.
     * If there is no serializer who can handle the provided filename, an
     * {@link IllegalArgumentException} is thrown.
     * 
     * @param filename
     * @return 
     */
    public ScenarioSerializer getSerializerForFilename(String filename){
        for (ScenarioSerializer serializer : serializers) {
            if (serializer.accepts(filename)){
                return serializer;
            }
        }
        
        throw new IllegalArgumentException("No Serializer found for filename "+filename);
    }
    
}
