/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.context;

import com.cognitivemedicine.cdsp.simulator.api.ContextService;
import java.util.HashMap;
import java.util.Map;

/**
 * This class serves as a registry where {@link ContextService}s can be 
 * registered and looked up. 
 * 
 * @author esteban
 */
public class ServiceRegistry {
    
    private Map<String, ContextService> services = new HashMap<>();
    
    public void registerService(String serviceName, ContextService service){
        registerService(serviceName, service, true);
    }
    
    public void registerService(String serviceName, ContextService service, boolean failOnDuplicated){
        ContextService oldService = services.put(serviceName, service);
        if (failOnDuplicated && oldService != null){
            throw new IllegalArgumentException("There is already a service of type "+service.getClass().getName()+"registered");
        }
    }
    
    public <T extends ContextService> T getService(String serviceName){
        T service = (T) services.get(serviceName);
        
        if (service == null ){
            throw new IllegalArgumentException("No service with name '"+serviceName+"' was found in the registry");
        }
        
        return service;
    }
    
}
