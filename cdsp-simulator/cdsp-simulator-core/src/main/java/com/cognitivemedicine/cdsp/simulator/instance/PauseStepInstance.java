/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.instance;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.PauseStepDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author esteban
 */
public class PauseStepInstance extends AbstractStepInstance {
    
    private final static Logger LOG = LoggerFactory.getLogger(PauseStepInstance.class);
    
    private final PauseStepDefinition definition;

    public PauseStepInstance(PauseStepDefinition definition) {
        super(definition);
        this.definition = definition;
    }

    @Override
    public void execute(ScenarioContext context) {
        LOG.debug("-- [START] PAUSE: "+definition);
        
        if (!context.isSkipPauseSteps()){
            context.getScenarioInstance().pause();
        } else {
            LOG.debug("Pause Steps are disabled for this execution");
        }
        
        LOG.debug("-- [END] PAUSE");
    }
    
}
