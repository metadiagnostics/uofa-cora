/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.instance;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognitivemedicine.cdsp.simulator.api.ScenarioInstanceEventListener;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;

/**
 *
 * @author esteban
 */
public class ScenarioInstance implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ScenarioInstance.class);
    private static final long PAUSE_CHECK_INTERVAL = 1000;

    private String instanceId;
    private List<StepInstance> steps = new ArrayList<>();
    private ScenarioContext context;
    
    private List<ScenarioInstanceEventListener> listeners = new ArrayList<>();

    private volatile boolean pause = false;
    private volatile boolean stop = false;
    
    public ScenarioInstance(String instanceId) {
        this.instanceId = instanceId;
    }
    
    public void init(ScenarioContext context) {
        this.context = context;
        this.context.setScenarioInstance(this);
    }

    @Override
    public void run() {
        
        boolean pauseListenerNotified = false;
        
        listeners.forEach(l -> l.onScenarioRun());
        for (StepInstance step : steps) {
            if (stop) {
                break;
            }
            
            while (pause) {
                
                if (!pauseListenerNotified){
                    listeners.forEach(l -> l.onScenarioPause());
                    pauseListenerNotified = true;
                }
                
                try {
                    Thread.sleep(PAUSE_CHECK_INTERVAL);
                } catch (InterruptedException e) {
                    LOG.warn("Thread interrupted exception in ScenarioInstance.");
                }
            }
            
            if (pauseListenerNotified){
                listeners.forEach(l -> l.onScenarioResume());
                pauseListenerNotified = false;
            }
            
            try{
                step.setExecutionStartTime(System.currentTimeMillis());
                step.execute(context);
                listeners.forEach(l -> l.onScenarioStep(step));
            } catch (Exception e){
                listeners.forEach(l -> l.onScenarioAbort(step, e));
                break;
            }
        }
        listeners.forEach(l -> l.onScenarioFinish());
    }

    public void pause() {
        this.pause = true;
    }

    public void resume() {
        this.pause = false;
    }

    public void stop() {
        this.stop = true;
    }

    public List<StepInstance> getSteps() {
        return steps;
    }

    public void addStep(StepInstance step) {
        this.steps.add(step);
    }
    
    public void addEventListener(ScenarioInstanceEventListener listener) {
        this.listeners.add(listener);
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

}
