/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.context;

import com.cognitivemedicine.cdsp.simulator.api.ContextService;
import com.cognitivemedicine.cdsp.simulator.instance.PauseStepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.instance.WaitStepInstance;
import com.cognitivemedicine.cdsp.simulator.runtime.ParameterResolver;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author esteban
 */
public class ScenarioContext {

    private final ServiceRegistry serviceRegistry;
    private final ParameterResolver parameterResolver;
    private ScenarioInstance scenarioInstance;

    private final Map<String, String> initParameters = new HashMap<>(); 
    
    /**
     * Useful to skip {@link WaitStepInstance} steps while testing.
     */
    private boolean skipWaitSteps;
    
    /**
     * Useful to skip {@link PauseStepInstance} steps while testing.
     */
    private boolean skipPauseSteps;

    public ScenarioContext() {
        this.serviceRegistry = new ServiceRegistry();
        this.parameterResolver = new ParameterResolver(this);
    }

    public ScenarioInstance getScenarioInstance() {
        return scenarioInstance;
    }

    public void setScenarioInstance(ScenarioInstance scenarioInstance) {
        this.scenarioInstance = scenarioInstance;
    }

    public void registerContextService(String serviceName, ContextService service){
        serviceRegistry.registerService(serviceName, service);
    }
    
    public <T extends ContextService> T getContextService(String serviceName){
        return serviceRegistry.getService(serviceName);
    }
    
    public ParameterResolver getParameterResolver() {
        return parameterResolver;
    }
    
    public void addInitParameter(String name, String parameter) {
        this.initParameters.put(name, parameter);
    }
    
    public void addInitParameter(Map<String,String> parameters) {
        this.initParameters.putAll(parameters);
    }

    public Map<String, String> getInitParameters() {
        return initParameters;
    }
    
    public boolean isSkipWaitSteps() {
        return skipWaitSteps;
    }

    public void setSkipWaitSteps(boolean skipWaitSteps) {
        this.skipWaitSteps = skipWaitSteps;
    }

    public boolean isSkipPauseSteps() {
        return skipPauseSteps;
    }

    public void setSkipPauseSteps(boolean skipPauseSteps) {
        this.skipPauseSteps = skipPauseSteps;
    }
    
}
