/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.runtime;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.util.DateUtil;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author esteban
 */
public class ParameterResolver {

    public final static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    public final static String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    
    private final ScenarioContext context;

    private class PrefixAndSuffix{
        String prefix;
        int prefixStart;
        String suffix;
        int suffixEnd;
    }
    
    public ParameterResolver(ScenarioContext context) {
        this.context = context;
    }
    
    public String resolveJsonResource(String source, Map<String, String> params) {
        
        params = getFinalParameters(params);
        
        StringBuilder sb = new StringBuilder();
        
        try (StringReader is = new StringReader(source);) {
            List<String> line = IOUtils.readLines(is);
            for (String s : line) {
                int p1;
                
                while ((p1 = s.indexOf("${")) > -1) {
                    int p2 = s.indexOf("}", p1);
                    
                    String key = s.substring(p1 + 2, p2);
                    int p3 = key.indexOf(":");
                    String dflt = "";
                    
                    if (p3 > 0) {
                        dflt = key.substring(p3 + 1);
                        key = key.substring(0, p3);
                    }
                    
                    String value = params.get(key);
                    
                    if (value == null && !dflt.isEmpty()) {
                        value = dflt;
                    }
                    
                    if (value == null) {
                        throw new RuntimeException("Reference not found: " + key);
                    }
                    
                    PrefixAndSuffix prefixAndSuffix = getVariablePrefixAndSuffix(s, p1, p2);
                    
                    String r = eval(prefixAndSuffix.prefix+value+prefixAndSuffix.suffix);
                    s = s.substring(0, Math.min(prefixAndSuffix.prefixStart, p1)) + r + s.substring(Math.max(prefixAndSuffix.suffixEnd, p2) + 1);
                }
                
                sb.append(s).append('\n');
            }
        } catch (Exception e) {
            throw new IllegalStateException("Exception parsing resource", e);
        }
        
        return sb.toString();
    }
    
    public Map<String, String> resolveParametersMap(Map<String, String> originalParams) {
        originalParams = getFinalParameters(originalParams);
        
        Map<String, String> finalParameters = new HashMap<>();
        for (Map.Entry<String, String> entry : originalParams.entrySet()) {
            finalParameters.put(entry.getKey(), this.eval(entry.getValue()));
        }
        
        return finalParameters;
    }
    
    public String resolveParameterValue(String value){
        return this.eval(value);
    }
    
    private PrefixAndSuffix getVariablePrefixAndSuffix(String line, int variableStart, int variableEnd){
        PrefixAndSuffix result = new PrefixAndSuffix();

        //Find the last " before the variable start.
        int first = 0;
        for (int i = 0; i < variableStart; i++) {
            if (line.charAt(i) == '"'){
                first = i;
            }
        }
        
        //Find the first " after the variable end
        int last = variableEnd;
        for (int i = variableEnd; i < line.toCharArray().length; i++) {
            char c = line.charAt(i);
            if (line.charAt(i) == '"'){
                last = i;
            }
        }
        
        result.prefixStart = first+1;
        result.prefix = line.substring(result.prefixStart, variableStart);
        result.suffixEnd = variableEnd;
        result.suffix = line.substring(result.suffixEnd+1, last);
        
        return result;
    }
    
    /**
     * Override the definition parameters with the init parameters of this
     * particular context.
     * 
     * @param originalParameters
     * @return 
     */
    private Map<String, String> getFinalParameters(Map<String, String> originalParameters){
        Map<String, String> parameters = new HashMap<>();
        parameters.putAll(originalParameters);
        parameters.putAll(context.getInitParameters());
        
        return parameters;
    }
    
    /**
     * Evaluate an expression.
     * 
     * @param exp The expression. The general format is
     *            <p>
     *            <code>type/value</code>
     *            </p>
     *            If <code>type</code> is omitted, an exception is thrown (subclasses
     *            of this class may change this behavior).
     *            Possible values for <code>type</code> are:
     *            <ul>
     *            <li>value - A literal value; inserted as is</li>
     *            <li>date - A date value; can be a relative date (T+n, for example).
     *            The value could also contain a the desired format by appending it using a '/'. I.e: T-10y/MM-dd-yyyy</li>
     *            <li>datetime - A date value; can be a relative date (T+n, for example).
     *            The value could also contain a the desired format by appending it using a '/'. I.e: T-10y/MM-dd-yyyy</li>
     *            <li>(Not yet supported) image - A file containing an image</li>
     *            <li>snippet - A file containing a snippet to be inserted</li>
     *            </ul>
     * 
     * @return The result of the evaluation.
     */
    protected String eval(String exp) {
        int i = exp.indexOf('/');
      
        if (i == -1) {
     	    return evalUntypedExpression(exp);
        }
        
        String type = exp.substring(0, i);
        String value = exp.substring(i + 1);
        
        if ("value".equals(type)) {
            return value;
        }
        
        if ("date".equals(type)) {
            return doDate(value, true);
        }
        
        if ("datetime".equals(type)) {
            return doDate(value, false);
        }
        
        if ("snippet".equals(type)) {
            return doSnippet(exp);
        }
        
        throw new RuntimeException("Unknown type: " + type);
    }
    
    private String doSnippet(String value) {
        return value;
    }
    
    protected String evalUntypedExpression(String exp){
        throw new RuntimeException("Unable to eval expression: " + exp+". It doesn't contain a type.");
    }
    
    protected String doDate(String value, boolean dateOnly) {

        String dateValue = value;
        String dateFormat = dateOnly? DEFAULT_DATE_FORMAT : DEFAULT_DATE_TIME_FORMAT;
        if (value.contains("/")){
            dateValue = value.substring(0, value.indexOf("/"));
            dateFormat = value.substring(value.indexOf("/")+1);
        }
        
        Date date = DateUtil.parseDate(dateValue);
        if (dateOnly){
            date = DateUtil.stripTime(date);
        }

        if (date != null) {
            return new SimpleDateFormat(dateFormat).format(date);
        } else {
            throw new RuntimeException("Bad date specification: " + value);
        }
    }
    
}
