/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.api;

import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;

/**
 *
 * @author esteban
 */
public interface ScenarioInstanceEventListener {
    
    /**
     * Invoked when the scenario is started (before any step is executed).
     */
    public void onScenarioRun();
    
    /**
     * Invoked after a step was successfully executed.
     * @param step 
     */
    public void onScenarioStep(StepInstance step);
    
    /**
     * Invoked when the scenario is completed (after all steps are correctly
     * executed).
     */
    public void onScenarioFinish();
    
    /**
     * Invoked when the scenario is paused.
     */
    public void onScenarioPause();
    
    /**
     * Invoked when the scenario is resumed.
     */
    public void onScenarioResume();
    
    /**
     * Invoked when an exception is thrown from within a step.
     * @param step
     * @param e
     */
    public void onScenarioAbort(StepInstance step, Exception e);
}
