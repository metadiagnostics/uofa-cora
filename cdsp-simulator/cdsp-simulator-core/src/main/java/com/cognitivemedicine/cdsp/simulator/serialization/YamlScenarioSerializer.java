/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.serialization;

import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author esteban
 */
public class YamlScenarioSerializer implements ScenarioSerializer {

    private static final String ALLOWED_REGEX = ".*\\.yaml";
    
    @Override
    public String serializeScenarioDefinition(ScenarioDefinition definition){
        Yaml y = new Yaml();
        return y.dump(definition);
    }

    @Override
    public boolean accepts(String filename) {
        return org.apache.commons.lang.StringUtils.isNotBlank(filename) && filename.matches(ALLOWED_REGEX);
    }
    
    public ScenarioDefinition deserializeScenarioDefinition(String name, File definitionFile) throws IOException{
        String definition = FileUtils.readFileToString(definitionFile, Charset.forName("UTF-8"));
        return deserializeScenarioDefinition(name, definition);
    }
    
    @Override
    public ScenarioDefinition deserializeScenarioDefinition(String name, Reader destinationReader) throws IOException{
        return deserializeScenarioDefinition(name, IOUtils.toString(destinationReader));
    }
    
    public ScenarioDefinition deserializeScenarioDefinition(String name, String definition){
        Yaml y = new Yaml();
        ScenarioDefinition sd = y.loadAs(definition, ScenarioDefinition.class);
        
        //Override scenario's name if one is provided
        if (!StringUtils.isBlank(name)){
            sd.setName(name);
        }
        
        return sd;
    }
    
}
