/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.definition;

import com.cognitivemedicine.cdsp.simulator.instance.SetParameterStepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;

/**
 * This step can be used to set a parameter (or overriding an existing one)
 * as part of an scenario.
 * @author esteban
 */
public class SetParameterStepDefinition extends AbstractStepDefinition {
    
    private String value;

    @Override
    public StepInstance createInstance() {
        return new SetParameterStepInstance(this);
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SetParameterStepDefinition{" + "name='" + getName() + "', value='" + value + "'}";
    }

}
