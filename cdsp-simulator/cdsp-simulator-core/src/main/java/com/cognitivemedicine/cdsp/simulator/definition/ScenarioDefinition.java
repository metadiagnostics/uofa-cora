/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.definition;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A scenario consists in an ordered, linear list of steps.
 * 
 * @author esteban
 */
public class ScenarioDefinition {
    
    private String name;
    private List<StepDefinition> steps = new ArrayList<>();

    public ScenarioDefinition() {
    }

    public ScenarioDefinition(String name) {
        this.name = name;
    }
    
    public ScenarioInstance createInstance(ScenarioContext context){
        
        ScenarioInstance scenario = new ScenarioInstance(UUID.randomUUID().toString());
        scenario.init(context);
      
        for (StepDefinition step : steps) {
       		
            scenario.addStep(step.createInstance());
          
        }
      
        return scenario;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StepDefinition> getSteps() {
        return steps;
    }

    public void setSteps(List<StepDefinition> steps) {
        this.steps = steps;
    }
    
    public void addStep(StepDefinition step){
        this.steps.add(step);
    }
}
