/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator;

import com.cognitivemedicine.cdsp.simulator.api.ScenarioInstanceEventListenerAdapter;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.PauseStepDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.util.MockStepDefinition;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class PauseStepTest {

    @Test
    public void doTest() throws Exception {
        
        CountDownLatch pauseLatch = new CountDownLatch(1);
        CountDownLatch finishLatch = new CountDownLatch(1);

        ScenarioContext ctx = new ScenarioContext();

        MockStepDefinition step1 = new MockStepDefinition();
        MockStepDefinition step2 = new MockStepDefinition();
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();

        scenarioDefinition.addStep(step1);
        scenarioDefinition.addStep(new PauseStepDefinition());
        scenarioDefinition.addStep(step2);

        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        scenarioInstance.addEventListener(new ScenarioInstanceEventListenerAdapter() {

            @Override
            public void onScenarioPause() {
                pauseLatch.countDown();
            }

            @Override
            public void onScenarioFinish() {
                finishLatch.countDown();
            }

        });

        //Run the simulation in a separate Thread
        new Thread(scenarioInstance).start();

        //Wait for the scenario to be paused
        if (!pauseLatch.await(10, TimeUnit.SECONDS)){
            throw new IllegalStateException("Timeout waiting for the scenario to be paused");
        }
        
        //Only the 1st Step was executed
        assertThat(step1.getCreatedInstance().isExecuted(), is(true));
        assertThat(step2.getCreatedInstance().isExecuted(), is(false));

        //Resume the execution
        scenarioInstance.resume();

        //Wait for the scenario to be completed
        if (!finishLatch.await(10, TimeUnit.SECONDS)){
            throw new IllegalStateException("Timeout waiting for the scenario to be completed");
        }

        //The 2 steps were executed now
        assertThat(step1.getCreatedInstance().isExecuted(), is(true));
        assertThat(step2.getCreatedInstance().isExecuted(), is(true));
    }

}
