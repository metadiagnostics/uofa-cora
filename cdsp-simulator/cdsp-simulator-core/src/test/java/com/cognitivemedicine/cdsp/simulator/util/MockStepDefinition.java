/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.util;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.AbstractStepDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.StepDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.AbstractStepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;

/**
 *
 * @author esteban
 */
public class MockStepDefinition extends AbstractStepDefinition {

    public static class MockStepInstance extends AbstractStepInstance {

        private boolean executed;

        public MockStepInstance(StepDefinition stepDefinition) {
            super(stepDefinition);
        }
        
        @Override
        public void execute(ScenarioContext context) {
            executed = true;
        }

        public boolean isExecuted() {
            return executed;
        }
        
    }
    
    private final MockStepInstance instance;

    public MockStepDefinition() {
        instance = new MockStepInstance(this);
    }
    
    @Override
    public StepInstance createInstance() {
        return instance;
    }
    
    public MockStepInstance getCreatedInstance(){
        return instance;
    }
    
}
