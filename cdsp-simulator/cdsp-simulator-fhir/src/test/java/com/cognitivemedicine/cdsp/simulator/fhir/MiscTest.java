/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.fhir;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.cognitivemedicine.cdsp.simulator.definition.CreateOrUpdateResourceStepDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ResourceInstanceDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ResourceTemplateDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.SetParameterStepDefinition;
import com.cognitivemedicine.cdsp.simulator.fhir.config.ContextConfigurator;
import com.cognitivemedicine.cdsp.simulator.fhir.util.MockFHIRService;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;

import com.cognitivemedicine.cdsp.simulator.serialization.YamlScenarioSerializer;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import org.apache.commons.io.FileUtils;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import org.hl7.fhir.dstu3.model.Patient;

/**
 *
 * @author esteban
 */
public class MiscTest {
    
    private YamlScenarioSerializer serializer = new YamlScenarioSerializer();

    @Test
    public void doTest() throws Exception{
        
        MockFHIRService fhirService = new MockFHIRService();
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(fhirService).configureContext(ctx);
        
        ResourceTemplateDefinition template = this.createResourceTemplateDefinition("T-1", this.readFile("/resource/patient-sample-2.xml"));
        
        ResourceInstanceDefinition instance1 = new ResourceInstanceDefinition();
        instance1.setId("R1");
        instance1.setTemplate(template);
        instance1.addParameter("code", "value/code-1");
        instance1.addParameter("relatedperson", "value/ref-1");
        instance1.addParameter("dob", "date/T-10y");
        
        ResourceInstanceDefinition instance2 = new ResourceInstanceDefinition();
        instance2.setId("R2");
        instance2.setTemplate(template);
        instance2.addParameter("code", "value/code-1");
        instance2.addParameter("relatedperson", "R1");
        instance2.addParameter("dob", "date/T-1y");
        
        
        CreateOrUpdateResourceStepDefinition createR1 = new CreateOrUpdateResourceStepDefinition();
        createR1.setResourceDefinition(instance1);
        
        CreateOrUpdateResourceStepDefinition createR2 = new CreateOrUpdateResourceStepDefinition();
        createR2.setResourceDefinition(instance2);
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        
        //Patient 1
        scenarioDefinition.addStep(createR1);
        
        //Patient 2
        scenarioDefinition.addStep(createR2);
        
        String dump = serializer.serializeScenarioDefinition(scenarioDefinition);
        
        System.out.println("\ndump = \n" + dump);
        System.out.println("\n");
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        
        scenarioInstance.run();
        
        assertThat(fhirService.getResources().size(), is(2));
        assertThat(fhirService.searchResourceByClass(Patient.class).size(), is(2));
    }

    /**
     * A Patient is created as part of an scenario and then updated by another
     * based on it's identifier with code system "urn:oid:1.2.36.146.595.217.0.1"
     * 
     * @throws Exception 
     */
    @Test
    public void doResourceUpdateByIdentifier1Test() throws Exception{
        MockFHIRService fhirService = new MockFHIRService();
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(fhirService).configureContext(ctx);
        
        ResourceTemplateDefinition template = this.createResourceTemplateDefinition("T-1", this.readFile("/resource/patient-sample-2.xml"));
        
        //Create a Patient
        ResourceInstanceDefinition instance1 = new ResourceInstanceDefinition();
        instance1.setId("R1");
        instance1.setTemplate(template);
        instance1.addParameter("code", "value/code-1");
        instance1.addParameter("relatedperson", "value/ref-1");
        instance1.addParameter("dob", "date/T-10y");
        
        CreateOrUpdateResourceStepDefinition createPatient = new CreateOrUpdateResourceStepDefinition();
        createPatient.setResourceDefinition(instance1);
        
        
        //Update the Patient (identified by its urn:oid:1.2.36.146.595.217.0.1 id)
        ResourceInstanceDefinition instance2 = new ResourceInstanceDefinition();
        instance2.setId("R2");
        instance2.setTemplate(template);
        instance2.addParameter("code", "value/code-2");
        instance2.addParameter("relatedperson", "value/ref-2");
        instance2.addParameter("dob", "date/T-10y");
        
        CreateOrUpdateResourceStepDefinition updatePatient = new CreateOrUpdateResourceStepDefinition();
        updatePatient.setResourceDefinition(instance2);
        updatePatient.setExistingResourceCodeSystem("urn:oid:1.2.36.146.595.217.0.1");
        
        //Scenario 1 (Patient Creation)
        ScenarioDefinition scenario1Definition = new ScenarioDefinition();
        scenario1Definition.addStep(createPatient);
        
        //Scenario 2 (Patient Update)
        ScenarioDefinition scenario2Definition = new ScenarioDefinition();
        scenario2Definition.addStep(updatePatient);
        
        
        //Scenario 1 Execution
        ScenarioInstance scenario1Instance = scenario1Definition.createInstance(ctx);
        scenario1Instance.run();

        assertThat(fhirService.getResources().size(), is(1));
        Patient patient = (Patient) fhirService.getResources().values().iterator().next();
        assertThat(patient.getIdentifierFirstRep().getType().getCodingFirstRep().getCode(), is("code-1"));
        assertThat(patient.getLinkFirstRep().getOther().getReference(), is("ref-1"));
        
        
        //Scenario 2 Execution
        ScenarioInstance scenario2Instance = scenario2Definition.createInstance(ctx);
        scenario2Instance.run();

        assertThat(fhirService.getResources().size(), is(1));
        patient = (Patient) fhirService.getResources().values().iterator().next();
        assertThat(patient.getIdentifierFirstRep().getType().getCodingFirstRep().getCode(), is("code-2"));
        assertThat(patient.getLinkFirstRep().getOther().getReference(), is("ref-2"));
    }
    
    /**
     * A first Scenario creates a Patient (with an identifier with System urn:oid:1.2.36.146.595.217.0.1).
     * A second Scenario creates a different Patient (with an identifier with System urn:oid:1.2.36.146.595.217.0.2).
     *
     * This test demonstrates that the CreateOrUpdateResourceStep will create a
     * new Resource if there is no Resource with the specified System and Code.
     * 
     * @throws Exception 
     */
    @Test
    public void doResourceUpdateByIdentifier2Test() throws Exception{
        MockFHIRService fhirService = new MockFHIRService();
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(fhirService).configureContext(ctx);
        
        
        //Create a Patient
        ResourceTemplateDefinition template1 = this.createResourceTemplateDefinition("T-1", this.readFile("/resource/patient-sample-2.xml"));
        ResourceInstanceDefinition instance1 = new ResourceInstanceDefinition();
        instance1.setId("R1");
        instance1.setTemplate(template1);
        instance1.addParameter("code", "value/code-1");
        instance1.addParameter("relatedperson", "value/ref-1");
        instance1.addParameter("dob", "date/T-10y");
        
        CreateOrUpdateResourceStepDefinition createPatient = new CreateOrUpdateResourceStepDefinition();
        createPatient.setResourceDefinition(instance1);
        
        
        //Create another patient, or update it if there is already a patient 
        //with the same code as speciffied by the resource's "urn:oid:1.2.36.146.595.217.0.2"
        //identifier. 
        ResourceTemplateDefinition template2 = this.createResourceTemplateDefinition("T-2", this.readFile("/resource/patient-sample-3.xml"));
        ResourceInstanceDefinition instance2 = new ResourceInstanceDefinition();
        instance2.setId("R2");
        instance2.setTemplate(template2);
        instance2.addParameter("code", "value/code-2");
        instance2.addParameter("dob", "date/T-10y");
        
        CreateOrUpdateResourceStepDefinition updatePatient = new CreateOrUpdateResourceStepDefinition();
        updatePatient.setResourceDefinition(instance2);
        updatePatient.setExistingResourceCodeSystem("urn:oid:1.2.36.146.595.217.0.2");
        
        //Scenario 1 (Patient Creation)
        ScenarioDefinition scenario1Definition = new ScenarioDefinition();
        scenario1Definition.addStep(createPatient);
        
        //Scenario 2 (Patient Creation)
        ScenarioDefinition scenario2Definition = new ScenarioDefinition();
        scenario2Definition.addStep(updatePatient);
        
        
        //Scenario 1 Execution
        ScenarioInstance scenario1Instance = scenario1Definition.createInstance(ctx);
        scenario1Instance.run();

        assertThat(fhirService.getResources().size(), is(1));
        final Patient patient1 = (Patient) fhirService.getResources().values().iterator().next();
        assertThat(patient1.getIdentifierFirstRep().getType().getCodingFirstRep().getCode(), is("code-1"));
        assertThat(patient1.getLinkFirstRep().getOther().getReference(), is("ref-1"));
        
        
        //Scenario 2 Execution
        ScenarioInstance scenario2Instance = scenario2Definition.createInstance(ctx);
        scenario2Instance.run();

        assertThat(fhirService.getResources().size(), is(2));
        Patient patient2 = (Patient) fhirService.getResources().values().stream()
            .filter(p -> p != patient1)
            .findFirst()
            .orElse(null);
        assertThat(patient2.getIdentifierFirstRep().getType().getCodingFirstRep().getCode(), is("code-2"));
    }
    
    /**
     * This test demonstrates how initParameters in the ScenarioContext can 
     * overwrite parameters from an individual CreateOrUpdateResourceStepDefinition 
     * step.
     * 
     * @throws Exception 
     */
    @Test
    public void doParameterOverwriteOnResourceCreationTest() throws Exception{

        MockFHIRService fhirService = new MockFHIRService();
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(fhirService).configureContext(ctx);
        
        String originalCode = "code-1";
        String newCode = "code-2";
        
        ctx.addInitParameter("code", "value/"+newCode);
        
        ResourceTemplateDefinition template = this.createResourceTemplateDefinition("T-1", this.readFile("/resource/patient-sample-2.xml"));
        
        ResourceInstanceDefinition instance1 = new ResourceInstanceDefinition();
        instance1.setId("R1");
        instance1.setTemplate(template);
        instance1.addParameter("code", "value/"+originalCode);
        instance1.addParameter("relatedperson", "value/ref-1");
        instance1.addParameter("dob", "date/T-10y");
        
        CreateOrUpdateResourceStepDefinition createR1 = new CreateOrUpdateResourceStepDefinition();
        createR1.setResourceDefinition(instance1);
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        
        //Patient 1
        scenarioDefinition.addStep(createR1);
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        
        scenarioInstance.run();
        
        assertThat(fhirService.getResources().size(), is(1));
        assertThat(((Patient)fhirService.getResources().values().iterator().next()).getIdentifierFirstRep().getType().getCodingFirstRep().getCode(), is(newCode));
    }
    
    @Test
    public void doSetParameterStepTest() throws Exception{
        String originalCode = "code-1";
        String dob = "datetime/T-10y";
        
        MockFHIRService fhirService = new MockFHIRService();
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(fhirService).configureContext(ctx);

        SetParameterStepDefinition setDOBStep = new SetParameterStepDefinition();
        setDOBStep.setName("dob");
        setDOBStep.setValue(dob);
        
        ResourceTemplateDefinition template = this.createResourceTemplateDefinition("T-1", this.readFile("/resource/patient-sample-3.xml"));
        ResourceInstanceDefinition instance1 = new ResourceInstanceDefinition();
        instance1.setId("R1");
        instance1.setTemplate(template);
        instance1.addParameter("code", "value/"+originalCode);
        instance1.addParameter("relatedperson", "value/ref-1");
        
        CreateOrUpdateResourceStepDefinition createR1 = new CreateOrUpdateResourceStepDefinition();
        createR1.setResourceDefinition(instance1);
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        
        //Patient 1
        scenarioDefinition.addStep(setDOBStep);
        scenarioDefinition.addStep(createR1);
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        
        scenarioInstance.run();
        
        assertThat(fhirService.getResources().size(), is(1));
        assertThat(((Patient)fhirService.getResources().values().iterator().next()).getBirthDate(), not(nullValue()));
    }

    private ResourceTemplateDefinition createResourceTemplateDefinition(String id, String content){
        ResourceTemplateDefinition resource = new ResourceTemplateDefinition();
        resource.setId(id);
        resource.setDefinition(content);
        
        return resource;
    }
    
    private String readFile(String path) throws IOException, URISyntaxException{
        return FileUtils.readFileToString(new File(MiscTest.class.getResource(path).toURI()), Charset.forName("UTF-8"));
    }
}
