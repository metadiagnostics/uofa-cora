/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.fhir.util;

import ca.uhn.fhir.context.FhirContext;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import static java.util.stream.Collectors.toList;
import org.hl7.fhir.dstu3.model.Base;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 *
 * @author esteban
 */
public class MockFHIRService implements FHIRService {

    private Map<String, IBaseResource> resources = new HashMap<>();

    @Override
    public FhirContext getFhirContext() {
        return FhirContext.forDstu3();
    }

    @Override
    public IBaseResource saveOrUpdate(IBaseResource resource) {
        String id = resource.getIdElement() != null && resource.getIdElement().getIdPart() != null ? resource.getIdElement().getIdPart()
                : UUID.randomUUID().toString();
        IBaseResource r = resources.get(id);
        if (r == null) {
            resource.setId(id);
        }
        this.resources.put(id, resource);

        return resource;
    }

    public Map<String, IBaseResource> getResources() {
        return resources;
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourceByIdentifier(String codeSystem, String code, Class<T> clazz) {

        List<T> results = new ArrayList<>();
        if (!Resource.class.isAssignableFrom(clazz)) {
            return results;
        }

        for (IBaseResource r : resources.values()) {
            if (clazz.isAssignableFrom(r.getClass())) {
                try {
                    Base[] identifiers = ((Resource) r).getProperty("identifier".hashCode(), "identifier", true);
                    if (identifiers == null) {
                        continue;
                    }

                    String c = Arrays.stream(identifiers).filter(id -> id instanceof Identifier).map(id -> (Identifier) id)
                            .filter(id -> codeSystem.equals(id.getSystem())).findFirst()
                            .orElse(new Identifier().setSystem(codeSystem).setValue(null)).getValue();

                    if (code.equals(c)) {
                        results.add((T) r);
                    }
                } catch (FHIRException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }

        return results;
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourceByCoding(String codeSystem, String code, Class<T> clazz) {

        List<T> results = new ArrayList<>();
        if (!Resource.class.isAssignableFrom(clazz)) {
            return results;
        }

        for (IBaseResource r : resources.values()) {
            if (clazz.isAssignableFrom(r.getClass())) {
                try {
                    Base[] codeableConcept = ((Resource) r).getProperty("code".hashCode(), "code", true);
                    if (codeableConcept == null || codeableConcept.length == 0 || !(codeableConcept[0] instanceof CodeableConcept)) {
                        continue;
                    }



                    String c = ((CodeableConcept) codeableConcept[0]).getCoding().stream().filter(cc -> codeSystem.equals(cc.getSystem()))
                            .findFirst().orElse(new Coding().setSystem(codeSystem).setCode(null)).getCode();

                    if (code.equals(c)) {
                        results.add((T) r);
                    }
                } catch (FHIRException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }

        return results;
    }

    @Override
    public <T extends IBaseResource> T searchResourceById(IIdType idType, Class<T> clazz) {
        String id = idType.getIdPart();

        IBaseResource resource = resources.get(id);

        if (resource == null) {
            return null;
        }

        if (!clazz.isAssignableFrom(resource.getClass())) {
            throw new IllegalArgumentException("The resource with id '" + id + "' is not of type '" + clazz.getName() + "' but of type '"
                    + resource.getClass().getName() + "'");
        }

        return (T) resource;
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourceByIdentifierAndCode(String identifierSystem, String identifier,
            String codeSystem, String code, Class<T> clazz) {
        List<T> results = new ArrayList<>();
        if (!Resource.class.isAssignableFrom(clazz)) {
            return results;
        }

        for (IBaseResource r : resources.values()) {
            if (clazz.isAssignableFrom(r.getClass())) {
                try {
                    Base[] codeableConcept = ((Resource) r).getProperty("code".hashCode(), "code", true);
                    Base[] identifiers = ((Resource) r).getProperty("identifier".hashCode(), "identifier", true);
                    if (codeableConcept == null || codeableConcept.length == 0 || !(codeableConcept[0] instanceof CodeableConcept)) {
                        continue;
                    }



                    String c = ((CodeableConcept) codeableConcept[0]).getCoding().stream().filter(cc -> codeSystem.equals(cc.getSystem()))
                            .findFirst().orElse(new Coding().setSystem(codeSystem).setCode(null)).getCode();
                    String i = Arrays.stream(identifiers).filter(id -> id instanceof Identifier).map(id -> (Identifier) id)
                            .filter(id -> codeSystem.equals(id.getSystem())).findFirst()
                            .orElse(new Identifier().setSystem(codeSystem).setValue(null)).getValue();

                    if (code.equals(c) && identifier.equals(i)) {
                        results.add((T) r);
                    }
                    if (code.equals(c)) {
                        results.add((T) r);
                    }
                } catch (FHIRException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }

        return results;
    }

    public <T extends IBaseResource> List<T> searchResourceByClass(Class<T> clazz) {
        return resources.values().stream().filter(r -> clazz.isAssignableFrom(r.getClass())).map(r -> (T) r).collect(toList());
    }
}
