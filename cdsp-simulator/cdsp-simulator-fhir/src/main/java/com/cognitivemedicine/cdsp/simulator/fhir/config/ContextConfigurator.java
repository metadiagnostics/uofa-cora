/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.fhir.config;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRParameterResolver;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRResourceInstantiator;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRService;

/**
 * Class used to configure a {@link ScenarioContext} with all the services
 * required by this module.
 * 
 * @author esteban
 */
public class ContextConfigurator {
    
    private final FHIRService fhirService;
    private final FHIRParameterResolver parameterResolver;
    private final FHIRResourceInstantiator resourceInstantiator;

    public ContextConfigurator(FHIRService fhirService) {
        this(fhirService, null, new FHIRResourceInstantiator());
    }

    public ContextConfigurator(FHIRService fhirService, FHIRParameterResolver parameterResolver, FHIRResourceInstantiator resourceInstantiator) {
        this.fhirService = fhirService;
        this.parameterResolver = parameterResolver;
        this.resourceInstantiator = resourceInstantiator;
    }
    
    public void configureContext(ScenarioContext context){
        context.registerContextService(ContextRegistryConstants.FHIR_SERVICE_KEY, fhirService);
        context.registerContextService(ContextRegistryConstants.FHIR_RESOURCE_INSTANTIATOR_KEY, resourceInstantiator);
        context.registerContextService(ContextRegistryConstants.FHIR_PARAMETER_RESOLVER_KEY, parameterResolver != null ? parameterResolver : new FHIRParameterResolver(context));
    }
    
}
