/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.definition;

import com.cognitivemedicine.cdsp.simulator.instance.CreateOrUpdateResourceStepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;

/**
 * This is the definition of a {@link CreateOrUpdateResourceStepInstance} Step.
 * See the documentation in {@link CreateOrUpdateResourceStepInstance} to see
 * how this step behaves on runtime.
 * 
 * @author esteban
 */
public class CreateOrUpdateResourceStepDefinition extends AbstractStepDefinition {

    private ResourceInstanceDefinition resourceDefinition;
    private String existingResourceCodeSystem;
    
    public CreateOrUpdateResourceStepDefinition() {
    }

    public CreateOrUpdateResourceStepDefinition(ResourceInstanceDefinition resourceDefinition) {
        this.resourceDefinition = resourceDefinition;
    }

    public CreateOrUpdateResourceStepDefinition(ResourceInstanceDefinition resourceDefinition, String existingResourceCodeSystem) {
        this.resourceDefinition = resourceDefinition;
        this.existingResourceCodeSystem = existingResourceCodeSystem;
    }
    
    @Override
    public StepInstance createInstance() {
        return new CreateOrUpdateResourceStepInstance(this);
    }

    public ResourceInstanceDefinition getResourceDefinition() {
        return resourceDefinition;
    }

    public void setResourceDefinition(ResourceInstanceDefinition resourceDefinition) {
        this.resourceDefinition = resourceDefinition;
    }

    public String getExistingResourceCodeSystem() {
        return existingResourceCodeSystem;
    }

    public void setExistingResourceCodeSystem(String existingResourceCode) {
        this.existingResourceCodeSystem = existingResourceCode;
    }

    @Override
    public String toString() {
        return "CreateOrUpdateResourceStepDefinition{" + "resourceDefinition=" + resourceDefinition + ", existingResourceCode=" + existingResourceCodeSystem + '}';
    }
    
}
