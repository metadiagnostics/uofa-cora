/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.definition;

import org.apache.commons.lang.Validate;

import com.cognitivemedicine.cdsp.simulator.instance.CreateOrUpdateResourceWithIdentifierAndCodeStepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;

/**
 * This is the definition of a {@link CreateOrUpdateResourceWithIdentifierAndCodeStepInstance} Step.
 * See the documentation in {@link CreateOrUpdateResourceWithIdentifierAndCodeStepInstance} to see
 * how this step behaves on runtime.
 */
public class CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition extends CreateOrUpdateResourceStepDefinition {

    private final String existingResourceIdentifierSystem;
    
    public CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition() {
        this(null);
    }

    public CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition(ResourceInstanceDefinition resourceDefinition) {
        this(resourceDefinition, null, null);
    }

    public CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition(ResourceInstanceDefinition resourceDefinition, String existingResourceCodeSystem,
            String existingResourceIdentifierSystem) {
        super(resourceDefinition, existingResourceCodeSystem);
        Validate.notEmpty(existingResourceIdentifierSystem);
        this.existingResourceIdentifierSystem = existingResourceIdentifierSystem;
    }
    
    @Override
    public StepInstance createInstance() {
        return new CreateOrUpdateResourceWithIdentifierAndCodeStepInstance(this);
    }

    public String getExistingResourceIdentifierSystem() {
        return existingResourceIdentifierSystem;
    }
    
}
