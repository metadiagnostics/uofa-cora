/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.definition;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class represents a FHIR resource instance definition.
 * 
 * @author esteban
 */
public class ResourceInstanceDefinition {
    
    /**
     * The id of the instance definition in the scenario
     */
    private String id;
    
    private ResourceTemplateDefinition template;
    private Map<String, String> parameters = new HashMap<>();
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResourceTemplateDefinition getTemplate() {
        return template;
    }

    public void setTemplate(ResourceTemplateDefinition template) {
        this.template = template;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
    
    public void addParameter(String key, String value) {
        this.parameters.put(key, value);
    }

    @Override
    public String toString() {
        return "ResourceInstanceDefinition{" + "id=" + id + ", parameters=[" + parameters.entrySet().stream().map(e -> "{"+e.getKey()+" -> "+e.getValue()+"}").collect(Collectors.joining(", ")) + "]}";
    }
    
}
