/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.runtime;

import ca.uhn.fhir.context.FhirContext;
import com.cognitivemedicine.cdsp.simulator.api.ContextService;
import java.util.List;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 *
 * @author esteban
 */
public interface FHIRService extends ContextService {
    
    public FhirContext getFhirContext();
    
    public IBaseResource saveOrUpdate(IBaseResource resource);
    
    public <T extends IBaseResource> List<T> searchResourceByIdentifier(String codeSystem, String code, Class<T> clazz);
    
    public <T extends IBaseResource> List<T> searchResourceByIdentifierAndCode(String identifierSystem, String identifier, String codeSystem, String code, Class<T> clazz);
    
    public <T extends IBaseResource> List<T> searchResourceByCoding(String codeSystem, String code, Class<T> clazz);
    
    public <T extends IBaseResource> T searchResourceById(IIdType id, Class<T> clazz);
    
}
