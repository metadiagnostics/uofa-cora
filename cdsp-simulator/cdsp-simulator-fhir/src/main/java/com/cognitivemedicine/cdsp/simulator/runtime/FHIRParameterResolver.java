/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.runtime;

import ca.uhn.fhir.model.primitive.BaseDateTimeDt;
import ca.uhn.fhir.model.primitive.DateDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import com.cognitivemedicine.cdsp.simulator.api.ContextService;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.fhir.config.ContextRegistryConstants;
import com.cognitivemedicine.cdsp.simulator.util.DateUtil;
import java.util.Date;
import org.hl7.fhir.instance.model.api.IBaseResource;

/**
 * Subclass of {@link ParameterResolver} with 2 fundamental differences:
 * <ul>
 *  <li>An untyped expression is treated as a reference to a previously defined
 *  resource</li>
 *  <li>The default Date and Datetime format are those specified by FHHIR.
 *  You cant specify any other format</li>
 * </ul>
 * 
 * @author esteban
 */
public class FHIRParameterResolver extends ParameterResolver implements ContextService{
    
    private final FHIRResourceInstantiator instantiator;
    
    public FHIRParameterResolver(ScenarioContext context) {
        super(context);
        instantiator = context.getContextService(ContextRegistryConstants.FHIR_RESOURCE_INSTANTIATOR_KEY);
    }

    @Override
    protected String evalUntypedExpression(String exp) {
        IBaseResource resource = instantiator.getInstantiatedResource(exp);

        if (resource == null) {
            throw new RuntimeException("Resource not defined: " + exp);
        }

        return resource.getIdElement().getResourceType() + "/" + resource.getIdElement().getIdPart();
    }
    
    
    @Override
    protected String doDate(String value, boolean dateOnly) {
        Date date = DateUtil.parseDate(value);
        
        if (date != null) {
            BaseDateTimeDt dtt = dateOnly ? new DateDt(date) : new DateTimeDt(date);
            value = dtt.getValueAsString();
        } else {
            throw new RuntimeException("Bad date specification: " + value);
        }
        
        return value;
    }
}
