/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.runtime;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

import com.cognitivemedicine.cdsp.fhir.client.BaseService;

import ca.uhn.fhir.context.FhirContext;

/**
 *
 * @author esteban
 */
public class FhirServiceImpl implements FHIRService {
    
    private final BaseService baseService;

    public FhirServiceImpl(BaseService baseService) {
        this.baseService = baseService;
    }

    @Override
    public FhirContext getFhirContext() {
        return baseService.getClient().getFhirContext();
    }

    @Override
    public IBaseResource saveOrUpdate(IBaseResource resource) {
        return baseService.createOrUpdateResource(resource);
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourceByIdentifier(String codeSystem, String code, Class<T> clazz) {
        return baseService.searchResourcesByIdentifier(codeSystem, code, clazz);
    }
    
    @Override
    public <T extends IBaseResource> List<T> searchResourceByIdentifierAndCode(String identifierSystem, String identifier,
            String codeSystem, String code, Class<T> clazz) {
        if (StringUtils.isBlank(identifier) && StringUtils.isBlank(code)) {
            throw new IllegalArgumentException("Identifier and code are both blank, you must supply at least one.");
        }
        if (StringUtils.isBlank(identifier)) {
            return this.searchResourceByCoding(codeSystem, code, clazz);
        }
        if (StringUtils.isBlank(codeSystem)){
            return this.searchResourceByIdentifier(identifierSystem, identifier, clazz);
        }
        return baseService.searchResourcesByIdentifierAndCoding(identifierSystem, identifier, codeSystem, code, clazz);
    }
    
    @Override
    public <T extends IBaseResource> List<T> searchResourceByCoding(String codeSystem, String code, Class<T> clazz) {
        return baseService.searchResourcesByCoding(codeSystem, code, clazz);
    }

    @Override
    public <T extends IBaseResource> T searchResourceById(IIdType id, Class<T> clazz) {
        return baseService.searchResourceById(id.getIdPart(), clazz);
    }

}
