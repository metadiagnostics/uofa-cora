/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.instance;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.CreateOrUpdateResourceStepDefinition;
import com.cognitivemedicine.cdsp.simulator.definition.CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition;
import com.cognitivemedicine.cdsp.simulator.fhir.config.ContextRegistryConstants;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRResourceInstantiator;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRService;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.dstu3.model.Base;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This step creates or updates a FHIR Resource in a FHIR Server. When executed,
 * this step will first look if the resource already exists in the server by
 * using the provided
 * {@link CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition#existingResourceCodeSystem} and
 * {@link CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition#getExistingResourceIdentifierSystem()}. If a
 * resource with that coding and identifier already exists, it is updated with the definition
 * referenced by
 * {@link CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition#resourceDefinition}. If the
 * resource doesn't exist, it is created.
 */
public class CreateOrUpdateResourceWithIdentifierAndCodeStepInstance extends AbstractStepInstance {

    private final static Logger LOG = LoggerFactory.getLogger(CreateOrUpdateResourceWithIdentifierAndCodeStepInstance.class);

    private final CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition definition;

    private IBaseResource resource;

    public CreateOrUpdateResourceWithIdentifierAndCodeStepInstance(CreateOrUpdateResourceWithIdentifierAndCodeStepDefinition definition) {
        super(definition);
        Validate.notNull(definition);
        this.definition = definition;
    }

    @Override
    public void execute(ScenarioContext context) {
        LOG.debug("-- [START] CREATE OR UPDATE RESOURCE: " + definition);

        FHIRService fhirService = context.getContextService(ContextRegistryConstants.FHIR_SERVICE_KEY);
        FHIRResourceInstantiator instantiator = context.getContextService(ContextRegistryConstants.FHIR_RESOURCE_INSTANTIATOR_KEY);

        resource = instantiator.instantiateResource(definition.getResourceDefinition(), context);

        IBaseResource existingResource = findResourceInServer(resource, definition.getExistingResourceCodeSystem(),
                definition.getExistingResourceIdentifierSystem(), fhirService);

        // If the resource already exists, then update it with the new definition
        if (existingResource != null) {
            resource.setId(existingResource.getIdElement());
        }
        resource = fhirService.saveOrUpdate(resource);

        LOG.trace("Resource id: " + resource.getIdElement().getValue());
        LOG.debug("-- [END] CREATE OR UPDATE RESOURCE");
    }

    private IBaseResource findResourceInServer(IBaseResource resource, String codeSystemToCheck, String identifierSystemToCheck,
            FHIRService service) {

        if ((codeSystemToCheck == null && identifierSystemToCheck == null) || !(resource instanceof Resource)) {
            if (resource.getIdElement() == null || resource.getIdElement().getValue() == null) {
                return null;
            }
            return service.searchResourceById(resource.getIdElement(), resource.getClass());
        } else {

            List<? extends IBaseResource> resources;

            String identifier = null;
            String code = null;

            // Try to get the identifier of the resource for the given identifier system.
            if (StringUtils.isNotBlank(identifierSystemToCheck)) {
                identifier = extractIdentifierValue((Resource) resource, identifierSystemToCheck);
            }
            // Try to get the code of the resource.
            if (StringUtils.isNotBlank(codeSystemToCheck)) {
                code = extractCodeValue((Resource) resource, codeSystemToCheck);
            }
            resources = service.searchResourceByIdentifierAndCode(identifierSystemToCheck, identifier, codeSystemToCheck, code,
                    resource.getClass());

            if (resources == null || resources.isEmpty()) {
                return null;
            } else if (resources.size() == 1) {
                return resources.get(0);
            } else {
                throw new IllegalArgumentException(
                        "More than on resource with the Identifier " + codeSystemToCheck + "/" + code + " exist in the FHIR server!");
            }
        }

    }

    /**
     * Try to extract the identifier value of the resource for the given code system.
     * @param resource
     * @param codeSystem
     * @return
     */
    private String extractIdentifierValue(Resource resource, String codeSystem) {
        try {
            Base[] identifiers = ((Resource) resource).getProperty("identifier".hashCode(), "identifier", false);

            if (identifiers == null) {
                return null;
            }

            return Arrays.stream(identifiers).filter(id -> id instanceof Identifier).map(id -> (Identifier) id)
                    .filter(id -> codeSystem.equals(id.getSystem())).findFirst()
                    .orElse(new Identifier().setSystem(codeSystem).setValue(null)).getValue();

        } catch (FHIRException ex) {
            throw new IllegalStateException("Exception extracting Resource identifier.", ex);
        }
    }

    /**
     * Tries to extract the code property of a resource for a given code system.
     * @param resource
     * @param codeSystem
     * @return
     */
    private String extractCodeValue(Resource resource, String codeSystem) {
        try {
            Base[] codeableConcept = ((Resource) resource).getProperty("code".hashCode(), "code", false);

            if (codeableConcept == null || codeableConcept.length == 0 || !(codeableConcept[0] instanceof CodeableConcept)) {
                return null;
            }

            return ((CodeableConcept) codeableConcept[0]).getCoding().stream().filter(c -> codeSystem.equals(c.getSystem())).findFirst()
                    .orElse(new Coding().setSystem(codeSystem).setCode(null)).getCode();

        } catch (FHIRException ex) {
            throw new IllegalStateException("Exception extracting Resource coding", ex);
        }
    }

    public IBaseResource getResource() {
        return resource;
    }

}
