/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.simulator.runtime;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.cognitivemedicine.cdsp.simulator.api.ContextService;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ResourceInstanceDefinition;
import com.cognitivemedicine.cdsp.simulator.fhir.config.ContextRegistryConstants;
import java.util.HashMap;
import java.util.Map;
import org.hl7.fhir.instance.model.api.IBaseResource;

/**
 *
 * @author esteban
 */
public class FHIRResourceInstantiator implements ContextService {
    
    private final Map<String, IBaseResource> instantiatedResources = new HashMap<>();
    
    public IBaseResource instantiateResource(ResourceInstanceDefinition definition, ScenarioContext context){
        try{
            IBaseResource resource = this.parseResource(definition.getTemplate().getDefinition(), definition.getParameters(), context);
            instantiatedResources.put(definition.getId(), resource);
            
            return resource;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    public IBaseResource getInstantiatedResource(String id){
        return instantiatedResources.get(id);
    }
    
    private IBaseResource parseResource(String source, Map<String, String> params, ScenarioContext context) {
        FHIRService fhirService = context.getContextService(ContextRegistryConstants.FHIR_SERVICE_KEY);
        FHIRParameterResolver fhirParameterService = context.getContextService(ContextRegistryConstants.FHIR_PARAMETER_RESOLVER_KEY);
        
        
        FhirContext ctx = fhirService.getFhirContext();
        IParser parser = ctx.newXmlParser();
        
        
        return parser.parseResource(fhirParameterService.resolveJsonResource(source, params));
    }
    
}
