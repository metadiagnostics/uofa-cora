# README #

Welcome to the Clinical Decision Support Platform Project.

#### Core resources are ####

* Wiki at https://lab.cognitivemedicine.com/wiki/display/ProdDev/Team+Home
* Jira at https://lab.cognitivemedicine.com/jira/secure/RapidBoard.jspa?projectKey=NEOC&useStoredSettings=true&rapidView=19
* Jenkins at http://64.87.15.70:8104/view/CORA/job/CORA%20Platform%20Build%20All/
* Crucible at http://64.87.15.70:8060/

### License ###
  Copyright 2016,2017  Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

### What is this repository for? ###
This repository is the parent of the CORA system.

### Contribution guidelines ###
 
 * Either clone the either repo or develop on feature branch based off on develop.
 * All work should have a code review with at least two people signing off it.
 * All work is integrated into develop via pull requests.
 * Pull request will first build the feature branch in jenkins to make sure it is clean
 * Pull requests with merge conflicts will generally not be integrated until the conflicts are resolved by the requester.

### Building Source ###
 * Run mvn clean install to build and install all modules.

### Deploy To Tomcat ###
 * Configure TOMCAT_HOME and CORA_HOME environment variables, with the the tomcat home full path, and the with the cora checkout full path.
 * Run scripts/localDeploy.sh. This will deploy war files to the configured apache tomcat. 

### Who do I talk to? ###
* jgoodnough@cogmedsys.com
* CORA_dev@cognitivemedicine.com
* Other community or team contact


